<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<title>출석 현황 보기</title>

<!-- 스타일 -->
<style type="text/css">
.main {
	width: 555px;
	height: 655px;
	padding: 0.5cm;
}

#LineItemTable {
	margin-top: 20px;
	width: 500px;
}

#SelectStatus {
	margin-top: 50px;
	width: 70%;
	height: 30px;
}

.Heading {
	display: table;
	font-weight: bold;
	text-align: center;
	color :#191919;
	background-color: #FCFCFC;
	width: 100%;
	border-bottom: solid #D5D5D5;
	border-width: thin;
	margin-bottom: 0.05cm;
}

.lineItemRow {
	display: table;
	background-color: #F6F6F6;
	text-align: center;
	width: 500px;
	border-bottom: solid #D5D5D5;
	border-width: thin;
	margin-bottom: 0.05cm;
}

.date, .name, .status {
	display: table-cell;
	border: solid;
	border-width: thin;
	padding-left: 5px;
	padding-right: 5px;
	width: 30%;
	border: none;
	color: #4C4C4C;
}

 
</style>

<script>
$(document).ready(function() {
	
	selectStatus(".main1");
	
	$("#SelectStatus").change(function() {
		if (this.value == "날짜"){
			selectStatus(".main1");
		}
		else {
			selectStatus(".main2");
		}
	
	});
	
	function selectStatus(status){
		$(".main1").hide();
		$(".main2").hide();
		$(status).show();
	}
	
});

</script>
</head>
<body>
	<jsp:include page="/Manage/headerVolunteerManage.jsp" flush="false" />
	<section class="sectiondiv">
		<div class="sectiontitle">
			<font size=6>출석현황</font>
		</div>
		
		<select name="SelectStatus" id="SelectStatus">
			<option value="날짜">날짜 별 현황</option>
			<option value="참여인원">참여인원 별 현황</option>
		</select>
	
	<div class="main1">
		
		<font size="6" color="#191919">날짜 별 출석현황</font>
		<font size="3" color="#191919">(${volunteer.startDate}~ ${volunteer.endDate})</font>

		<div id="LineItemTable">
			<div class="Heading">
				<div class="date">
					<p>날짜</p>
				</div>

				<div class="name">
					<p>이름</p>
				</div>

				<div class="status">
					<p>상태</p>
				</div>

			</div>
			<c:forEach var="attendance" items="${attendances}">
				<div class='lineItemRow'>
					<div class='date'>${attendance.date}</div>
					<div class='name'>${attendance.member.name}</div>
					<div class='status'>${attendance.status}</div>
				</div>
			</c:forEach>
		</div>


	</div>

	<div class="main2">
		<font size="6" color="#191919">참여인원 별 출석현황</font>
		<font size="3" color="#191919">(${volunteer.startDate}~ ${volunteer.endDate})</font>
		<div id="LineItemTable">
			<div class="Heading">
				<div class="date">
					<p>이름</p>
				</div>

				<div class="status">
					<p>날짜</p>
				</div>

				<div class="date">
					<p>상태</p>
				</div>

			</div>

			<c:forEach var="attendance" items="${attendances1}">
				<div class='lineItemRow'>
					<div class='name'>${attendance.member.name}</div>
					<div class='date'>${attendance.date}</div>
					<div class='status'>${attendance.status}</div>
				</div>
			</c:forEach>

		</div>


	</div>

	</section>
</body>
</html>
