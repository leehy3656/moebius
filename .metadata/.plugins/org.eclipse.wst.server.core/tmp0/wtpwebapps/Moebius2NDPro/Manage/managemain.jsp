<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>봉사활동 관리</title>

<style>
.main {
	margin-top: 200px;
	height: 1250px;
	width: 1550px;
}

#imageslide1 {
	margin-left :75px;
	position: relative;
	width: 90%;
	height: 500px;
} 

#imageslide1 img {
	position: absolute;
	z-index: -1;
	width: 100%;
	height: 100%;
}

.mainBox {
	margin-top :50px;
	position:relative;
	height: 30%;
	display: flex;
	width: 100%;
}

.article {
	height: 100%;
	width: 50%;
	border: solid #A6A6A6;
	border-width: thin;  
}

.title {
	margin-bottom: 0.2cm;
	text-align: center;
	background-color: #D5D5D5;
	height: 0.8cm;
}

.box1 ,.box2{
	display: flex;
}

.MainIMG{
	position:relative;
	width: 180px;
	height:100%;
	margin-left: auto;
	margin-right: auto; 
	margin-top:0.4cm;
	margin-bottom: 0.3cm; 
}

.news {
	height: 100%;
	width: 50%;
	border: solid #A6A6A6;
	border-width: thin;
}

.MainArticle , .date{
	width: 70%;
	padding-left:0.5cm;
	margin-top:0.4cm;
	margin-bottom: 0.3cm; 
}
.MainArticle a{
text-decoration: none;
color: #6C6C6C;
}

.date {
	width: 30%;
}

#btnimage, #btnimage2 {
	position: relative;
	top: 200px;
	z-index: 2;
	height: 50px;
	cursor: pointer;
}

#btnimage2 {
	left: 1200px;
}
</style>

<script>
$(document).ready(function a(){
    var slow = 200 ;
     
     $("#imageslide1 img").delay(4000).show();
     $("#imageslide1 img").animate({left : "+=1550px"},slow);
     $("#imageslide1 img").delay(4000).show();
     $("#imageslide1 img").animate({left : "+=1550px"},slow);
     $("#imageslide1 img").delay(4000).show();
     $("#imageslide1 img").animate({left : "-=1550px"},slow);
     $("#imageslide1 img").delay(4000).show();
     $("#imageslide1 img").animate({left : "-=1550px"},slow,a);  
 /*   div.animate({width: '300px', opacity: '0.8'}, "slow"); 
   div.animate({height: '100px', opacity: '0.4'}, "slow");
   div.animate({width: '100px', opacity: '0.8'}, "slow"); */

   function PrepareHowto() {
		alert("삑");
		location.href="/celab/Manage/howto.jsp"; 
	};
   
});



</script>
</head>
<body>
<jsp:include page="/Manage/headerVolunteerManage.jsp" flush="false" />
<div class="main">
	<div id="imageslide1" style="overflow: hidden;">
			<img src="/celab/images/ManageImage1.jpg" id="pointitemimage5" style="left: -3100px;"> 
			<img src="/celab/images/ManageImage2.png" id="pointitemimage4" style="left: -1550px;" />
			<img src="/celab/images/ManageImage3.jpg" id="pointitemimage1" style="left: 0px;"> 
		</div>
		<div class="mainBox">
			<div class="article">
				<div class="title">재능기부 활동 현황</div>
				<c:forEach var="volunteer" items="${VolunteerList}"
					varStatus="status" begin="0" end="4">
					<div class="box1">
						<div class="MainArticle">
							<a href="/celab/prepareDetailView.do?no=${volunteer.no}&listno=1">${volunteer.title}</a><br>
						</div>
						<div class="date">${volunteer.date}</div>
					</div>
				</c:forEach>
			</div>
			
			<div class="news">
				<div class="title">베스트 재능기부</div>
				<div class="box2">
				<c:forEach var="bestVolunteer" items="${BestVolList}"
					varStatus="status" begin="0" end="2">
						<div class="MainIMG">
						<div>
							<a href="/celab/prepareDetailView.do?no=${bestVolunteer.no}&listno=1">
							<img src="${ctx}/celab/images/${bestVolunteer.filevolunteer}"
							 style="width:180px;height:210px;">
							</a>
							</div>
						</div>
				</c:forEach> 
				</div>
			</div>
		</div>
	</div>
</body>
</html>