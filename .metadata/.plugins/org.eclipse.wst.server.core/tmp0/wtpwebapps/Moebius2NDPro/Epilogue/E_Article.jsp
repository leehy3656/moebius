<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript">
	function updateEp(no,e) { 
		
	var content = $(e).parent().parent().parent().find("textarea").val();
	//var img = document.getElementById("ImgRegist").value;
	 location.href = "/celab/EpUpdate.do?content=" + content
									 +"&no="+no;
	}
	function EpRegist() {
		var content = document.getElementById("EpTextArea").value;
		var img = document.getElementById("ImgRegist").value;
		var listno = 1;
		location.href = "/celab/EpRegist.do?content=" + content + "&img=" + img + "&listno=" + listno;
	}

	function ImgRegist() {
		location.href = "ImgRegist.do";
	} 
	$(document).ready(function() {	
		$(".Update").hide();
	$(".EpObject .UpdatePrepare").click(function () { 
		$(this).parent().parent().parent().find("textarea").prop("disabled",false);
		$(this).hide();
		$(this).parent().find(".Update").show();
	});
	
	});
</script>
<style type="text/css">
a
{
	text-decoration: none;
	color: #6A84B7;
}
.main {
	border-top :solid #ff7012;
	width: 70%;
	padding-top: 0.2cm;
	margin-top: 7cm;
	margin-left: auto;
	margin-right: auto;
}

.epilogue {
	margin-bottom:1cm;
	display: flex;
}

#EpTextArea {
	width: 100%;
	height: 3cm;
}

.EpObject {
	display: flex;
 	border-top: solid #8C8C8C;
	border-width: thin; 
	width: 100%;
	height: 4.7cm;
	margin-left: auto;
	margin-right: auto;
}
.UpdatePrepare, #RemoveA, .Update{
	height:0.8cm;
	width:1.5cm;
	cursor: pointer;
	text-decoration: none;
	color: #6A84B7;
}
#contentAndtextBox{
	
	width:72%;
}
#ImgBox{
	margin-top:0.7cm;
	width:20%;
}
#epContentBox{
	/* border:solid;
	border-width:thin; */
	height:1cm;
	padding-left: 0.2cm;
	padding-top: 0.2cm;
	display:flex;
}

.epTextArea {
	width: 97%;
	height: 3cm;
	padding: 0.2cm;
	background-color: white;
}

.EpButtonBox {
	height: auto;
}

#EpRegist, #ImgRegist {
	height: 1.6cm;
	width: 100%;
}

.page {
	border: solid;
	margin-left: auto;
	margin-right: auto;
}
</style>
<title>Insert title here</title>
</head>
<body>
<jsp:include page="../mainpage/headerMoebius.jsp" flush="false" />
<section class="sectiondiv">
	<div class="main">
		<div class="epilogue">
			<textarea id="EpTextArea">후기를 등록해 주세요.</textarea>
			<div class="EpButtonBox">
				<input type="file" id="ImgRegist" name="epilogueIMG">
				<!-- <input type="button" id="ImgRegist" value="사진 첨부" onclick="ImgRegist()">  -->
				<input type="button" id="EpRegist" value="후기 등록"
					onclick="EpRegist()">
			</div>
		</div>
		<br>
		<c:forEach var="epilogue" items="${epilogueList}" varStatus="status"
			begin="${(listno)*listSize-5}" end="${(listno)*listSize-1}">
			<div class="EpObject">
			<div id="contentAndtextBox">
				<div id="epContentBox">
				NO : ${status.count+(listno-1)*listSize}&nbsp;&nbsp;&nbsp;ID : ${member.idname}&nbsp;&nbsp;&nbsp;${epilogue.date}
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<div class="Update">
				<a onclick="updateEp(${epilogue.no},this)">완료</a>
				</div>
				<div class="UpdatePrepare">수정</div>|&nbsp;
				<div id="RemoveA">
				<a href="/celab/EpRemove.do?no=${epilogue.no}&listno=1">삭제</a>
				</div>
				</div>
					<textarea class="epTextArea" disabled="disabled">${epilogue.content}</textarea>
				</div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<div id="ImgBox">
				<img src="http://192.168.0.15:8080/celab/images/moebius.jpg"
						class="floating1" width="auto" height="auto">
				<%-- ${epilogue.img} --%>
				</div>
			</div>
			<br>
		</c:forEach>

		<br>
		<div class="page">
			<c:forEach var="no" begin="1" end="${pageSize}">
				<a href="/celab/EpSelect.do?listno=${no}">${no}</a>
			</c:forEach>
		</div>
	</div>
	</section>
</body>
</html>