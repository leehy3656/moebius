<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <style type="text/css">

.buyitemdiv
{
	position: relative;
	width:70%; 
	
	margin-left:auto;
	margin-right:auto;
	

 }
.onerow 
{
	border-bottom: solid #D5D5D5;
	border-width: thin;
	width: 100%;
	height: 40px;
	background-color: #F6F6F6;
	margin-top: 0.05cm;
	font-weight: bold;
	padding-top: 10px;
	padding-left: 5px;
}

.tworow 
{
	margin-top: 0.05cm;
	border-bottom: solid #D5D5D5;
	border-width: thin;
	width: 100%;
	height: 40px;
	background-color: #FCFCFC;
	padding-top: 10px;
	padding-left: 5px;
}
.itemiinfo hr
{
border-color: #FF7012;
}
.itemiinfo input
{
	border: none;
	width: 100%;
	
	height: 38px;
	background-color: #FCFCFC;
}
.btndiv button {
	margin-top:50px;
	height: 40px;
	background-color: #212121;
	color: white;
	margin-left:20%;
	width:16%;
}
.post {
	margin-top: 0.05cm;
	border-bottom: solid #D5D5D5;
	border-width: thin;
	width: 100%;
	height: 40px;
	background-color: #FCFCFC;
}

.postText {
	border: none;
	width: 29%;
	height: 38px;
	background-color: #FCFCFC;
}
.addressText{
	width: 40%;
	height: 38px;
	border: none;
	background-color: #FCFCFC;
	
}

.postCheck {
	width: 28%;
	height: 38px;
}
.detailAddress {
	width: 100%;
	height: 38px;
	border: none;
	background-color: #F6F6F6;
}

</style>
<title>Insert title here</title>
</head>
<body>
<jsp:include page="/mainpage/headerMoebius.jsp" flush="false"/>
<section class="sectiondiv">
<div class="buyitemdiv">
<div class="sectiontitle">
		<font size=6>물품구매</font>
			</div>
			<form action ="buyitem.do" method="post" id="f2">
<input type="hidden" name="itemno" value="${pointproduct.no}">
<input type="hidden" name="detailquantity" value="${quantity}">
<br>
<div class="itemiinfo">
<hr>
<div class="onerow">
제품명
</div>
<div class="tworow">
${pointproduct.name}
</div>
<div class="onerow">
제품가격
</div>
<div class="tworow">
P.${pointproduct.price*quantity}
</div>
<div class="onerow">
구매수량
</div>
<div class="tworow">
 ${quantity}
 </div>
</div>
<div class="memberinfo">
<div class="onerow">
수령지
</div>
		<div class="post">
				<input type='text' class="postText" value="${member.post}" id="posttxt" name="post"/> 
				<input type='text' class="addressText" value="${member.address}" id="place" name="place"/>
				<input
					type='button' class="postCheck" value="찾기" onclick='startSearPost()' />
			</div>
				<input type='text' class="detailAddress" value="${member.detailAddress}" name="place2"/>
			
<%-- <input type="text" name="place" value="${member.address}">
<div class="onerow">
상세주소:
</div>
<input type="text" name="place2" value="${member.detailAddress}">--%>
</div> 
 <!--
 			<div id="post">
				<input type='text' id="postText" placeholder="   우편번호" /> 
				<input type='text' id="addressText" placeholder="주소" />
				<input
					type='button' id="postCheck" value="찾기" onclick='startSearPost()' />
			</div>
   -->


</form>
<div class="btndiv">
<button  onclick="submitbtn()">구매</button>
<button  onclick="pointmain()">취소</button> 
</div>
</div>






</section>
</body>
<script>
function startSearPost() {
	var myWindow = window.open("Member/Post/address.jsp", "우편주소검색",
	"resizable=no,width=605, height=710"); 
} 
function 주소받다(post, postAddress) {
	var txtPost = document.getElementById("posttxt");
	var txtPostAddress = document.getElementById("place");
	txtPost.value = post;
	txtPostAddress.value = postAddress;
}
function pointmain(){
	
	location.href="/celab/pointproductload.do";
}
function submitbtn(){
	alert("구매되었습니다");
	$("#f2").submit();
	
}


</script>
</html>