<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="/celab/Message/sockjs-0.3.js"></script>
<script src="/celab/Message/stomp.js"></script>
<title>메세지 보내기</title>
</head>
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<style>
.main {
	width: 500px;
	height: 500px; 
	border: solid #C3C3C3; 
	border-width: thin;
}
.recieveUser, .buttonBox{
margin-top: 20px;
}
#contentText{
margin-top: 20px;
}
#recieveUser{
width: 85%;
background-color: white;
}
#contentText1{
width:85%;
height:300px;
margin-left: 39px; 
}
#messageButton{
margin-left: 415px;   
}
</style>
<script type="text/javascript">
var CONTEXT = '${pageContext.request.contextPath}';
function sendAjax(url, params, doneCallback) {

	$.ajax({
		url : url,
		type : 'POST',
		dataType : 'text', //받는 타입
		data : params,
		traditional : true,
		cache : false,
		//contentType: 'application/json ; charset=UTF-8',
		//contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
		//mimeType: 'application/json',(mimeType은 동영상, 음악 파일 등을 쏠때 해당)
		//contentType: 'application/json ; charset=UTF-8',
		//mimeType: 'application/json',

		success : function(data) {
			doneCallback(data);
		},

		error : function(request, status, error) {
			alert("SSibal");
			alert("code:" + request.status + "\n" + "message:"
					+ request.responseText + "\n" + "error:" + error);
		}
	});
}

var ws = new SockJS('http://192.168.0.23:15674/stomp');
var client = Stomp.over(ws);
// SockJS does not support heart-beat: disable heart-beats
client.heartbeat.incoming = 0;
client.heartbeat.outgoing = 0;

var on_connect = function(x) {
	console.log('success');
  
};
var on_error =  function() {
  console.log('error');
};
client.connect('guest', 'guest', on_connect, on_error, '/');

function sendMessage(){
		var url = CONTEXT + "/saveMessage.do";
		var text = $("#contentText1").val();
		var receiveMember = $("#receieveUserNo").val();
		var type = "fMessage";
		
		var params={
			type 			: type,
			receiveMember	: receiveMember,
			content			: text
		};
	
	    if (text) {
	      	client.send('/amq/queue/no'+receiveMember+'#1', { priority: 9 }, text);
	        $('#contentText1').val("");
	      }
	    
	    sendAjax(url,params,receiveMessageCallback);
	    return false;
	  }

function receiveMessageCallback(result){
	if(result){
		alert("메세지가 전송되었습니다.");
		window.close();
	}
}
</script>
<body>
	<div class="main">
		<div class="recieveUser">
			&nbsp;To : <input type="text" id="recieveUser" value="${memberName}" disabled>
		</div>
		<div id="contentText">
			<textarea id ='contentText1' placeholder="전달하고싶은 메세지를 입력해주세요." ></textarea>
		</div>
		<div class="buttonBox">
			<input type="button" id="messageButton" onclick="sendMessage()" value="보내기">
		</div>
	</div>
	<input type="hidden" id="receieveUserNo" value ="${memberNo}" />
	
	
</body>
</html>