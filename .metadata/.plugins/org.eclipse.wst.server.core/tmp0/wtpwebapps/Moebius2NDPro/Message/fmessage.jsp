<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="/celab/Message/sockjs-0.3.js"></script>
<script src="/celab/Message/stomp.js"></script>
<title>메시지</title>

<!-- 스타일 -->
<style>
#LineItemTable {
	margin-top: 20px;
	width: 600px;
}

.Heading {
	display: table;
	font-weight: bold;
	text-align: center;
	background-color: #FCFCFC;
	width: 100%;
	border-bottom: solid #D5D5D5;
	border-width: thin;
	margin-bottom: 0.05cm;
}

.lineItemRow {
	display: table;
	background-color: #F6F6F6;
	text-align: center;
	border-bottom: solid #D5D5D5;
	border-width: thin;
	margin-bottom: 0.05cm;
}

.name {
	display: table-cell;
	border: solid;
	border-width: thin;
	padding-left: 5px;
	padding-right: 5px;
	width: 15%;
	border: none;
	color: #4C4C4C;
}

.date {
	display: table-cell;
	border: solid;
	border-width: thin;
	padding-left: 5px;
	padding-right: 5px;
	width: 25%;
	border: none;
	vertical-align:middle;
	color: #4C4C4C;
}

.content {
	display: table-cell;
	border: solid;
	border-width: thin;
	padding-left: 5px;
	padding-right: 5px;
	width: 30%;
	border: none;
	vertical-align:middle;
	color: #4C4C4C;
}

.check {
	display: table-cell;
	border: solid;
	border-width: thin;
	padding-left: 5px;
	padding-right: 5px;
	width: 30%;
	border: none;
	vertical-align:middle;
	color: #4C4C4C;
}

#allSelect {
	width: 40px;
	height:40px;
	background-image:url('http://192.168.0.23:8080/celab/images/checkAll.png');
	cursor: pointer;
}

#delete {
	width: 40px;
	height:40px;
	background-image:url('http://192.168.0.23:8080/celab/images/deleteAll.png');
	cursor: pointer;
}
</style>

<!-- 스크립트 -->
<script>
var CONTEXT = '${pageContext.request.contextPath}';

function sendMessage(memberNo){
	
	var myWindow = window.open("/celab/PrepareSendmessage.do?memberNo="+memberNo,"메세지","width=800, height=450");
	
	
}


$(document).ready(function() {
	var fSize = $("#fSize").val();
	var messageSize = $("#messageSize").val();
	var size =parseInt(fSize) + parseInt(messageSize);
	var check=0; 
	var messageCheck = document.getElementsByName("messageCheck");
	
	
	$("#allSelect").click( function (){
		for(var i=0; i<size; i++){
			if(messageCheck[i].checked == true){
					check +=1;
			}
		}
		
		if(check == size){
			for(var i=0; i<size; i++){
				messageCheck[i].checked = false;
			}
			check =0;
		}
		else if(check<size){
			for(var i=0; i<size; i++){
				messageCheck[i].checked = true;
			}
			check=0;
		}
		
	}); 
	
	$("#delete").click( function (){
		var url = CONTEXT +"/deleteMessage.do";
		var check = true;
		var check1 = true;
		
		while(check == true){
			for(var i=0; i<size; i++){
				if(messageCheck[i].checked == true){
					
					params={
							messageNo :$(messageCheck[i]).val()
					}
					sendAjax(url,params,resultCallback);
					$(messageCheck[i]).parent().parent().remove();
					size=size-1;
					
					break;
				}
			}
			
			for(var j=0; j<size; j++){
				if(messageCheck[j].checked == true){
					check1 = true;
					break;
				}
				else{
					check1 = false;
				}
				
			}
			
			if(size ==0 || check1 == false){
				check = false;
			}
		}
	});

});

function resultCallback(data) {
	if(data){
		console.log('success'); 
	}
}

function sendAjax(url, params, doneCallback) {

	$.ajax({
		url : url,
		type : 'POST',
		dataType : 'text', //받는 타입
		data : params,
		traditional : true,
		cache : false,
		//contentType: 'application/json ; charset=UTF-8',
		//contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
		//mimeType: 'application/json',(mimeType은 동영상, 음악 파일 등을 쏠때 해당)
		//contentType: 'application/json ; charset=UTF-8',
		//mimeType: 'application/json',

		success : function(data) {
			doneCallback(data);
		},

		error : function(request, status, error) {
			alert("code:" + request.status + "\n" + "message:"
					+ request.responseText + "\n" + "error:" + error);
		}
	});
}

</script>
</head>

<body>
<div id="LineItemTable">
	<div class="Heading">
		<div class="name">
			<p>보낸사람</p>
		</div>

		<div class="content">
			<p>내용</p>
		</div>
		
		<div class="date">
			<p>일시</p>		
		</div>
		
		<div class="check">
			<input type="button" id="allSelect" value="" />
			<input type="button" id="delete" value="" />
		</div>
		
	</div>
		<c:forEach var="message" items="${f}">
		<div class='lineItemRow'>
			<div class='name'>
				<b onclick = "sendMessage(${message.sender.idno})"> ${message.sender.idname}</b>
			</div>
			
			<div class='content'>
				<b>${message.content}</b>
			</div>
			
			<div class='date'>
				<b> ${message.date}</b>
			</div>
			
			<div class ='check' >
				<input type="checkbox" name="messageCheck" value="${message.no}"/>
			</div>
			
		</div>
	</c:forEach>
	
	
	<c:forEach var="message" items="${messages}">
		<div class='lineItemRow'>
			<div class='name'>
				<b onclick = "sendMessage(${message.sender.idno})"> ${message.sender.idname}</b>
			</div>
			
			<div class='content'>
				${message.content}
			</div>
			
			<div class='date'>
				<b> ${message.date}</b>
			</div>
			
			<div class ='check'>
				<input type="checkbox" name="messageCheck" value="${message.no}"/>
			</div>
			
		</div>
	</c:forEach>
	</div>
<input type="hidden" id="memberNo" value="${memberNo}"/>
<input type="hidden" id="fSize" value= "${f.size()}"/>
<input type="hidden" id="messageSize" value= "${messages.size()}"/>
</body>

</html>