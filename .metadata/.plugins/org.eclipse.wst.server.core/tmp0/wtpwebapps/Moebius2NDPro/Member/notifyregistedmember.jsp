<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
   <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
  
<!DOCTYPE html>
<html>
<head>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<meta charset="UTF-8" />

<title>환영 합니다.</title>

<!-- 스타일 -->
<style type="text/css">
#main {
	padding: 20px;
	padding-top: 0;
	width:70%;
	border: 50%;
	position: relative;
	margin-left:auto;
	margin-right:auto;
	
	z-index: -1;
	border: 50%;
}

.SingupCompleteArea {
	display: table;
	width: 100%;
	border: solid;
	border :none;
	border-width: medium;
	background-color: white;
	margin-bottom: 0.2cm;
	margin-left: auto;
	margin-right: auto;
}

#logo {
	border: solid;
	border-width: thin;
	border :none;
	color: #212121;
	width: 100%;
	margin-bottom: 100px;
}

#welcomeText{
	width : 100%;
	height : 50px;
	border : none;
	background-color : white;
	text-align: center;
	font-size: 50px;
	margin-bottom: 20px;

}

#welcomeText1{
	width : 100%;
	height : 20px;
	border : none;
	background-color : white;
	text-align: center;
	font-size: 20px;
}

#welcomeText2{
	width : 100%;
	height : 20px;
	border : none;
	background-color : white;
	text-align: center;
	font-size: 20px;
	margin-bottom: 20px;
}

#welcomeText3{
	width : 100%;
	height : 20px;
	border : none;
	background-color : white;
	text-align: center;
	font-size: 20px;
}

#welcomeText4{
	width : 100%;
	height : 20px;
	border : none;
	background-color : white;
	text-align: center;
	font-size: 20px;
	margin-bottom: 20px;
}

#startBtn{
	width : 100%;
	height : 20px;
	border : none;
	background-color : white;
	text-align: center;
	font-size: 20px;
}


#
</style>

<!-- 스크립트 -->
<script>
function start(){
	location.href="/celab/mainpage/headerMoebius.jsp";
}

$(document).ready(function() {
	
});
</script>

</head>


<!-- HTML -->
<body>
<jsp:include page="/mainpage/headerMoebius.jsp" flush="false"/>   
<section class="sectiondiv">
<br>
<div id="main">
	<div class="sectiontitle">
		<font size=6>회원가입 완료</font>
	</div>
	<div class="SingupCompleteArea">
		<div id="logo" style="font-size: 3.0em">
			<p align="center">MOEBIUS</p>
		</div>
			<input type='text' id ='welcomeText' value = '환영합니다!' disabled/>
			<input type= 'text' id = 'welcomeText1' value = '${member.name}님, 회원가입을 축하드립니다.'/>
			<input type= 'text' id = 'welcomeText2' value = '뫼비우스의 새로운 아이디는 ${member.idname} 입니다.'/>
			<input type= 'text' id = 'welcomeText3' value = '당신의 작은 관심이 우리 사회를 더 아름답게 합니다. '/>
			<input type= 'text' id = 'welcomeText4' value = '사회에서 받은 재능을 다시 사회로! 함께 아름다운 세상을 만들어 나갑시다. '/>
			<input type= 'button' id = 'startBtn' value = '시작하기' onclick = 'start()'/>
		</div>
	</div>	
</section>		
</body>

</html>
