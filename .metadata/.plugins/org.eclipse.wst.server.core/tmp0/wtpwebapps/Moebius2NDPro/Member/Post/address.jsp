<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<title>우편번호 검색</title>

<!-- 스타일 -->
<style type="text/css">
#LineItemTable
{
    display: table;
    width: 800px;
}
.main{
width:555px;
height:655px; 
padding: 0.5cm;    
}
#dong
{
	width :500px; 
}
#dongBtn
{
	width : 15%;
}
ul#zipList li {
    display:inline;
}

.SearchListBox{
border:solid #8C8C8C;
border-width:thin ;
height:500px;
width:550px;
overflow:auto;
margin-top:0.5cm;
table-layout: fixed;
}
#zipList a{
	text-decoration: none;
	color: #6A84B7;
}
</style>

<script type="text/javascript">

var CONTEXT = '${pageContext.request.contextPath}';

function sendAjax(url, params, doneCallback) {
	
	$.ajax({
		url : url,
		type : 'GET',
		dataType : 'json', //받는 타입
		data : params,
		traditional: true,
		cache: false,
		//contentType: 'application/json ; charset=UTF-8',
		//contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
		//mimeType: 'application/json',(mimeType은 동영상, 음악 파일 등을 쏠때 해당)
		//contentType: 'application/json ; charset=UTF-8',
	    //mimeType: 'application/json',

		success : function(data) {
			doneCallback(data);
		},
		
		error:function(request,status,error){
	        alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
		}
	});
}

//우편번호 조회
function searchZipCode(){
	$('#zipList > li').remove();
	var dong = $("#dong").val();
	if(!dong ) {
		alert("동/읍/면을 입력해주세요.");
		return false;
	} else {
		var url = "http://192.168.0.90:8088/eclipseWeb/zipCode/zipCode.ajax";
		var params = {
				dong : dong
		}
		sendAjax(url, params, searchZipCodeCallback);
	}
}

//우편번호 조회 callback
function searchZipCodeCallback(result) {
	for( var key in result ) {
		var mapResult = result[key];
		for( var mapKey in mapResult ) {
			$('#zipList').append(
					'<li>'+
						'<a href="javascript:selectZipCode(\''+mapKey+'\',\''+mapResult[mapKey]+'\')">'+
						'('+mapKey+')'+ mapResult[mapKey] +
						'</a>'+
						'<br>'+
					'</li>'
				);
		}
	}
}
function selectZipCode(post, postAddress){
	opener.주소받다(post,postAddress);
	self.close();
	
}
</script>
</head>
<body>
<div class="main">
	<h1>우편조회</h1>
	
	동,읍,면 조회
	<input type="radio" name="selectPost" checked/>
	도로명 조회
	<input type="radio" name="selectPost"/><br>
	
	<input type="text" id="dong" placeholder="동명, 건물명,우편번호" 
	onkeydown="if(event.keyCode == 13){searchZipCode(); return false;}" />
	
	<input type="button" id="dongButton" value='검색' onclick='searchZipCode()'/>
	<div class="SearchListBox">
	<ul id="zipList"></ul>   
	</div>
	</div>
	</body>
</html>
