<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<meta charset="UTF-8" />
<link rel="stylesheet" type="text/css" href="/celab/css/allcss.css" />

<!-- 스타일 -->
<style>
.sectiondiv p {
	font-weight: bold;
}

.volunteerdetailsee{
	position: relative;
	width: 70%;
	margin-left: auto;
	margin-right: auto;
}

#head{
	border: solid;
	border-width :thin;
	border-left : none;
	border-right : none;
	border-bottom : none;
	width : 100%;
	height : 30px;
	background-color: #F6F6F6;
	text-align: center;
	color: black;
}

#line {
	width : 100%;
}
#subline {
	
	border : solid;
	border-width: thin;
	border-left : none;
	border-right : none;
	border-bottom : none;
	width : 100%;
	border-color : #A6A6A6;
}

#line1{
	width :100%;
	display: flex;
}

#title{
	border : none;
	width : 30%;
	height : 30px;
	background-color: #F6F6F6;
	text-align: center;
	color: black;
}

#content{
	border : none;
	width : 65%;
	height : 30px;
	text-align: center;
	background-color: white;
	color: black;
}

#title1{
	border : none;
	width : 15.1%;
	height : 30px;
	background-color: #F6F6F6;
	text-align: center;
	color: black;
}

#content1{
	border : none;
	width : 33.3%;
	height : 30px;
	text-align: center;
	background-color: white;
	color: black;
}

.container {
	border-bottom:solid #D5D5D5;
	border-width:thin;
	width:33.3%;
	float: left;
	height: 100px;
	color:#5D5D5D;
}
/* //////////////////////////////////////// */

a
{
	text-decoration: none;
	color: #6A84B7;
}
.main {
	border-top :solid #ff7012;
	width: 70%;
	padding-top: 0.2cm;
	margin-top: 1cm;
	margin-left: auto;
	margin-right: auto;
}
.detailImage
{
position:relative;
width:100%;

}

.epilogue {
	margin-bottom:1cm;
	display: flex;
}

#EpTextArea {
	width: 12.7cm;
	height: 3cm;
}


.EpObject {
	display: flex;
 	border-top: solid #8C8C8C;
	border-width: thin; 
	width: 100%;
	height: 4.7cm;
	margin-left: auto;
	margin-right: auto;
}
.UpdatePrepare, #RemoveA, .Update{
	height:0.8cm;
	width:1.5cm;
	cursor: pointer;
	text-decoration: none;
	color: #6A84B7;
}
#contentAndtextBox{
	
	width:72%;
}
#ImgBox{
	margin-top:0.5cm;
	width:20%;
}

#epContentBox{
	/* border:solid;
	border-width:thin; */
	height:1cm;
	padding-left: 0.2cm;
	padding-top: 0.2cm;
	display:flex;
}
.EpText{
}
.epTextArea {
	width: 97%;
	height: 3cm;
	padding: 0.2cm;
	background-color: white;
}

.EpButtonBox {
	height: auto;
	width:30%;
}

#EpRegist, #ImgRegist {
	height: 1.5cm;
	width: 90%; 
	margin-left: 0.5cm; 
}

.page {
	border: solid;
	margin-left: auto;
	margin-right: auto;
}
.UpdateBox{
display: flex;
}
</style>

<!-- 스크립트 -->
<script>
	
	function sendMessage(){
		var myWindow = window.open("/celab/Message/recv1.html","메세지","width=430, height=800");
	}
	
	function updatevolunteer(){
		location.href = "/celab/updateVolunteer.do?no=${volunteer.no}";
	
	}
	
	function applyVolunteer(){
		var url = CONTEXT +"/applyVolunteer.do";
		var volunteerNum = ${volunteer.no};
		var params={
			volunteerNum : volunteerNum,
		}
		sendAjax(url, params, applyVolunteerCallback);
	}
	
	function applyVolunteerCallback(result){
		if(result =="fail"){
			alert("이미 신청하셨습니다.");
		}
		else if( result =="success") {
			alert("신청 완료");
		}
	}
		
	$(document).ready(function() {

						function sex1() {
							var csex = ${volunteer.sex};
							var sex = document.getElementsByName("sex");
							/* alert(csex); */
							for (var i = 0; i < sex.length; i++) {

								if (sex.item(i).value == csex) {
									sex.item(i).checked = "true";
								} else if (sex.item(i).value == csex) {
									sex.item(i).checked = "true";
								} else if (sex.item(i).value == csex) {
									sex.item(i).checked = "true";
								}
							}
						}
						sex1();
					});
	
	/*======================================================================================  */
	/* function updateEp(no,e) { 
		
	var content = $(e).parent().parent().parent().find("textarea").val();
	//var img = document.getElementById("ImgRegist").value;
	alert(content);
	 location.href = "/celab/EpUpdate.do?content=" + content
									 +"&no="+no;
	} */
	  
	/*function EpRegist() {
		 var content = document.getElementById("EpTextArea").value;
		var img = document.getElementById("ImgRegist").value;
		var listno = 1;
		location.href = "/celab/EpRegist.do?content=" + content + "&img=" + img + "&listno=" + listno+"&no="+${volunteer.no} ; 
	}*/
	
	function readURL(input,no) {   
		 if(input.files && input.files[0]){
		   var reader = new FileReader();
		   reader.onload = function(e){
		    //$('#uploadImage').html("<img id=img src=''>");
		   
		 $('#EpImg'+no).attr('src', e.target.result);
		    
		   }
		   reader.readAsDataURL(input.files[0]);
		 }  
		  
		}
		
		function updateEp(no,e) { 
			
			$('#ep'+no).submit();

		
	 /*    $.ajax({
	        url: '/celab/EpUpdate.do',
	        type: "post",
	        dataType: "text",
	        data: data,
	        cache: false,
	        processData: false,
	        contentType: false,
	        success: function(data) {
	        	alert("123");
	        }, error: function(data) {
	        	alert("error");
	        }
	    }); */
		

		}

	function ImgRegist() {
		location.href = "ImgRegist.do";
	} 
	$(document).ready(function() {	
		$(".requestvl").hide();
		$(".updatevl").hide();
		var showupdate = ${volunteer.memberPerson.idno};
		var member = ${member.idno};
		if(showupdate == member){
			$(".updatevl").show();
		}else{
			
			$(".requestvl").show();
		}
		
		$(".main").hide();
		var showhide=${volunteer.status};
		
		
		if(showhide==7){
			$(".main").show();		
		}
		
		
	 	
		
		
		
		$(".Update").hide();
		$(".fileEp").hide();
	$(".EpObject .UpdatePrepare").click(function () { 
		alert("aaa"); 
		$(this).parent().parent().parent().find("textarea").prop("disabled",false);
		$(this).hide();
		$(this).parent().find(".Update").show();
		$(this).parent().parent().parent().parent().find(".fileEp").show();  
	}); 
	
	});
</script>

<title>재능기부 활동</title> 
</head>

<!-- HTML -->
<body>
<jsp:include page="../mainpage/headerMoebius.jsp" flush="false"/>
<jsp:include page="/mainpage/logincheck.jsp" flush="false"/>
<section class="sectiondiv">
	<div class="volunteerdetailsee">
		<div class="sectiontitle">
			<font size=6>재능기부 활동</font>
		</div>
		<input type="text" id="head" value ="봉사활동 정보" disabled/>
		
		<div id="line">
			<div id ="subline">
				<input type="text" id="title" value="제목" disabled/>
				<input type="text" id="content" value="${volunteer.title}" disabled/>
			</div>
			
			<div id ="subline">
				<input type="text" id="title" value="신청인" disabled/>
				<%-- <a href="" onclick="sendMessage()"> ${volunteer.memberPerson.idname} </a> --%>
			</div>
			
			<div id ="subline">
				<input type="text" id="title" value="연락처" disabled/>
				<input type="text" id="content" value="${volunteer.phoneno}" disabled/>
			</div>
			
			<div id ="subline">
				<input type="text" id="title" value="주소" disabled/>
				<input type="text" id="content" value="${volunteer.post}     ${volunteer.place} ${volunteer.place2} " disabled/>
			</div>
			<div id="line1" >
				<div id ="subline">
					<input type="text" id="title" value="요청인원" disabled/>
					<input type="text" id ="content" value="${volunteer.participant}명" disabled/>
				</div>
					
				<div id ="subline">	
					<input type="text" id="title" value="성별" disabled/>
					<c:choose>
						<c:when test="${volunteer.sex ==1}">
							<input type="text" id ="content" value="남성" disabled/>
						</c:when>
					
						<c:when test="${volunteer.sex ==2}">
							<input type="text" id ="content" value="남성" disabled/>
						</c:when>
					
						<c:otherwise>
							<input type="text" id ="content" value="무관" disabled/>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
			
			<div id="line1" >
				<div id ="subline">
					<input type="text" id="title1" value="봉사기간" disabled/>
					<input type="text" id ="content1" value="${volunteer.startDate} ~  ${volunteer.endDate}" disabled/>
					<input type="text" id="title1" value="요청요일" disabled/>
					<c:forEach var="weekday" items="${weekdays}" >
						${weekday.name}
					</c:forEach>
				</div>
			</div>
			
			<input type="text" id="head" value ="필요재능" disabled/>
			<div class="container">
					<c:forEach var="ability" items="${ability1}">
						<font>${ability.abilityName}</font>
						<br> 
					</c:forEach>
				</div>
				
				<div class="container">
					<c:forEach var="ability" items="${ability2}">
						${ability.abilityName}
						<br> 
					</c:forEach>
				</div>
				
				<div class="container">
					<c:forEach var="ability" items="${ability3}">
						${ability.abilityName}
						<br> 
					</c:forEach>
				</div>
			<input type="text" id="head" value ="상세 사항" disabled/>
			<img src="${ctx}/celab/images/${volunteer.filevolunteer}" width=100% height=30%>
			<textarea  rows="15" cols="92" name="memo" disabled="disabled">${volunteer.memo}</textarea>
		</div>
			<input type="button" value="봉사신청하기" id="testbtn" name='regist' onclick="applyVolunteer()" class= requestvl>
			<input type="button" value="수정" name='update' onclick='updatevolunteer()' class='updatevl'>
	</div>
	
	<div class="main">
		<form action="EpRegist.do" method="post" enctype="multipart/form-data">
		<div class="epilogue">
		<div class ="EpText">
			<textarea id="EpTextArea" name ="CONTENT">후기를 등록해 주세요.</textarea>
			</div>
			<div class="EpButtonBox">  
				<input type="file" id="ImgRegist" name="IMG">
				<!-- <input type="button" id="ImgRegist" value="사진 첨부" onclick="ImgRegist()">  -->
				<input type="hidden" name="volunteer" id="volunteer" value='${volunteer.no}'>
				<input type="submit" id="EpRegist" value="후기 등록">
			</div>
		</div>
		</form>
		<br>
		
		<c:forEach var="epilogue" items="${epilogueList}" varStatus="status"
			begin="${(listno)*listSize-5}" end="${(listno)*listSize-1}">
			<div>
			<form action = "EpUpdate.do" method="post" enctype="multipart/form-data" id="ep${epilogue.no}">
			<div class="EpObject">
			
			<div id="contentAndtextBox">
				<div id="epContentBox">
				NO : ${status.count+(listno-1)*listSize}&nbsp;&nbsp;&nbsp;ID : ${epilogue.member.idname}&nbsp;&nbsp;&nbsp;${epilogue.date}
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<c:choose>
				<c:when test="${member.idno==epilogue.member.idno}">
				<div class="UpdateBox"> 
				<div class="Update">
				<a onclick="updateEp(${epilogue.no},this)">완료</a>
				</div>
				<div class="UpdatePrepare">수정</div>|&nbsp;
				<div id="RemoveA">
				<a href="/celab/EpRemove.do?no=${epilogue.no}&listno=1">삭제</a>
				</div>
				</div>
				</c:when>
				</c:choose>
				</div>
					<textarea name="content" class="epTextArea" disabled="disabled">${epilogue.content}</textarea> 
				</div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<div id="ImgBox">
				<input type="file" id="fileEp" class="fileEp" name="fileEp" onchange="readURL(this,${epilogue.no})"/> 
				<img src="${ctx}/celab/epimages/${epilogue.img}"
						class="floating1" width="150px" height="150px" id="EpImg${epilogue.no}">
					<input type="hidden" name="no" value="${epilogue.no}"/>	
				</div>
					
			</div>
		</form>
		</div>
			<br>
		</c:forEach>

		<br>
		<div class="page">
			<c:forEach var="no" begin="1" end="${pageSize}">
				<a href="/celab/EpSelect.do?listno=${no}&no=${volunteer.no}">${no}</a>
			</c:forEach>
		</div>
	</div>
</section>

</body>

</html>