<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>Insert title here</title>

<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<style>
/*  body {

	background-image:url("../images/background5.png");
}  */
body {
overflow-x: hidden;
overflow-y: hidden;
}

.main {
	height: 20cm;
	width: 86%;
	margin-left: 15%;
}

.mainBox {
position:relative;
	/* margin-top: 35%; */
	height: 39%;
	display: flex;
	width: 100%;
}

.title {
	margin-bottom: 0.2cm;
	text-align: center;
	background-color: #D5D5D5;
	height: 0.8cm;
}

.article {
	height: 100%;
	width: 50%;
	border: solid #A6A6A6;
	border-width: thin;  
}

.news {
	height: 100%;
	width: 50%;
	border: solid #A6A6A6;
	border-width: thin;
}

.box1 ,.box2{
	display: flex;
}


.MainArticle , .date{
	width: 70%;
	padding-left:0.5cm;
	margin-top:0.4cm;
	margin-bottom: 0.3cm; 
}
.MainArticle a{
text-decoration: none;
color: #6C6C6C;
}

.date {
	width: 30%;
}

.MainIMG{
	position:relative;
	 width: 180px;
	 height:100%;
	 margin-left: auto;
	margin-right: auto; 
	margin-top:0.4cm;
	margin-bottom: 0.3cm; 
}

#imageslide1 {
	position: relative;
	width: 1360px;
	height: 520px;
}

#imageslide1 img {
	position: absolute;
	z-index: -1;
	width: 1360px;
	height: 520px;
}

#btnimage, #btnimage2 {
	position: relative;
	top: 200px;
	z-index: 2;
	height: 50px;
	cursor: pointer;
}

#btnimage2 {
	left: 1200px;
}

.footer {
	padding-left: 7cm;
	padding-right:0px;
	border-top: solid;
	border-width: thin;
	color : #5D5D5D;
	padding-top:5px;
	vertical-align:middle;
	text-align : center;
	background-color: #BDBDBD;
}
.FooterMain{
width:1600px;
height:5cm; 
padding : 0px;
margin-top : 60px;
background-color: #BDBDBD;
}
#alert{
margin-left: auto;
margin-right: auto;
margin-top: 3cm;
}
</style>

<script type="text/javascript">
		
		$(document).ready(function a(){
	         var slow = 2000 ;
	          
	          $("#imageslide1 img").delay(2000).show();
	          $("#imageslide1 img").animate({left : "+=1360px"},slow);
	          $("#imageslide1 img").delay(2000).show();
	          $("#imageslide1 img").animate({left : "+=1360px"},slow);
	          $("#imageslide1 img").delay(2000).show();
	          $("#imageslide1 img").animate({left : "-=1360px"},slow);
	          $("#imageslide1 img").delay(2000).show();
	          $("#imageslide1 img").animate({left : "-=1360px"},slow,a);  
	      /*   div.animate({width: '300px', opacity: '0.8'}, "slow"); 
	        div.animate({height: '100px', opacity: '0.4'}, "slow");
	        div.animate({width: '100px', opacity: '0.8'}, "slow"); */
	   });
//////////////////////////////////////////////////////////////////////

</script>


</head>

<body>
<jsp:include page="/mainpage/headerMoebius.jsp" flush="false" />
	<div class="main"> 
		<div id="imageslide1" style="overflow: hidden;">
				<img src="/celab/images/mainimage/back3-1.png" id="pointitemimage5"
				style="left: -2720px;"> 
				<img src="/celab/images/mainimage/back2-1.png"
				id="pointitemimage4" style="left: -1360px;"> 
				<img src="/celab/images/mainimage/back1-1.png" id="pointitemimage1"
				style="left: 0px;"> 
				<img src="/celab/images/mainimage/back2-1.png"
				id="pointitemimage2" style="left: 2720px;"> 
				<img src="/celab/images/mainimage/back3-1.png" id="pointitemimage3"
				style="left: 1360px;">
		</div>
		<!-- <img src="/celab/images/mainimage/leftbtn.png" id="btnimage">
		 <img src="/celab/images/mainimage/rightbtn.png" id="btnimage2"> -->
		<div class="mainBox">
			<div class="article">
				<div class="title">최근 봉사 신청게시글</div>
				<c:forEach var="volunteer" items="${VolunteerList}"
					varStatus="status" begin="0" end="4">
					<div class="box1">
						<div class="MainArticle">
							<a href="/celab/prepareDetailView.do?no=${volunteer.no}&listno=1">${volunteer.title}</a><br>
						</div>
						<div class="date">${volunteer.date}</div>
					</div>
				</c:forEach>
			</div>
			<div class="news">
				<div class="title">베스트 재능 기부활동</div>
				<div class="box2">
				
				<c:choose>
				<c:when test="${BestVolList.size()==0}">
			<div id="alert">선정된 베스트 재능 기부활동이 없습니다.</div> 
				</c:when>
				</c:choose>
				
				
				<c:forEach var="bestVolunteer" items="${BestVolList}"
					varStatus="status" begin="0" end="2">
						<div class="MainIMG">
						<div>
							<a href="/celab/prepareDetailView.do?no=${bestVolunteer.no}&listno=1">
							<img src="${ctx}/celab/images/${bestVolunteer.filevolunteer}"
							 style="width:180px;height:210px;">
							</a>
							</div>
						</div>
				</c:forEach> 
				</div>
			</div>
		</div>
	</div>
	<div class="FooterMain">
		 <footer class="footer">
			<font size=2px>업체명:Moebius 사업자등록번호:113-86-81827 개인정보관리책임자:박준용 주소:서울특별시 구로구 디지털로 27길 (구로동, 이스페이스 410호)<br>
			 대표전화:1600-6606 수혜기관 직통번호:02-6219-0011 FAX:02-6291-0009<br>
			계좌번호:예금주-Moebius [신한은행]140-010-421386 이메일:info@fhs.kr</font><br>
			<font size=1px>Copyright 2015 ⓒ  Moebius. All Rights Reserved.</font> </footer> 
	</div>

</body>

</html>