/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: Apache Tomcat/7.0.57
 * Generated at: 2015-05-21 02:40:25 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp.Message;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class messageSend_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("<script src=\"/celab/Message/sockjs-0.3.js\"></script>\r\n");
      out.write("<script src=\"/celab/Message/stomp.js\"></script>\r\n");
      out.write("<title>메세지 보내기</title>\r\n");
      out.write("</head>\r\n");
      out.write("<script\r\n");
      out.write("\tsrc=\"http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js\"></script>\r\n");
      out.write("<style>\r\n");
      out.write(".main {\r\n");
      out.write("\twidth: 500px;\r\n");
      out.write("\theight: 500px; \r\n");
      out.write("\tborder: solid #C3C3C3; \r\n");
      out.write("\tborder-width: thin;\r\n");
      out.write("}\r\n");
      out.write(".recieveUser, .buttonBox{\r\n");
      out.write("margin-top: 20px;\r\n");
      out.write("}\r\n");
      out.write("#contentText{\r\n");
      out.write("margin-top: 20px;\r\n");
      out.write("}\r\n");
      out.write("#recieveUser{\r\n");
      out.write("width: 85%;\r\n");
      out.write("background-color: white;\r\n");
      out.write("}\r\n");
      out.write("#contentText1{\r\n");
      out.write("width:85%;\r\n");
      out.write("height:300px;\r\n");
      out.write("margin-left: 39px; \r\n");
      out.write("}\r\n");
      out.write("#messageButton{\r\n");
      out.write("margin-left: 415px;   \r\n");
      out.write("}\r\n");
      out.write("</style>\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("var CONTEXT = '");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null, false));
      out.write("';\r\n");
      out.write("function sendAjax(url, params, doneCallback) {\r\n");
      out.write("\r\n");
      out.write("\t$.ajax({\r\n");
      out.write("\t\turl : url,\r\n");
      out.write("\t\ttype : 'POST',\r\n");
      out.write("\t\tdataType : 'text', //받는 타입\r\n");
      out.write("\t\tdata : params,\r\n");
      out.write("\t\ttraditional : true,\r\n");
      out.write("\t\tcache : false,\r\n");
      out.write("\t\t//contentType: 'application/json ; charset=UTF-8',\r\n");
      out.write("\t\t//contentType : 'application/x-www-form-urlencoded; charset=UTF-8',\r\n");
      out.write("\t\t//mimeType: 'application/json',(mimeType은 동영상, 음악 파일 등을 쏠때 해당)\r\n");
      out.write("\t\t//contentType: 'application/json ; charset=UTF-8',\r\n");
      out.write("\t\t//mimeType: 'application/json',\r\n");
      out.write("\r\n");
      out.write("\t\tsuccess : function(data) {\r\n");
      out.write("\t\t\tdoneCallback(data);\r\n");
      out.write("\t\t},\r\n");
      out.write("\r\n");
      out.write("\t\terror : function(request, status, error) {\r\n");
      out.write("\t\t\talert(\"SSibal\");\r\n");
      out.write("\t\t\talert(\"code:\" + request.status + \"\\n\" + \"message:\"\r\n");
      out.write("\t\t\t\t\t+ request.responseText + \"\\n\" + \"error:\" + error);\r\n");
      out.write("\t\t}\r\n");
      out.write("\t});\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write("var ws = new SockJS('http://192.168.0.23:15674/stomp');\r\n");
      out.write("var client = Stomp.over(ws);\r\n");
      out.write("// SockJS does not support heart-beat: disable heart-beats\r\n");
      out.write("client.heartbeat.incoming = 0;\r\n");
      out.write("client.heartbeat.outgoing = 0;\r\n");
      out.write("\r\n");
      out.write("var on_connect = function(x) {\r\n");
      out.write("\tconsole.log('success');\r\n");
      out.write("  \r\n");
      out.write("};\r\n");
      out.write("var on_error =  function() {\r\n");
      out.write("  console.log('error');\r\n");
      out.write("};\r\n");
      out.write("client.connect('guest', 'guest', on_connect, on_error, '/');\r\n");
      out.write("\r\n");
      out.write("function sendMessage(){\r\n");
      out.write("\t\tvar url = CONTEXT + \"/saveMessage.do\";\r\n");
      out.write("\t\tvar text = $(\"#contentText1\").val();\r\n");
      out.write("\t\tvar receiveMember = $(\"#receieveUserNo\").val();\r\n");
      out.write("\t\tvar type = \"fMessage\";\r\n");
      out.write("\t\t\r\n");
      out.write("\t\tvar params={\r\n");
      out.write("\t\t\ttype \t\t\t: type,\r\n");
      out.write("\t\t\treceiveMember\t: receiveMember,\r\n");
      out.write("\t\t\tcontent\t\t\t: text\r\n");
      out.write("\t\t};\r\n");
      out.write("\t\r\n");
      out.write("\t    if (text) {\r\n");
      out.write("\t      \tclient.send('/amq/queue/no'+receiveMember+'#1', { priority: 9 }, text);\r\n");
      out.write("\t        $('#contentText1').val(\"\");\r\n");
      out.write("\t      }\r\n");
      out.write("\t    \r\n");
      out.write("\t    sendAjax(url,params,receiveMessageCallback);\r\n");
      out.write("\t    return false;\r\n");
      out.write("\t  }\r\n");
      out.write("\r\n");
      out.write("function receiveMessageCallback(result){\r\n");
      out.write("\tif(result){\r\n");
      out.write("\t\talert(\"메세지가 전송되었습니다.\");\r\n");
      out.write("\t\twindow.close();\r\n");
      out.write("\t}\r\n");
      out.write("}\r\n");
      out.write("</script>\r\n");
      out.write("<body>\r\n");
      out.write("\t<div class=\"main\">\r\n");
      out.write("\t\t<div class=\"recieveUser\">\r\n");
      out.write("\t\t\t&nbsp;To : <input type=\"text\" id=\"recieveUser\" value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${memberName}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null, false));
      out.write("\" disabled>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\t<div id=\"contentText\">\r\n");
      out.write("\t\t\t<textarea id ='contentText1' placeholder=\"전달하고싶은 메세지를 입력해주세요.\" ></textarea>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\t<div class=\"buttonBox\">\r\n");
      out.write("\t\t\t<input type=\"button\" id=\"messageButton\" onclick=\"sendMessage()\" value=\"보내기\">\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t</div>\r\n");
      out.write("\t<input type=\"hidden\" id=\"receieveUserNo\" value =\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${memberNo}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null, false));
      out.write("\" />\r\n");
      out.write("\t\r\n");
      out.write("\t\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try {
            if (response.isCommitted()) {
              out.flush();
            } else {
              out.clearBuffer();
            }
          } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
