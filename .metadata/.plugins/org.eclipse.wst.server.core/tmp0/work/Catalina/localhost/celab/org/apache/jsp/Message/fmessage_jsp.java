/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: Apache Tomcat/7.0.57
 * Generated at: 2015-05-22 00:20:45 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp.Message;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class fmessage_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fc_005fforEach_0026_005fvar_005fitems;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _005fjspx_005ftagPool_005fc_005fforEach_0026_005fvar_005fitems = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
  }

  public void _jspDestroy() {
    _005fjspx_005ftagPool_005fc_005fforEach_0026_005fvar_005fitems.release();
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<script\r\n");
      out.write("\tsrc=\"http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js\"></script>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("<script src=\"/celab/Message/sockjs-0.3.js\"></script>\r\n");
      out.write("<script src=\"/celab/Message/stomp.js\"></script>\r\n");
      out.write("<title>메시지</title>\r\n");
      out.write("\r\n");
      out.write("<!-- 스타일 -->\r\n");
      out.write("<style>\r\n");
      out.write("#LineItemTable {\r\n");
      out.write("\tmargin-top: 20px;\r\n");
      out.write("\twidth: 600px;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write(".Heading {\r\n");
      out.write("\tdisplay: table;\r\n");
      out.write("\tfont-weight: bold;\r\n");
      out.write("\ttext-align: center;\r\n");
      out.write("\tbackground-color: #FCFCFC;\r\n");
      out.write("\twidth: 100%;\r\n");
      out.write("\tborder-bottom: solid #D5D5D5;\r\n");
      out.write("\tborder-width: thin;\r\n");
      out.write("\tmargin-bottom: 0.05cm;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write(".lineItemRow {\r\n");
      out.write("\tdisplay: table;\r\n");
      out.write("\tbackground-color: #F6F6F6;\r\n");
      out.write("\ttext-align: center;\r\n");
      out.write("\tborder-bottom: solid #D5D5D5;\r\n");
      out.write("\tborder-width: thin;\r\n");
      out.write("\tmargin-bottom: 0.05cm;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write(".name {\r\n");
      out.write("\tdisplay: table-cell;\r\n");
      out.write("\tborder: solid;\r\n");
      out.write("\tborder-width: thin;\r\n");
      out.write("\tpadding-left: 5px;\r\n");
      out.write("\tpadding-right: 5px;\r\n");
      out.write("\twidth: 15%;\r\n");
      out.write("\tborder: none;\r\n");
      out.write("\tcolor: #4C4C4C;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write(".date {\r\n");
      out.write("\tdisplay: table-cell;\r\n");
      out.write("\tborder: solid;\r\n");
      out.write("\tborder-width: thin;\r\n");
      out.write("\tpadding-left: 5px;\r\n");
      out.write("\tpadding-right: 5px;\r\n");
      out.write("\twidth: 25%;\r\n");
      out.write("\tborder: none;\r\n");
      out.write("\tvertical-align:middle;\r\n");
      out.write("\tcolor: #4C4C4C;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write(".content {\r\n");
      out.write("\tdisplay: table-cell;\r\n");
      out.write("\tborder: solid;\r\n");
      out.write("\tborder-width: thin;\r\n");
      out.write("\tpadding-left: 5px;\r\n");
      out.write("\tpadding-right: 5px;\r\n");
      out.write("\twidth: 30%;\r\n");
      out.write("\tborder: none;\r\n");
      out.write("\tvertical-align:middle;\r\n");
      out.write("\tcolor: #4C4C4C;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write(".check {\r\n");
      out.write("\tdisplay: table-cell;\r\n");
      out.write("\tborder: solid;\r\n");
      out.write("\tborder-width: thin;\r\n");
      out.write("\tpadding-left: 5px;\r\n");
      out.write("\tpadding-right: 5px;\r\n");
      out.write("\twidth: 30%;\r\n");
      out.write("\tborder: none;\r\n");
      out.write("\tvertical-align:middle;\r\n");
      out.write("\tcolor: #4C4C4C;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write("#allSelect {\r\n");
      out.write("\twidth: 40px;\r\n");
      out.write("\theight:40px;\r\n");
      out.write("\tbackground-image:url('http://192.168.0.23:8080/celab/images/checkAll.png');\r\n");
      out.write("\tcursor: pointer;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write("#delete {\r\n");
      out.write("\twidth: 40px;\r\n");
      out.write("\theight:40px;\r\n");
      out.write("\tbackground-image:url('http://192.168.0.23:8080/celab/images/deleteAll.png');\r\n");
      out.write("\tcursor: pointer;\r\n");
      out.write("}\r\n");
      out.write("</style>\r\n");
      out.write("\r\n");
      out.write("<!-- 스크립트 -->\r\n");
      out.write("<script>\r\n");
      out.write("var CONTEXT = '");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null, false));
      out.write("';\r\n");
      out.write("\r\n");
      out.write("function sendMessage(memberNo){\r\n");
      out.write("\t\r\n");
      out.write("\tvar myWindow = window.open(\"/celab/PrepareSendmessage.do?memberNo=\"+memberNo,\"메세지\",\"width=800, height=450\");\r\n");
      out.write("\t\r\n");
      out.write("\t\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("$(document).ready(function() {\r\n");
      out.write("\tvar fSize = $(\"#fSize\").val();\r\n");
      out.write("\tvar messageSize = $(\"#messageSize\").val();\r\n");
      out.write("\tvar size =parseInt(fSize) + parseInt(messageSize);\r\n");
      out.write("\tvar check=0; \r\n");
      out.write("\tvar messageCheck = document.getElementsByName(\"messageCheck\");\r\n");
      out.write("\t\r\n");
      out.write("\t\r\n");
      out.write("\t$(\"#allSelect\").click( function (){\r\n");
      out.write("\t\tfor(var i=0; i<size; i++){\r\n");
      out.write("\t\t\tif(messageCheck[i].checked == true){\r\n");
      out.write("\t\t\t\t\tcheck +=1;\r\n");
      out.write("\t\t\t}\r\n");
      out.write("\t\t}\r\n");
      out.write("\t\t\r\n");
      out.write("\t\tif(check == size){\r\n");
      out.write("\t\t\tfor(var i=0; i<size; i++){\r\n");
      out.write("\t\t\t\tmessageCheck[i].checked = false;\r\n");
      out.write("\t\t\t}\r\n");
      out.write("\t\t\tcheck =0;\r\n");
      out.write("\t\t}\r\n");
      out.write("\t\telse if(check<size){\r\n");
      out.write("\t\t\tfor(var i=0; i<size; i++){\r\n");
      out.write("\t\t\t\tmessageCheck[i].checked = true;\r\n");
      out.write("\t\t\t}\r\n");
      out.write("\t\t\tcheck=0;\r\n");
      out.write("\t\t}\r\n");
      out.write("\t\t\r\n");
      out.write("\t}); \r\n");
      out.write("\t\r\n");
      out.write("\t$(\"#delete\").click( function (){\r\n");
      out.write("\t\tvar url = CONTEXT +\"/deleteMessage.do\";\r\n");
      out.write("\t\tvar check = true;\r\n");
      out.write("\t\tvar check1 = true;\r\n");
      out.write("\t\t\r\n");
      out.write("\t\twhile(check == true){\r\n");
      out.write("\t\t\tfor(var i=0; i<size; i++){\r\n");
      out.write("\t\t\t\tif(messageCheck[i].checked == true){\r\n");
      out.write("\t\t\t\t\t\r\n");
      out.write("\t\t\t\t\tparams={\r\n");
      out.write("\t\t\t\t\t\t\tmessageNo :$(messageCheck[i]).val()\r\n");
      out.write("\t\t\t\t\t}\r\n");
      out.write("\t\t\t\t\tsendAjax(url,params,resultCallback);\r\n");
      out.write("\t\t\t\t\t$(messageCheck[i]).parent().parent().remove();\r\n");
      out.write("\t\t\t\t\tsize=size-1;\r\n");
      out.write("\t\t\t\t\t\r\n");
      out.write("\t\t\t\t\tbreak;\r\n");
      out.write("\t\t\t\t}\r\n");
      out.write("\t\t\t}\r\n");
      out.write("\t\t\t\r\n");
      out.write("\t\t\tfor(var j=0; j<size; j++){\r\n");
      out.write("\t\t\t\tif(messageCheck[j].checked == true){\r\n");
      out.write("\t\t\t\t\tcheck1 = true;\r\n");
      out.write("\t\t\t\t\tbreak;\r\n");
      out.write("\t\t\t\t}\r\n");
      out.write("\t\t\t\telse{\r\n");
      out.write("\t\t\t\t\tcheck1 = false;\r\n");
      out.write("\t\t\t\t}\r\n");
      out.write("\t\t\t\t\r\n");
      out.write("\t\t\t}\r\n");
      out.write("\t\t\t\r\n");
      out.write("\t\t\tif(size ==0 || check1 == false){\r\n");
      out.write("\t\t\t\tcheck = false;\r\n");
      out.write("\t\t\t}\r\n");
      out.write("\t\t}\r\n");
      out.write("\t});\r\n");
      out.write("\r\n");
      out.write("});\r\n");
      out.write("\r\n");
      out.write("function resultCallback(data) {\r\n");
      out.write("\tif(data){\r\n");
      out.write("\t\tconsole.log('success'); \r\n");
      out.write("\t}\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write("function sendAjax(url, params, doneCallback) {\r\n");
      out.write("\r\n");
      out.write("\t$.ajax({\r\n");
      out.write("\t\turl : url,\r\n");
      out.write("\t\ttype : 'POST',\r\n");
      out.write("\t\tdataType : 'text', //받는 타입\r\n");
      out.write("\t\tdata : params,\r\n");
      out.write("\t\ttraditional : true,\r\n");
      out.write("\t\tcache : false,\r\n");
      out.write("\t\t//contentType: 'application/json ; charset=UTF-8',\r\n");
      out.write("\t\t//contentType : 'application/x-www-form-urlencoded; charset=UTF-8',\r\n");
      out.write("\t\t//mimeType: 'application/json',(mimeType은 동영상, 음악 파일 등을 쏠때 해당)\r\n");
      out.write("\t\t//contentType: 'application/json ; charset=UTF-8',\r\n");
      out.write("\t\t//mimeType: 'application/json',\r\n");
      out.write("\r\n");
      out.write("\t\tsuccess : function(data) {\r\n");
      out.write("\t\t\tdoneCallback(data);\r\n");
      out.write("\t\t},\r\n");
      out.write("\r\n");
      out.write("\t\terror : function(request, status, error) {\r\n");
      out.write("\t\t\talert(\"code:\" + request.status + \"\\n\" + \"message:\"\r\n");
      out.write("\t\t\t\t\t+ request.responseText + \"\\n\" + \"error:\" + error);\r\n");
      out.write("\t\t}\r\n");
      out.write("\t});\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write("</script>\r\n");
      out.write("</head>\r\n");
      out.write("\r\n");
      out.write("<body>\r\n");
      out.write("<div id=\"LineItemTable\">\r\n");
      out.write("\t<div class=\"Heading\">\r\n");
      out.write("\t\t<div class=\"name\">\r\n");
      out.write("\t\t\t<p>보낸사람</p>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\r\n");
      out.write("\t\t<div class=\"content\">\r\n");
      out.write("\t\t\t<p>내용</p>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\t\r\n");
      out.write("\t\t<div class=\"date\">\r\n");
      out.write("\t\t\t<p>일시</p>\t\t\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\t\r\n");
      out.write("\t\t<div class=\"check\">\r\n");
      out.write("\t\t\t<input type=\"button\" id=\"allSelect\" value=\"\" />\r\n");
      out.write("\t\t\t<input type=\"button\" id=\"delete\" value=\"\" />\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\t\r\n");
      out.write("\t</div>\r\n");
      out.write("\t\t");
      if (_jspx_meth_c_005fforEach_005f0(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("\t\r\n");
      out.write("\t\r\n");
      out.write("\t");
      if (_jspx_meth_c_005fforEach_005f1(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("\t</div>\r\n");
      out.write("<input type=\"hidden\" id=\"memberNo\" value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${memberNo}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null, false));
      out.write("\"/>\r\n");
      out.write("<input type=\"hidden\" id=\"fSize\" value= \"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${f.size()}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null, false));
      out.write("\"/>\r\n");
      out.write("<input type=\"hidden\" id=\"messageSize\" value= \"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${messages.size()}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null, false));
      out.write("\"/>\r\n");
      out.write("</body>\r\n");
      out.write("\r\n");
      out.write("</html>");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try {
            if (response.isCommitted()) {
              out.flush();
            } else {
              out.clearBuffer();
            }
          } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_c_005fforEach_005f0(javax.servlet.jsp.PageContext _jspx_page_context)
          throws java.lang.Throwable {
    javax.servlet.jsp.PageContext pageContext = _jspx_page_context;
    javax.servlet.jsp.JspWriter out = _jspx_page_context.getOut();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_005fforEach_005f0 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _005fjspx_005ftagPool_005fc_005fforEach_0026_005fvar_005fitems.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_c_005fforEach_005f0.setPageContext(_jspx_page_context);
    _jspx_th_c_005fforEach_005f0.setParent(null);
    // /Message/fmessage.jsp(240,2) name = var type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_c_005fforEach_005f0.setVar("message");
    // /Message/fmessage.jsp(240,2) name = items type = javax.el.ValueExpression reqTime = true required = false fragment = false deferredValue = true expectedTypeName = java.lang.Object deferredMethod = false methodSignature = null
    _jspx_th_c_005fforEach_005f0.setItems(new org.apache.jasper.el.JspValueExpression("/Message/fmessage.jsp(240,2) '${f}'",_el_expressionfactory.createValueExpression(_jspx_page_context.getELContext(),"${f}",java.lang.Object.class)).getValue(_jspx_page_context.getELContext()));
    int[] _jspx_push_body_count_c_005fforEach_005f0 = new int[] { 0 };
    try {
      int _jspx_eval_c_005fforEach_005f0 = _jspx_th_c_005fforEach_005f0.doStartTag();
      if (_jspx_eval_c_005fforEach_005f0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\r\n");
          out.write("\t\t<div class='lineItemRow'>\r\n");
          out.write("\t\t\t<div class='name'>\r\n");
          out.write("\t\t\t\t<b onclick = \"sendMessage(");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${message.sender.idno}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null, false));
          out.write(")\"> ");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${message.sender.idname}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null, false));
          out.write("</b>\r\n");
          out.write("\t\t\t</div>\r\n");
          out.write("\t\t\t\r\n");
          out.write("\t\t\t<div class='content'>\r\n");
          out.write("\t\t\t\t<b>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${message.content}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null, false));
          out.write("</b>\r\n");
          out.write("\t\t\t</div>\r\n");
          out.write("\t\t\t\r\n");
          out.write("\t\t\t<div class='date'>\r\n");
          out.write("\t\t\t\t<b> ");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${message.date}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null, false));
          out.write("</b>\r\n");
          out.write("\t\t\t</div>\r\n");
          out.write("\t\t\t\r\n");
          out.write("\t\t\t<div class ='check' >\r\n");
          out.write("\t\t\t\t<input type=\"checkbox\" name=\"messageCheck\" value=\"");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${message.no}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null, false));
          out.write("\"/>\r\n");
          out.write("\t\t\t</div>\r\n");
          out.write("\t\t\t\r\n");
          out.write("\t\t</div>\r\n");
          out.write("\t");
          int evalDoAfterBody = _jspx_th_c_005fforEach_005f0.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_005fforEach_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (java.lang.Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_005fforEach_005f0[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_005fforEach_005f0.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_005fforEach_005f0.doFinally();
      _005fjspx_005ftagPool_005fc_005fforEach_0026_005fvar_005fitems.reuse(_jspx_th_c_005fforEach_005f0);
    }
    return false;
  }

  private boolean _jspx_meth_c_005fforEach_005f1(javax.servlet.jsp.PageContext _jspx_page_context)
          throws java.lang.Throwable {
    javax.servlet.jsp.PageContext pageContext = _jspx_page_context;
    javax.servlet.jsp.JspWriter out = _jspx_page_context.getOut();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_005fforEach_005f1 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _005fjspx_005ftagPool_005fc_005fforEach_0026_005fvar_005fitems.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_c_005fforEach_005f1.setPageContext(_jspx_page_context);
    _jspx_th_c_005fforEach_005f1.setParent(null);
    // /Message/fmessage.jsp(262,1) name = var type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_c_005fforEach_005f1.setVar("message");
    // /Message/fmessage.jsp(262,1) name = items type = javax.el.ValueExpression reqTime = true required = false fragment = false deferredValue = true expectedTypeName = java.lang.Object deferredMethod = false methodSignature = null
    _jspx_th_c_005fforEach_005f1.setItems(new org.apache.jasper.el.JspValueExpression("/Message/fmessage.jsp(262,1) '${messages}'",_el_expressionfactory.createValueExpression(_jspx_page_context.getELContext(),"${messages}",java.lang.Object.class)).getValue(_jspx_page_context.getELContext()));
    int[] _jspx_push_body_count_c_005fforEach_005f1 = new int[] { 0 };
    try {
      int _jspx_eval_c_005fforEach_005f1 = _jspx_th_c_005fforEach_005f1.doStartTag();
      if (_jspx_eval_c_005fforEach_005f1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\r\n");
          out.write("\t\t<div class='lineItemRow'>\r\n");
          out.write("\t\t\t<div class='name'>\r\n");
          out.write("\t\t\t\t<b onclick = \"sendMessage(");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${message.sender.idno}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null, false));
          out.write(")\"> ");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${message.sender.idname}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null, false));
          out.write("</b>\r\n");
          out.write("\t\t\t</div>\r\n");
          out.write("\t\t\t\r\n");
          out.write("\t\t\t<div class='content'>\r\n");
          out.write("\t\t\t\t");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${message.content}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null, false));
          out.write("\r\n");
          out.write("\t\t\t</div>\r\n");
          out.write("\t\t\t\r\n");
          out.write("\t\t\t<div class='date'>\r\n");
          out.write("\t\t\t\t<b> ");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${message.date}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null, false));
          out.write("</b>\r\n");
          out.write("\t\t\t</div>\r\n");
          out.write("\t\t\t\r\n");
          out.write("\t\t\t<div class ='check'>\r\n");
          out.write("\t\t\t\t<input type=\"checkbox\" name=\"messageCheck\" value=\"");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${message.no}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null, false));
          out.write("\"/>\r\n");
          out.write("\t\t\t</div>\r\n");
          out.write("\t\t\t\r\n");
          out.write("\t\t</div>\r\n");
          out.write("\t");
          int evalDoAfterBody = _jspx_th_c_005fforEach_005f1.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_005fforEach_005f1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (java.lang.Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_005fforEach_005f1[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_005fforEach_005f1.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_005fforEach_005f1.doFinally();
      _005fjspx_005ftagPool_005fc_005fforEach_0026_005fvar_005fitems.reuse(_jspx_th_c_005fforEach_005f1);
    }
    return false;
  }
}
