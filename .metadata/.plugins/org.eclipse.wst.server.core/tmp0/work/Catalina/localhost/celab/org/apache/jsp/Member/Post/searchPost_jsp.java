/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: Apache Tomcat/7.0.57
 * Generated at: 2015-04-20 00:21:50 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp.Member.Post;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class searchPost_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("<script\tsrc=\"http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js\"></script>\r\n");
      out.write("<title>Insert title here</title>\r\n");
      out.write("<style type=\"text/css\">\r\n");
      out.write("#LineItemTable\r\n");
      out.write("{\r\n");
      out.write("    display: table;\r\n");
      out.write("    margin-top: 10px;\r\n");
      out.write("    width : 50%;\r\n");
      out.write("}\r\n");
      out.write(".Heading\r\n");
      out.write("{\r\n");
      out.write("    display: table-row;\r\n");
      out.write("    font-weight: bold;\r\n");
      out.write("    text-align: center;\r\n");
      out.write("    width : 100%;\r\n");
      out.write("}\r\n");
      out.write(".lineItemRow\r\n");
      out.write("{\r\n");
      out.write("    display: table-row;\r\n");
      out.write("}\r\n");
      out.write(".post\r\n");
      out.write("{\r\n");
      out.write("    display: table-cell;\r\n");
      out.write("    border: solid;\r\n");
      out.write("    border-width: thin;\r\n");
      out.write("    padding-left: 5px;\r\n");
      out.write("    padding-right: 5px;\r\n");
      out.write("    width : 30%;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write(".address\r\n");
      out.write("{\r\n");
      out.write("    display: table-cell;\r\n");
      out.write("    border: solid;\r\n");
      out.write("    border-width: thin;\r\n");
      out.write("    padding-left: 5px;\r\n");
      out.write("    padding-right: 5px;\r\n");
      out.write("    width : 60%;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write("</style>\r\n");
      out.write("<script>\r\n");
      out.write("\tfunction sendPost(post, aa) {\r\n");
      out.write("\t\topener.주소받다(post, aa);\r\n");
      out.write("\t\tself.close();\r\n");
      out.write("\t}\r\n");
      out.write("\r\n");
      out.write("\t$(document).ready(function() {\r\n");
      out.write("\t\t$(\"#city\").change(function() {\r\n");
      out.write("\t\t\tif (this.value == \"서울\") {\r\n");
      out.write("\t\t\t\ttxt = \"<SiGunGuListResponse><cmmMsgHeader><requestMsgId/><responseMsgId/>\";\r\n");
      out.write("\t\t\t\ttxt = txt +\"<responseTime>20150402:164645570</responseTime>\";\r\n");
      out.write("\t\t\t\ttxt = txt + \"<successYN>Y</successYN>\";\r\n");
      out.write("\t\t\t\ttxt = txt + \"<returnCode>00</returnCode>\";\r\n");
      out.write("\t\t\t\ttxt = txt + \"<errMsg/></cmmMsgHeader><siGunGuList>\";\r\n");
      out.write("\t\t\t\ttxt = txt + \"<signguCd>광진구</signguCd>\";\r\n");
      out.write("\t\t\t\ttxt = txt + \"</siGunGuList><siGunGuList>\";\r\n");
      out.write("\t\t\t\ttxt = txt + \"<signguCd>서대문구</signguCd>\";\r\n");
      out.write("\t\t\t\ttxt = txt + \"</siGunGuList><siGunGuList>\";\r\n");
      out.write("\t\t\t\ttxt = txt + \"<signguCd>중구</signguCd>\";\r\n");
      out.write("\t\t\t\ttxt = txt + \"</siGunGuList></SiGunGuListResponse>\";\r\n");
      out.write("\t\t\t\tif (window.DOMParser) {\r\n");
      out.write("\t\t\t\t\tparser = new DOMParser();\r\n");
      out.write("\t\t\t\t\txmlDoc = parser.parseFromString(txt, \"text/xml\");\r\n");
      out.write("\t\t\t\t} else // Internet Explorer\r\n");
      out.write("\t\t\t\t{\r\n");
      out.write("\t\t\t\t\txmlDoc = new ActiveXObject(\"Microsoft.XMLDOM\");\r\n");
      out.write("\t\t\t\t\txmlDoc.async = false;\r\n");
      out.write("\t\t\t\t\txmlDoc.loadXML(txt);\r\n");
      out.write("\t\t\t\t}\r\n");
      out.write("\t\t\t\tdocument.getElementById(\"1\").innerHTML = xmlDoc.getElementsByTagName(\"signguCd\")[0].childNodes[0].nodeValue;\r\n");
      out.write("\t\t\t\tdocument.getElementById(\"2\").innerHTML = xmlDoc.getElementsByTagName(\"signguCd\")[1].childNodes[0].nodeValue;\r\n");
      out.write("\t\t\t\tdocument.getElementById(\"3\").innerHTML = xmlDoc.getElementsByTagName(\"signguCd\")[2].childNodes[0].nodeValue;\r\n");
      out.write("\t\t\t\t\r\n");
      out.write("\t\t\t\t\t\t\t\r\n");
      out.write("\t\t\t\t// url을 보내서 xml 파일을 받아오고 xmlDoc(객체) 형식으로 save 했다고 치고..... \r\n");
      out.write("\t\t\t\t\r\n");
      out.write("\t\t\t\t\r\n");
      out.write("\t\t\t}\r\n");
      out.write("\t\t\telse{\r\n");
      out.write("\t\t\t\t\r\n");
      out.write("\t\t\t\t\r\n");
      out.write("\t\t\t}\r\n");
      out.write("\t\t});\r\n");
      out.write("\t});\r\n");
      out.write("\r\n");
      out.write("\tfunction viewPost() {\r\n");
      out.write("\t\ttxt = \"<LotNumberListResponse>\"\r\n");
      out.write("\t\t\t+\"<cmmMsgHeader>\"\r\n");
      out.write("\t\t\t+\"<requestMsgId/>\"\r\n");
      out.write("\t\t\t+\"<responseMsgId/>\"\r\n");
      out.write("\t\t\t+\"<responseTime>20150406:153023290</responseTime>\"\r\n");
      out.write("\t\t\t+\"<successYN>Y</successYN>\"\r\n");
      out.write("\t\t\t+\"<returnCode>00</returnCode>\"\r\n");
      out.write("\t\t\t+\"<errMsg/>\"\r\n");
      out.write("\t\t\t+\"</cmmMsgHeader>\"\r\n");
      out.write("\t\t\t+\"<lotNumberList>\"\r\n");
      out.write("\t\t\t+\"<no>1</no>\"\r\n");
      out.write("\t\t\t+\"<zipNo>137-061</zipNo>\"\r\n");
      out.write("\t\t\t+\"<adres>서울 서초구 방배1동</adres>\"\r\n");
      out.write("\t\t\t+\"</lotNumberList>\"\r\n");
      out.write("\t\t\t+\"<lotNumberList>\"\r\n");
      out.write("\t\t\t+\"<no>2</no>\"\r\n");
      out.write("\t\t\t+\"<zipNo>137-716</zipNo>\"\r\n");
      out.write("\t\t\t+\"<adres>서울 서초구 방배1동 외환은행방배사옥 935-34</adres>\"\r\n");
      out.write("\t\t\t+\"</lotNumberList>\"\r\n");
      out.write("\t\t\t+\"<lotNumberList>\"\r\n");
      out.write("\t\t\t+\"<no>3</no>\"\r\n");
      out.write("\t\t\t+\"<zipNo>137-815</zipNo>\"\r\n");
      out.write("\t\t\t+\"<adres>서울 서초구 방배1동 100~199</adres>\"\r\n");
      out.write("\t\t\t+\"</lotNumberList>\"\r\n");
      out.write("\t\t\t+\"<lotNumberList>\"\r\n");
      out.write("\t\t\t+\"<no>4</no>\"\r\n");
      out.write("\t\t\t+\"<zipNo>137-816</zipNo>\"\r\n");
      out.write("\t\t\t+\"<adres>서울 서초구 방배1동 300~399</adres>\"\r\n");
      out.write("\t\t\t+\"</lotNumberList>\"\r\n");
      out.write("\t\t\t+\"<lotNumberList>\"\r\n");
      out.write("\t\t\t+\"<no>5</no>\"\r\n");
      out.write("\t\t\t+\"<zipNo>137-841</zipNo>\"\r\n");
      out.write("\t\t\t+\"<adres>서울 서초구 방배1동 883~901</adres>\"\r\n");
      out.write("\t\t\t+\"</lotNumberList>\"\r\n");
      out.write("\t\t\t+\"<lotNumberList>\"\r\n");
      out.write("\t\t\t+\"<no>6</no>\"\r\n");
      out.write("\t\t\t+\"<zipNo>137-842</zipNo>\"\r\n");
      out.write("\t\t\t+\"<adres>서울 서초구 방배1동 902~913</adres>\"\r\n");
      out.write("\t\t\t+\"</lotNumberList>\"\r\n");
      out.write("\t\t\t+\"<lotNumberList>\"\r\n");
      out.write("\t\t\t+\"<no>7</no>\"\r\n");
      out.write("\t\t\t+\"<zipNo>137-843</zipNo>\"\r\n");
      out.write("\t\t\t+\"<adres>서울 서초구 방배1동 914~928</adres>\"\r\n");
      out.write("\t\t\t+\"</lotNumberList>\"\r\n");
      out.write("\t\t\t+\"<lotNumberList>\"\r\n");
      out.write("\t\t\t+\"<no>8</no>\"\r\n");
      out.write("\t\t\t+\"<zipNo>137-844</zipNo>\"\r\n");
      out.write("\t\t\t+\"<adres>서울 서초구 방배1동 929~941</adres>\"\r\n");
      out.write("\t\t\t+\"</lotNumberList>\"\r\n");
      out.write("\t\t\t+\"<lotNumberList>\"\r\n");
      out.write("\t\t\t+\"<no>9</no>\"\r\n");
      out.write("\t\t\t+\"<zipNo>137-062</zipNo>\"\r\n");
      out.write("\t\t\t+\"<adres>서울 서초구 방배2동</adres>\"\r\n");
      out.write("\t\t\t+\"</lotNumberList>\"\r\n");
      out.write("\t\t\t+\"<lotNumberList>\"\r\n");
      out.write("\t\t\t+\"<no>10</no>\"\r\n");
      out.write("\t\t\t+\"<zipNo>137-709</zipNo>\"\r\n");
      out.write("\t\t\t+\"<adres>서울 서초구 방배2동 서울방배경찰서 455-10</adres>\"\r\n");
      out.write("\t\t\t+\"</lotNumberList>\"\r\n");
      out.write("\t\t\t+\"<lotNumberList>\"\r\n");
      out.write("\t\t\t+\"<no>11</no>\"\r\n");
      out.write("\t\t\t+\"<zipNo>137-710</zipNo>\"\r\n");
      out.write("\t\t\t+\"<adres>서울 서초구 방배2동 농수축산신보빌딩 446-2</adres>\"\r\n");
      out.write("\t\t\t+\"</lotNumberList>\"\r\n");
      out.write("\t\t\t+\"</LotNumberListResponse>\";\r\n");
      out.write("\r\n");
      out.write("\t\tif (window.DOMParser) {\r\n");
      out.write("\t\t\tparser = new DOMParser();\r\n");
      out.write("\t\t\txmlDoc = parser.parseFromString(txt, \"text/xml\");\r\n");
      out.write("\t\t} else // Internet Explorer\r\n");
      out.write("\t\t{\r\n");
      out.write("\t\t\txmlDoc = new ActiveXObject(\"Microsoft.XMLDOM\");\r\n");
      out.write("\t\t\txmlDoc.async = false;\r\n");
      out.write("\t\t\txmlDoc.loadXML(txt);\r\n");
      out.write("\t\t}\r\n");
      out.write("\t\t\r\n");
      out.write("\t\tvar size =xmlDoc.getElementsByTagName(\"adres\").length;\r\n");
      out.write("\t\tfor(var i=0; i<size; i++ ){\r\n");
      out.write("\t\tdocument.getElementById(\"zipNo\"+i).innerHTML = xmlDoc.getElementsByTagName(\"zipNo\")[i].childNodes[0].nodeValue;\r\n");
      out.write("\t\tdocument.getElementById(\"adres\"+i).innerHTML = xmlDoc.getElementsByTagName(\"adres\")[i].childNodes[0].nodeValue;\r\n");
      out.write("\t\t}\r\n");
      out.write("\t\t\r\n");
      out.write("\t\t//=====data======\r\n");
      out.write("\t\t//data update\r\n");
      out.write("\t\tvar saleLineItem = {post: xmlDoc.getElementsByTagName(\"zipNo\").childNodes[0].nodeValue, address: xmlDoc.getElementsByTagName(\"adres\")[0].childNodes[0].nodeValue };\r\n");
      out.write("\t\t//====view=======\r\n");
      out.write("\t\t//view update    \t\r\n");
      out.write("\t\t$(\"#total\").text(sale.total);\r\n");
      out.write("\t\tvar lineItemRow = $(\"<div class='lineItemRow'>\"\r\n");
      out.write("\t\t\t \t\t +\"<div class='post'></div>\"\r\n");
      out.write("\t\t\t \t\t +\"<div class='address'></div></div>\");\r\n");
      out.write("\t\t\r\n");
      out.write("\t\t$(\"#LineItemTable\").append(lineItemRow);\r\n");
      out.write("\t\tlineItemRow[0].__data__ = saleLineItem;\r\n");
      out.write("\t\tupdateSaleLineItemAndTotal(saleLineItem,lineItemRow);\r\n");
      out.write("\t\t//clear item inputs view\r\n");
      out.write("\t\t$(\"#no\").val(\"\");\r\n");
      out.write("\t\t$(\"#name\").val(\"\");\r\n");
      out.write("\t\t$(\"#price\").val(\"\");\r\n");
      out.write("\t\t$(\"#count\").val(\"\");\r\n");
      out.write("\t\tsearchedItem = null;\r\n");
      out.write("\t\t\r\n");
      out.write("\t}\r\n");
      out.write("</script>\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("\t<h1>우편조회</h1>\r\n");
      out.write("\r\n");
      out.write("\t<select name=\"city\" id=\"city\">\r\n");
      out.write("\t\t<option value=\"\">시도</option>\r\n");
      out.write("\t\t<option value=\"서울\" >서울특별시</option>\r\n");
      out.write("\t\t<option value=\"부산\">부산광역시</option>\r\n");
      out.write("\t\t<option value=\"대구\">대구광역시</option>\r\n");
      out.write("\t\t<option value=\"인천\">인천광역시</option>\r\n");
      out.write("\t\t<option value=\"광주\">광주광역시</option>\r\n");
      out.write("\t\t<option value=\"대전\">대전광역시</option>\r\n");
      out.write("\t\t<option value=\"울산\">울산광역시</option>\r\n");
      out.write("\t</select>\r\n");
      out.write("\r\n");
      out.write("\t<select name=\"town\" disabled>\r\n");
      out.write("\t\t<option value=\"\">시군구</option>\r\n");
      out.write("\t\t<option id=\"1\" >서초구</option>\r\n");
      out.write("\t\t<option id=\"2\">서초구</option>\r\n");
      out.write("\t\t<option id=\"3\">서초구</option>\r\n");
      out.write("\t\t<option id=\"4\">서초구</option>\r\n");
      out.write("\t</select>\r\n");
      out.write("\t<input type=\"text\" id=\"dong\" placeholder=\"동명, 건물명,우편번호\" name=\"dong\">\r\n");
      out.write("\r\n");
      out.write("\t<input type=\"button\" value='검색' onclick='viewPost()' />\r\n");
      out.write("\r\n");
      out.write("\t<div id=\"LineItemTable\">   \r\n");
      out.write("    \t<div class=\"Heading\">\r\n");
      out.write("    \t    <div class=\"post\">\r\n");
      out.write("        \t    <p>우편번호</p>\r\n");
      out.write("        \t</div>\r\n");
      out.write("        \r\n");
      out.write("        \t<div class=\"address\">\r\n");
      out.write("            \t<p>주소</p>\r\n");
      out.write("        \t</div>\r\n");
      out.write("    \t</div>\r\n");
      out.write("\t</div>\r\n");
      out.write("\t\r\n");
      out.write("\t</body>\r\n");
      out.write("</html>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try {
            if (response.isCommitted()) {
              out.flush();
            } else {
              out.clearBuffer();
            }
          } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
