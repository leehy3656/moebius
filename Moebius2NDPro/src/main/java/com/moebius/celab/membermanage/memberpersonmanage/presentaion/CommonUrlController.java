package com.moebius.celab.membermanage.memberpersonmanage.presentaion;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

@Controller
public class CommonUrlController {
	
	@RequestMapping(value="zipCode.do")
	@ResponseBody
	public List<Map<String, Object>> searchZipCode(
			@RequestParam(value="dong", required=true) String dong) throws Exception {
		System.out.println("dong :"+dong);
		List<Map<String, Object>> zipCodeList = new ArrayList<Map<String, Object>>();
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document document;
        document = db.parse(URLDecoder.decode("http://openapi.epost.go.kr/postal/retrieveLotNumberAdressService/retrieveLotNumberAdressService/getDetailList?searchSe=dong&srchwrd="+
        			dong+"&encoding=utf-8&serviceKey=UpjUjNujbPpbvvpzxXAhXQbjVgOpbAPwA%2FsmvpMqwncB9sgiF7SfOAEsPRNptCQw1QRWLkylX3eNAzklVhpU9Q%3D%3D","utf-8"));
        NodeList list = document.getElementsByTagName("detailList");
        
        System.out.println(list.getLength());
        XPathFactory xpf = XPathFactory.newInstance();
        XPath xp = xpf.newXPath();
        
        for( int i=1; i<=list.getLength(); i++ ) {
        	Map<String, Object> map = new HashMap<String, Object>();
        	String zipNo = xp.evaluate("//detailList[position()="+i+"]/zipNo/text()", document.getDocumentElement());
        	String address = xp.evaluate("//detailList[position()="+i+"]/adres/text()", document.getDocumentElement());
        	map.put(zipNo, address.trim());
        	zipCodeList.add(map);
        }
		
		return zipCodeList;
		
	}
}
