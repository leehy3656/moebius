package com.moebius.celab.membermanage.memberpersonmanage.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.moebius.celab.shoppingmanage.business.OutputManager;
import com.moebius.celab.shoppingmanage.common.Output;
import com.moebius.celab.volunteermanage.business.VolunteerManager2;
import com.moebius.celab.volunteermanage.common.Volunteer;



@Service
public class MemberdataloadManager {
	@Autowired
	VolunteerManager2 volunteerManager2;
	@Autowired
	OutputManager outputManager;
	@Transactional
	public List<Volunteer> Wload(int idno ){
		
	 return volunteerManager2.Wload(idno);
	}
	@Transactional
	public List<Output> Pload(int idno ){
		
	 return outputManager.Pload(idno);
	}
	@Transactional
	public List<Volunteer> Rload(int idno ){
		
	 return volunteerManager2.Rload(idno);
	}
}
