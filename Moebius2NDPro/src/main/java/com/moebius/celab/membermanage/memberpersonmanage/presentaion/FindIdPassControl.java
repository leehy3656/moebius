package com.moebius.celab.membermanage.memberpersonmanage.presentaion;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.moebius.celab.membermanage.memberpersonmanage.business.MemberManager;
import com.moebius.celab.membermanage.memberpersonmanage.common.Member;

@Controller
public class FindIdPassControl {

	@Autowired
	MemberManager memberManager;

	@Autowired
	private JavaMailSender mailSender;

	@RequestMapping(value = "FindId.do", method = RequestMethod.GET)
	public ModelAndView findId(@RequestParam("name") String name,
			@RequestParam("email") String email) {

		ModelAndView mv = new ModelAndView();
		Member findMember = new Member();
		findMember = memberManager.memberSelectNameEmail(name, email);
		
		mv.addObject("findIdMember",findMember);
		mv.setViewName("/Member/findIdResult.jsp");
		
		
		return mv;
	}

	@RequestMapping(value = "FindPass.do", method = RequestMethod.GET)
	public ModelAndView FindPass(
			@RequestParam("id") String id,
			@RequestParam("name") String name,
			@RequestParam("email") String email) {

		ModelAndView mv = new ModelAndView();
		Member findMember = new Member();
		findMember = memberManager.memberSelectIdNameEmail(id,name, email);
		
		mv.addObject("findPassMember",findMember);
		mv.setViewName("/Member/findPassResult.jsp");
		
		
		return mv;
	}

	@RequestMapping(value = "receiveIdCnumber.do", method = RequestMethod.POST)
	public @ResponseBody String receiveIdCnumber(
			@RequestParam("name") String name,
			@RequestParam("email") String email) {

		int intCnumber = (int) (Math.random() * 1000000);
		Member member;
		member = memberManager.memberSelectNameEmail(name, email);

		if (member == null) {
			return "null";
		}

		else {

			try {
				String from = "so123os89@gmail.com";
				String subject = "아이디 찾기:";
				String cNumber = "인증번호:";

				subject += name + email;
				cNumber += intCnumber;
				System.out.println("cNumber:"+ cNumber);

				MimeMessage message = mailSender.createMimeMessage();

				MimeMessageHelper messageHelper = new MimeMessageHelper(
						message, true, "UTF-8");

				messageHelper.setTo(email);
				messageHelper.setText(cNumber);
				messageHelper.setFrom(from);
				messageHelper.setSubject(subject); // 메일제목은 생략이 가능하다

				mailSender.send(message);

			} catch (Exception e) {
				System.out.println(e);
			}

			return String.valueOf(intCnumber);
		}

	}
	
	
	@RequestMapping(value = "receivePassCnumber.do", method = RequestMethod.POST)
	public @ResponseBody String receivePassCnumber(
			@RequestParam("id") String id,
			@RequestParam("name") String name,
			@RequestParam("email") String email,
			HttpServletRequest request) {
		int intCnumber = (int) (Math.random() * 1000000);
		Member member;
		member = memberManager.memberSelectIdNameEmail(id,name, email);
		request.getSession().setAttribute("member", member);

		if (member == null) {
			return "null";
		}

		else {

			try {
				String from = "so123os89@gmail.com";
				String subject = "아이디 찾기:";
				String cNumber = "인증번호:";

				subject += name + email;
				cNumber += intCnumber;
				System.out.println("cNumber:"+ cNumber);

				MimeMessage message = mailSender.createMimeMessage();

				MimeMessageHelper messageHelper = new MimeMessageHelper(
						message, true, "UTF-8");

				messageHelper.setTo(email);
				messageHelper.setText(cNumber);
				messageHelper.setFrom(from);
				messageHelper.setSubject(subject); // 메일제목은 생략이 가능하다

				mailSender.send(message);

			} catch (Exception e) {
				System.out.println(e);
			}

			return String.valueOf(intCnumber);
		}

	}
	
	

}
