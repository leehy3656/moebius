package com.moebius.celab.membermanage.memberpersonmanage.dataservice;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import com.moebius.celab.membermanage.memberpersonmanage.common.Member;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class MemberDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public MemberDAO() {
	}

	public Member selectSignupID(String idname) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Member.class);
		criteria.add(Restrictions.like("idname", idname));
		List<Member> memberList = criteria.list();

		if (memberList.size() == 0) {
			return null;
		} else {
			return memberList.get(0);
		}

	}
	
	public Member selectEmail(String email){
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Member.class);
		criteria.add(Restrictions.eq("email",email)).add(Restrictions.eq("rank", 1));
		List<Member> memberList = criteria.list();
		return memberList.get(0);
	}

	public List<Member> selectID(String idname) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Member.class);
		criteria.add(Restrictions.like("idname", "%" + idname + "%")).add(
				Restrictions.eq("rank", 1));
		List<Member> memberList = criteria.list();
		HashSet al = new HashSet(memberList);
		ArrayList<Member> relist = new ArrayList<Member>(al);
		return relist;
	}

	public List<Member> selectName(String name) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Member.class);
		criteria.add(Restrictions.like("name", "%" + name + "%")).add(
				Restrictions.eq("rank", 1));
		List<Member> memberlist = criteria.list();

		HashSet al = new HashSet(memberlist);

		ArrayList<Member> relist = new ArrayList<Member>(al);
		return relist;
	}
	
	public Member findId(Member member){
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Member.class);
		criteria.add(Restrictions.eq("name",member.getName() )).add(
				Restrictions.eq("email", member.getEmail() )).add(Restrictions.eq("rank",1));
		List<Member> memberList = criteria.list();
		return memberList.get(0);
	}
	
	public Member findPassword(Member member){
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Member.class);
		criteria.add(Restrictions.eq("name", member.getName() ))
		.add(Restrictions.eq("idname", member.getIdname() ))
		.add(Restrictions.eq("email", member.getEmail() ))
		.add(Restrictions.eq("phoneno", member.getPhoneno() ))
		.add(Restrictions.eq("rank",1));
		List<Member> memberList = criteria.list();
		return memberList.get(0);
	}
	

	

	public int save(Member member) {
		Session session = sessionFactory.getCurrentSession();
		int no =(Integer) session.save(member);
		
		return no;
	}

	public int idselect(String str) {
		Session session = sessionFactory.getCurrentSession();

		Criteria criteria = session.createCriteria(Member.class);
		criteria.add(Restrictions.like("idname", str, MatchMode.EXACT));

		List<Member> members = criteria.list();

		if (members.size() == 0) {
			return 1;
		}

		return 2;

	}

	public void update(Member member) {
		Session session = sessionFactory.getCurrentSession();
		session.update(member);
	}

	public int delete(int idno) {
		Session session = sessionFactory.getCurrentSession();
		Member member = (Member) session.get(Member.class,
				new Integer(idno));
		session.delete(member);
		return 1;
	}

	public Member select(int idno) {
		Session session = sessionFactory.getCurrentSession();
		// System.out.println(idno);
		Member member = (Member) session.get(Member.class,
				new Integer(idno));
		// System.out.println(member.getName());
		return member;
	}

	/*
	 * public int MemberPointplus(int idno , int point ) { //����Ʈ ���� Session
	 * session = sessionFactory.getCurrentSession(); MemberPerson member =
	 * (MemberPerson)session.get(MemberPerson.class, new Integer(idno)); int
	 * pointno=member.getPoint(); member.setPoint(pointno+point);
	 * session.update(member);
	 * 
	 * return 2;
	 * 
	 * } public int MemberPointminus(int idno , int point ) { //����Ʈ ����
	 * Session session = sessionFactory.getCurrentSession(); MemberPerson member
	 * = (MemberPerson)session.get(MemberPerson.class, new Integer(idno)); int
	 * pointno=member.getPoint(); if(pointno > point){
	 * member.setPoint(pointno-point); session.update(member); }else{ return 1;
	 * } return 2; }
	 */
	public Member login(String idname) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Member.class);
		criteria.add(Restrictions.eq("idname", idname));
		// criteria.setProjection(Projections.distinct(Projections.property("no")));
		System.out.println("idname:"+idname);
		
		List<Member> members = criteria.list();
		
		/* 찾는 아이디가 없는 경우 null 값 return */
		if (members.size() == 0) {
			return null;
		}
		else{
			return members.get(0);
		}
		
	}
	
	public Member memberSelectNameEmail(String name, String email){
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Member.class);
		
		criteria.add(Restrictions.eq("name", name))
		.add(Restrictions.eq("email", email))
		.add(Restrictions.eq("rank",1));
		
		List<Member> memberList = criteria.list();
		
		if(memberList.size() == 0){
			return null;
		}
		else{
			return memberList.get(0);
		}
	}
	
	
	public Member memberSelectIdNameEmail(String id, String name, String email){
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Member.class);
		
		criteria.add(Restrictions.eq("idname", id))
		.add(Restrictions.eq("name", name))
		.add(Restrictions.eq("email", email))
		.add(Restrictions.eq("rank",1));
		
		List<Member> memberList = criteria.list();
		
		if(memberList.size() == 0){
			return null;
		}
		else{
			return memberList.get(0);
		}
	}
	
	public List<Member> memberSelectAbilitys(List<Integer> abilityNos) {

		String strAbilityNos = "";
		for (int abilityNo : abilityNos) {
			if (strAbilityNos.length() > 0) {
				strAbilityNos += ",";
			}
			strAbilityNos += String.valueOf(abilityNo);
		}

		Session session = sessionFactory.getCurrentSession();
		return session
				.createSQLQuery(
						"select distinct member.no,idname,pw,name,sex,birthDate,email,phoneno"
						+ ",rank,point,detailAddress,profileImage,Date,post,address "
						+ "From member inner join ability_member on member.no = ability_member.member_no "
						+ "where ability_member.ability_no in ("+ strAbilityNos + ")" )
				.addEntity(Member.class).list();

	}

	

}