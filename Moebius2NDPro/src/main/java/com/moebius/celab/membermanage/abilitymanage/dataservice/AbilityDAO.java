package com.moebius.celab.membermanage.abilitymanage.dataservice;

import java.util.List;



import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import com.moebius.celab.membermanage.abilitymanage.common.Ability;

@Repository
public class AbilityDAO {
	@Autowired
	private SessionFactory sessionFactory = null;
	
	
	public AbilityDAO() {
    }
	
	
	
	
	public List<Ability> selectAll(){
		Session session = sessionFactory.getCurrentSession();
		//System.out.println(1);
		List<Ability> abilitya =(List<Ability>) session.createCriteria(Ability.class).list(); 
		
		//System.out.println(abilitya.get(0).getAbilityName());
		
		return abilitya;
	}
	public List<Ability> selectByNo(){
		Session session = sessionFactory.getCurrentSession();
		
		return null;
	}
	public Ability select(int no){
		Session session = sessionFactory.getCurrentSession();
		
		Ability ability=(Ability)session.get(Ability.class, new Integer(no));
		
		return ability;
	}
	
	
	
	
}
