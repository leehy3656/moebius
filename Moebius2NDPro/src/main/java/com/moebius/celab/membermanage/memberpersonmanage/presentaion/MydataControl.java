package com.moebius.celab.membermanage.memberpersonmanage.presentaion;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import net.sf.json.*;
import net.sf.json.util.CycleDetectionStrategy;

import com.moebius.celab.membermanage.memberpersonmanage.business.MemberdataloadManager;
import com.moebius.celab.membermanage.memberpersonmanage.common.Member;
import com.moebius.celab.shoppingmanage.common.Output;
import com.moebius.celab.shoppingmanage.common.Shopping;
import com.moebius.celab.volunteermanage.common.Volunteer;



@Controller
public class MydataControl {
	@Autowired
	MemberdataloadManager memberdataloadManager;
	
	@RequestMapping(value = "Wload.do")
	public @ResponseBody JSONObject loadW(@RequestParam(value = "listno", defaultValue = "1") int listno,HttpServletRequest request) {
		
/*		JsonConfig config = new JsonConfig();
		config.setExcludes(new String[] {});
		config.setIgnoreDefaultExcludes(false);
		config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);*/
		
		
		JSONObject ob=new JSONObject();
		
		Member member=(Member) request.getSession().getAttribute("member");
		
		
		List<Volunteer> vs=memberdataloadManager.Wload(member.getIdno());
		int listsize=10;
		
		double totallistno = Math.ceil(vs.size()/(double)listsize);
		
		//JSONObject ob=JSONObject.fromObject(vs, config);
		
		ob.put("memberW", vs);
		ob.put("listno",listno);
		ob.put("totalno",totallistno);
		ob.put("listsize", listsize);
		
		
		
		return ob;
	}
	@RequestMapping(value = "Pload.do")
	public @ResponseBody JSONObject Pload(@RequestParam(value = "listno", defaultValue = "1") int listno,HttpServletRequest request) {




		JSONObject o=new JSONObject();
		

		Member member=(Member) request.getSession().getAttribute("member");
		
		
		List<Output> vs=memberdataloadManager.Pload(member.getIdno());
		int listsize=10;

		double totallistno = Math.ceil(vs.size()/(double)listsize);
		
		List<Integer> shoppingNo=new ArrayList<Integer>();
		List<Integer> quantityNo=new ArrayList<Integer>();
		List<String> shoppingName=new ArrayList<String>();
		List<String> buyitemDate=new ArrayList<String>();
		
		SimpleDateFormat sb=new SimpleDateFormat("YYYY-MM-dd");
		String datestr;
		for(Output out:vs){
			
			shoppingNo.add(out.getShopping().getNo());
			quantityNo.add(out.getQuantity());
			shoppingName.add(out.getShopping().getName());
			datestr=sb.format(out.getDate());
			buyitemDate.add(datestr);
		}
		
		
		
		
		/*ob.put("memberP", vs);
		ob.put("listnoP",listno);
		ob.put("totalnoP",totallistno);
		ob.put("listsizeP", listsize);*/

		//List<Object> o= new ArrayList<Object>();
		o.put("shoppingNo",shoppingNo);
		o.put("quantityNo",quantityNo);
		o.put("shoppingName",shoppingName);
		o.put("buyitemDate",buyitemDate);
		o.put("listno",listno);
		o.put("totalno",totallistno);
		o.put("listsize",listsize);
		return o;
	}
	@RequestMapping(value = "Rload.do")
	public @ResponseBody JSONObject loadR(@RequestParam(value = "listno", defaultValue = "1") int listno,HttpServletRequest request) {
		
/*		JsonConfig config = new JsonConfig();
		config.setExcludes(new String[] {});
		config.setIgnoreDefaultExcludes(false);
		config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);*/
		
		
		JSONObject ob=new JSONObject();
		
		Member member=(Member) request.getSession().getAttribute("member");
		
		
		List<Volunteer> vs=memberdataloadManager.Rload(member.getIdno());
		
		int listsize=10;
		
		double totallistno = Math.ceil(vs.size()/(double)listsize);
		
		
		
		
		
		
		
		//JSONObject ob=JSONObject.fromObject(vs, config);
		
		ob.put("memberW", vs);
		ob.put("listno",listno);
		ob.put("totalno",totallistno);
		ob.put("listsize", listsize);
		
		
		
		return ob;
	}

}
