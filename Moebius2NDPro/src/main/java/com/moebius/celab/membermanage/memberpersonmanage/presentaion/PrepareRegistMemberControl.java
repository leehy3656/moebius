package com.moebius.celab.membermanage.memberpersonmanage.presentaion;

import java.util.ArrayList;
import java.util.List;

import com.moebius.celab.membermanage.abilitymanage.business.AbilityManager;
import com.moebius.celab.membermanage.abilitymanage.common.Ability;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PrepareRegistMemberControl {
	
	@Autowired
	AbilityManager abilityManager;
	
	
	@RequestMapping("PrepareRegistMember.do")
	public ModelAndView prepareRegistMember(){
		List<Ability> abilitys = null;
		List<Ability> abilitys1 = null;
		List<Ability> abilitys2 = null;
		List<Ability> abilitys3 = null;
		
		if(abilitys == null){
			abilitys = new ArrayList<Ability>();
		}
		
		if(abilitys1 == null){
			abilitys1 = new ArrayList<Ability>();
		}
		
		if(abilitys2 == null){
			abilitys2 = new ArrayList<Ability>();
		}
		
		if(abilitys3 == null){
			abilitys3 = new ArrayList<Ability>();
		}
		
		abilitys = abilityManager.selectAll();
		
		for(int i=0; i<abilitys.size(); i++){
			
			switch(i%3){
			
			case 0:
				abilitys1.add(abilitys.get(i));
				break;

			
			case 1:
				abilitys2.add(abilitys.get(i));
				break;
				
			case 2:
				abilitys3.add(abilitys.get(i));
				break;
			}
			
		}
		
		ModelAndView mv = new ModelAndView();
		
		mv.addObject("abilitys1", abilitys1);
		mv.addObject("abilitys2", abilitys2);
		mv.addObject("abilitys3", abilitys3);
		mv.setViewName("/Member/registmember.jsp");
		
		return mv;
		
	}

}
