package com.moebius.celab.membermanage.memberpersonmanage.common;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.moebius.celab.membermanage.abilitymanage.common.Ability;


@Entity(name = "com.moebius.celab.membermanage.memberpersonmanage.common")
@Table(name = "Member")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class Member {

	public Member() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "no")
	private int idno;
	
	@Generated(GenerationTime.ALWAYS)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date", insertable = false, updatable = false)
	private Date date;

	

	@Column(name = "idname")
	private String idname;

	@Column(name = "pw")
	private String password;

	@Column(name = "name")
	private String name;

	@Column(name = "sex")
	private int sex;

	@Column(name = "birthDate")
	private String birthDate;

	@Column(name = "email")
	private String email;

	@Column(name = "phoneno")
	private String phoneno;

	@Column(name = "rank")
	private int rank = 1;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "ABILITY_MEMBER", joinColumns = { @JoinColumn(name = "MEMBER_no") }, inverseJoinColumns = { @JoinColumn(name = "Ability_no") })
	private Set<Ability> abilitys = new HashSet<Ability>();
	
	@Column(name="point")
    private int point=0;
	
	@Column(name = "post")
	private String post;
	
	@Column(name = "address")
	private String address;

	@Column(name="detailAddress")
	private String detailAddress;
	
	@Column(name="profileImage")
	private String profileImage;
	

	/*@OneToMany(mappedBy="member",fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	private Set<Output> output;
	
	
	public Set<Output> getOutput() {
		return output;
	}

	public void setOutput(Set<Output> output) {
		this.output = output;
	}
*/
	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}

	public int getIdno() {
		return idno;
	}

	public void setIdno(int idno) {
		this.idno = idno;
	}

	public String getIdname() {
		return idname;
	}

	public void setIdname(String idname) {
		this.idname = idname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneno() {
		return phoneno;
	}

	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}
	
	public Set<Ability> getAbilitys() {
		return abilitys;
	}

	public void setAbilitys(Set<Ability> abilitys) {
		this.abilitys = abilitys;
	}
	
	public int getPoint() {
		return point;
	}

	public void setPoint(int point) {
		this.point = point;
	}
	
	public String getPost() {
		return post;
	}

	public void setPost(String post) {
		this.post = post;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDetailAddress() {
		return detailAddress;
	}

	public void setDetailAddress(String detailAddress) {
		this.detailAddress = detailAddress;
	}

	public String getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}
	
	public void addAbility(Ability ability){
		//바구니 없으면 만들고 
		if(abilitys == null){
			abilitys = new HashSet<Ability>();
		}
		
		//바구니에 넣어
		abilitys.add(ability);
	}

}