package com.moebius.celab.membermanage.memberpersonmanage.presentaion;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.moebius.celab.membermanage.memberpersonmanage.business.MemberManager;
import com.moebius.celab.membermanage.memberpersonmanage.common.Member;
import com.moebius.celab.messagemanager.business.MessageManager;
import com.moebius.celab.messagemanager.common.Message;

@Controller
public class MemberLoginControl {

	@Autowired
	MemberManager memberManager;
	@Autowired
	MessageManager messageManager;

	@RequestMapping(value = "main.do")
	public ModelAndView main() {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("redirect:/MainMoebius.do");
		return mv;
	}

	@RequestMapping(value = "logout.do")
	public ModelAndView logout(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		request.getSession().removeAttribute("member");
		request.getSession().removeAttribute("memberPass");
		mv.setViewName("redirect:/MainMoebius.do");
		return mv;
	}

	@RequestMapping("idPassCheck.do")
	@ResponseBody
	String idPassCheck(@RequestParam("id") String id,
			@RequestParam("password") String password,
			HttpServletRequest request) {

		String textID = id.trim();
		String textPass = password.trim();
		Member member = memberManager.memberLogin(textID, textPass);

		/* 아이디나 비번이 공란일 경우 */
		if (textID.length() == 0 || textPass.length() == 0) {
			return "fail";
		}
		/* id와 pass에 맞는 회원이 없을 경우 */
		else if (member == null) {
			return "fail";
		} else {
			// 회원이 제재 되었을 경우
			int rankno = memberManager.MemberSelect(member.getIdno()).getRank();

			if (rankno == 2||rankno==3) {
				return "fail1";
			} else {
				return "success";
			}

		}
	}

	@RequestMapping("login.do")
	ModelAndView checkMember(@RequestParam("id") String id,
			@RequestParam("password") String password,
			HttpServletRequest request) {

		ModelAndView mv = new ModelAndView();

		String textID = id.trim();
		String textPass = password.trim();

		Member member = memberManager.memberLogin(textID, textPass);
		request.getSession().setAttribute("memberPass", member.getPassword());
		request.getSession().setAttribute("member", member);
		
		mv.addObject("member", member);
		mv.addObject("status", "success");
		
		mv.setViewName("redirect:/MainMoebius.do");
		return mv;
	}
}
