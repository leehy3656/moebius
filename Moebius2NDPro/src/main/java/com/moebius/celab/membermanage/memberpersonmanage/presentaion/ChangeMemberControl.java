package com.moebius.celab.membermanage.memberpersonmanage.presentaion;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.moebius.celab.membermanage.memberpersonmanage.business.MemberManager;
import com.moebius.celab.membermanage.memberpersonmanage.common.Member;
import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

@Controller
public class ChangeMemberControl {
	@Autowired
	MemberManager memberManager;

	String errorMessage;
	boolean signupCheck = true;
	String uploadFile = "default.png";
	
	@RequestMapping(value = "ChangeMember.do", method=RequestMethod.POST)
	public @ResponseBody String changeMember(@RequestBody Member member, HttpServletRequest request) throws UnsupportedEncodingException{
		
		Member beforeMember = new Member();
		
		// Password
		String password = member.getPassword();
		boolean pwSpaceCheck = Pattern.matches("[^\\p{Space}]+", password);

		// Name
		String name = member.getName();
		boolean nameCheck = Pattern.matches("^[가-힣a-zA-Z]*$", name);

		// Sex
		int sex = member.getSex();

		// Date
		String birthDate = member.getBirthDate();

		// email
		String email = member.getEmail();
		boolean emailCheck = Pattern.matches(
				"^[_a-zA-Z0-9-\\.]+@[\\.a-zA-Z0-9-]+\\.[a-zA-Z]+$", email);
		// address
		String post = member.getPost();
		String address = member.getAddress();
		String detailAddress = member.getDetailAddress();

		// PhoneNumber
		String phoneNo = member.getPhoneno();
		boolean phoneNoCheck = Pattern.matches("^[0-9]*$", phoneNo);
		// /////////////////////////////////////////////////////////////////////////////////////////////
		// 비밀 번호 예외 처리
		
		// /////////////////////////////////////////////////////////////////////////////////////////////
		// 비밀 번호 예외 처리
		if (password.length() == 0) {
			System.out.println("비밀번호를 입력해주세요.");
			signupCheck = false;
			errorMessage = "비밀번호를 입력해주세요.";
		}

		else if (!pwSpaceCheck) {
			System.out.println("비밀번호에 공백이 있습니다.");
			signupCheck = false;
			errorMessage = "비밀번호에 공백이 있습니다.";
		} else if (password.length() < 6 || password.length() > 16) {
			System.out.println("PASSWORD는 6~16 자 사이만 가능합니다.");
			signupCheck = false;
			errorMessage = "PASSWORD는 6~16 자 사이만 가능합니다.";
		}

		// /////////////////////////////////////////////////////////////////////////////////////////////
		// 이름 예외 처리

		else if (name.length() == 0) {
			System.out.println("이름을 입력해 주세요.");
			signupCheck = false;
			errorMessage = "이름을 입력해 주세요.";
		}

		else if (!nameCheck) {
			System.out.println("이름은 한글, 영어 대소문자만 가능합니다.");
			signupCheck = false;
			errorMessage = "이름은 한글, 영어 대소문자만 가능합니다.";
		}
		
		// /////////////////////////////////////////////////////////////////////////////////////////////
		// 성별 예외 처리
		else if (sex == 0) {
			System.out.println("성별을 입력하세요.");
			signupCheck = false;
			errorMessage = "성별을 입력하세요";
		}
		// /////////////////////////////////////////////////////////////////////////////////////////////
		// 날짜 예외 처리
		else if (birthDate.length() == 0) {
			System.out.println("날짜를 입력하세요.");
			signupCheck = false;
			errorMessage = "날짜를 입력하세요";
		}
		// /////////////////////////////////////////////////////////////////////////////////////////////
		// 이메일 예외 처리
		else if (email.length() == 0) {
			System.out.println("이메일을 입력해 주세요.");
			signupCheck = false;
			errorMessage = "이메일을 입력해 주세요.";
		}

		else if (!emailCheck) {
			System.out.println("이메일 형식을 확인해 주세요.");
			signupCheck = false;
			errorMessage = "이메일 형식을 확인해 주세요.";
		}

		// ////////////////////////////////////////////////////////////////////////////////////////////
		// 주소 예외처리
		else if (post.length() == 0 || address.length() == 0
				|| detailAddress.length() == 0) {
			System.out.println("주소를 입력해 주세요.");
			signupCheck = false;
			errorMessage = "주소를 입력해주세요.";

		}
		// /////////////////////////////////////////////////////////////////////////////////////////////
		// 폰번호 예외처리

		else if (phoneNo.length() == 0) {
			System.out.println("핸드폰 번호를 입력해 주세요.");
			signupCheck = false;
			errorMessage = "핸드폰 번호를 입력해 주세요.";
		} else if (!phoneNoCheck) {
			System.out.println("핸드폰 번호는 숫자만 입력 가능합니다.");
			signupCheck = false;
			errorMessage = "핸드폰 번호는 숫자만 입력 가능합니다.";
		} 
		
		else {
			signupCheck = true;
		}

		if (signupCheck == true) {
			beforeMember = (Member) request.getSession().getAttribute("member");
			member.setIdno(beforeMember.getIdno());
			member.setIdname(beforeMember.getIdname());
			
			if(!uploadFile.equals("default.png")){
				member.setProfileImage(uploadFile);
				System.out.println(uploadFile);
			}
			uploadFile ="default.png";
			
			memberManager.memberUpdate(member);
			request.getSession().setAttribute("member", member);
			request.getSession().setAttribute("memberPass", member.getPassword());
			
			name = URLEncoder.encode(name, "UTF-8");
			return "SUCCESS" + "#" + name;
		}

		else {
			errorMessage = URLEncoder.encode(errorMessage, "UTF-8");
			return "FAIL" + "#" + errorMessage;

		}
	}
	
	@RequestMapping(value = "changeProfileImage.do", method = RequestMethod.POST)
	@ResponseBody
	public String upload(HttpServletRequest request,
	        HttpServletResponse response) {
	
    int maxSize  = 1024*1024*10;        
 
    ServletContext scontext = request.getSession().getServletContext();
	 String savefile = "images";
	 String savePath = scontext.getRealPath(savefile);
	 System.out.println("savePath :"+ savePath);
    try{
 
        MultipartRequest multi = new MultipartRequest(request, savePath, maxSize, "UTF-8", new DefaultFileRenamePolicy());
        // 파일업로드
        uploadFile = multi.getFilesystemName("profileImage");
        System.out.println("upLoadFile :"+ uploadFile);
    }catch(Exception e){
        e.printStackTrace();
    }
	    return "success";
	}
	
	@RequestMapping(value ="DropMember.do")
	public ModelAndView dropMember(HttpServletRequest request){
		
		Member member = (Member) request.getSession().getAttribute("member");
		member.setRank(3);
		
		memberManager.memberUpdate(member);
		request.getSession().removeAttribute("member");
		request.getSession().removeAttribute("memberPass");
		
		ModelAndView mv = new ModelAndView();
		mv.setViewName("/MainMoebius.do");
		
		return mv;
	}

}
