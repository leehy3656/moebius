package com.moebius.celab.membermanage.abilitymanage.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.moebius.celab.membermanage.abilitymanage.common.Ability;
import com.moebius.celab.membermanage.abilitymanage.dataservice.AbilityDAO;

@Service
public class AbilityManager {
	@Autowired
	private AbilityDAO abilityDAO=null;
	
	
	public void AbilityManager(){
		
	}
	
	@Transactional
	public List<Ability> selectAll(){
		//체크박스 가져오기
		
		return abilityDAO.selectAll();
		
	}
	@Transactional
	public List<Ability> selectByNo(){
		
		
		return null;
	}
	@Transactional
	public Ability selectAbility(int no){
		
		
		
		return abilityDAO.select(no);
	}
	
}
