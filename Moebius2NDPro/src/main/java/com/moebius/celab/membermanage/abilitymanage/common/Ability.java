package com.moebius.celab.membermanage.abilitymanage.common;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity/*(name = "com.moebius.celab.membermanage.abilitymanage.common")*/
@Table(name = "Ability")
public class Ability {

    
    public  Ability() {
    }
    @Id
   	@GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="no")
	private int adNo;
    
    
    @Column(name="Name")
    private String abilityName;
    
   // @ManyToMany(mappedBy="abilitys", cascade = {CascadeType.ALL})
    //private Set<MemberPerson> memberPersons = new HashSet<MemberPerson>();
    
    
   // @ManyToMany(cascade = {CascadeType.ALL},mappedBy="abilitys2")
  // private Set<MemberCompany> memberCompanys = new HashSet<MemberCompany>();

    
    
    
    /*
    public Set<MemberCompany> getMemberCompanys() {
		return memberCompanys;
	}


	public void setMemberCompanys(Set<MemberCompany> memberCompanys) {
		this.memberCompanys = memberCompanys;
	}


	public Set<MemberPerson> getEmployees() {
		return memberPersons;
	}


	public void setMemberPerson(Set<MemberPerson> memberPersons) {
		this.memberPersons = memberPersons;
	}
	*/


	public int getAdNo() {
		return adNo;
	}


	public void setAdNo(int adNo) {
		this.adNo = adNo;
	}


	public String getAbilityName() {
		return abilityName;
	}


	public void setAbilityName(String abilityName) {
		this.abilityName = abilityName;
	}
	
	/*
	public void addMemberPerson(MemberPerson memberPerson){
		//memberPerson.addAbility(this);
		//바구니 없으면 만들고 
		if(memberPersons == null){
			memberPersons = new HashSet<MemberPerson>();
		}
		
		//바구니에 넣어
		memberPersons.add(memberPerson);
	}
	*/
	



}