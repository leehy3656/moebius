package com.moebius.celab.membermanage.memberpersonmanage.business;

import java.io.IOException;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.moebius.celab.membermanage.memberpersonmanage.common.Member;
import com.moebius.celab.membermanage.memberpersonmanage.dataservice.MemberDAO;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

@Service
public class MemberManager {
	@Autowired
	private MemberDAO memberDAO;
	
   public MemberManager() {
    }

   	@Transactional
   	public Member memberSelectSignupID(String idname ){
   		return memberDAO.selectSignupID(idname);
   	}
   	
    @Transactional
    public List<Member> memberSelectID(String idname) {
    	
        return memberDAO.selectID(idname);
    }
    @Transactional
    public List<Member> memberSelectName(String name){
    	return memberDAO.selectName(name);
    }
    
    @Transactional
    public Member memberFindId(Member member){
    	return memberDAO.findId(member);
    }
    
    @Transactional
    public Member memberFindPassword(Member member){
    	return memberDAO.findPassword(member);
    }
    
    @Transactional
    public Member memberSelectEmail(String email){
    	return memberDAO.selectEmail(email);
    }


    @Transactional
    public int memberSave(Member member) {
    	int no= memberDAO.save(member);
        
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection;
		
        try {
			connection = factory.newConnection();
			Channel channel = connection.createChannel();
	        channel.exchangeDeclare("EXCHANGER/user", "topic");
	        
	        channel.queueDeclare("no"+no+"#1", true, false, false, null).getQueue();
	        channel.queueBind("no"+no+"#1", "EXCHANGER/user", "all.#");
	        channel.queueBind("no"+no+"#1", "EXCHANGER/user", "no" +no+ "#1");

	        channel.queueDeclare("no"+no+"#2", true, false, false, null).getQueue();
	        channel.queueBind("no"+no+"#2", "EXCHANGER/user", "all.#");
	        channel.queueBind("no"+no+"#2", "EXCHANGER/user", "no" +no+ "#2");
	        
	        System.out.println("QueName :"+no);
	        
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			return no;
		}
        
    }
    @Transactional
    public int idselect(String str) {
        return memberDAO.idselect(str);
    }
    @Transactional
    public void memberUpdate(Member member ) {
    	memberDAO.update(member);
    }

    @Transactional
    public Member MemberSelect(int idno) {
        return memberDAO.select(idno);
    }

    
  /*  @Transactional
    public int MemberPointplus(int idno , int point ) {
    	return memberDAO.MemberPointplus(idno, point);
    }
    @Transactional
    public int MemberPointminus(int idno , int point ) {
    	return memberDAO.MemberPointminus(idno, point);
    }*/

    @Transactional
    public Member memberLogin(String idname ,String password ) {
    	
    	Member member = memberDAO.login(idname);
    	
    	
    	if(member == null){
    		return null;
    	}
    	else if(member.getPassword().equals(password)){
    		return member;
    	}
    	
    	else{
    		return null;
    	}
    	
         
    }
/*
    @Transactional
    public int MemberBan(int idno ) {
        return memberDAO.ban(idno);
    }
    
    @Transactional
    public List<PostAddress> selectaddress(String address ) {
        return postaddressManager.select(address);
    }
    
    @Transactional
    public List<Ability> abilityAll() {
    	
        return abilityManager.selectAll();
    	
    }
    
    @Transactional
    public  Ability selectAbility(int no){
    	
        return abilityManager.selectAbility(no);
    	
    }*/
    @Transactional
    public  int memberdel(int no){
    	
        return memberDAO.delete(no);
    	
    }
    @Transactional
    public Member memberSelectNameEmail(String name, String email){
    	
    	return memberDAO.memberSelectNameEmail(name, email);
    }
    
    @Transactional
    public Member memberSelectIdNameEmail(String id, String name, String email){
    	
    	return memberDAO.memberSelectIdNameEmail(id, name, email);
    }
    @Transactional
    public List<Member> memberSelectAbilitys(List<Integer> abilityNos){
    	
    	return memberDAO.memberSelectAbilitys(abilityNos);
    	
    }
    
 
}