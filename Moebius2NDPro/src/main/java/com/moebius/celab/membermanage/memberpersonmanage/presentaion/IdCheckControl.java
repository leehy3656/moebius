package com.moebius.celab.membermanage.memberpersonmanage.presentaion;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.moebius.celab.membermanage.memberpersonmanage.business.MemberManager;
import com.moebius.celab.membermanage.memberpersonmanage.common.Member;

@Controller
public class IdCheckControl {
	@Autowired
	MemberManager memberManager;
	Member member = null;
	String check;

	@RequestMapping(value = "idCheck.do", method = RequestMethod.POST)
	public @ResponseBody String idCheck(@RequestParam("idname") String idname) throws UnsupportedEncodingException {
		if (member == null) {
			member = new Member();
		}
		member = memberManager.memberSelectSignupID(idname);
		
		if(member == null){
			check ="사용가능";
		}
		
		else{
			check = "사용불가";
		}
		check = URLEncoder.encode(check, "UTF-8");
		return check;

	}
}
