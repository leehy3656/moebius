package com.moebius.celab.membermanage.memberpersonmanage.presentaion;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.moebius.celab.membermanage.memberpersonmanage.business.MemberManager;
import com.moebius.celab.membermanage.memberpersonmanage.common.Member;

@Controller
public class changePasswordControl {
	
	@Autowired
	MemberManager memberManager;
	
	@RequestMapping("updatePass.do")
	ModelAndView updatePass(@RequestParam("password")String password, HttpServletRequest request){
		
		System.out.println("password:"+ password);
		
		Member member = (Member) request.getSession().getAttribute("member");
		member.setPassword(password);
		
		memberManager.memberUpdate(member);
		
		ModelAndView mv = new ModelAndView();
		mv.addObject("member",member);
		mv.setViewName("redirect:/MainMoebius.do");
		return mv;
	}

}
