package com.moebius.celab.messagemanager.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.moebius.celab.membermanage.memberpersonmanage.common.Member;
import com.moebius.celab.messagemanager.common.Message;
import com.moebius.celab.messagemanager.dataservice.MessageDAO;

@Service
public class MessageManager {

	@Autowired
	MessageDAO messageDAO;

	@Transactional
	public int save(Message message) {

		return messageDAO.save(message);
	}

	@Transactional
	public List<Message> selectByMemberType(Member member, String type) {

		return messageDAO.selectByMemberType(member, type);
	}

	@Transactional
	public List<Message> msgcheck(Member member, String type) {

		return messageDAO.selectByNew(member, type);
	}

	@Transactional
	public void updatemsg(List<Message> messages) {

		for (Message msg : messages) {
			msg.setStatus("읽음");
			messageDAO.updatemsg(msg);
		}
	}
	
	@Transactional
	public Message selectByNo(int no){
		return messageDAO.selectByNo(no);
	}
	
	@Transactional
	public void delete(Message message){
		messageDAO.delete(message);
	}
}
