package com.moebius.celab.messagemanager.presentation;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.moebius.celab.membermanage.memberpersonmanage.business.MemberManager;
import com.moebius.celab.membermanage.memberpersonmanage.common.Member;
import com.moebius.celab.messagemanager.common.Message;

@Controller
public class PrepareSendMessageControl {
	
	@Autowired
	MemberManager memberManager;
	
	@RequestMapping(value ="PrepareSendmessage.do")
	ModelAndView PrepareSendmessage(HttpServletRequest request, @RequestParam("memberNo")int no){
		System.out.println("memberNo :"+no);
		Member member = new Member();
		member = memberManager.MemberSelect(no);
		
		List<Message> messages = new ArrayList<Message>(); 
		
		ModelAndView mv = new ModelAndView();
		
		
		mv.addObject("memberNo",member.getIdno());
		mv.addObject("memberName",member.getIdname());
		mv.addObject("messages", messages);
		mv.setViewName("/Message/messageSend.jsp");
		
		return mv;
		
	}

}
