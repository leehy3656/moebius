package com.moebius.celab.messagemanager.presentation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.moebius.celab.messagemanager.business.MessageManager;
import com.moebius.celab.messagemanager.common.Message;

@Controller
public class DeleteMessageControl {
	
	@Autowired
	MessageManager messageManager;
	
	@RequestMapping(value = "deleteMessage.do")
	@ResponseBody String deleteMessage(@RequestParam("messageNo") int messageNo) {
		
		Message message = messageManager.selectByNo(messageNo);
		messageManager.delete(message);
		
		return "ok";
	}
}
