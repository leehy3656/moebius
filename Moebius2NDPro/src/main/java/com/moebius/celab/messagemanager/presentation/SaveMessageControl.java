package com.moebius.celab.messagemanager.presentation;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.moebius.celab.membermanage.memberpersonmanage.business.MemberManager;
import com.moebius.celab.membermanage.memberpersonmanage.common.Member;
import com.moebius.celab.messagemanager.business.MessageManager;
import com.moebius.celab.messagemanager.common.Message;

@Controller
public class SaveMessageControl {
	@Autowired
	MessageManager messageManager;
	
	@Autowired
	MemberManager memberManager;

	@RequestMapping(value = "saveMessage.do")
	@ResponseBody String saveMessage(@RequestParam("type") String type, @RequestParam("receiveMember") int receiveMemberNo,
			@RequestParam("content") String content, HttpServletRequest request) {
		System.out.println("HEre??");
		
		Member member = (Member) request.getSession().getAttribute("member");
		Member receiveMember = memberManager.MemberSelect(receiveMemberNo);

		System.out.println("type :"+ type);
		System.out.println("content :"+ content);
		System.out.println("send Member Name :"+ member.getName());
		System.out.println("receive Member Name :" +receiveMember.getName());
		
		Message message = new Message();
		message.setType(type);
		message.setContent(content);
		message.setSender(member);
		message.setReceiver(receiveMember);
		message.setStatus("안읽음");
		messageManager.save(message);
		
		return "OK";
	}

}
