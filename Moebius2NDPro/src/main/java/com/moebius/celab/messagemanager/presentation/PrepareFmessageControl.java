package com.moebius.celab.messagemanager.presentation;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonObjectFormatVisitor;
import com.moebius.celab.membermanage.memberpersonmanage.common.Member;
import com.moebius.celab.messagemanager.business.MessageManager;
import com.moebius.celab.messagemanager.common.Message;

@Controller
public class PrepareFmessageControl {
	@Autowired
	MessageManager messageManager;
	@RequestMapping(value ="PrepareFmessage.do")
	ModelAndView prepareVmessage(HttpServletRequest request){
		Member member = new Member();
		member = (Member) request.getSession().getAttribute("member");
		
		List<Message> messages = new ArrayList<Message>(); 
		messages = messageManager.selectByMemberType(member,"fMessage");
		System.out.println(messages.size());
		List<Message> f=messageManager.msgcheck(member, "fMessage");
		
		messageManager.updatemsg(f);
		
		
		ModelAndView mv = new ModelAndView();
		
		mv.addObject("memberNo",member.getIdno());
		mv.addObject("messages", messages);
		mv.addObject("f", f);
		mv.setViewName("/Message/fmessage.jsp");
		
		return mv;
		
	}
	
	@RequestMapping(value ="checkMsg.do")
	public @ResponseBody JSONObject CheckMsg(HttpServletRequest request){
		Member member = new Member();
		JSONObject ob=new JSONObject();
		if(request.getSession().getAttribute("member")!=null){
		member = (Member) request.getSession().getAttribute("member");

		List<Message> v=messageManager.msgcheck(member, "vMessage");
		List<Message> f=messageManager.msgcheck(member, "fMessage");
		
		ob.put("vmsg",v.size());
		ob.put("fmsg",f.size());
		}else
		{
			ob.put("vmsg",0);
			ob.put("fmsg",0);
		}
		return ob;
		
	}

}
