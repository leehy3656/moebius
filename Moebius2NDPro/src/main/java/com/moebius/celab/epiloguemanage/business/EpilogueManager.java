package com.moebius.celab.epiloguemanage.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.moebius.celab.epiloguemanage.common.Epilogue;
import com.moebius.celab.epiloguemanage.dataservice.EpilogueDAO;
import com.moebius.celab.volunteermanage.business.VolunteerManager;
import com.moebius.celab.volunteermanage.common.Volunteer;

@Service
public class EpilogueManager {
	@Autowired
	EpilogueDAO epilogueDAO;
	
	@Transactional
	public void save(Epilogue epilogue){
		
		epilogueDAO.save(epilogue);
		
	}
	@Transactional
	public List<Epilogue> SelectAll(){
		return epilogueDAO.SelectAll();
	}
	@Transactional
	public Epilogue selectByNo(int no){
		return epilogueDAO.selectByNo(no);
	}
	@Transactional
	public Epilogue selectById(String id){
		return epilogueDAO.selectById(id);
	}
	@Transactional
	public void remove(Epilogue epilogue){
		 epilogueDAO.remove(epilogue);
	}
	@Transactional
	public void update(Epilogue epilogue){
		 epilogueDAO.update(epilogue);
	}
	@Transactional
	public List<Epilogue> selectByVolunteer(Volunteer volunteer){
		return epilogueDAO.selectByVolunteer(volunteer);
	}
	

}
