package com.moebius.celab.epiloguemanage.common;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import com.moebius.celab.membermanage.memberpersonmanage.common.Member;
import com.moebius.celab.volunteermanage.common.Volunteer;

@Entity
@Table(name = "EPILOGUE")
public class Epilogue {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "no")
	int no;
	
	@Generated(GenerationTime.ALWAYS)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DATE", insertable=false, updatable=false)
	Date date;
	
	@ManyToOne
	@JoinColumn(name = "VOLUNTEER_NO")
	private Volunteer volunteer;
	
	@ManyToOne
	@JoinColumn(name = "MEMBER_NO")
	private Member member;

	@Column(name = "CONTENT")
	String content;
	
	@Column(name = "IMG")
	String img;

	public int getNo() {
		return no;
	}

	public void setNo(int no) {
		this.no = no;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public Volunteer getVolunteer() {
		return volunteer;
	}

	public void setVolunteer(Volunteer volunteer) {
		this.volunteer = volunteer;
	}
	public Member getMember() {
		return member;
	}

	public void setMember(Member memberPerson) {
		this.member = memberPerson;
	}
}
