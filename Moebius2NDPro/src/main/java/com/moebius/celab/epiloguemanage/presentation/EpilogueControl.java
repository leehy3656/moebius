package com.moebius.celab.epiloguemanage.presentation;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.moebius.celab.epiloguemanage.business.EpilogueManager;
import com.moebius.celab.epiloguemanage.common.Epilogue;
import com.moebius.celab.membermanage.memberpersonmanage.common.Member;

@Controller
public class EpilogueControl {
	Member member = new Member();
	@Autowired
	EpilogueManager epilogueManager;
	Epilogue epilogue = null;
	List<Epilogue> epilogues = null;
	
	int listSize = 5;

	/*@RequestMapping("EpRegist.do")
	public ModelAndView EpRegist(@RequestParam("content") String content,
								 @RequestParam("img") String img,
								 HttpServletRequest request)
								 {
		member =(Member) request.getSession().getAttribute("member");
		String id = member.getIdname();
		System.out.println(content);
		System.out.println(img);
		if (epilogue == null) {
			epilogue = new Epilogue();
		}
		epilogue.setContent(content);
		epilogue.setId(id);
		epilogue.setImg(img);

		epilogueManager.save(epilogue);

		ModelAndView mv = new ModelAndView();
		mv.addObject(epilogue);
		mv.setViewName("EpSelect.do");
		return mv;
	}
	@RequestMapping("EpUpdate.do")
	public ModelAndView EpUpdate(@RequestParam("content") String content,
								 @RequestParam("no") int no,
								 HttpServletRequest request){
		
		
		System.out.println("업데이트 들어옴");
		if (epilogue == null) {
			epilogue = new Epilogue();
		}
		epilogue = epilogueManager.selectByNo(no);
		epilogue.setContent(content);
		
		epilogueManager.update(epilogue);
		//epilogue.setImg(img);
		System.out.println("업데이트");
		
		ModelAndView mv = new ModelAndView();
		mv.setViewName("EpSelect.do");
		return mv;
	}
	@RequestMapping("EpRemove.do")
	public ModelAndView EpRemove(@RequestParam("no") int no,
								 @RequestParam("listno") int listno)
								 {
		if (epilogue == null) {
			epilogue = new Epilogue();
		}
		epilogue=epilogueManager.selectByNo(no);
		epilogueManager.remove(epilogue);

		ModelAndView mv = new ModelAndView();
		mv.addObject(listno);
		mv.setViewName("EpSelect.do");
		return mv;
	}

	@RequestMapping("EpSelect.do")
	public ModelAndView EpSelect(@RequestParam(value="listno",defaultValue="1") int listno, HttpServletRequest request) {
		member =(Member) request.getSession().getAttribute("member");
		ModelAndView mv = new ModelAndView();
		
		if(epilogues == null){
			epilogues = new ArrayList<Epilogue>();
		}
		
		epilogues = epilogueManager.SelectAll();
		
		System.out.println(epilogues.size());
		
		double pageSize = Math.ceil(epilogues.size()/(double)listSize);
		
		mv.addObject("member", member);
		mv.addObject("listSize",listSize);
		mv.addObject("pageSize",pageSize);
		mv.addObject("listno",listno);
		mv.addObject("epilogueList",epilogues);
		mv.setViewName("/Epilogue/E_Article.jsp");
		return mv;
	}*/

}
