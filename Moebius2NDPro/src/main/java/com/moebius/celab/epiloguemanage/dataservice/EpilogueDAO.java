package com.moebius.celab.epiloguemanage.dataservice;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.moebius.celab.attendancemanage.common.Attendance;
import com.moebius.celab.epiloguemanage.common.Epilogue;
import com.moebius.celab.volunteermanage.common.Volunteer;

@Repository
public class EpilogueDAO {
	public EpilogueDAO(){
		
	}
	@Autowired
	private SessionFactory sessionFactory;
	
	public void save(Epilogue epilogue){
		Session session = sessionFactory.getCurrentSession();
		session.save(epilogue);
	}
	public List<Epilogue> SelectAll(){
		Session session = sessionFactory.getCurrentSession();
		Criteria cr = session.createCriteria(Epilogue.class);
		List<Epilogue> epilogues = (List<Epilogue>)cr.list();
		return epilogues;
	}
	public Epilogue selectByNo(int no){
		Session session = sessionFactory.getCurrentSession();
		Epilogue 찾은Epilogue = (Epilogue) session.get(Epilogue.class,new Integer(no));
		return 찾은Epilogue;
	}
	public Epilogue selectById(String id){
		Session session = sessionFactory.getCurrentSession();
		Epilogue 찾은Epilogue = (Epilogue) session.get(Epilogue.class,new String(id));
		return 찾은Epilogue;
	}
	public void update(Epilogue epilogue) {
		Session session = sessionFactory.getCurrentSession();

		session.update(epilogue);
	}
	public void remove(Epilogue epilogue){
		Session session = sessionFactory.getCurrentSession();

		session.delete(epilogue);
	}
	
	public List<Epilogue>selectByVolunteer(Volunteer volunteer){
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session
				.createCriteria(Epilogue.class)
				.add(Restrictions.eq("volunteer", volunteer))
				.addOrder(Order.asc("date"))
				.setResultTransformer(
						CriteriaSpecification.DISTINCT_ROOT_ENTITY);

		List<Epilogue> epilogues = criteria.list();

		return epilogues;
		
	}


}
