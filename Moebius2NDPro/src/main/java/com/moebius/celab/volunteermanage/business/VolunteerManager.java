package com.moebius.celab.volunteermanage.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.moebius.celab.membermanage.memberpersonmanage.common.Member;
import com.moebius.celab.volunteermanage.common.Volunteer;
import com.moebius.celab.volunteermanage.dataservice.VolunteerDAO;

@Service
public class VolunteerManager {

	public VolunteerManager() {
	}

	@Autowired
	VolunteerDAO volunteerDAO;

	@Transactional
	public List<Volunteer> selectAll() {
		return volunteerDAO.selectAll();
	}
	@Transactional
	public List<Volunteer> selectApprovalOderBy(int status) {
		return volunteerDAO.selectApprovalOderBy(status);
	}
	@Transactional
	public List<Volunteer> selectSearched(List<String> addresses, List<String> abilities,String sex,String startDate, String endDate, String sido ){
		return volunteerDAO.selectSearched(addresses,abilities,sex,startDate, endDate, sido);
	}
	@Transactional
	public List<Volunteer> selectBySex(String sex){
		return volunteerDAO.selectBySex(sex);
	}
	
	@Transactional
	public Volunteer selectByNo(int no) {
		return volunteerDAO.selectByNo(no);
	}
	
	@Transactional
	public List<Volunteer> selectByWriter(Member member) {
		return volunteerDAO.selectByWriter(member);
	}
	@Transactional
	public List<Volunteer> BsetselectByvolunteer(int no) {
		return volunteerDAO.BestselectByvolunteer(no);
	}
	@Transactional
	public List<Volunteer> selectByComplete() {
		return volunteerDAO.selectByComplete();
	}
	@Transactional
	public List<Volunteer> selectByPlace(String keyword) {
		return volunteerDAO.selectByPlace(keyword);
	}
	
}