package com.moebius.celab.volunteermanage.presentation;

import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.moebius.celab.membermanage.memberpersonmanage.common.Member;
import com.moebius.celab.volunteermanage.business.VolunteerManager;
import com.moebius.celab.volunteermanage.business.VolunteerManager2;
import com.moebius.celab.volunteermanage.common.Volunteer;

@Controller
public class ApplyVolunteerContol {
	
	@Autowired
	VolunteerManager2 volunteerManage2;
	
	@Autowired
	VolunteerManager volunteerManage;
	
	
	@RequestMapping("/applyVolunteer1.do")
	ModelAndView applyVolunteer(@RequestParam("no") int no, @RequestParam("listno") int listno, HttpServletRequest request){
		Volunteer volunteer = new Volunteer();
		volunteer = volunteerManage.selectByNo(no);
		
		Member member = new Member();
		member = (Member) request.getSession().getAttribute("member");
		System.out.println("Member Name: "+member.getName());
		volunteer.addMember(member);
		volunteerManage2.updateVolunteer(volunteer);
		
		ModelAndView mv = new ModelAndView();
		mv.setViewName("/kks/detailView.jsp");
		mv.addObject("volunteer", volunteer);
		mv.addObject("listno", listno);
		return mv;
		
	}
	
	@ResponseBody			  
	@RequestMapping(value = "/applyVolunteer.do")
	public String applyVolunteer(@RequestParam(value = "volunteerNum")int volunteerNo,
			HttpServletRequest request) {
		Member member = new Member();
		Volunteer volunteer = volunteerManage.selectByNo(volunteerNo);
		member = (Member) request.getSession().getAttribute("member");

		Iterator<Member> iterator = volunteer.getMembers().iterator();
		
		while (iterator.hasNext()) {
			Member aMember = iterator.next();
			
			if(aMember.getIdname().equals(member.getIdname())){
				return "fail";
			}
		}
		
		if(volunteer.getParticipant() <= volunteer.getMembers().size() ){
			return "fail1";
		}
		else{
		volunteer.addMember(member);
		volunteerManage2.updateVolunteer(volunteer);
		return "success";
		}
	}
	

}
