package com.moebius.celab.volunteermanage.dataservice;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.moebius.celab.volunteermanage.common.Volunteer;


@Repository
public class VolunteerDAO2 {
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<Volunteer> selectByidno(int idno){
		
		Session session = sessionFactory.getCurrentSession();
		/*Criteria cr = session.createCriteria(Volunteer.class);
		List<Volunteer> volunteers = (List<Volunteer>)cr.list();*/
		
		return session.createSQLQuery("select *from volunteer where member="+idno).addEntity(Volunteer.class).list();
		
	}
	
	public int saveVolunteer(Volunteer volunteer){
		Session session = sessionFactory.getCurrentSession();
    	
    	int no = (Integer) session.save(volunteer);
        return no;
	}
	
	public void update(Volunteer volunteer) {
		Session session = sessionFactory.getCurrentSession();
		session.update(volunteer);
	}
	public void delete(int no) {
		Session session = sessionFactory.getCurrentSession();
   	 	Volunteer volunteer =  (Volunteer) session.get(Volunteer.class, new Integer(no));//찾기
       session.delete(volunteer);
	}
	public List<Volunteer> rload(int idno){
		
		Session session = sessionFactory.getCurrentSession();
		/*Criteria cr = session.createCriteria(Volunteer.class);
		List<Volunteer> volunteers = (List<Volunteer>)cr.list();*/
		
		return session.createSQLQuery("select a.* from volunteer a left join member_volunteer b on a.no=b.Volunteer_no where b.Member_no="+idno).addEntity(Volunteer.class).list();
		
	}

}
