package com.moebius.celab.volunteermanage.presentation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.moebius.celab.membermanage.abilitymanage.business.AbilityManager;
import com.moebius.celab.membermanage.abilitymanage.common.Ability;
import com.moebius.celab.membermanage.memberpersonmanage.common.Member;

import com.moebius.celab.volunteermanage.business.VolunteerManager2;
import com.moebius.celab.volunteermanage.common.Volunteer;
import com.moebius.celab.weekdaymanage.business.WeekDayManager;
import com.moebius.celab.weekdaymanage.common.Weekday;



@Controller
public class registVolunteerability {
	@Autowired
	AbilityManager abilityManager;
	@Autowired
	WeekDayManager weekdayManager;
	@Autowired
	VolunteerManager2 volunteerManager;
	
	Member mem =null;
	
	
	@RequestMapping("loadAbility.do")
	public ModelAndView volunteerabilityload(HttpServletRequest request){
		ModelAndView mv = new ModelAndView();
		
		List<Ability> abilitys = null;
		List<Ability> abilitys1 = null;
		List<Ability> abilitys2 = null;
		List<Ability> abilitys3 = null;
		
		if(abilitys == null){
			abilitys = new ArrayList<Ability>();
		}
		
		if(abilitys1 == null){
			abilitys1 = new ArrayList<Ability>();
		}
		
		if(abilitys2 == null){
			abilitys2 = new ArrayList<Ability>();
		}
		
		if(abilitys3 == null){
			abilitys3 = new ArrayList<Ability>();
		}
		
		abilitys = abilityManager.selectAll();
		
		for(int i=0; i<abilitys.size(); i++){
			
			switch(i%3){
			
			case 0:
				abilitys1.add(abilitys.get(i));
				break;

			
			case 1:
				abilitys2.add(abilitys.get(i));
				break;
				
			case 2:
				abilitys3.add(abilitys.get(i));
				break;
			}
			
		}
		if(mem==null){
			mem=new Member();
		}
		
		mem=(Member) request.getSession().getAttribute("member");
		
		List<Weekday> wlist= weekdayManager.selectAll();
		
		mv.addObject("abilitys1", abilitys1);
		mv.addObject("abilitys2", abilitys2);
		mv.addObject("abilitys3", abilitys3);
		
		mv.addObject("weekday",wlist);
		mv.addObject("member",mem);
		mv.setViewName("/Volunteer/registVolunteerMap.jsp");
		
		
		
		return mv;
	}
	@RequestMapping("volunteermsg.do")
	@ResponseBody
	public String shoppingUpdate(
			HttpServletRequest request) throws IOException{

		
		
		String shoppingJson=request.getParameter("volunteer");

		
		JSONObject ob=JSONObject.fromObject(JSONSerializer.toJSON(shoppingJson));

		
		
		
		ObjectMapper mapper = new ObjectMapper(); 
		String shoppingstr=ob.getString("volunteer");
		Volunteer volunteermsg=new Volunteer();
		
		
		try {
			
			volunteermsg = mapper.readValue(shoppingstr, Volunteer.class);
			//제이슨 다시 객체로
			
		} catch (IOException e) {e.printStackTrace();}

		
		volunteerManager.prepareMessage(volunteermsg);
		
		
		return "성공";
	}

}
