package com.moebius.celab.volunteermanage.dataservice;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.moebius.celab.membermanage.memberpersonmanage.common.Member;
import com.moebius.celab.volunteermanage.common.Volunteer;

@Repository
public class VolunteerDAO {
	public VolunteerDAO() {

	}

	@Autowired
	private SessionFactory sessionFactory;

	public List<Volunteer> selectAll() {
		Session session = sessionFactory.getCurrentSession();
		Criteria cr = session.createCriteria(Volunteer.class)
				.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);		
		List<Volunteer> volunteers = (List<Volunteer>) cr.list();
		return volunteers;
	}
	public List<Volunteer> selectApprovalOderBy(int status) {
		Session session = sessionFactory.getCurrentSession();
		Criteria cr = session.createCriteria(Volunteer.class)
				.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY)
				.add(Restrictions.eq("status", status))
				.addOrder(Order.desc("date"));
		List<Volunteer> volunteers = (List<Volunteer>) cr.list();
		return volunteers;
	}

	public List<Volunteer> selectSearched1(List<String> addresses,
			List<String> abilitues, String sex) {

		return null;
	}

	public List<Volunteer> selectSearched(List<String> addresses,
			List<String> abilities, String sex, String startDate, String endDate, String sido ) {

		String str = "select distinct a.* from volunteer a join ability_volunteer b  on a.no=b.Volunteer_no where status=2 ";
		String str2="";
		String substr2="";
		String sqlstr="";
		String sexstr="";
		String datestr="";
		String dateend="";
		if(addresses.size()!=0){
			str2="and (a.place like '%"+sido+"%' and ";
			
			for(String add:addresses){
				if(sqlstr.length()>0){
					sqlstr=sqlstr+"or";
				}
				
				sqlstr=sqlstr+" a.place like '%"+add+"%' ";
				
				
			}
			str2=str2+sqlstr+")";
		}
		
		if (abilities.size() != 0) {
			
			substr2=" and b.Ability_no in (";
			String abstr="";
			for (String ab : abilities) {
				if (abstr.length() > 0) {
					abstr += ",";
				}
				abstr += "'" + ab + "'";
			}
			substr2=substr2+abstr+")";
		}
		
		if (sex.length() != 0) {
			
			sexstr=" and a.sex in (3,"+sex+")";
			
		}
		
		if(startDate.length()>0){
			
			datestr=" and a.startDate >= '"+startDate+"'";
			
		}
		if(endDate.length()>0){
			
			dateend=" and a.endDate <= '"+endDate+"'";
			
		}
		
		
		str=str+str2+substr2+sexstr+datestr+dateend;
		System.out.println(str);
		

		Session session = sessionFactory.getCurrentSession();
		return session.createSQLQuery(str).addEntity(Volunteer.class).list();
		
	}

	public List<Volunteer> selectBySex(String sex) {
		String str = String.valueOf(sex);
		System.out.println("sex :" + str);
		Session session = sessionFactory.getCurrentSession();
		Criteria cr = session.createCriteria(Volunteer.class);
		cr.add(Restrictions.in("sex", new String[] { sex, "무관" }));
		List<Volunteer> volunteerSex = cr.list();
		return volunteerSex;
	}

	public Volunteer selectByNo(int no) {
		// System.out.println("no :"+ no);

		Session session = sessionFactory.getCurrentSession();
		Volunteer volunteer = (Volunteer) session.get(Volunteer.class,
				new Integer(no));

		return volunteer;
	}
	
	public List<Volunteer> selectByWriter(Member member) {
		Session session = sessionFactory.getCurrentSession();

		Criteria criteria = session.createCriteria(Volunteer.class)
				.add(Restrictions.eq("memberPerson", member))
				.addOrder(Order.asc("status"));
		List<Volunteer> volunteers = criteria.list();

		// HashSet al = new HashSet(volunteers);
		// ArrayList<Volunteer> relist = new ArrayList<Volunteer>(al);
		List<Volunteer> dis = new ArrayList<Volunteer>();
		for (int ii = 0; ii < volunteers.size(); ii++) {
			if (!dis.contains(volunteers.get(ii))) {
				dis.add(volunteers.get(ii));
			}
		}

		return dis;
	}
	
	public List<Volunteer> BestselectByvolunteer(int no) {
		Session session = sessionFactory.getCurrentSession();

		return session.createSQLQuery(
				"select distinct * from volunteer where status =" +no +" order by date desc"
				).addEntity(Volunteer.class).list();
	}
	
	public List<Volunteer> selectByComplete(){
		
		Session session = sessionFactory.getCurrentSession();
		return session.createSQLQuery(
				"select distinct * from volunteer where status in(7,8) order by date desc"
				).addEntity(Volunteer.class).list();
	}
public List<Volunteer> selectByPlace(String keyword){
		
		Session session = sessionFactory.getCurrentSession();
		return session.createSQLQuery(
				"select distinct * from volunteer where place like '%"+keyword+"%' and status=2"
				).addEntity(Volunteer.class).list();
	}
	
}
