package com.moebius.celab.volunteermanage.presentation;



import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.moebius.celab.membermanage.memberpersonmanage.common.Member;
import com.moebius.celab.volunteermanage.business.VolunteerManager2;
import com.moebius.celab.volunteermanage.common.Volunteer;
import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;





@Controller
public class registVolunteerController {
	@Autowired
	VolunteerManager2 volunteerManager2;
	
	Member mem =null;
	String uploadFile = "default.png";
	
	@RequestMapping(value="registVolunteer.do" , method = RequestMethod.POST)
	public @ResponseBody Volunteer registV(@RequestBody Volunteer volunteer , HttpServletRequest request){


		if(mem==null){
		mem=new Member();
		}
		mem=(Member) request.getSession().getAttribute("member");
		System.out.println("멤버확인 :"+mem);
		volunteer.setMemberPerson(mem);
		
		
		if(!uploadFile.equals("default.png")){
		volunteer.setFilevolunteer(uploadFile);
		System.out.println(uploadFile);
		}
		uploadFile="default.png";
		
		volunteerManager2.registVolunteer(volunteer);
		
	return volunteer;
	
}
	@RequestMapping(value = "upload.do", method = RequestMethod.POST)
	@ResponseBody
	public String upload(HttpServletRequest request,
	        HttpServletResponse response) {
	
    int maxSize  = 1024*1024*10;        
 
    ServletContext scontext = request.getSession().getServletContext();
	 String savefile = "images";
	 String savePath = scontext.getRealPath(savefile);
    
 
    try{
 
        MultipartRequest multi = new MultipartRequest(request, savePath, maxSize, "UTF-8", new DefaultFileRenamePolicy());
         
      
 
        // 파일업로드
        uploadFile = multi.getFilesystemName("filevolunteer");

    }catch(Exception e){
        e.printStackTrace();
    }
	  
	    
	     
	    return "success";
	}

	
}
