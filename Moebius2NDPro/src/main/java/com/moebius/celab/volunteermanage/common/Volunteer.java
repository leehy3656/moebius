package com.moebius.celab.volunteermanage.common;


import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.moebius.celab.attendancemanage.common.Attendance;
import com.moebius.celab.membermanage.abilitymanage.common.Ability;
import com.moebius.celab.membermanage.memberpersonmanage.common.Member;
import com.moebius.celab.weekdaymanage.common.Weekday;

@Entity(name="com.moebius.celab.volunteermanage.common")
@Table(name = "VOLUNTEER")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property = "@idno")
public class Volunteer {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "no")
	private int no;

	@Generated(GenerationTime.ALWAYS)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date", insertable = false, updatable = false)
	private Date date;

	@Column(name = "title")
	private String title;
		
	@Column(name = "startDate")
	private String startDate;
	
	@Column(name = "endDate")
	private String endDate;
	
	@Column(name = "place")
	private String place;
	
	@Column(name = "participant")
	private int participant;
	
	@Column(name="phoneno")
	private String phoneno;

	@Column(name="place2")
	private String place2;

	@Column(name="sex")
	private int sex;
	
	@Column(name="memo")
	private String memo;
	
	@Column(name = "status")
	private int status;
	
	@ManyToOne
	@JoinColumn(name = "MEMBER")
	private Member memberPerson;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "ABILITY_VOLUNTEER", joinColumns = { @JoinColumn(name = "Volunteer_no") }, inverseJoinColumns = { @JoinColumn(name = "Ability_no") })
	private Set<Ability> abilitys = new HashSet<Ability>();
	
	

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "WEEKDAY_VOLUNTEER", joinColumns = { @JoinColumn(name = "Volunteer_no") }, inverseJoinColumns = { @JoinColumn(name = "WEEKDAY_no") })
	private Set<Weekday> weekday;
	
	@Column(name="filevolunteer")
	private String filevolunteer;
	
	@Column(name="hit")
	private int hit=0;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "MEMBER_VOLUNTEER", joinColumns ={ @JoinColumn(name = "Volunteer_no")}, inverseJoinColumns = { @JoinColumn(name = "Member_no") } )
	Set<Member> members;
	
	@Column(name="post")
	private String post;
	
	
	
	@Column(name="gps")
	private String gps;
	
	
	
	
	
	
	public String getGps() {
		return gps;
	}

	public void setGps(String gps) {
		this.gps = gps;
	}

	public Set<Weekday> getWeekday() {
		return weekday;
	}

	public void setWeekday(Set<Weekday> weekday) {
		this.weekday = weekday;
	}
	public int getHit() {
		return hit;
	}

	public void setHit(int hit) {
		this.hit = hit;
	}

	public int getNo() {
		return no;
	}

	public void setNo(int no) {
		this.no = no;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public int getParticipant() {
		return participant;
	}

	public void setParticipant(int participant) {
		this.participant = participant;
	}

	public String getPhoneno() {
		return phoneno;
	}

	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}

	public String getPlace2() {
		return place2;
	}

	public void setPlace2(String place2) {
		this.place2 = place2;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Member getMemberPerson() {
		return memberPerson;
	}

	public void setMemberPerson(Member memberPerson) {
		this.memberPerson = memberPerson;
	}

	public Set<Ability> getAbilitys() {
		return abilitys;
	}

	public void setAbilitys(Set<Ability> abilitys) {
		this.abilitys = abilitys;
	}
	public String getFilevolunteer() {
		return filevolunteer;
	}

	public void setFilevolunteer(String filevolunteer) {
		this.filevolunteer = filevolunteer;
	}
	

	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	public Set<Member> getMembers() {
		return members;
	}

	public void setMembers(Set<Member> members) {
		this.members = members;
	}
	
	public void addMember(Member member) {
		if (members == null) {
			members = new HashSet<Member>();
		}
		// 바구니에 넣어
		members.add(member);
	}
	
	public String getPost() {
		return post;
	}

	public void setPost(String post) {
		this.post = post;
	}
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
}
