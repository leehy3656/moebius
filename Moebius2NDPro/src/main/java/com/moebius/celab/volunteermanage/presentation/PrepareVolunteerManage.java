package com.moebius.celab.volunteermanage.presentation;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.moebius.celab.membermanage.abilitymanage.common.Ability;
import com.moebius.celab.membermanage.memberpersonmanage.common.Member;
import com.moebius.celab.volunteermanage.business.VolunteerManager;
import com.moebius.celab.volunteermanage.business.VolunteerManager2;
import com.moebius.celab.volunteermanage.common.Volunteer;
import com.moebius.celab.weekdaymanage.common.Weekday;

import java.text.SimpleDateFormat;
import java.util.Calendar;

@Controller
public class PrepareVolunteerManage {

	@Autowired
	VolunteerManager volunteerManager;

	@Autowired
	VolunteerManager2 volunteerManager2;
	
	@RequestMapping("PrepareManageMain.do")
	public ModelAndView volunteerabilityload(HttpServletRequest request){
		ModelAndView mv = new ModelAndView();
		List<Volunteer> volunteers =volunteerManager.selectByComplete();
		List<Volunteer> bestVolunteers = volunteerManager.BsetselectByvolunteer(9);
		mv.addObject("VolunteerList", volunteers);
		mv.addObject("BestVolList", bestVolunteers);
		mv.setViewName("/Manage/managemain.jsp");
		return mv;
	}

	@RequestMapping(value = "PrepareVolunteerManage.do", method = RequestMethod.POST)
	public @ResponseBody List<Volunteer> PrepareVolunteerMana(HttpServletRequest request)
	{
		List<Volunteer> volunteers = new ArrayList<Volunteer>();
		Member member = new Member();
		
		member =(Member) request.getSession().getAttribute("member");
		System.out.println("member :"+member.getName());
		volunteers =volunteerManager.selectByWriter(member);
		
		System.out.println("VolunteerSize :"+ volunteers.size() );


		return volunteers;

	}
}
