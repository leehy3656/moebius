package com.moebius.celab.volunteermanage.presentation;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.moebius.celab.addressmanager.business.AddressManager;
import com.moebius.celab.addressmanager.common.Address;
import com.moebius.celab.common.EnVolCheckSession;
import com.moebius.celab.membermanage.abilitymanage.business.AbilityManager;
import com.moebius.celab.membermanage.abilitymanage.common.Ability;
import com.moebius.celab.membermanage.memberpersonmanage.common.Member;
import com.moebius.celab.volunteermanage.business.VolunteerManager;
import com.moebius.celab.volunteermanage.common.Volunteer;
import com.moebius.celab.weekdaymanage.business.WeekDayManager;
import com.moebius.celab.weekdaymanage.common.Weekday;

@Controller
/* @RequestMapping("/celab") */
public class ArticleLoadControl {
	@Autowired
	VolunteerManager volunteerManager;
	@Autowired
	AbilityManager abilityManager;
	@Autowired
	WeekDayManager weekdayManager;
	@Autowired
	AddressManager addressManager;
	

	Member mem = null;
	List<Volunteer> volunteers = null;
	int listSize = 5;

	@RequestMapping("AllVolunteer.do")
	public ModelAndView ArticleLoad(
			@RequestParam(value = "listno", defaultValue = "1") int listno,
			@RequestParam(value = "chk_menu", defaultValue = "0") int chk_menu,
			HttpServletRequest request) {
		
		if(chk_menu == 1){
			request.getSession().removeAttribute("chk_address");
			request.getSession().removeAttribute("chk_ability");
			request.getSession().removeAttribute("chk_sex");
			request.getSession().removeAttribute("StartDate");
			request.getSession().removeAttribute("EndDate");
			request.getSession().removeAttribute("SiDo");
		}

		List<String> abilities = null;
		List<String> addresses = null;
		String sex;
		String startDate;
		String endDate;
		String sido;

		addresses = (List<String>) request.getSession().getAttribute("chk_address");
		abilities = (List<String>) request.getSession().getAttribute("chk_ability");
		sex = (String) request.getSession().getAttribute("chk_sex");
		startDate = (String) request.getSession().getAttribute("StartDate");
		endDate = (String) request.getSession().getAttribute("EndDate");
		sido = (String) request.getSession().getAttribute("SiDo");

		if (volunteers == null) {
			System.out.println("Volunteer Null");
			volunteers = new ArrayList<Volunteer>();
		}

		if (abilities == null) {
			abilities = new ArrayList<String>();
		}
		if (addresses == null) {
			addresses = new ArrayList<String>();
		}
		if (sex == null) {
			sex = new String();
		}
		if (startDate == null){
			startDate = new String();
		}
		if (endDate == null){
			endDate = new String();
		}
		if (sido == null){
			sido = new String();
		}
		System.out.println("시작날짜 : "+startDate);
		System.out.println("종료날짜 : "+endDate);
		System.out.println("시도 : "+sido);
		volunteers = volunteerManager.selectSearched(addresses, abilities, sex, startDate, endDate, sido);

		double pageSize = Math.ceil(volunteers.size() / (double) listSize);

		ModelAndView mv = new ModelAndView();
		
		List<Ability> abilitys = null;
		List<Ability> abilitys1 = null;
		List<Ability> abilitys2 = null;
		List<Ability> abilitys3 = null;

		if (abilitys == null) {
			abilitys = new ArrayList<Ability>();
		}

		if (abilitys1 == null) {
			abilitys1 = new ArrayList<Ability>();
		}

		if (abilitys2 == null) {
			abilitys2 = new ArrayList<Ability>();
		}

		if (abilitys3 == null) {
			abilitys3 = new ArrayList<Ability>();
		}

		abilitys = abilityManager.selectAll();

		for (int i = 0; i < abilitys.size(); i++) {

			switch (i % 3) {

			case 0:
				abilitys1.add(abilitys.get(i));
				break;

			case 1:
				abilitys2.add(abilitys.get(i));
				break;

			case 2:
				abilitys3.add(abilitys.get(i));
				break;
			}

		}

		List<Weekday> wlist = weekdayManager.selectAll();
		
		List<Address> addressSeoul = addressManager.selectBySido("서울");
		List<Address> addressBusan = addressManager.selectBySido("부산");
		List<Address> addressDeagu = addressManager.selectBySido("대구");
		List<Address> addressIncheon = addressManager.selectBySido("인천");
		List<Address> addressGwangju = addressManager.selectBySido("광주");
		List<Address> addressDeajun = addressManager.selectBySido("대전");
		List<Address> addressUlsan = addressManager.selectBySido("울산");
		List<Address> addressGangwon = addressManager.selectBySido("강원");
		List<Address> addressGyungi = addressManager.selectBySido("경기");
		List<Address> addressGyungnam = addressManager.selectBySido("경남");
		List<Address> addressGyungbook = addressManager.selectBySido("경북");
		List<Address> addressJunnam = addressManager.selectBySido("전남");
		List<Address> addressJunbook = addressManager.selectBySido("전북");
		List<Address> addressJeju =addressManager.selectBySido("제주");
		List<Address> addressChoongnam =addressManager.selectBySido("충남");
		List<Address> addressChoongbook =addressManager.selectBySido("충북");

		mv.addObject("abilitys1", abilitys1);
		mv.addObject("abilitys2", abilitys2);
		mv.addObject("abilitys3", abilitys3);

		mv.addObject("weekday", wlist);
		
		mv.addObject("addressSeoul",addressSeoul);
		mv.addObject("addressBusan",addressBusan);
		mv.addObject("addressDeagu",addressDeagu);
		mv.addObject("addressIncheon",addressIncheon);
		mv.addObject("addressGwangju",addressGwangju);
		mv.addObject("addressDeajun",addressDeajun);
		mv.addObject("addressUlsan",addressUlsan);
		mv.addObject("addressGangwon",addressGangwon);
		mv.addObject("addressGyungi",addressGyungi);
		mv.addObject("addressGyungnam",addressGyungnam);
		mv.addObject("addressGyungbook",addressGyungbook);
		mv.addObject("addressJunnam",addressJunnam);
		mv.addObject("addressJunbook",addressJunbook);
		mv.addObject("addressJeju",addressJeju);
		mv.addObject("addressChoongnam",addressChoongnam);
		mv.addObject("addressChoongbook",addressChoongbook);

		mv.addObject("listSize", listSize);
		mv.addObject("pageSize", pageSize);
		mv.addObject("listno", listno);
		mv.addObject("volunteerList", volunteers);

		mv.setViewName("/Volunteer/volunteer_Board.jsp");

		return mv;

	}

	@ResponseBody
	@RequestMapping("/searchBoadMakeSession.do")
	public boolean searchBoadMakeSession(
			HttpServletRequest request,
			@RequestParam(value = "chk_address", required = false) List<String> chk_address,
			@RequestParam(value = "chk_ability", required = false) List<String> chk_ability,
			@RequestParam(value = "chk_sex") String chk_sex,
			@RequestParam(value = "StartDate") String StartDate,
			@RequestParam(value = "EndDate") String EndDate,
			@RequestParam(value = "SiDo") String SiDo){

		boolean isFlag = false;

		// 선택된 주소 세션 저장
		try {
			request.getSession()
					.setAttribute(
							EnVolCheckSession.CHK_ADDRESS.getSessionName(),
							chk_address);
			request.getSession()
					.setAttribute(
							EnVolCheckSession.CHK_ABILITY.getSessionName(),
							chk_ability);
			request.getSession().setAttribute(
					EnVolCheckSession.CHK_SEX.getSessionName(), chk_sex);
			request.getSession().setAttribute(
					EnVolCheckSession.STARTDATE.getSessionName(), StartDate);
			request.getSession().setAttribute(
					EnVolCheckSession.ENDDATE.getSessionName(), EndDate);
			request.getSession().setAttribute(
					EnVolCheckSession.SIDO.getSessionName(), SiDo);
			isFlag = true;
		} catch (Exception e) {
			isFlag = false;
		}
		System.out.println("Session Flag: " + isFlag);
		return isFlag;
	}

	@RequestMapping("MapVolunteer.do")
	public @ResponseBody JSONObject MapVolunteer(
			@RequestParam(value = "keyword", defaultValue = "") String keyword)
	{
		
		System.out.println("검색 키워드 들어옴 :"+keyword);
		
		
		JSONObject ob=new JSONObject();
		
		
		List<Volunteer> vs=volunteerManager.selectByPlace(keyword);	
		
		ob.put("volunteer", vs);
		
	return ob;
	}
	
}
