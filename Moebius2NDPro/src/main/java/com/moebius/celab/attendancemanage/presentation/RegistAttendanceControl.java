package com.moebius.celab.attendancemanage.presentation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;




import com.moebius.celab.attendancemanage.business.AttendanceManager;
import com.moebius.celab.attendancemanage.common.Attendance;
import com.moebius.celab.membermanage.memberpersonmanage.business.MemberManager;
import com.moebius.celab.membermanage.memberpersonmanage.common.Member;
import com.moebius.celab.volunteermanage.business.VolunteerManager;
import com.moebius.celab.volunteermanage.business.VolunteerManager2;
import com.moebius.celab.volunteermanage.common.Volunteer;

@Controller
public class RegistAttendanceControl {

	@Autowired
	AttendanceManager attendanceManager;

	@Autowired
	VolunteerManager volunteerManager;
	
	@Autowired
	VolunteerManager2 volunteerManager2;

	@Autowired
	MemberManager memberManager;

	@Autowired
	MemberManager memberPersonManager;

	@RequestMapping(value = "RegistAttendance.do", method = RequestMethod.POST)
	public @ResponseBody String registMember(@RequestBody Attendance attendance)
			throws ParseException {

		int volunteerNo = attendance.getVolunteer().getNo();
		Volunteer volunteer = volunteerManager.selectByNo(volunteerNo);
		attendance.setVolunteer(volunteer);

		int memberNo = attendance.getMember().getIdno();
		attendance.setMember(memberManager.MemberSelect(memberNo));

		System.out.println("volunteerNo :" + attendance.getVolunteer().getNo());
		System.out.println("MemberName :" + attendance.getMember().getName());

		System.out.println("attendance Date:" + attendance.getDate());
		String AttendanceCheck = attendanceManager.selectByDate(
				attendance.getDate(), attendance.getVolunteer(),
				attendance.getMember());

		// 이미 출석체크가 완료된 날 일 경우 FALSE
		if (AttendanceCheck.equals("false")) {
			System.out.println("Fail");
			return "fail";
		}

		else {
			attendanceManager.registAttendance(attendance);
			
			SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd");
		    
			Date dStartDate = transFormat.parse(volunteer.getStartDate());
			Date dEndDate = transFormat.parse(volunteer.getEndDate());
			long diff = dEndDate.getTime() - dStartDate.getTime();			
			long diffDays = diff / (24 * 60 * 60 * 1000);
			
			List<Attendance> attendances = new ArrayList<Attendance>();
			
			for (Member member : volunteer.getMembers()){
				attendances = attendanceManager.selectByMember(volunteer,member);
			}
		    
			System.out.println("diffDays :"+ diffDays);
			System.out.println("Regi attendances.size() :"+ attendances.size());
			
			if (attendances.size() == diffDays+1) {
				//포인트 지급 완료- status 바꿔줌
				volunteer.setStatus(8);
				volunteerManager2.updateVolunteer(volunteer);
				
				for (Member member : volunteer.getMembers()) {
					attendances = attendanceManager.selectByAttendance(
							volunteer, member, "출석");
					member.setPoint(member.getPoint() + attendances.size()*10);
					memberManager.memberUpdate(member);
					System.out.println(member.getName()+"님의 포인트 지급 완료");
				}
				return "point";
			}
		
			return "attendance";
		}
	}

}