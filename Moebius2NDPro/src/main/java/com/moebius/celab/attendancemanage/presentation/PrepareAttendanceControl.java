package com.moebius.celab.attendancemanage.presentation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.moebius.celab.attendancemanage.business.AttendanceManager;
import com.moebius.celab.attendancemanage.common.Attendance;
import com.moebius.celab.membermanage.memberpersonmanage.common.Member;
import com.moebius.celab.volunteermanage.business.VolunteerManager;
import com.moebius.celab.volunteermanage.common.Volunteer;

@Controller
public class PrepareAttendanceControl {
	@Autowired
	VolunteerManager volunteerManager = new VolunteerManager();

	@Autowired
	AttendanceManager attendanceManager = new AttendanceManager();

	int listSize=3;
	@RequestMapping("prepareAttendance.do")
	ModelAndView prepareAttendance(@RequestParam("date") String date,
			@RequestParam("no") int no) throws ParseException {

		String check = "false";
		Volunteer volunteer = volunteerManager.selectByNo(no);
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd");
		String currentDate = transFormat.format(calendar.getTime());

		Date dEndDate = transFormat.parse(volunteer.getEndDate());
		Date dCurrentDate = transFormat.parse(currentDate);

		ModelAndView mv = new ModelAndView();
		System.out.println("Date :" + date);

		while (check.equals("false")) {
			Date dDate = transFormat.parse(date);
			calendar.setTime(dDate);
			System.out.println("Date :" + date);
			check = attendanceManager.selectByDate1(date, volunteer);
			
			if(check.equals("false")){
				calendar.add(Calendar.DATE, 1);
				date = transFormat.format(calendar.getTime());
			}
		}

		Date dDate1 = transFormat.parse(date);
		int compare1 = dCurrentDate.compareTo(dDate1);
		int compare2 = dEndDate.compareTo(dDate1);

		if (compare1 >= 0 && compare2 >= 0) {
			mv.addObject("result", "true");
			mv.addObject("date", date); // 검색한 봉사활동 날짜
			mv.addObject("volunteer", volunteer); // 봉사활동 번호
			mv.addObject("membersNum", volunteer.getMembers().size()); // 봉사활동
			mv.addObject("members", sortMember(volunteer));// 봉사활동의 멤버
		} else {// 그외의 경우
			mv.addObject("volunteer", volunteer); // 봉사활동 번호
			mv.addObject("result", "false1");
			System.out.println("Fail-3");
		}
		mv.setViewName("/Manage/attendancebook.jsp");
		return mv;
	}

	@RequestMapping("prepareStatusAttendance.do")
	ModelAndView prepareStatusAttendance(@RequestParam("no") int no,
							@RequestParam(value = "listno", defaultValue = "1") int listno) {
		System.out.println("no :" + no);

		Volunteer volunteer = new Volunteer();
		volunteer = volunteerManager.selectByNo(no);
		List<Object> datelist = attendanceManager.selectByAllDate(volunteer.getNo());
		List<Object[]> attendances = new ArrayList<Object[]>();
		attendances = attendanceManager.selectByVolunteer(volunteer,datelist);
		System.out.println("attendances Size :" + attendances.size());
		
	
		double pageSize = Math.ceil(datelist.size() / (double) listSize);

		List<Attendance> attendances1 = new ArrayList<Attendance>();
		//attendances1 = attendanceManager.selectByVolunteer1(volunteer);

		ModelAndView mv = new ModelAndView();

		mv.addObject("listSize", listSize);
		mv.addObject("pageSize", pageSize);
		mv.addObject("listno", listno);
		mv.addObject("volunteer", volunteer);
		mv.addObject("datelist", datelist);
		mv.addObject("attendances", attendances);
		//mv.addObject("attendances1", attendances1);
		mv.setViewName("/Manage/attendanceStatus.jsp");

		return mv;
	}

	@RequestMapping("prepareStatusAttendance1.do")
	ModelAndView prepareStatusAttendance1(@RequestParam("no") int no) {
		System.out.println("no :" + no);

		Volunteer volunteer = new Volunteer();
		volunteer = volunteerManager.selectByNo(no);

		List<Attendance> attendances = new ArrayList<Attendance>();
		attendances = attendanceManager.selectByVolunteer2(volunteer);
		System.out.println("attendances Size :" + attendances.size());

		List<Attendance> attendances1 = new ArrayList<Attendance>();
		attendances1 = attendanceManager.selectByVolunteer1(volunteer);

		ModelAndView mv = new ModelAndView();

		mv.addObject("volunteer", volunteer);
		mv.addObject("attendances", attendances);
		mv.addObject("attendances1", attendances1);
		mv.setViewName("/Manage/attendanceStatus1.jsp");

		return mv;
	}

	List<Member> sortMember(Volunteer volunteer) {
		List<Member> sortedMember = new ArrayList<Member>();

		Iterator<Member> iterator = volunteer.getMembers().iterator();
		while (iterator.hasNext()) {
			Member aMember = iterator.next();
			sortedMember.add(aMember);
		}
		System.out.println("List Size :" + sortedMember.size());

		Comparator<Member> comparator = new Comparator<Member>() {
			@Override
			public int compare(Member arg0, Member arg1) {
				// TODO Auto-generated method stub
				return arg0.getName().compareTo(arg1.getName());
			}

		};
		Collections.sort(sortedMember, comparator);

		return sortedMember;
	}

}
