package com.moebius.celab.attendancemanage.presentation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.moebius.celab.attendancemanage.business.AttendanceManager;
import com.moebius.celab.membermanage.memberpersonmanage.common.Member;
import com.moebius.celab.volunteermanage.business.VolunteerManager;
import com.moebius.celab.volunteermanage.common.Volunteer;

@Controller
public class SearchAttendanceControl {
	@Autowired
	VolunteerManager volunteerManager;

	@Autowired
	AttendanceManager attendanceManager;

	@RequestMapping("searchAttendance.do")
	ModelAndView searchAttendance(@RequestParam("date") String date,
			@RequestParam("no") int no) throws ParseException {

		System.out.println("Date :" + date);
		Volunteer volunteer = volunteerManager.selectByNo(no);

		String AttendanceCheck = attendanceManager.selectByDate1(date,
				volunteer);

		ModelAndView mv = new ModelAndView();
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd");
		String currentDate = transFormat.format(calendar.getTime());

		Date dStartDate = transFormat.parse(volunteer.getStartDate());
		Date dEndDate = transFormat.parse(volunteer.getEndDate());
		Date dCurrentDate = transFormat.parse(currentDate);
		Date dDate = transFormat.parse(date);
		int compare = dStartDate.compareTo(dDate);

		// 이미 출석체크가 완료된 날 일 경우 FALSE
		if (AttendanceCheck.equals("false")) {
			mv.addObject("volunteer", volunteer); // 봉사활동 번호
			mv.addObject("result", "false");
			System.out.println("Fail");
		}
		// 봉사 시작날짜보다 뒤면서,
		else if (compare <= 0) {
			// 오늘날짜보다 전이면서 봉사 끝나는 날보다 전인 경우 TRUE
			int compare1 = dCurrentDate.compareTo(dDate);
			int compare2 = dEndDate.compareTo(dDate);

			if (compare1 >= 0 && compare2 >= 0) {
				mv.addObject("result", "true");

				mv.addObject("date", date); // 검색한 봉사활동 날짜
				mv.addObject("volunteer", volunteer); // 봉사활동 번호
				mv.addObject("membersNum", volunteer.getMembers().size()); // 봉사활동 참여자 수
				mv.addObject("members", sortMember(volunteer));// 봉사활동의 멤버

				System.out.println("Sucess");

			}// 오늘보다 뒤거나 봉사 끝나는 날 보다 뒤일 경우 FALSE1
			else {
				mv.addObject("volunteer", volunteer); // 봉사활동 번호
				mv.addObject("result", "false1");
				System.out.println("Fail-1");
			}
		}

		else {// 그외의 경우
			mv.addObject("volunteer", volunteer); // 봉사활동 번호
			mv.addObject("result", "false2");
			System.out.println("Fail-2");
		}

		mv.setViewName("/Manage/attendancebook.jsp");

		return mv;

	}

	@RequestMapping("forwardAttendance.do")
	ModelAndView forwardAttendance(@RequestParam("date") String date,
			@RequestParam("no") int no) throws ParseException {

		String check = "false";
		Volunteer volunteer = volunteerManager.selectByNo(no);
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd");
		String currentDate = transFormat.format(calendar.getTime());

		Date dEndDate = transFormat.parse(volunteer.getEndDate());
		Date dCurrentDate = transFormat.parse(currentDate);

		ModelAndView mv = new ModelAndView();
		System.out.println("Date :" + date);

		while (check.equals("false")) {
			Date dDate = transFormat.parse(date);
			calendar.setTime(dDate);
			calendar.add(Calendar.DATE, 1);
			date = transFormat.format(calendar.getTime());
			System.out.println("Date :" + date);
			check = attendanceManager.selectByDate1(date, volunteer);
		}

		Date dDate1 = transFormat.parse(date);

		int compare1 = dCurrentDate.compareTo(dDate1);
		int compare2 = dEndDate.compareTo(dDate1);

		if (compare1 >= 0 && compare2 >= 0) {
			mv.addObject("result", "true");
			mv.addObject("date", date); // 검색한 봉사활동 날짜
			mv.addObject("volunteer", volunteer); // 봉사활동 번호
			mv.addObject("membersNum", volunteer.getMembers().size()); // 봉사활동
			mv.addObject("members", sortMember(volunteer));// 봉사활동의 멤버
		} else {// 그외의 경우
			mv.addObject("volunteer", volunteer); // 봉사활동 번호
			mv.addObject("result", "false1");
			System.out.println("Fail-3");
		}
		mv.setViewName("/Manage/attendancebook.jsp");

		return mv;
	}
	
	@RequestMapping("backwardAttendance.do")
	ModelAndView backwardAttendance(@RequestParam("date") String date,
			@RequestParam("no") int no) throws ParseException {

		String check = "false";
		Volunteer volunteer = volunteerManager.selectByNo(no);
		
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd");

		Date dStartDate = transFormat.parse(volunteer.getStartDate());

		ModelAndView mv = new ModelAndView();
		System.out.println("Date :" + date);

		while (check.equals("false")) {
			Date dDate = transFormat.parse(date);
			calendar.setTime(dDate);
			calendar.add(Calendar.DATE, -1);
			date = transFormat.format(calendar.getTime());
			System.out.println("Date :" + date);
			check = attendanceManager.selectByDate1(date, volunteer);
		}

		Date dDate1 = transFormat.parse(date);

		int compare1 = dStartDate.compareTo(dDate1);

		if (compare1 <= 0) {
			mv.addObject("result", "true");
			mv.addObject("date", date); // 검색한 봉사활동 날짜
			mv.addObject("volunteer", volunteer); // 봉사활동 번호
			mv.addObject("membersNum", volunteer.getMembers().size()); // 봉사활동
			mv.addObject("members", sortMember(volunteer));// 봉사활동의 멤버
		} else {// 그외의 경우
			mv.addObject("volunteer", volunteer); // 봉사활동 번호
			mv.addObject("result", "false2");
			System.out.println("Fail-3");
		}
		mv.setViewName("/Manage/attendancebook.jsp");

		return mv;
	}
	
	List<Member> sortMember(Volunteer volunteer){
		List<Member> sortedMember = new ArrayList<Member>();
		
		Iterator<Member> iterator = volunteer.getMembers().iterator();
		while (iterator.hasNext()) {
			Member aMember = iterator.next();
			sortedMember.add(aMember);
		}
		System.out.println("List Size :"+ sortedMember.size());
		
		Comparator<Member> comparator = new Comparator<Member>() {
			@Override
			public int compare(Member arg0, Member arg1) {
				// TODO Auto-generated method stub
				return arg0.getName().compareTo(arg1.getName());
			}

		};
		Collections.sort(sortedMember, comparator);
		
		return sortedMember;
	} 
	
	
}