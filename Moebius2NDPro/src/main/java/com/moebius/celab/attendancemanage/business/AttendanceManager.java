package com.moebius.celab.attendancemanage.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.moebius.celab.attendancemanage.common.Attendance;
import com.moebius.celab.attendancemanage.dataservice.AttendanceDAO;
import com.moebius.celab.membermanage.memberpersonmanage.common.Member;
import com.moebius.celab.volunteermanage.common.Volunteer;

@Service
public class AttendanceManager {
	
	@Autowired
	AttendanceDAO attendanceDAO;
	
	@Transactional
    public int registAttendance(Attendance attendance) {
    	
    	return attendanceDAO.registAttendance(attendance); 
    }
	
	@Transactional
	public String selectByDate(String date, Volunteer volunteer, Member member) {
		
		return attendanceDAO.selectByDate(date, volunteer, member);
	}
	
	@Transactional
	public String selectByDate1(String date, Volunteer volunteer) {
		
		return attendanceDAO.selectByDate1(date, volunteer);
	}
	
	@Transactional
	public List<Object[]> selectByVolunteer(Volunteer volunteer,List<Object> datelist){
		String str="select distinct a.no as member,a.name from member a left join attendance b on a.no=b.MEMBER_no where b.Volunteer_no="+volunteer.getNo();
		
		//List<Attendance> datelist = attendanceDAO.selectByAllDate(volunteer.getNo());
		System.out.println("사이즈"+datelist.size());
		for(int i=0;i<datelist.size();i++){
		str="select distinct a.* ,b.status as '"+datelist.get(i)+"' from ("+str+") a left join (select Member_no,status,date,volunteer_no from attendance) b on a.member=b.Member_no  where b.Volunteer_No="+volunteer.getNo()+" and b.date='"+datelist.get(i)+"'";	
			
		}

	System.out.println("완성쿼리"+str);
		//str= "select a.*, b.status as"; 
		return attendanceDAO.selectByVolunteer(str);
	}
	@Transactional
	public List<Object> selectByAllDate(int no){
		
		return attendanceDAO.selectByAllDate(no);
	}
	
	@Transactional
	public List<Attendance> selectByVolunteer1(Volunteer volunteer){
		
		return attendanceDAO.selectByVolunteer1(volunteer);
	}
	@Transactional
	public List<Attendance> selectByVolunteer2(Volunteer volunteer){
		
		return attendanceDAO.selectByVolunteer2(volunteer);
	}
	
	@Transactional
	public List<Attendance> selectByAttendance(Volunteer volunteer, Member member, String status){
		
		return attendanceDAO.selectByAttendance(volunteer, member, status);
	}
	
	@Transactional
	public List<Attendance> selectByMember(Volunteer volunteer, Member member){
		
		return attendanceDAO.selectByMember(volunteer, member);
	}
	

}
