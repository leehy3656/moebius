package com.moebius.celab.attendancemanage.dataservice;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.moebius.celab.attendancemanage.common.Attendance;
import com.moebius.celab.membermanage.memberpersonmanage.common.Member;
import com.moebius.celab.volunteermanage.common.Volunteer;

@Repository
public class AttendanceDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public int registAttendance(Attendance attendance) {
		Session session = sessionFactory.getCurrentSession();

		int no = (Integer) session.save(attendance);
		return no;
	}

	public String selectByDate(String date, Volunteer volunteer, Member member) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Attendance.class);
		criteria.add(Restrictions.eq("date", date))
				.add(Restrictions.eq("volunteer", volunteer))
				.add(Restrictions.eq("member", member));

		List<Member> attendance = criteria.list();

		if (attendance.size() != 0) {
			return "false";
		} else {
			return "true";
		}
	}

	public String selectByDate1(String date, Volunteer volunteer) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Attendance.class);
		criteria.add(Restrictions.eq("date", date)).add(
				Restrictions.eq("volunteer", volunteer));

		List<Member> attendance = criteria.list();

		if (attendance.size() != 0) {
			return "false";
		} else {
			return "true";
		}
	}

	public List<Attendance> selectByVolunteer2(Volunteer volunteer) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session
				.createCriteria(Attendance.class)
				.add(Restrictions.eq("volunteer", volunteer))
				.addOrder(Order.asc("date"))
				.setResultTransformer(
						CriteriaSpecification.DISTINCT_ROOT_ENTITY);

		List<Attendance> attendances = criteria.list();

		return attendances;
	}
	public List<Object[]> selectByVolunteer(String str) {
		Session session = sessionFactory.getCurrentSession();
		

		/* return session.createSQLQuery(
				str
				).addEntity(Object.class).list();*/
		System.out.println("쿼리문:"+ str);
		 return session.createSQLQuery(
				str
				).list();
	
	}

	public List<Attendance> selectByVolunteer1(Volunteer volunteer) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session
				.createCriteria(Attendance.class)
				.add(Restrictions.eq("volunteer", volunteer))
				.addOrder(Order.asc("member"))
				.addOrder(Order.asc("date"))
				.setResultTransformer(
						CriteriaSpecification.DISTINCT_ROOT_ENTITY);

		List<Attendance> attendances = criteria.list();

		return attendances;
	}

	public List<Attendance> selectByAttendance(Volunteer volunteer,
			Member member, String status) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session
				.createCriteria(Attendance.class)
				.add(Restrictions.eq("volunteer", volunteer))
				.add(Restrictions.eq("member", member))
				.add(Restrictions.eq("status", status))
				.setResultTransformer(
						CriteriaSpecification.DISTINCT_ROOT_ENTITY);

		List<Attendance> attendances = criteria.list();

		return attendances;
	}

	public List<Attendance> selectByMember(Volunteer volunteer, Member member) {
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session
				.createCriteria(Attendance.class)
				.add(Restrictions.eq("volunteer", volunteer))
				.add(Restrictions.eq("member", member))
				.setResultTransformer(
						CriteriaSpecification.DISTINCT_ROOT_ENTITY);

		List<Attendance> attendances = criteria.list();

		return attendances;
	}
public List<Object> selectByAllDate(int no){
		
		Session session = sessionFactory.getCurrentSession();
		
		System.out.println("들어옴"+no);
		return session.createSQLQuery(
				"select distinct date from attendance where volunteer_no ="+no
				).list();
	}

}
