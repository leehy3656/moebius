package com.moebius.celab.shoppingmanage.dataserivce;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.moebius.celab.shoppingmanage.common.Category;
import com.moebius.celab.shoppingmanage.common.CategoryItem;
import com.moebius.celab.shoppingmanage.common.Shopping;






@Repository
public class CategoryDAO {
	@Autowired
	private SessionFactory sessionFactory;
	
	 public List<Category> allShoppingCategory(int no) {
	        //카테고리 다가져오기
	    	Session session = sessionFactory.getCurrentSession();
	    	
	    	List<Category> shoppingcategorys = session.createSQLQuery(
	    			"select * from Category where Category_no="+no
	    			).addEntity(Category.class).list();
	    	return shoppingcategorys;
	    }
	 
	 public List<CategoryItem> CategoryItemList(int no) {
	        //카테고리 다가져오기
	    	Session session = sessionFactory.getCurrentSession();
	    	
	    	System.out.println("디비");
	    	
	    	return session.createSQLQuery(
	    			"select distinct a.* from categoryitem a left outer join categorytable b on a.no=b.categoryitem_no where b.category_no="+no
	    			).addEntity(CategoryItem.class).list();
	    }
	 public CategoryItem selectCategoryItem(int no) {
	        //카테고리 다가져오기
	    	Session session = sessionFactory.getCurrentSession();
	    	 List<CategoryItem> a=session.createSQLQuery(
	    			"select * from CategoryItem where no="+no
	    			).addEntity(CategoryItem.class).list();
	    	
	    	
	    	return a.get(0);
	    }
	 
	 public Category selectByName(String name) {
	        //카테고리 이름 검색
		 Session session = sessionFactory.getCurrentSession();
		  List<Category> a=(List<Category>) session.createSQLQuery("select * from Category where name='"+name+"'").addEntity(Category.class).list();
	    	
		  return a.get(0);
	    }
	 public CategoryItem selectByNameItem(String name) {
	        //카테고리아이템 이름 검색
		 Session session = sessionFactory.getCurrentSession();
		  List<CategoryItem> a=(List<CategoryItem>) session.createSQLQuery("select * from CategoryItem where name='"+name+"'").addEntity(CategoryItem.class).list();
	    	
		  return a.get(0);
	    }
	 
	 //selectBySuperCategory
	 public Category selectBySuperCategory(int no) {
	        //카테고리 아이템의 상위카테고리 검색
		 Session session = sessionFactory.getCurrentSession();
		 List<Category> a=session.createSQLQuery(
					"select a.* from category a left outer join categorytable b on a.no=b.category_no where b.categoryitem_no="+no	 
					    			).addEntity(Category.class).list();
		 
		 return  a.get(0);
	    	
		  
	    }
	 public Category selectBySuper2Category(int no) {
	        //카테고리의 상위카테고리 검색
		 Session session = sessionFactory.getCurrentSession();
		 List<Category> a=session.createSQLQuery(
					"select * from Category where no=(select Category_no from Category where no="+no+")"	 
					    			).addEntity(Category.class).list();
		 
		 return  a.get(0);
	    	
		  
	    }
	
	 
}

