package com.moebius.celab.shoppingmanage.presentation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.moebius.celab.common.Imageupload;
import com.moebius.celab.membermanage.memberpersonmanage.common.Member;
import com.moebius.celab.shoppingmanage.business.CategoryManager;
import com.moebius.celab.shoppingmanage.business.ShoppingManager;
import com.moebius.celab.shoppingmanage.common.Category;
import com.moebius.celab.shoppingmanage.common.CategoryItem;
import com.moebius.celab.shoppingmanage.common.Instocklist;
import com.moebius.celab.shoppingmanage.common.Shopping;
import com.moebius.celab.sponsormanage.common.Sponsor;

@Controller
public class pointproductcontorller {
@Autowired
private ShoppingManager shoppingManager;
@Autowired
private CategoryManager categoryManager;
	
	@RequestMapping("pointproductload.do")
	public ModelAndView ArticleLoad(@RequestParam(value="listno",defaultValue="1") int listno,
									@RequestParam(value="category",defaultValue="1")int category,
									@RequestParam(value="categorysub",defaultValue="0")int categorysub,
									@RequestParam(value="keyword",defaultValue="")String keyword,
									@RequestParam(value = "check_shopping", defaultValue = "0") int check_shopping,
									HttpServletRequest request
									) {
		
		if(check_shopping == 1){
			request.getSession().removeAttribute("category");
			request.getSession().removeAttribute("categorysub");
			request.getSession().removeAttribute("keyword");
		}
		
		
		
		
		
		List<Integer> serachNo=new ArrayList<Integer>();
		int listSize = 8;
		
		int receive1=1;
		int receive2=0;
		String receive3="";
		
		if(request.getSession().getAttribute("category")==null)
		{
			
			request.getSession().setAttribute("category",1);
			request.getSession().setAttribute("categorysub",0);
			request.getSession().setAttribute("keyword","");
			
		}
		
		
		
		
	/*	
		if(category!=1)
		{
			request.getSession().setAttribute("category",category);
		}
		if(categorysub!=0)
		{
			request.getSession().setAttribute("categorysub",categorysub);
		}
		if(!keyword.equals(""))
		{
			request.getSession().setAttribute("keyword",keyword);
		}
		*/
		
		if(category!=1||categorysub!=0||!keyword.equals("")){
			request.getSession().setAttribute("category",category);
			request.getSession().setAttribute("categorysub",categorysub);
			request.getSession().setAttribute("keyword",keyword);
			
		}
		
		
		
		
		
		 receive1 =  (Integer) request.getSession().getAttribute("category");
			
		 receive2 =  (Integer) request.getSession().getAttribute("categorysub");
		
		 receive3 = (String) request.getSession().getAttribute("keyword");
		
		
		
		if(receive2==0){
			List<CategoryItem> cgis=categoryManager.twoCategory(receive1);
			
			for(CategoryItem cos:cgis){
				
				serachNo.add(cos.getNo());
				
			}
		}else{
			serachNo.add(receive2);
			
		}
		/*request.getSession().setAttribute("category",category);
		request.getSession().setAttribute("categorysub",categorysub);
		request.getSession().setAttribute("keyword",keyword);*/
		
		
		List<Category> categorys=categoryManager.firstCategory(1);
		List<CategoryItem> subcategorys=categoryManager.twoCategory(receive1);
		
		List<Shopping> shoppings = shoppingManager.CategorySerachShopping(serachNo,receive3);
		
		
		
		double pageSize = Math.ceil(shoppings.size()/(double)listSize);
	
		ModelAndView mv = new ModelAndView();
		
		mv.addObject("category",categorys);
		mv.addObject("subcategory",subcategorys);
		mv.addObject("listSize",listSize);
		mv.addObject("pageSize",pageSize);
		mv.addObject("listno",listno);
		mv.addObject("shoppinglist",shoppings);
		
		
		
		if(receive1==1&&receive2==0){
			List<String> sildesrc=new ArrayList<String>();
		for(int i = 0;i<3;i++){	
		
			sildesrc.add(shoppings.get(i).getImagesrc());
			
		}
		mv.addObject("sildesrc",sildesrc);
		mv.setViewName("/shopping/shoppingselect.jsp");
		}else
		{
			mv.setViewName("/shopping/shoppingSerach.jsp");
		}
		
		
		return mv;
	}
//pointproductdetail.do
	@RequestMapping("pointproductdetail.do")
	public ModelAndView pointproductdetail(@RequestParam("no")int no){
		ModelAndView mv=new ModelAndView();
		Shopping shopping=shoppingManager.selectByNo(no);
		mv.addObject("pointproduct",shopping);
		mv.setViewName("/shopping/shoppingdetail.jsp");
		
		return mv;
	}
	
	@RequestMapping("buyitem.do")
	public ModelAndView buyitem(@RequestParam("itemno")int no,@RequestParam("detailquantity")int quantity,
			@RequestParam("place")String place,@RequestParam("place2")String place2,
			HttpServletRequest request){

		String str=shoppingManager.buyitem((Member)request.getSession().getAttribute("member"), 
				shoppingManager.selectByNo(no), quantity,place,place2);
		ModelAndView mv=new ModelAndView();
		
		
		mv.setViewName("redirect:pointproductload.do");
		return mv;
	}
	@RequestMapping("buyWindow.do")
	public ModelAndView buyWindow(@RequestParam("itemno")int no,@RequestParam("detailquantity")int quantity,
			HttpServletRequest request){
		
		Shopping shopping = shoppingManager.selectByNo(no);
		//Member member=(Member)request.getSession().getAttribute("member");
		
		System.out.println(shopping.getName());
		
		ModelAndView mv=new ModelAndView();
		
		
		mv.addObject("pointproduct",shopping);
		//mv.addObject("member",member);
		mv.addObject("quantity",quantity);
		mv.setViewName("/shopping/buyShopping.jsp");
		
		return mv;
	}
	
	@RequestMapping("checkPoint.do")
	@ResponseBody
	public String buyWindow(@RequestParam("price")int price,
			HttpServletRequest request){
		//System.out.println("컨잼");
		String str="1";
		Member member=(Member)request.getSession().getAttribute("member");
		
		if(member.getPoint()>=price){
		str="ok";	
		}
		
		return str;
	}
	@RequestMapping("shoppingUpload.do")
	@ResponseBody
	public String shoppingUpload(
			HttpServletRequest request) throws IOException{

		System.out.println("받음");
		
		String shoppingJson=request.getParameter("shoppingJson");

		
		JSONObject ob=JSONObject.fromObject(JSONSerializer.toJSON(shoppingJson));
		
		Imageupload registproductimage= new Imageupload();

		
		String jsonString = ob.getString("image");

		
		String imagename=registproductimage.uploadimage(jsonString,"/productimages/");
		
		
		System.out.println("이미지 네임: "+imagename);
		
		
		
		ObjectMapper mapper = new ObjectMapper(); 
		String shoppingstr=ob.getString("shopping");
		Shopping shopping=new Shopping();
		
		try {
			
			shopping = mapper.readValue(shoppingstr, Shopping.class);
			//제이슨 다시 객체로
			
		} catch (IOException e) {e.printStackTrace();}
		
		shopping.setImagesrc(imagename);
	
		
		
		String sponsorstr=ob.getString("sponsor");
		Sponsor sponsor=new Sponsor();
		
		try {
			
		sponsor = mapper.readValue(sponsorstr, Sponsor.class);
			//제이슨 다시 객체로
			
		} catch (IOException e) {e.printStackTrace();}
		
		String instockliststr=ob.getString("instocklist");
		Instocklist instocklist=new Instocklist();
		
		try {
			
			instocklist = mapper.readValue(instockliststr, Instocklist.class);
			
			
		} catch (IOException e) {e.printStackTrace();}
			
		
		shopping.setSponsor(sponsor);
		
		instocklist.setShopping(shopping);
		
		
		shoppingManager.registShopping(shopping);
		shoppingManager.registInstock(instocklist);
		
		
		return "성공";
	}
	
	@RequestMapping("shoppingUpdate.do")
	@ResponseBody
	public String shoppingUpdate(
			HttpServletRequest request) throws IOException{

		
		
		String shoppingJson=request.getParameter("shoppingJson");

		
		JSONObject ob=JSONObject.fromObject(JSONSerializer.toJSON(shoppingJson));

		
		
		
		ObjectMapper mapper = new ObjectMapper(); 
		String shoppingstr=ob.getString("shopping");
		Shopping shopping=new Shopping();
		
		System.out.println("쇼핑"+shoppingstr);
		
		
		try {
			
			shopping = mapper.readValue(shoppingstr, Shopping.class);
			//제이슨 다시 객체로
			
		} catch (IOException e) {e.printStackTrace();}

		
		String sponsorstr=ob.getString("sponsor");
		Sponsor sponsor=new Sponsor();
		System.out.println("쇼핑"+sponsorstr);
		try {
			
	sponsor = mapper.readValue(sponsorstr, Sponsor.class);
			//제이슨 다시 객체로
			
		} catch (IOException e) {e.printStackTrace();}
		
		String instockliststr=ob.getString("instocklist");
		Instocklist instocklist=new Instocklist();
		
		try {
			
			instocklist = mapper.readValue(instockliststr, Instocklist.class);
			
			
		} catch (IOException e) {e.printStackTrace();}
			
		
		shopping.setSponsor(sponsor);
		
		instocklist.setShopping(shopping);
		
		
		shoppingManager.updateShopping(shopping);
		shoppingManager.registInstock(instocklist);
		
		
		return "성공";
	}
		
		
		@RequestMapping("shoppingUpdate2.do")
		@ResponseBody
		public String shoppingUpdate2(
				HttpServletRequest request) throws IOException{
			System.out.println("받음");
			
			String shoppingJson=request.getParameter("shoppingJson");

			
			JSONObject ob=JSONObject.fromObject(JSONSerializer.toJSON(shoppingJson));

			
			ObjectMapper mapper = new ObjectMapper(); 
			String shoppingstr=ob.getString("shopping");
			Shopping shopping=new Shopping();
			
			try {
				
				shopping = mapper.readValue(shoppingstr, Shopping.class);
				//제이슨 다시 객체로
				
			} catch (IOException e) {e.printStackTrace();}
			
			
		
			
			
			String sponsorstr=ob.getString("sponsor");
			Sponsor sponsor=new Sponsor();
			
			try {
				
		sponsor = mapper.readValue(sponsorstr, Sponsor.class);
				//제이슨 다시 객체로
				
			} catch (IOException e) {e.printStackTrace();}
			
				
			
			String jsonString = ob.getString("image");
			
			if(!jsonString.equals("a")){

			Imageupload registproductimage= new Imageupload();
			String imagename=registproductimage.uploadimage(jsonString,"/productimages/");
			shopping.setImagesrc(imagename);
			}
			
			
			
			
			
			shopping.setSponsor(sponsor);
			
			
			
			
			shoppingManager.updateShopping(shopping);
			
			
			
			return "성공";
			
			
		
		}
}

