package com.moebius.celab.shoppingmanage.common;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "Category")
public class Category {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="no")
	int no;
	@Column(name="name")
	String name;
	@ManyToOne
	@JoinColumn(name="category_no")
	Category pcategory;
	
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy="pcategory")
	List<Category> category;

	@OneToMany(targetEntity=CategoryItem.class,cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	@JoinTable(name = "categoryItem", 
     joinColumns = { @JoinColumn(name = "category_no") }, 
     inverseJoinColumns = { @JoinColumn(name = "categoryItem_no") })
	List<CategoryItem> catagoryItem;
	public int getNo() {
		return no;
	}

	public void setNo(int no) {
		this.no = no;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Category getPcategory() {
		return pcategory;
	}

	public void setPcategory(Category pcategory) {
		this.pcategory = pcategory;
	}

	public List<Category> getCategory() {
		return category;
	}

	public void setCategory(List<Category> category) {
		this.category = category;
	}

	public List<CategoryItem> getCatagoryItem() {
		return catagoryItem;
	}

	public void setCatagoryItem(List<CategoryItem> catagoryItem) {
		this.catagoryItem = catagoryItem;
	}
}

