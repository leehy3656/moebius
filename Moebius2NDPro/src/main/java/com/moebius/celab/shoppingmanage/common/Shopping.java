package com.moebius.celab.shoppingmanage.common;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import com.moebius.celab.sponsormanage.common.Sponsor;

@Entity
@Table(name = "Shopping")
public class Shopping {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "no")
	int no;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "categoryitem_no")
	CategoryItem category;

	@Column(name = "name")
	String name;
	@Column(name = "price")
	int price;
	@Column(name = "totalquantity")
	int quantity;
	@Column(name = "imagesrc")
	String imagesrc;
	
	@Column(name = "content")
	String content;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "sponsor_no")
	Sponsor sponsor;

	@OneToMany(mappedBy="shopping", fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	 @JsonManagedReference
	Set<Output> output = null;
	
	
	
	
	
	public void addOutput(Output output){
		
		output.setShopping(this);
		if(this.output == null){
			this.output = new HashSet<Output>();
		}
		this.setQuantity(quantity-output.getQuantity());
		
		this.output.add(output);
	}
	
	public Set<Output> getOutput() {
		return output;
	}

	public void setOutput(Set<Output> output) {
		this.output = output;
	}
	public Sponsor getSponsor() {
		return sponsor;
	}
	public void setSponsor(Sponsor sponsor) {
		this.sponsor = sponsor;
	}
	public CategoryItem getCategory() {
		return category;
	}
	public void setCategory(CategoryItem category) {
		this.category = category;
	}
	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getImagesrc() {
		return imagesrc;
	}
	public void setImagesrc(String imagesrc) {
		this.imagesrc = imagesrc;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
}
