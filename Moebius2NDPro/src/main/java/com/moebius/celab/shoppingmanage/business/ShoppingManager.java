package com.moebius.celab.shoppingmanage.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.moebius.celab.membermanage.memberpersonmanage.business.MemberManager;
import com.moebius.celab.membermanage.memberpersonmanage.common.Member;
import com.moebius.celab.shoppingmanage.common.Category;
import com.moebius.celab.shoppingmanage.common.CategoryItem;
import com.moebius.celab.shoppingmanage.common.Instocklist;
import com.moebius.celab.shoppingmanage.common.Output;
import com.moebius.celab.shoppingmanage.common.Shopping;
import com.moebius.celab.shoppingmanage.dataserivce.ShoppingDAO;
import com.moebius.celab.sponsormanage.business.Sponsormanager;
import com.moebius.celab.sponsormanage.common.Sponsor;

@Service
public class ShoppingManager {
	@Autowired
	private ShoppingDAO shoppingDAO;
	@Autowired
	private Sponsormanager sponsorManager;
	@Autowired
	private CategoryManager categoryManager;
	@Autowired
	private OutputManager outputManager;
	@Autowired
	private MemberManager memberManager;
	
	
	 @Transactional
	 public int registInstock(Instocklist instocklist) {
	
		 return shoppingDAO.saveInstocklist(instocklist);
		 
		 
	 }
	 
	 @Transactional
	 public int registShopping(Shopping shopping) {
	
		 return shoppingDAO.saveShopping(shopping);
		 
		 
	 }
	
	
	 @Transactional
	 public List<Sponsor> loadsponsor(){
		 
		 return sponsorManager.selectByall();
	 }
	 @Transactional
	 public Sponsor selectsponsor(String name){
		List<Sponsor> spon=sponsorManager.selectByname(name);
		 
		 return spon.get(0);
	 }
	 
	 
	 @Transactional
	 public List<Category> loadCategory(int no){
		 
		 return categoryManager.firstCategory(no);
	 }
	 @Transactional
	 public List<CategoryItem> twoCategory(int no){
		 
		 return categoryManager.twoCategory(no);
	 }
	
	 
	
	 @Transactional
	 public CategoryItem selectByNoCategory(int no){
		 
		 return categoryManager.selectByNoCategory(no);
	 }
	 @Transactional
	 public Category selectByNameCategory(String name){
		 
		 return categoryManager.selectByNameCategory(name);
	 }
	 @Transactional
	 public CategoryItem selectByNameCategoryItem(String name){
		 
		 return categoryManager.selectByNameCategoryItem(name);
	 }
	 
	 
	 
	/* @Transactional
	 public int registShopping(Shopping shopping,FileDialog dlg) throws Exception{
		 //System.out.println("매니저 시작");
		 
		 uploadimage up=new uploadimage();
		 
		 String str = up.uploadimage(dlg);
		 
		 System.out.println(str);
		 shopping.setImagesrc(str);
		 
		 
		 return shoppingDAO.saveShopping(shopping);
		 
		 
	 }*/
	 @Transactional
	 public void updateShopping(Shopping shopping){
		 
		 shoppingDAO.updateShopping(shopping);
	 }
	 
	 @Transactional
	 public Shopping selectByNo(int no) {
		 
		
		 
		 return shoppingDAO.selectByNo(no);
		 
		 
	 }
	 @Transactional
	 public Category selectBySuperCategory(int no) {
		 
		
		 
		 return categoryManager.selectBySuperCategory(no);
		 
		 
	 }
	 //selectBySuper2Category
	 @Transactional
	 public Category selectBySuper2Category(int no) {
		 
		
		 
		 return categoryManager.selectBySuper2Category(no);
		 
		 
	 }
	 @Transactional
	 public List<Output> loadOutputs(int no) {
		 
		
		 
		 return outputManager.loadOutputs(no);
		 
		 
	 }
	 @Transactional
	 public List<Shopping> serachShopping(String name) {
		 
		
		 
		 return shoppingDAO.serachShopping(name);
		 
		 
	 }
	 @Transactional
	 public List<Shopping> CategorySerachShopping(List<Integer> no,String name) {
		 
		
		 
		 return shoppingDAO.CategorySerachShopping(no, name);
		 
		 
	 }
	 @Transactional
	 public String buyitem(Member member,Shopping shopping ,int quantity,String place,String place2) {
		 String str="1";
		
		 
		if(member.getPoint()>shopping.getPrice()){
			
			member.setPoint(member.getPoint()-shopping.getPrice()*quantity);
			memberManager.memberUpdate(member);
			
			Output out = new Output();
			out.setMember(member);
			out.setPlace(place);
			out.setPlace2(place2);
			out.setQuantity(quantity);
			shopping.addOutput(out);
			
			shoppingDAO.updateShopping(shopping);
			
			str="2";
			
		}
		 
		 
		 
		 return str;
		 
		 
	 }
}
