package com.moebius.celab.shoppingmanage.common;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.moebius.celab.membermanage.memberpersonmanage.common.Member;




@Entity
@Table(name = "Output")

public class Output {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "no")
	private int no;
	@Generated(GenerationTime.ALWAYS) 
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="date",insertable=false,updatable=false)
	private Date date;
	@Column(name = "quantity")
	private int quantity;
	@Column(name = "reason")
	private String reason;
	@Column(name = "place")
	private String place;
	@Column(name = "place2")
	private String place2;
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	 @JsonBackReference
	@JoinColumn(name = "Shopping_no")
	private Shopping shopping;
	
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "Member_no")
	private Member member;
	
	
	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public String getPlace2() {
		return place2;
	}
	public void setPlace2(String place2) {
		this.place2 = place2;
	}
	public Shopping getShopping() {
		return shopping;
	}
	public void setShopping(Shopping shopping) {
		this.shopping = shopping;
	}
	public Member getMember() {
		return member;
	}
	public void setMember(Member member) {
		this.member = member;
	}
	
	
}