package com.moebius.celab.common.mail.business;

import javax.mail.internet.MimeMessage;





import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

 

@Controller

public class MailController {

	

	@Autowired 

	private JavaMailSender mailSender;

	

	

	

	@RequestMapping(value = "mail.do")

	public ModelAndView sendMail(
			@RequestParam("emailTitle")String emailTitle,
			@RequestParam("emailname")String emailname,
			@RequestParam("emailmemo")String emailmemo
			) {

		

		try {
			String from 	= "so123os89@gmail.com";

			String subject	= "질문:";

			String memo="보낸 사람:";
			
			subject=subject+emailTitle;
			
			memo=memo+emailname+"\n"+emailmemo;
			

			MimeMessage message = mailSender.createMimeMessage();

			MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");

			messageHelper.setTo("so123os89@gmail.com");

			messageHelper.setText(memo);

			messageHelper.setFrom(from);

			messageHelper.setSubject(subject);	// 메일제목은 생략이 가능하다

			

			mailSender.send(message);

		} catch(Exception e){

			System.out.println(e);

		}

		ModelAndView mv = new ModelAndView();
		
		
		mv.setViewName("redirect:/MainMoebius.do");
		
		return mv;

	}



}


