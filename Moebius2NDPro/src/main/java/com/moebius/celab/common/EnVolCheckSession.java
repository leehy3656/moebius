package com.moebius.celab.common;

public enum EnVolCheckSession {
	
	CHK_ADDRESS("chk_address"),
	CHK_ABILITY("chk_ability"),
	CHK_SEX("chk_sex"),
	STARTDATE("StartDate"),
	ENDDATE("EndDate"),
	SIDO("SiDo");
	
	String sessionName;
	
	EnVolCheckSession(String sessionName) {
		this.sessionName = sessionName;
	}

	public String getSessionName() {
		return sessionName;
	}
	
	public void setSessionName(String sessionName) {
		this.sessionName = sessionName;
	}
	
}
