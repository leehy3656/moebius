package com.moebius.celab.sponsormanage.common;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.moebius.celab.adminmanage.common.Staff;
import com.moebius.celab.shoppingmanage.common.Shopping;


@Entity(name="com.moebius.celab.shoppingmanage1.common")
@Table(name="Sponsor")
public class Sponsor {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "no")
	private int no;
	@Generated(GenerationTime.ALWAYS) 
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="date",insertable=false,updatable=false)
	private Date date;
	@Column(name = "name")
	private String name;
	@Column(name = "contents")
	private String contents;
	@Column(name = "logoimg")
	private String logoimg;
	@Column(name = "manager")
	private String manager;
	@Column(name = "phone")
	private String phone;
	@Column(name = "logoimgDetail")
	private String logoimgDetail;
	@Column(name = "webaddress")
	private String webaddress;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "staff_no")
	Staff staff;
	
	public String getLogoimgDetail() {
		return logoimgDetail;
	}
	public void setLogoimgDetail(String logoimgDetail) {
		this.logoimgDetail = logoimgDetail;
	}
	public Staff getStaff() {
		return staff;
	}
	public void setStaff(Staff staff) {
		this.staff = staff;
	}
	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getContents() {
		return contents;
	}
	public void setContents(String contents) {
		this.contents = contents;
	}
	public String getManager() {
		return manager;
	}
	public void setManager(String manager) {
		this.manager = manager;
	}
	public String getWebaddress() {
		return webaddress;
	}
	public void setWebaddress(String webaddress) {
		this.webaddress = webaddress;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getLogoimg() {
		return logoimg;
	}
	public void setLogoimg(String logoimg) {
		this.logoimg = logoimg;
	}
	

	
}
