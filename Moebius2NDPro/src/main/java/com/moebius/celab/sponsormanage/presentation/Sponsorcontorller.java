package com.moebius.celab.sponsormanage.presentation;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.moebius.celab.common.Imageupload;
import com.moebius.celab.shoppingmanage.common.Shopping;
import com.moebius.celab.sponsormanage.business.Sponsormanager;
import com.moebius.celab.sponsormanage.common.Sponsor;



@Controller
public class Sponsorcontorller {
	
	@Autowired
	Sponsormanager sponsormanager;
	
	@RequestMapping("sponsorload.do")
	public ModelAndView SponsorBoardLoad(@RequestParam("listno") int listno){
		
	
		List<Sponsor> sponsers = sponsormanager.selectByall();
		System.out.println("확인확인"+sponsers);
		int listSize = 12;
		double pageSize = Math.ceil(sponsers.size()/(double)listSize);
		ModelAndView mv = new ModelAndView();
		mv.addObject("listSize",listSize);
		mv.addObject("pageSize",pageSize);
		mv.addObject("listno",listno);
		mv.addObject("sponserlist",sponsers);
		mv.setViewName("/Sponsor/sponsorselect.jsp");
		return mv;
		
	}
	@RequestMapping("sponsorJsor2.do")
	public String Sponsor2(HttpServletRequest request) {
		String sponsorJson = request.getParameter("sponsorJson2");
		JSONObject ob=JSONObject.fromObject(JSONSerializer.toJSON(sponsorJson));
		
		Imageupload registsponsorimage= new Imageupload();
		Imageupload registsponsorimage2= new Imageupload();
		String jsonString1 = ob.getString("image1");
		String jsonString2 = ob.getString("image2");
		
		String imagename1="default.png";
		try {
			imagename1 = registsponsorimage.uploadimage(jsonString1,"/sponsorimage/");
		} catch (IOException e1) {
			// TODO 자동 생성된 catch 블록
			e1.printStackTrace();
		}
		String imagename2="default.png";
		try {
			imagename2 = registsponsorimage2.uploadimage(jsonString2,"/sponsordetail/");
		} catch (IOException e1) {
			// TODO 자동 생성된 catch 블록
			e1.printStackTrace();
		}
		
		ObjectMapper mapper = new ObjectMapper(); 
		String sponsorstr=ob.getString("sponsor");
		Sponsor sponsor = new Sponsor();
		
		try {
			
			sponsor = mapper.readValue(sponsorstr, Sponsor.class);
			//제이슨 다시 객체로
			
		} catch (IOException e) {e.printStackTrace();}
		
		System.out.println(sponsor.getContents());
		System.out.println(sponsor.getName());
		System.out.println(sponsor.getPhone());

		
		sponsor.setLogoimg(imagename1);
		sponsor.setLogoimgDetail(imagename2);
		sponsormanager.registSponsor(sponsor);
		return "성공";
		
			
		
	}
	@RequestMapping("sponsorUp.do")
	public String Sponsorupdate(HttpServletRequest request) throws IOException{
		String sponsorJson = request.getParameter("sponsorJson");
		JSONObject ob=JSONObject.fromObject(JSONSerializer.toJSON(sponsorJson));
		
		ObjectMapper mapper = new ObjectMapper(); 
		String sponsorstr=ob.getString("sponsor");
		Sponsor sponsor = new Sponsor();
		Imageupload registsponsorimage= new Imageupload();
		try {
			
			sponsor = mapper.readValue(sponsorstr, Sponsor.class);
			//제이슨 다시 객체로
			
		} catch (IOException e) {e.printStackTrace();}
		
		
		
		String jsonString1 = ob.getString("image1");
		String jsonString2 = ob.getString("image2");
		if(!jsonString1.equals("a")){
			
			String imagename1=registsponsorimage.uploadimage(jsonString1,"/sponsorimage/");
			sponsor.setLogoimg(imagename1);
		}
		if(!jsonString2.equals("b")){
			
			String imagename2=registsponsorimage.uploadimage(jsonString2,"/sponsordetail/");
			sponsor.setLogoimgDetail(imagename2);
		}
		
		
		sponsormanager.updateSponsor(sponsor);
		return "성공";
	}
	@RequestMapping("sponsorDelete.do")
	public String Sponsordelete(HttpServletRequest request) throws IOException{
		String sponsorJson = request.getParameter("sponsorJsondelete");
		JSONObject ob=JSONObject.fromObject(JSONSerializer.toJSON(sponsorJson));
		
		ObjectMapper mapper = new ObjectMapper(); 
		String sponsorno=ob.getString("sponsor");
		System.out.println("객체확인"+sponsorno);
		/*int no = Integer.valueOf(sponsorno);*/
		Sponsor sponsor = new Sponsor();
		
		try {
			
			sponsor = mapper.readValue(sponsorno, Sponsor.class);
			//제이슨 다시 객체로
			
		} catch (IOException e) {e.printStackTrace();}
		int no = sponsor.getNo();
		
		sponsormanager.remove(no);
		return "성공";
		
			
		
	}
}