package com.moebius.celab.addressmanager.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.moebius.celab.addressmanager.common.Address;
import com.moebius.celab.addressmanager.dataservice.AddressDAO;

@Service
public class AddressManager {
	@Autowired
	AddressDAO addressDAO;
	
	@Transactional
	public List<Address> selectBySido(String sido){
		return addressDAO.selectBySido(sido);
	}

}
