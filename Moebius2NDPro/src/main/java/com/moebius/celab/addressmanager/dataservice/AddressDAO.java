package com.moebius.celab.addressmanager.dataservice;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.moebius.celab.addressmanager.common.Address;


@Repository
public class AddressDAO {
	
	@Autowired
	private SessionFactory sessionfactory;
	
	public List<Address> selectBySido(String sido){
		Session session = sessionfactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Address.class);
		criteria.add(Restrictions.eq("sido", sido));
		List<Address> address =	criteria.list();
		
		return address;
		
	}

}
