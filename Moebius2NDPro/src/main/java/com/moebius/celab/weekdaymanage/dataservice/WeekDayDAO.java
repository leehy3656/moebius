package com.moebius.celab.weekdaymanage.dataservice;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.moebius.celab.weekdaymanage.common.Weekday;

@Repository
public class WeekDayDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	public List<Weekday> selectAll(){
		Session session = sessionFactory.getCurrentSession();
		Criteria cr = session.createCriteria(Weekday.class);
		List<Weekday> weekdays = (List<Weekday>)cr.list();
		return weekdays;
	}  
}
