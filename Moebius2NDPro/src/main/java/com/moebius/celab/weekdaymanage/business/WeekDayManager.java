package com.moebius.celab.weekdaymanage.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.moebius.celab.weekdaymanage.common.Weekday;
import com.moebius.celab.weekdaymanage.dataservice.WeekDayDAO;
@Service
public class WeekDayManager {
	@Autowired
	WeekDayDAO weekdayDAO;
	

	@Transactional
	public List<Weekday> selectAll(){
		
	 return weekdayDAO.selectAll();
	}

}
