package com.moebius.celab;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.moebius.celab.membermanage.memberpersonmanage.common.Member;
import com.moebius.celab.messagemanager.business.MessageManager;
import com.moebius.celab.messagemanager.common.Message;
import com.moebius.celab.volunteermanage.business.VolunteerManager;
import com.moebius.celab.volunteermanage.common.Volunteer;

@Controller
public class MainController {

	@Autowired
	VolunteerManager volunteerManager;
	List<Volunteer> volunteers = null;
	List<Volunteer> BestVolunteers = null;
	@Autowired
	MessageManager messageManager;

	
	@RequestMapping("MainMoebius.do")
	public ModelAndView Main(){
		System.out.println("ddd");
		if(volunteers == null){
			volunteers = new ArrayList<Volunteer>();
		}
		ModelAndView mv = new ModelAndView();
		volunteers = volunteerManager.selectApprovalOderBy(2);
		BestVolunteers = volunteerManager.selectApprovalOderBy(9);
		mv.addObject("VolunteerList",volunteers);
		mv.addObject("BestVolList",BestVolunteers);
		
		/*if(request.getSession().getAttribute("member")==null){
			
			mv.addObject("fmessage",0);
			mv.addObject("vmessage",0);
		
		}else
		{ Member mem=(Member) request.getSession().getAttribute("member");
			List<Message> fmessage= messageManager.selectByMemberType(mem,"fMessage");
			List<Message> vmessage=messageManager.selectByMemberType(mem,"vMessage");
			mv.addObject("fmessage",fmessage.size());
			mv.addObject("vmessage",vmessage.size());
		
		}*/
		
		
		
		
		
		mv.setViewName("/mainpage/mainmoebius.jsp");
		return mv;
		
	}
}