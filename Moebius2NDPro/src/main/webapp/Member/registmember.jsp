<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
   <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
  
<!DOCTYPE html>
<html>
<head>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<meta charset="UTF-8" />
<title>회원가입</title>
<!-- 스타일 -->
<style type="text/css">

p{
color:red;;
}
#main {
	/* border: solid;
	border-width: thin;
	border-color: #444444;  */
	padding: 20px;
	padding-top: 0;
	width:70%;
	border: 50%;
	position: relative;
	margin-left:auto;
	margin-right:auto;
	
	/* top: 12%; */
	z-index: -1;
	border: 50%;
	
}

.SingupArea {
	display: table;
	width: 100%;
	border: solid;
	border-color: #ff7012;
	border-left :none;
	border-right :none;
	border-bottom :none;
	border-width: medium;
	background-color: white;
	margin-bottom: 0.2cm;
	margin-left: auto;
	margin-right: auto;
}

/* 아이디, 비밀번호  */
#id {
	border-bottom: solid #D5D5D5;
	border-width: thin;
	width: 100%;
	height: 40px;
	background-color: #F6F6F6;
	margin-top: 0.05cm;
}

#idText {
	width: 33%;
	height: 38px;
	border: none;
	margin-left: 2%;
	background-color: #F6F6F6;
}

#idCheckText {
	width: 35%;
	height: 38px;
	border: none;
	background-color: #F6F6F6;
}

#idCheck {
	width: 25%;
	height: 38px;
	
}

#password {
	margin-top: 0.05cm;
	border-bottom: solid #D5D5D5;
	border-width: thin;
	width: 100%;
	height: 40px;
	background-color: #FCFCFC;
}

#passwordText {
	border: none;
	width: 98%;
	margin-left: 2%;
	height: 38px;
	background-color: #FCFCFC;
}

#rpassword {
	margin-top: 0.05cm;
	border-bottom: solid #D5D5D5;
	border-width: thin;
	width: 100%;
	height: 40px;
	background-color: #F6F6F6;
	
}
#rPasswordText {
	width: 98%;
	margin-left: 2%;
	height: 38px;
	border: none;
	background-color: #F6F6F6;
}
/* 이름, 성별, 생년월일 , 이메일  */
#information {
	border: solid;
	border-width: thin;
	display: flex;
	width: 100%;
	border: none;
	background-color: white;
}

#Pinformation {
	width: 70%;
}

#profile {
	width: 30%;
}

#fileName {
	width: 98%;
	height: 30px;
	border: solid;
	border-width: thin;
	border: none;
}

#uploadImage {
	width: 97%;
	height: 133px;
}

#img{
	width: 100%;
	height: 133px;
}

#name {
	margin-top: 0.05cm;
	border-bottom: solid #D5D5D5;
	border-width: thin;
	width: 100%;
	height: 40px;
}

#nameText {
	width: 100%;
	height: 38px;
	border: none;
	background-color: #FCFCFC;
}

#sex {
	margin-top: 0.05cm;
	border-bottom: solid #D5D5D5;
	border-width: thin;
	width: 100%;
	height: 40px;
	color:#5D5D5D;
}

#sexText {
	width: 50%;
	height: 38px;
	border: none;
	background-color: #F6F6F6;
	color:#5D5D5D;
}
#sex{
background-color: #F6F6F6;
}

#sexRadio {
	vertical-align:middle;
	width: 10%;
	height: 18px;
}

#birth {
	margin-top: 0.05cm;
	border-bottom: solid #D5D5D5;
	border-width: thin;
	width: 100%;
	height: 40px;
}
#birthDate{
	height: 38px;
	width: 100%;
	border: none;
	background-color: #FCFCFC;
	color:#5D5D5D;
	padding-left: 2%;
}

#email {
	margin-top: 0.05cm;
	border-bottom: solid #D5D5D5;
	border-width: thin;
	width: 100%;
	height: 40px;
}

#emailEmail {
	width: 100%;
	height: 38px;
	border: none;
	background-color: #F6f6f6;
}
/* 우편번호, 상세주소, 휴대폰 번호  */
#post {
	margin-top: 0.05cm;
	border-bottom: solid #D5D5D5;
	border-width: thin;
	width: 100%;
	height: 40px;
	background-color: #FCFCFC;
}

#postText {
	border: none;
	width: 29%;
	height: 38px;
	background-color: #FCFCFC;
}
#addressText{
	width: 40%;
	height: 38px;
	border: none;
	background-color: #FCFCFC;
	
}

#postCheck {
	width: 28%;
	height: 38px;
}

#detailAddress {
	margin-top: 0.05cm;
	border-bottom: solid #D5D5D5;
	border-width: thin;
	width: 100%;
	height: 40px;
	background-color: white;
}

#detailAddressText {
	width: 100%;
	height: 38px;
	border: none;
	background-color: #F6F6F6;
}

#phone {
	margin-top: 0.05cm;
	border-bottom: solid #D5D5D5;
	border-width: thin;
	width: 100%;
	height: 40px;
	background-color: white;
}

#phoneText {
	width: 100%;
	height: 38px;
	border: none;
	background-color:#FCFCFC
}

#container {
	border-bottom:solid #D5D5D5;
	border-width:thin;
	width:33.3%;
	float: left;
	margin-top: 0.05cm;
	height: 100px;
	background-color: #F6F6F6;
	color:#5D5D5D;
}

#signup {
	border: solid;
	border-width: thin;
	border: none;
	width: 100%;
}

 #signupButton {
	margin-top:0.5cm;
	height: 40px;
	color: white;
	margin-left:42%;
	width:16%;
} 
</style>

<!-- 스크립트 -->
<script>
	
	function readURL(input){
	  
	 if(input.files && input.files[0]){
	   var reader = new FileReader();
	   reader.onload = function(e){
	    $('#uploadImage').html("<img id=img src=''>");
	    $('#img').attr('src', e.target.result);
	   }
	   reader.readAsDataURL(input.files[0]);
	 }  
	  
	}
	String.prototype.replaceAll = function(org, dest) {
    	return this.split(org).join(dest);
	}

	$(document).ready(function() {
		
		function registMember() {
			
			var memberperson={'@id':1,
					idname:			$("#idText").val()+"#ID"+$("#idCheckText").val(),
					password:		$("#passwordText").val(),
					name:			$("#nameText").val(),
					sex:			parseInt($("#sexRadio:checked").val()),
					birthDate:		$("#birthDate").val(),
					email:			$("#emailEmail").val(),
					phoneno:		$("#phoneText").val(),
					abilitys:		[],
					profileImage:	"default.png",
					post:			$("#postText").val(),
					address:		$("#addressText").val(),
					detailAddress:	$("#detailAddressText").val()
					};
		
		$("input[name=ability]:checked").each(function() {
			
			var selectedAbility = $(this).val();
			var adNo = selectedAbility.split('-')[0];
			var parcedAdNo=parseInt(adNo);
			var abilityName = selectedAbility.split('-')[1]; 
				
			var ability={
						adNo: parcedAdNo,
					abilityName: abilityName
			};
			
			memberperson.abilitys.push(ability);
							
		});
					
		var replacer= function(key, value) {	
	  		  if (key=="memberperson" && typeof value == "object") {
				  
	  		    return value['@id'];
	  		    
	  		  }
	  		  return value;
	  		};
	  		
	    	$.ajax({ 
	    	    url: "/celab/RegistMember.do",    
	    	    type: 'POST', 
	    	    dataType: 'text', //response가 text일때 json이면 json표기
	    	    data: JSON.stringify(memberperson, replacer), 
	    	    contentType: 'application/json ; charset=UTF-8',
	    	    mimeType: 'application/json',
	    	    
	    	    success: function(data) {
	    	    	var message =data.split('#');
	    	    	var result = message[0];
	    	    	var content = message[1];
	    	    	var fMessage = decodeURIComponent(content);
	    	    	parcedFMessage = fMessage.replaceAll("+"," ");

	    	    	if(result =="SUCCESS"){
	    	    		location.href="/celab/Member/notifyregistedmember.jsp";
	    	    	}
	    	    	else{
	    	    		alert(parcedFMessage);
	    	    	}
	    	    	
	    	    },
	    	    error:function( ) { 
	    	        alert("error");
	    	    }
	    	});
		}
		
		$("#idCheck").click(function() {
			$.post("/celab/idCheck.do", {
			idname : $("#idText").val()
			}, idCheckResult);
		});
		
		function checkIdPassword(){
			var id=$("#idText").val();
			var password =$("#passwordText").val();
			var rPassword =$("#rPasswordText").val()
			if(id == ""){
				var errorMessage = "아이디를 입력해 주세요.";
				alert(errorMessage);
				return false;
			}
			
			else if (password != rPassword ){ 
				var errorMessage = "비밀번호 확인이 불일치 합니다.";
				alert(errorMessage);
				return false;
			}
			else{
				return true;
			}
		}
		
		
		$("#signupButton").click(function() {
			
			if(checkIdPassword()){
				var data = new FormData();
				var afile=$('#fileName')[0].files[0];
				alert(afile);
				data.append("profileImage",afile);
			
	        	$.ajax({
	            	url: '/celab/uploadProfileImage.do',
	            	type: "post",
	            	dataType: "text",
	            	data: data,
	            	cache: false,
	            	processData: false,
	            	contentType: false,
	            	success: function(data, textStatus, jqXHR) {
	            	registMember();
	            	}, 
	            	
	            	error: function(jqXHR, textStatus, errorThrown) {
	            		alert("error");
	            	}
	        	});
			}

		});
		
	});
	
	function idCheckResult(text, status) {
		var idCheck = null;
		idCheck = text;
		var fMessage = decodeURIComponent(idCheck);
		$("#idCheckText").val(fMessage);
	}
	function startSearPost() {
		var myWindow = window.open("Member/Post/address.jsp", "우편주소검색",
		"resizable=no,width=605, height=710"); 
	} 
	function 아이디받다(id) {  
		var txtId = document.getElementById("txtId");
		txtId.value = id;
	}
	function 주소받다(post, postAddress) {
		var txtPost = document.getElementById("postText");
		var txtPostAddress = document.getElementById("addressText");
		txtPost.value = post;
		txtPostAddress.value = postAddress;
	}
</script>
</head>

<!-- HTML -->
<body>

<jsp:include page="/mainpage/headerMoebius.jsp" flush="false"/>  
<section class="sectiondiv">
	<div id="main">
		<div class="sectiontitle">
			<font size=6>회원가입</font>
		</div>
		
		<div class="SingupArea">
		
			<div id="id">
				<input type='text' id="idText" name='idname' placeholder="아이디" /> 
				<input type='text' id="idCheckText" disabled /> 
				<input type='button' id="idCheck" value='중복확인' />
			</div>


			<div id="password">
				<input type='password' id="passwordText" name='password'
					placeholder="비밀번호" /> 
			</div>
			
			<div id="rpassword">
				 <input type='password' id="rPasswordText" placeholder="비밀번호 확인" />
			</div>

			<div id="information">
				<div id="Pinformation">
					<div id="name">
						<input type='text' id="nameText" name='name' placeholder="   이름" />
					</div>

					<div id="sex">
						<input type='text' id="sexText" value="   성별" disabled /> 
						<input type='radio' name='sex' id="sexRadio" value="1" />남자
						<input type='radio' name='sex' id="sexRadio" value="2" />여자
					</div>

					<div id="birth">
						<input type="date" id="birthDate" min="1900-01-01"
							max="2015-08-31" name='birthDate' step="1"/>
					</div>

					<div id="email">
						<input type="email" id="emailEmail" placeholder="   이메일" name='email'>
					</div>

				</div>

				<div id="profile">
    			<div id="uploadImage"></div>
    			<input type="file" id="fileName" onchange="readURL(this)"/>
				</div>

			</div>

			<div id="post">
				<input type='text' id="postText" placeholder="   우편번호" /> 
				<input type='text' id="addressText" placeholder="주소" />
				<input
					type='button' id="postCheck" value="찾기" onclick='startSearPost()' />
			</div>

			<div id="detailAddress">
				<input type='text' id="detailAddressText" placeholder="   상세주소" />
			</div>

			<div id="phone"> 
				<input type='text' id="phoneText" name='phoneno' placeholder="   폰번호" />
			</div>
				<div id="container">
					<c:forEach var="ability" items="${abilitys1}">
						<input type="checkbox" value="${ability.adNo}-${ability.abilityName}" name='ability'/> ${ability.abilityName}
						 <br/>
					</c:forEach>
				</div>

				<div id="container">
					<c:forEach var="ability" items="${abilitys2}">
						<input type="checkbox" value="${ability.adNo}-${ability.abilityName}" name='ability'/> ${ability.abilityName} <br/>
					</c:forEach>
				</div>

				<div id="container">
					<c:forEach var="ability" items="${abilitys3}">
						<input type="checkbox" value="${ability.adNo}-${ability.abilityName}" name='ability'/> ${ability.abilityName} <br/>
					</c:forEach>
				</div>
			<div id="signup">
					<input type='button' id="signupButton" value="가입하기" />
				</div>
		</div>
	</div>

</section>
</body>

</html>
