<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<!-- 스타일 -->
<style type="text/css">
p{
color:red;;
}

#main {
	
	padding: 20px;
	padding-top: 0;
	width:70%;
	border: 50%;
	position: relative;
	margin-left:auto;
	margin-right:auto;
	z-index: -1;
	border: 50%;
}

#findIdResultArea {
	display: table;
	width: 100%;
	border: none;
	background-color: white;
	margin-top: 150px;
}

#result {
	margin-top: 20px;
	border: 1px solid #dddddd;
	border-radius: 25px;
	background: #dddddd;
	width: 80%;
}

#idDate {
	margin-left: 10px;
	margin-top: 6%;
	width: 100%;
}

#idText {
	width: 60%;
	height: 30px;
	background: #dddddd;
	border:none;
}

#dateText {
	margin-top: 5px;
	width: 60%;
	height: 30px;
	background: #dddddd;
	border: none;
	margin-bottom: 20px;
}

#loginBtn{
	margin-top : 10px;
	width: 20%;
	height: 30px;
	margin-left: 130px;
	border: none;
}

#findPasswordBtn{
	margin-top : 10px;
	width: 20%;
	height: 30px;
	margin-left: 30px;
	border: none;
}

</style>

<!-- 스크립트 -->

<script type="text/javascript">

	var cNumber; 
	
	$(document).ready(function() {
		
		$("#receiveBtn").click(function() {
			
			var url = CONTEXT + "/receiveCnumber.do";
			var params = {
					name : $("#nameText").val(),
					email : $("#emailText").val()
				}
			
			sendAjax(url, params, receiveCertificationCallback);
			
		});
		function receiveCertificationCallback(result){
			cNumber = result;
			
		}
	});
	
</script>

</head>

<!-- HTML -->
<body>

<jsp:include page="/mainpage/headerMoebius.jsp" flush="false"/> 
<section class="sectiondiv">
	<div id="main">
		<div class="sectiontitle">
			<font size=6>아이디 찾기</font>
		</div>
		<div id="findIdResultArea">
			<font>고객님의 ID 입니다.</font>
			
			<div id="result">
				<div id='idDate'>
					<font>I&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; D:</font>
					<input type='text' id='idText' value="${findIdMember.idname}" disabled/>
					<br> 
					<font>가입일시:</font>
					<input type='text' id='dateText' value="${findIdMember.date}" disabled/>
				</div>
			</div>
		</div>
	</div>
</section>
</body>

</html>