<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<!-- 스타일 -->
<style type="text/css">
p{
color:red;;
}

#main {
	
	padding: 20px;
	padding-top: 0;
	width:70%;
	border: 50%;
	position: relative;
	margin-left:auto;
	margin-right:auto;
	z-index: -1;
	border: 50%;
}

#findIdArea {
	display: table;
	width: 100%;
	border: none;
	background-color: white;
	margin-top: 150px;
}

#certification {
	margin-top: 20px;
	border: 1px solid #dddddd;
	border-radius: 25px;
	background: #dddddd;
	width: 80%;
}

#nameEmail {
	margin-left: 10px;
	margin-top: 6%;
	width: 100%;
}

#nameText {
	width: 60%;
	height: 30px;
	border:none;
}

#emailText {
	margin-top: 5px;
	width: 60%;
	height: 30px;
	border: none;
}

#receiveBtn {
	width: 30%;
	height: 30px;
	margin-left : 10px;
	border: none;
}

#cNumberText {
	margin-top: 5px;
	width: 60%;
	height: 30px;
	margin-bottom: 20px;
	border: none;
}
#confirmBtn{
	margin-top : 10px;
	width: 20%;
	height: 30px;
	margin-left: 400px;
	border: none;
}

</style>

<!-- 스크립트 -->

<script type="text/javascript">

	var cNumber; 
	
	$(document).ready(function() {
		
		$("#receiveBtn").click(function() {
			
			var url = CONTEXT + "/receiveIdCnumber.do";
			var params = {
					name : $("#nameText").val(),
					email : $("#emailText").val()
				}
			
			sendAjax(url, params, receiveCertificationCallback);
			
		});
		function receiveCertificationCallback(result){
			if(result == "null"){
				alert("이름이나, 이메일을 확인하여 주십시오");
			}
			else{
				cNumber = result;
				alert("인증번호가 발송되었습니다. 인증번호를 입력해주세요");
			}
		}
	});
	
	function checkCnumber(){
		if(cNumber ==$("#cNumberText").val()){
			
			location.href="/celab/FindId.do?name="+$("#nameText").val()+"&email="+$("#emailText").val();
		}
		else{
			alert("인증번호가 일치 하지 않습니다.");
		}
	}
	
</script>

</head>

<!-- HTML -->
<body>

<jsp:include page="/mainpage/headerMoebius.jsp" flush="false"/> 
<section class="sectiondiv">
	<div id="main">
		<div class="sectiontitle">
			<font size=6>아이디 찾기</font>
		</div>
		<div id="findIdArea">
			<font>회원 가입시 입력한 이메일 주소와 이메일 주소가 같아야, 인증번호를 받을 수 있습니다.</font>
			
			<div id="certification">
				<div id='nameEmail'>
					<input type='text' id='nameText' placeholder='이름' /> 
					<input type='email' id='emailText' placeholder="이메일" />
					<input type='button' id='receiveBtn' value='인증번호 받기' />
					
					<input type='text' id='cNumberText' placeholder="인증번호" />	
				</div>
			</div>
			<input type ="submit" id="confirmBtn" value="다음" onClick="checkCnumber()"/>
		</div>
	</div>
</section>
</body>

</html>