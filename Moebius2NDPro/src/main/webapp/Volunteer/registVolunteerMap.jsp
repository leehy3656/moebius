<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<script type="text/javascript" src="//apis.daum.net/maps/maps3.js?apikey=b28b87a459de4275f313c5b890cedd15&libraries=services"></script>



<script>
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="/celab/css/allcss.css" />
<meta charset="UTF-8" />
<style type="text/css">
#registVolunteer input {
	margin: 4px;
}

.titlediv, .phonenodiv, .datediv, .sexdiv, .abilitydiv, .textdiv {
	/* border: 2px solid #A6A6A6; */
	margin-top:0.1cm;
	padding: 5px;
	background-color:#F0F0F0;
}
.sexdiv{

}

.colordiv {
	width: 100%;
	height: 30px;
	/* background-color:#EAEAEA; */
	/* #FFC81E; */
	color: #000000;
	
}
.colordiv hr
{
border-color: #FF7012;
}
.spacediv
{
height: 10px;
}
#registVolunteer {
	z-index: -1;
	position: relative;
	width:70%; 
	padding: 0px;
	margin-left:auto;
	margin-right:auto;
	font-weight: bold;
	
}
.abilitydiv{
overflow-y: scroll;
}
.abilitydiv div
 {
	
	float: left;
	margin-right: 10%;
	
}
#img
{
position:relative;
width:100%;
height: 100%;
}
</style>
<title>Insert title here</title>
</head>
<body>
	<c:if test="${message ne null}">
		<script type="text/javascript">
			alert('${message}');
		</script>
	</c:if>
<jsp:include page="/mainpage/logincheck.jsp" flush="false"/>
<jsp:include page="/mainpage/headerMoebius.jsp" flush="false"/> 

<section class="sectiondiv">
	<div id="registVolunteer">
	<div class="sectiontitle">
		<font size=6>재능기부활동 요청</font>&nbsp;&nbsp; <font size=2>별표시(*)는
			필수입력사항입니다.</font>
			</div>
		<form action="/celab/registVolunteer.do" method=post id ="volunteerForm" name="volunteerForm">
			<input type="hidden" value='${member}' name="memberPerson" id="memberPerson">
			<input type="hidden" value=0 name="hit" id="hit">
<!--participant:    $("#participant").val(),
					hit:    		$("#hit").val()  -->
			<div class="colordiv">&nbsp;제목<hr></div>
			
			<div class="titlediv">
				제&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;목*&nbsp;<input type="text"
					name="title" size=75 id="title">
			</div>
			<div class="spacediv"></div>

			<div class="colordiv">&nbsp;주소<hr></div>
			<div class="phonenodiv">
				연&nbsp;락&nbsp;처*&nbsp;<input type="text" name="phoneno" id="phoneno"
					style="ime-mode: disabled;" onkeydown="return onlyNumber(event)">&nbsp;<font
					size=1>숫자만 입력가능합니다.</font><br>
					
				장&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;소*
				<input type="text" name="post" id="post">
				<input type="text" name="place" id="place">
				 <input
					type='button'  value="찾기" onclick='startSearPost()' /> 
				
					
				<br>
				상세장소*<input type="text" id="place2" name="place2" id="place2">
				
			</div>
			<div id="map" style="width:100%;height:350px;"></div>
			<div class="spacediv"></div>


			<div class="colordiv">&nbsp;기간<hr></div>
			<div class="datediv">

				날&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;짜*&nbsp;<input type="date"
					max="2050-01-01" step=1 name="startDate" id="startDate" onchange="weekdayset()">- <input
					type="date" max="2050-01-01" step=1 name="endDate" id="endDate" onchange="weekdayset()"><!-- <input
					type="button" value="요일선택" id="weekbtn"> -->
				<div class="weekday">
				
					<input type="checkbox" id="1" value="${weekday[0].no}-${weekday[0].name}" name="weekday" checked>일
					<input type="checkbox" id="2" value="${weekday[1].no}-${weekday[1].name}" name="weekday" checked>월
					<input type="checkbox" id="3" value="${weekday[2].no}-${weekday[2].name}" name="weekday" checked>화
					<input type="checkbox" id="4" value="${weekday[3].no}-${weekday[3].name}" name="weekday" checked>수
					<input type="checkbox" id="5" value="${weekday[4].no}-${weekday[4].name}" name="weekday" checked>목
					<input type="checkbox" id="6" value="${weekday[5].no}-${weekday[5].name}" name="weekday" checked>금
					<input type="checkbox" id="7" value="${weekday[6].no}-${weekday[6].name}" name="weekday" checked>토
					
				</div>
			</div>

			<div class="spacediv"></div>
			<div class="colordiv">&nbsp;모집인원<hr></div>
			<div class="sexdiv">
				성&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;별&nbsp;&nbsp; 
				<input type="radio" value="3" checked name="sex" id="sex">무관
				<input type="radio" value="1" name="sex" id="sex">남자
				<input type="radio" value="2" name="sex" id="sex">여 
				<br> 
				모집인원*&nbsp;<input type='text' id="participant" name="participant" size=5 style="ime-mode: disabled;" onkeydown="return onlyNumber(event)"
						value=1>명&nbsp;<font size=1>숫자만 입력가능합니다.</font> <br>
			</div>


			<div class="spacediv"></div>
			<div class="colordiv">&nbsp;필요재능<hr></div>
		<div class="abilitydiv">
				<div class="container">
				
					<c:forEach var="ability" items="${abilitys1}">
						<input type="checkbox" value="${ability.adNo}-${ability.abilityName}" name='ability'/> ${ability.abilityName}
						 <br/><!--.adNo}-${ability.abilityName}  -->
					</c:forEach>
				</div>

				<div class="container">
					<c:forEach var="ability" items="${abilitys2}">
						<input type="checkbox" value="${ability.adNo}-${ability.abilityName}" name='ability'/> ${ability.abilityName} <br/>
					</c:forEach>
				</div>

				<div class="container">
					<c:forEach var="ability" items="${abilitys3}">
						<input type="checkbox" value="${ability.adNo}-${ability.abilityName}" name='ability'/> ${ability.abilityName} <br/>
					</c:forEach>
				</div>

				
			</div>


			<div class="spacediv"></div>
			<div class="colordiv">&nbsp;구체적사항<hr></div>
			<div class="textdiv">
			<div id="uploadImage"></div>
			<!-- <input type="file" name="filevolunteer"> -->
<!-- <input type="text" name="filevolunteer" id="filevolunteer" value="test"> -->
  <input name="filevolunteer" id="filevolunteer" type="file" onchange="readURL(this)"/>
				<pre>
<textarea rows="8" cols="90" name="memo" id="memo">
만남장소,시간:
활동사항:

기타사항:

</textarea>
</pre>
			</div>


			<div align="right">
				<input type="button" value="등록" id ="registVbtn"><input type="button"
					value="취소" id="testbtn">
			</div>
		</form>
			

	</div>
	

</section>












</body>
<script>
var x="";
var y="";

function readURL(input){
	  
	 if(input.files && input.files[0]){
	   var reader = new FileReader();
	   reader.onload = function(e){
	    $('#uploadImage').html("<img id=img src=''>");
	    $('#img').attr('src', e.target.result);
	   }
	   reader.readAsDataURL(input.files[0]);
	 }  
	  
	}
function startSearPost() {
	var myWindow = window.open("Member/Post/address.jsp", "우편주소검색",
	"resizable=no,width=605, height=710"); 
} 
function 주소받다(post, postAddress) {
	var txtPost = document.getElementById("post");
	var txtPostAddress = document.getElementById("place");
	txtPost.value = post;
	txtPostAddress.value = postAddress;
	searchPlaces();
}
function weekdayinsert(){
	
	var time = new Date();
	var year = time.getYear();
	var month = time.getMonth() + 1;
	var day = time.getDate() + 7;

	var month2;
	var day2;
	if (day > 10) {
		day2 = day;
	}
	if (day < 10) {
		day2 = "0" + day;

	} else if (day == 10) {
		day2 = day;

	} else if (day > 28) {

		if ((year % 4 == 0) && (year % 100 != 0)
				|| (year % 400 == 0) && (month == 2)) {
			if (day > 29) {
				month = month + 1;
				day2 = day - 29;
				if (day2 < 10) {
					day2 = "0" + day2;

				}

			} else {
				day2 = day;
			}

		}
	

	else {
		if (month == 2) {
			if (day > 28) {
				month = month + 1;
				day2 = day - 28;
				if (day2 < 10) {
					day2 = "0" + day2;

				}
			} else {
				day2 = day;
			}
		}
		if (month == 1 || month == 3 || month == 5|| month == 7 || month == 8 || month == 10|| month == 12) {

			if (day > 31) {
				month = month + 1;
				day2 = day - 31;
				if (day2 < 10) {
					day2 = "0" + day2;

				}
			} else {
				day2 = day;
			}
		} else if(month == 2 || month == 4 || month == 6|| month == 9 || month == 11){

			if (day > 30) {
				month = month + 1;
				day2 = day - 30;
				if (day2 < 10) {
					day2 = "0" + day2;

				}
			} else {
				day2 = day;
			}
		}

	}
		}

	if (month < 10) {
		month2 = "0" + month;
	} else {
		month2 = month;
		if (month > 12) {
			month2 = 1;
		}
	}

	year = (year < 1000) ? year + 1900 : year;

	var today = year + "-" + month2 + "-" + day2;

	$("input[id=startDate]").prop("min", today);
	$("input[id=endDate]").prop("min", today);
	document.getElementById("startDate").value = today;
	document.getElementById("endDate").value = today;



	//$(".weekday").hide();
	


////////

	var dateString = document.getElementById("startDate").value;

	var dateArray = dateString.split("-");

	var dateObj = new Date(dateArray[0],dateArray[1],dateArray[2]);

	var dateString2 = document.getElementById("endDate").value;

	var dateArray2 = dateString2.split("-");

	var dateObj2 = new Date(dateArray2[0],dateArray2[1],dateArray2[2]);

	var betweenDay = (dateObj2.getTime() - dateObj.getTime())/ 1000 / 60 / 60 / 24;
	//alert(betweenDay);

	var y = parseInt(dateArray[0]);
	var m = parseInt(dateArray[1]);
	var d = parseInt(dateArray[2]);

	var a;
	var b;
	if (m == 1 || m == 2) {
		y = y - 1;
		a = y + y / 4 - y / 100 + y/ 400+ (m * 26 + 16)/ 10 + d;

		b = parseInt(a % 7);
		//alert("b는"+b);
	} else {
	
		a = y + y / 4 - y / 100 + y/ 400+ (m * 26 + 16)/ 10 + d;

		b = parseInt(a % 7);
		
	}

	var j = 0;

	if (betweenDay > 7) {
		for (var i = 1; i < 8; i++) {

			$("input[id=" + i + "]").prop('disabled',false);
			$("input[id=" + i + "]").prop('checked',true);

		}

	} else {
		for (var i = 1; i < 8; i++) {

			$("input[id=" + i + "]").prop('disabled',true);
			$("input[id=" + i + "]").prop('checked',false);

		}

		for (var i = b; j < betweenDay + 1; i++) {

			$("input[id=" + i + "]").prop('disabled',false);
			$("input[id=" + i + "]").prop('checked',true);

			j++;
			if (i > 7) {
				i = 0;
			}
		}
	}


}
	function onlyNumber(event) {

		var key = window.event ? event.keyCode : event.which;

		if ((event.shiftKey == false)
				&& ((key > 47 && key < 58) || (key > 95 && key < 106)

				|| key == 35 || key == 36 || key == 37 || key == 39

				|| key == 8 || key == 46)

		) {

			return true;

		} else {

			return false;

		}

	};
	function weekdayset(){

		
			var dateString = document.getElementById("startDate").value;
			var dateString2 = document.getElementById("endDate").value;
			
			if(dateString>dateString2){
				alert("기간을 확인해주세요.")
			}else{
			

			var dateArray = dateString.split("-");

			var dateObj = new Date(dateArray[0],dateArray[1],dateArray[2]);

			var dateArray2 = dateString2.split("-");

			var dateObj2 = new Date(dateArray2[0],dateArray2[1],dateArray2[2]);

			var betweenDay = (dateObj2.getTime() - dateObj.getTime())/ 1000 / 60 / 60 / 24;
			//alert(betweenDay);

			var y = parseInt(dateArray[0]);
			var m = parseInt(dateArray[1]);
			var d = parseInt(dateArray[2]);

			var a;
			var b;
			if (m == 1 || m == 2) {
				y = y - 1
				a = y + y / 4 - y / 100 + y/ 400+ (m * 26 + 16)/ 10 + d;

				b = parseInt(a % 7);
				//alert(b);
			} else {
				a = y + y / 4 - y / 100 + y/ 400+ (m * 26 + 16)/ 10 + d;

				b = parseInt(a % 7);

			}

			var j = 0;

			if (betweenDay > 7) {
				for (var i = 1; i < 8; i++) {

					$("input[id=" + i + "]").prop('disabled',false);
					$("input[id=" + i + "]").prop('checked',true);

				}

			} else {
				for (var i = 1; i < 8; i++) {

					$("input[id=" + i + "]").prop('disabled',true);
					$("input[id=" + i + "]").prop('checked',false);

				}

				for (var i = b; j < betweenDay+1; i++) {

					$("input[id=" + i + "]").prop('disabled',false);
					$("input[id=" + i + "]").prop('checked',true);

					j++;
					if (i >= 7) {
						i = 0;
					}
				}
			}

		}
	}
		/* else {

			$(".weekday").hide();
			document.getElementById("weekbtn").value = "요일선택";
			check = 0;
		} */
	
	$(document).ready(function() {
		
		weekdayinsert();
	/* $("#weekbtn").click(function() { */
		
	function registV() {
		var volunteer={'@idno':1,
				title:			$("#title").val(),
				startDate:		$("#startDate").val(),
				endDate:		$("#endDate").val(),
				sex:			parseInt($("#sex:checked").val()),
				memo:			$("#memo").val(),
				post:			$("#post").val(),
				place:			$("#place").val(),
				place2:			$("#place2").val(),
				abilitys:		[], 
				weekday:		[], 
				phoneno:		$("#phoneno").val(),
			 	memberPerson:	 null,  
				filevolunteer:	"default.png",
				participant:    parseInt($("#participant").val()),
				hit:    		parseInt($("#hit").val()),
				status: 1,
				gps: x+"-"+y
					
					};
		
		 $("input[name=ability]:checked").each(function() {
			
			var selectedAbility = $(this).val();
			var adNo = selectedAbility.split('-')[0];
			var parcedAdNo=parseInt(adNo);
			
			var abilityName = selectedAbility.split('-')[1]; 
				
			var ab={
					adNo: parcedAdNo,
					abilityName: abilityName
			};
			
			volunteer.abilitys.push(ab);
							
		}); 
		 $("input[name=weekday]:checked").each(function() {
			
			var weekday = $(this).val();
			var wno = weekday.split('-')[0];
			var no=parseInt(wno);
			var wname = weekday.split('-')[1]; 
				
			var weekd={
						no: no,
					name: wname
			};
			
			volunteer.weekday.push(weekd);
							
		});
					
		var replacer= function(key, value) {	
	  		  if (key=="volunteer" && typeof value == "object") {
				  
	  		    return value['@idno'];
	  		    
	  		  }
	  		  return value;
	  		};
	  		
	    	$.ajax({ 
	    	    url: "/celab/registVolunteer.do",    
	    	    type: 'POST', 
	    	    dataType: 'text', //response가 text일때 json이면 json표기
	    	    data: JSON.stringify(volunteer, replacer), 
	    	    contentType: 'application/json ; charset=UTF-8',
	    	    //mimeType: 'application/json',
	    	   

	    	    success: function(data) { 
	    	    	alert("등록되었습니다.");
	    	    	location.href="/celab/AllVolunteer.do?listno=1&chk_sex=0&chk_address=0&chk_ability=0";
	    	    },
	    	    error:function( ) { 
	    	        alert("error");
	    	    } 
	    	});

		
		

	}
	$("#registVbtn").click(function(){
		
	
		var winform = window.document.volunteerForm;
		
		if(winform.title.value.length<1){
			alert("제목을 입력해주세요");
			return;
		
		}
		if(winform.phoneno.value.length<10){
			alert("연락처를 확인해주세요");
			return;
		
		}
		if(winform.post.value.length<1||winform.place.value.length<1||winform.place2.value.length<1){
			alert("주소를 확인해주세요");
			return;
		
		}
		if(winform.startDate.value>winform.endDate.value){
			alert("기간을 확인해주세요");
			return;
		}
		if(x==""){
			alert("지도에 위치를 표시해주세요");
			return;
		}
		
		
		
		var data = new FormData();
		var afile=$('#filevolunteer')[0].files[0];
		if(afile==null){
			registV();
		
		}else{
		data.append("filevolunteer",afile);

		
        $.ajax({
            url: '/celab/upload.do',
            type: "post",
            dataType: "text",
            data: data,
            cache: false,
            processData: false,
            contentType: false,
            success: function(data, textStatus, jqXHR) {
            	registV();
            }, error: function(jqXHR, textStatus, errorThrown) {
            	alert("error");
            }
        });

		}
		
		
		
		
	});
					});
</script>
<script>
var ps = new daum.maps.services.Places();  

var mapContainer = document.getElementById('map'), // 지도를 표시할 div 
    mapOption = { 
        center: new daum.maps.LatLng(33.450701, 126.570667), // 지도의 중심좌표
        level: 3 // 지도의 확대 레벨
    };
//주소-좌표 변환 객체를 생성합니다
var geocoder = new daum.maps.services.Geocoder();

//주소 표시할 윈도우
infowindow = new daum.maps.InfoWindow({zindex:1}); 

// 지도를 표시할 div와  지도 옵션으로  지도를 생성합니다
var map = new daum.maps.Map(mapContainer, mapOption); 

var marker = new daum.maps.Marker({ 
    // 지도 중심좌표에 마커를 생성합니다 
    position: map.getCenter() 
}); 
// 지도에 마커를 표시합니다
marker.setMap(map);









daum.maps.event.addListener(map, 'click', function(mouseEvent) {        
    
    // 클릭한 위도, 경도 정보를 가져옵니다 
    var latlng = mouseEvent.latLng; 
    
    // 마커 위치를 클릭한 위치로 옮깁니다
    marker.setPosition(latlng);
    
   /*  var message = '클릭한 위치의 위도는 ' + latlng.getLat() + ' 이고, ';
    message += '경도는 ' + latlng.getLng() + ' 입니다';
    
    var resultDiv = document.getElementById('clickLatlng'); 
    resultDiv.innerHTML = message; */
    var resultDiv = document.getElementById('clickLatlng'); 
    
     x=latlng.getLat();
     y=latlng.getLng();
    
    
    
   // var message="위도는 "+latlng.getLat()+"이고 "+"경도는 "+latlng.getLng()+"이다";

    
    //resultDiv.innerHTML = x;
    
    searchAddrFromCoords(mouseEvent.latLng, function(status, result) {
        if (status === daum.maps.services.Status.OK) {
            var content = '<div style="padding:5px;">' + result[0].fullName + '</div>';

            marker.setPosition(mouseEvent.latLng);
            marker.setMap(map);
             infowindow.setContent(content);
            infowindow.open(map, marker); 
            
        }   
    });


    
});
function searchAddrFromCoords(coords, callback) {
    // 좌표로 주소 정보를 요청합니다
    geocoder.coord2addr(coords, callback);         
}
function searchPlaces() {

    var keyword = document.getElementById('place').value;
    alert(keyword);

    if (!keyword.replace(/^\s+|\s+$/g, '')) {
        alert('키워드를 입력해주세요!');
        return false;
    }

    // 장소검색 객체를 통해 키워드로 장소검색을 요청합니다
    ps.keywordSearch( keyword, placesSearchCB); 
    
}
function placesSearchCB(status, data, pagination) {
    if (status === daum.maps.services.Status.OK) {

       
        displayPlaces(data.places);
        


    } else if (status === daum.maps.services.Status.ZERO_RESULT) {

        alert('검색 결과가 존재하지 않습니다.');
        return;

    } else if (status === daum.maps.services.Status.ERROR) {

        alert('검색 결과 중 오류가 발생했습니다.');
        return;

    }
}
function displayPlaces(places) {


   
    
    fragment = document.createDocumentFragment();
    bounds = new daum.maps.LatLngBounds();
  
   
    
    for ( var i=0; i<places.length; i++ ) {

        // 마커를 생성하고 지도에 표시합니다
        var placePosition = new daum.maps.LatLng(places[i].latitude, places[i].longitude);
       
      
        bounds.extend(placePosition);

    }
    
 


    map.setBounds(bounds);
}


</script>

</html>
