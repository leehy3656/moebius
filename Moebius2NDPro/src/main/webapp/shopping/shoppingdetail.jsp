<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<title>Insert title here</title>
<style type="text/css">

.detailpointproduct
{
position: relative;
	width:70%; 
	
	margin-left:auto;
	margin-right:auto;

}
.detailname, .detailimage,.detailtextarea{

	position: relative;

	padding: 20px;
	
	float: left;
}
.detailname
{
width:35%;

padding: 10px;
margin: 5px;

}
.detailname p
{
padding: 5px;
}
.detailname hr
{
border-color: #FF7012;
}

.detailtextarea
{
clear: both;
}
.detailtextarea textarea
{
background-color: #FFFFFF;
}
.detailtextarea button
{
float:right;

}

</style>
<script>
function pointmain(){
	
	location.href="/celab/pointproductload.do";
}
/* $(document).ready(function() { */
function checkPoint(price){
	
	var q=window.document.f1;
	
	var quantity=f1.detailquantity.value;
	
	var pp=price*quantity;
	
	$.ajax({ 
	    url: "/celab/checkPoint.do",    
	    type: 'POST', 
	    dataType: 'text', //response가 text일때 json이면 json표기
	    data: {
	    	price:pp
	    }, 
	   
	   

	    success: function(data) { 
	    	itembuy(data);
	    },
	    error:function( ) { 
	        alert("error");
	    } 
	});
	
}
function itembuy(no){
	if(no==1){
		alert("포인트가 부족합니다.");
	}
	else{
		$("#itemform").submit();
		
	}
}
/* }); */


</script>
</head>
<body>
<jsp:include page="/mainpage/headerMoebius.jsp" flush="false"/>
<section class="sectiondiv">
<div class="detailpointproduct">
<div class="sectiontitle">
		<font size=6>물품정보</font>
			</div>
<form action="buyWindow.do" method="post" id="itemform" name="f1">
<div class="detailimage">

<img src="${ctx}/celab/productimages/${pointproduct.imagesrc}" style="width: 300px;height: 300px;">
</div>
<div class="detailname">
<p>내역</p>
<hr>
<p>후원:&nbsp; ${pointproduct.sponsor.name}</p>
<hr>
<p>품명:&nbsp; ${pointproduct.name}</p>
<hr>
<p>포인트:&nbsp;${pointproduct.price}&nbsp;.p</p>
<hr>
<p>수량:	&nbsp;<select name="detailquantity" style="width: 50px;">
				<c:forEach var="no" begin="1" end="${pointproduct.quantity}">
				<option value="${no}">${no}</option>
				</c:forEach>
				</select>
</p>
</div>
<div class="detailtextarea">
<textarea rows="15" cols="78" disabled="disabled">
${pointproduct.content}
</textarea><br>
<input type="hidden" name="itemno" value="${pointproduct.no}">
<%-- <input type="hidden" name="price" value="${pointproduct.price}">
<input type="hidden" name="itemname" value="${pointproduct.name}"> --%>
<button name="buycancel" onclick="pointmain()">취소</button><input type="button" value="구매" onclick="checkPoint(${pointproduct.price})">
</div>
</form>
</div>
</section>
</body>
</html>