<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="/mainpage/logincheck.jsp" flush="false"/> 
<title>Insert title here</title>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<script>
var CONTEXT = '${pageContext.request.contextPath}';
var length;

function sendAjax(url, params, doneCallback) {
	
	$.ajax({
		url : url,
		type : 'POST',
		dataType : 'json', //받는 타입
		data : params,
		traditional: true,
		cache: false,
		//contentType: 'application/json ; charset=UTF-8',
		//contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
		//mimeType: 'application/json',(mimeType은 동영상, 음악 파일 등을 쏠때 해당)
		//contentType: 'application/json ; charset=UTF-8',
	    //mimeType: 'application/json',

		success : function(data) {
			doneCallback(data);
		},
		
		error:function(request,status,error){
	        alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
		}
	});
}

function PrepareVolunteerManage() {

	var url = CONTEXT + "/PrepareVolunteerManage.do";
	var params = {
	}
	sendAjax(url, params, searchBoadMakeSessionCallback);
	document.getElementById("attendance_vHeader").disabled = true;

}

function startManageVolunteer() {
	location.href="/celab/PrepareManageMain.do"; 
}

function PrepareHowto() {
	location.href="/celab/Manage/howto.jsp"; 
}

function searchBoadMakeSessionCallback(result) {
	
	if (result) {
		length = result.length;
		document.getElementById("layer").style.display ="block";
		for(var i=0; i<result.length; i++){
			var lineItem =$("<li id ='bb"+i+"'><a href='/celab/prepareAttendance.do?date="+result[i].startDate+"&no="+result[i].no+"'>"+ result[i].title+"&nbsp;&nbsp;&nbsp;"+result[i].startDate +"<font>~</font>"+ result[i].endDate+ "</a></li>");
			$("#aa").append(lineItem);
		}
		
	} else {
		alert("Fail");
	}
}

function removeLayer(){
	document.getElementById("layer").style.display = "none";
	for(var i=0; i<length; i++){
		$("#bb"+i).remove();
	}
	document.getElementById("attendance_vHeader").disabled = false;
}


</script>
<link rel="stylesheet" type="text/css" href="/celab/css/allcss.css" />


<!-- 스타일 -->
<style>

#volunteerManage_vHeader
{
	width: 100%;
}

#home_vHeader{
	width: 2%;
	border : none;
	background-color: white;
	color: black; 
}

#attendance_vHeader{
	border : none;
	background-color: white;
	color: black; 
}

#howTo_vHeader{
	border : none;
	background-color: white;
	color: black; 
}

#layer {
	position: relative;
	width: 500px;
	background-color: #fff;
	border: 1px #c0c1bf solid;
	padding: 0 5px 0 5px;
	z-index: 1000;
	display: none;
}

#layer ul li {
	padding: 7px 4px;
	color: #929292;
	font-size: 11px;
	border-bottom: 1px #e5e5e5 solid;
}

#layer ul li a {
	font-size: 15px;
	color: #929292;
}

#layer ul li a:hover {
	color: #484848;
}

#layer .close {
	position: absolute;
	top: 5px;
	right: 5px;
}

</style>
</head>
<body>

<div id="volunteerManage_vHeader">
	<input type= "button" id= "home_vHeader"  value="홈" OnClick="startManageVolunteer()" />
	<font>|</font>
	<input type= "button" id= "attendance_vHeader" value="출석체크하기" OnClick="PrepareVolunteerManage()" />
	<font>|</font>
	<input type= "button" id= "howTo_vHeader" value="How to?" OnClick="PrepareHowto()" />
</div>
	<div id="layer">
  	
  	<div class="close">
  		<a  OnClick="removeLayer()">닫기</a>
  	</div>
  		<ul id="aa">
  		</ul>
 	</div>

	<div style="position:relative;top:10px;"></div>


</body>
</html>