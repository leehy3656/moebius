<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<title>출석 현황 보기</title>

<!-- 스타일 -->
<style type="text/css">
.main {
	width: 555px;
	height: 655px;
	padding: 0.5cm; 
}

#LineItemTable {
	margin-top: 20px;
	background-color: #FCFCFC;
	width:540px;
}

#SelectStatus {
	margin-top: 50px;
	width: 70%;
	height: 30px;
}

.Heading {
	display: table;
	font-weight: bold;
	text-align: center;
	color: #191919;
	background-color: #FCFCFC;
	border-bottom: solid #D5D5D5;
	border-width: thin;
	margin-bottom: 0.05cm;
}

.lineItemRow {
	display: table;
	background-color: #F6F6F6;
	text-align: center;
	border-bottom: solid #D5D5D5;
	border-width: thin;
	margin-bottom: 0.05cm;
}

.name, .date{

	display: table-cell;
	width:125px;
	border: solid;
	border-width: thin;
	border:none;
	padding-left: 5px;
	padding-right: 5px;
	color: #4C4C4C;
}
#name, #status{
	display: table-cell;
	width:125px;
	border: solid;
	border-width: thin;
	border:none;
	padding-left: 5px;
	padding-right: 5px;
	color: #4C4C4C;
}
.page {
	margin-top:20px;
	margin-left: auto; 
	margin-right: auto;
	margin-bottom:20px;
	display:flex;
}
#pNum{
margin-left: auto;
margin-right: auto;
}
#pNum a{
text-decoration: none;
color: #6A84B7;
}
</style>

<script>
	$(document).ready(function() {

		selectStatus(".main1");

		$("#SelectStatus").change(function() {
			if (this.value == "날짜") {
				selectStatus(".main1");
			} else {
				selectStatus(".main2");
			}

		});

		function selectStatus(status) {
			$(".main1").hide();
			$(".main2").hide();
			$(status).show();
		}
	
	});
</script>
</head>
<body>

	<div class="main1">
		<font size="6" color="#191919">날짜 별 출석현황</font> <font size="3"
			color="#191919">(${volunteer.startDate}~ ${volunteer.endDate})</font>

		<div id="LineItemTable">
			<div class="Heading">
				<div class="name">
					<p>이름</p>
				</div>
				<c:forEach var="datelist" items="${datelist}"
					begin="${(listno)*listSize-3}" end="${(listno)*listSize-1}">
					<div class="date">
						<p>${datelist}</p>
					</div>
				</c:forEach>
			</div>
			<c:forEach var="attendance" items="${attendances}">
				<div class='lineItemRow'>
					<div id='name'>${attendance[1]}</div>
						<c:forEach var="i" begin="${(listno)*listSize-2}"
						end="${(listno)*listSize}"> 
						<div id='status'>${attendance[i+1]}</div>
					</c:forEach>
				</div>
			</c:forEach>
		</div>
		<div class="page">
		<div id="pNum">
			<c:forEach var="no" begin="1" end="${pageSize}">
				<a href="/celab/prepareStatusAttendance.do?listno=${no}&no=${volunteer.no}">${no}</a>
			</c:forEach>
			</div>
		</div>


	</div>

	<%-- <div class="main2">
		<font size="6" color="#191919">참여인원 별 출석현황</font>
		<font size="3" color="#191919">(${volunteer.startDate}~ ${volunteer.endDate})</font>
		<div id="LineItemTable">
			<div class="Heading">
				<div class="date">
					<p>이름</p>
				</div>

				<div class="status">
					<p>날짜</p>
				</div>

				<div class="date">
					<p>상태</p>
				</div>

			</div>

			<c:forEach var="attendance" items="${attendances1}">
				<div class='lineItemRow'>
					<div class='name'>${attendance.member.name}</div>
					<div class='date'>${attendance.date}</div>
					<div class='status'>${attendance.status}</div>
				</div>
			</c:forEach>

		</div>


	</div> --%>


</body>
</html>
