<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="/celab/css/allcss.css" />
<style type="text/css">
.pointproductlist
{
position:relative;
width: 1200px;

}
.pointproductlist img,p
{
position:relative;
top:100px;
}
.sectiontitle
{
width:1200px;
display: flex;
height: 200px;
background-size:15% 150px;
background-image: url("/celab/images/mainimage/category1.png");
background-repeat: no-repeat;
position: relative;
top:-50px;

}

.sectiontitle font
{
position: relative;
top:50px;
left:10px;

}
.pointshop
{

	position: relative;
	width:70%; 
	
	margin-left:auto;
	margin-right:auto;
	
}

#btnimage, #btnimage2
{
position: relative;
top:-50px;
z-index: 2;
height: 50px;
cursor: pointer;
}
#btnimage2{
left:1100px;
}
.listnoproduct a
{
text-decoration: none; 
color:#000000;
position: relative;
top:100px;
}
#imageslide img {
	position: absolute;
	z-index: -1;
	width: 1200px;
	height: 300px;
}
.selectpoint
{
width:88%;
margin-left:-100px;
margin-top:40px;
background-size:100% 80px;
background-image: url("/celab/images/mainimage/category2.png");
background-repeat: no-repeat;

}
.selectpoint ul
{
list-style: none;
margin: 0;
padding: 0;
overflow: hidden;
}
.selectpoint ul li
{
position:relative;
float:left;
font-size: 25px;
font-weight: normal;
width: 150px;
margin-top:20px;
left:100px;

}
.selectpoint a
{
text-decoration: none;
color:#000000;

}

.subcategorylist ul
{
 list-style: none;
 height: 200px;

}
.subcategorylist ul li
{	

	font-size: 20px;
 	margin-top:15px; 
 	top:20px;
  	position:relative;
  	z-index: 5;
  	left:0px;
  	background-size:130px 40px;
	background-image: url("/celab/images/mainimage/category3.png");
	background-repeat: no-repeat;
}

</style>
<script>
$(document).ready(function(){
	
	$(".selectpoint div").hide();

	$(".selectpoint ul li").hover(function(){
        $(this).find("div").stop().slideToggle();
}, function(){
        $(this).find("div").stop().slideToggle();
        
});
	
	
	
	
	
	
	
	var leftno=0;
	
   $("#btnimage").click(function(){
	   leftno=leftno-1;
	   
	   if(leftno!=-3){
		   
	   $("#imageslide img").animate({
		   left :"+=1200px"
				  
	   });
	   
	   }else{
		   $("#pointitemimage1").animate({
			   left :"0px"
					  
		   });
		   $("#pointitemimage2").animate({
			   left :"1200px"
					  
		   });
		   $("#pointitemimage3").animate({
			   left :"2400px"
					  
		   });
		   $("#pointitemimage4").animate({
			   left :"-1200px"
					  
		   });
		   $("#pointitemimage5").animate({
			   left :"-2400px"
					  
		   });
		   leftno=0;
	   }

	  
   });
   $("#btnimage2").click(function(){
	  
 		leftno=leftno+1;
	   
	   if(leftno!=3){
		   
	   $("#imageslide img").animate({
		   left :"-=1200px"
				  
	   });
	   
	   }else{
		   $("#pointitemimage1").animate({
			   left :"0px"
					  
		   });
		   $("#pointitemimage2").animate({
			   left :"1200px"
					  
		   });
		   $("#pointitemimage3").animate({
			   left :"2400px"
					  
		   });
		   $("#pointitemimage4").animate({
			   left :"-1200px"
					  
		   });
		   $("#pointitemimage5").animate({
			   left :"-2400px"
					  
		   });
		   leftno=0;
	   }
	    

	  
   });
  
    	
 //	   align="center" 	
});
</script>
</head>
<body>
<jsp:include page="/mainpage/logincheck.jsp" flush="false"/> 
<jsp:include page="/mainpage/headerMoebius.jsp" flush="false"/>
<section class="sectiondiv">
<div class="pointshop">
<div class="sectiontitle">
		<font size=6>포인트 샵</font>
			
<div class="selectpoint">
				<ul>
					<c:forEach var="categoryl" items="${category}">
						<li><a href=""> ${categoryl.name} </a>
						<div class="subcategorylist">
							<ul>
								<c:forEach var="categoryitem" items="${categoryl.category}">
									<li><a href="pointproductload.do?category=${categoryitem.no}">&nbsp;&nbsp;${categoryitem.name} </a></li>
								</c:forEach>
								
							</ul>
							</div>
							</li>
							
					</c:forEach>
					
				</ul>
				<div class="subcategory">
					<ul>
						<c:forEach var="subitem" items="${subcategory}">
							<li><a href=""> ${subitem.name} </a></li>
						</c:forEach>
					</ul>
				</div>

			</div>
			</div>
<div id="imageslide" style="overflow: hidden;"> 
<img src="${ctx}/celab/productimages/${sildesrc[2]}" id="pointitemimage5" style="left:-2400px;" >

<img src="${ctx}/celab/productimages/${sildesrc[1]}" id="pointitemimage4" style="left:-1200px;" >

<img src="${ctx}/celab/productimages/${sildesrc[0]}" id="pointitemimage1" style="left:0px;" > 

<img src="${ctx}/celab/productimages/${sildesrc[1]}" id="pointitemimage2" style="left:2400px;" >


<img src="${ctx}/celab/productimages/${sildesrc[2]}" id="pointitemimage3"style="left:1200px;">


</div>
<img src="../celab/images/mainimage/leftbtn.png" id="btnimage">
<img src="../celab/images/mainimage/rightbtn.png" id="btnimage2">
<%-- <input type='button' id='btnimage' value="<<"/>
<input type='button' id='btnimage2' value=">>"/> --%>
			
<div class="pointproductlist">

<c:forEach var="shoppingl" items="${shoppinglist}" varStatus="status"  begin="${(listno-1)*listSize }" end="${listno*listSize-1}">
<div>
<a href="pointproductdetail.do?no=${shoppingl.no}">
<img src="${ctx}/celab/productimages/${shoppingl.imagesrc}" style="width: 230px; height: 230px">
</a>
<p>${shoppingl.name}
<br>P.${shoppingl.price}
</p> 
</div>
</c:forEach>

</div>

<div class="listnoproduct">
 
<c:forEach var="no" begin="1" end="${pageSize}">
<a href="pointproductload.do?listno=${no}&pointproduct=<%=session.getAttribute("pointproduct")%>&keyword=<%=session.getAttribute("keyword")%>">${no}</a>
</c:forEach> 

</div>
</div>
</section>

</body>
<style type="text/css">
body
{
background-image:url("../images/mainimage/backwi.png");
}
</style>
</html>






