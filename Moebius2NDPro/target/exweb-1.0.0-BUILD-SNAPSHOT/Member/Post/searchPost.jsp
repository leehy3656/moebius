<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script	src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<title>Insert title here</title>
<style type="text/css">
#LineItemTable
{
    display: table;
    margin-top: 10px;
    width : 50%;
}
.Heading
{
    display: table-row;
    font-weight: bold;
    text-align: center;
    width : 100%;
}
.lineItemRow
{
    display: table-row;
}
.post
{
    display: table-cell;
    border: solid;
    border-width: thin;
    padding-left: 5px;
    padding-right: 5px;
    width : 30%;
}

.address
{
    display: table-cell;
    border: solid;
    border-width: thin;
    padding-left: 5px;
    padding-right: 5px;
    width : 60%;
}

</style>
<script>
	function sendPost(post, aa) {
		opener.주소받다(post, aa);
		self.close();
	}

	$(document).ready(function() {
		$("#city").change(function() {
			if (this.value == "서울") {
				txt = "<SiGunGuListResponse><cmmMsgHeader><requestMsgId/><responseMsgId/>";
				txt = txt +"<responseTime>20150402:164645570</responseTime>";
				txt = txt + "<successYN>Y</successYN>";
				txt = txt + "<returnCode>00</returnCode>";
				txt = txt + "<errMsg/></cmmMsgHeader><siGunGuList>";
				txt = txt + "<signguCd>광진구</signguCd>";
				txt = txt + "</siGunGuList><siGunGuList>";
				txt = txt + "<signguCd>서대문구</signguCd>";
				txt = txt + "</siGunGuList><siGunGuList>";
				txt = txt + "<signguCd>중구</signguCd>";
				txt = txt + "</siGunGuList></SiGunGuListResponse>";
				if (window.DOMParser) {
					parser = new DOMParser();
					xmlDoc = parser.parseFromString(txt, "text/xml");
				} else // Internet Explorer
				{
					xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
					xmlDoc.async = false;
					xmlDoc.loadXML(txt);
				}
				document.getElementById("1").innerHTML = xmlDoc.getElementsByTagName("signguCd")[0].childNodes[0].nodeValue;
				document.getElementById("2").innerHTML = xmlDoc.getElementsByTagName("signguCd")[1].childNodes[0].nodeValue;
				document.getElementById("3").innerHTML = xmlDoc.getElementsByTagName("signguCd")[2].childNodes[0].nodeValue;
				
							
				// url을 보내서 xml 파일을 받아오고 xmlDoc(객체) 형식으로 save 했다고 치고..... 
				
				
			}
			else{
				
				
			}
		});
	});

	function viewPost() {
		txt = "<LotNumberListResponse>"
			+"<cmmMsgHeader>"
			+"<requestMsgId/>"
			+"<responseMsgId/>"
			+"<responseTime>20150406:153023290</responseTime>"
			+"<successYN>Y</successYN>"
			+"<returnCode>00</returnCode>"
			+"<errMsg/>"
			+"</cmmMsgHeader>"
			+"<lotNumberList>"
			+"<no>1</no>"
			+"<zipNo>137-061</zipNo>"
			+"<adres>서울 서초구 방배1동</adres>"
			+"</lotNumberList>"
			+"<lotNumberList>"
			+"<no>2</no>"
			+"<zipNo>137-716</zipNo>"
			+"<adres>서울 서초구 방배1동 외환은행방배사옥 935-34</adres>"
			+"</lotNumberList>"
			+"<lotNumberList>"
			+"<no>3</no>"
			+"<zipNo>137-815</zipNo>"
			+"<adres>서울 서초구 방배1동 100~199</adres>"
			+"</lotNumberList>"
			+"<lotNumberList>"
			+"<no>4</no>"
			+"<zipNo>137-816</zipNo>"
			+"<adres>서울 서초구 방배1동 300~399</adres>"
			+"</lotNumberList>"
			+"<lotNumberList>"
			+"<no>5</no>"
			+"<zipNo>137-841</zipNo>"
			+"<adres>서울 서초구 방배1동 883~901</adres>"
			+"</lotNumberList>"
			+"<lotNumberList>"
			+"<no>6</no>"
			+"<zipNo>137-842</zipNo>"
			+"<adres>서울 서초구 방배1동 902~913</adres>"
			+"</lotNumberList>"
			+"<lotNumberList>"
			+"<no>7</no>"
			+"<zipNo>137-843</zipNo>"
			+"<adres>서울 서초구 방배1동 914~928</adres>"
			+"</lotNumberList>"
			+"<lotNumberList>"
			+"<no>8</no>"
			+"<zipNo>137-844</zipNo>"
			+"<adres>서울 서초구 방배1동 929~941</adres>"
			+"</lotNumberList>"
			+"<lotNumberList>"
			+"<no>9</no>"
			+"<zipNo>137-062</zipNo>"
			+"<adres>서울 서초구 방배2동</adres>"
			+"</lotNumberList>"
			+"<lotNumberList>"
			+"<no>10</no>"
			+"<zipNo>137-709</zipNo>"
			+"<adres>서울 서초구 방배2동 서울방배경찰서 455-10</adres>"
			+"</lotNumberList>"
			+"<lotNumberList>"
			+"<no>11</no>"
			+"<zipNo>137-710</zipNo>"
			+"<adres>서울 서초구 방배2동 농수축산신보빌딩 446-2</adres>"
			+"</lotNumberList>"
			+"</LotNumberListResponse>";

		if (window.DOMParser) {
			parser = new DOMParser();
			xmlDoc = parser.parseFromString(txt, "text/xml");
		} else // Internet Explorer
		{
			xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
			xmlDoc.async = false;
			xmlDoc.loadXML(txt);
		}
		
		var size =xmlDoc.getElementsByTagName("adres").length;
		for(var i=0; i<size; i++ ){
		document.getElementById("zipNo"+i).innerHTML = xmlDoc.getElementsByTagName("zipNo")[i].childNodes[0].nodeValue;
		document.getElementById("adres"+i).innerHTML = xmlDoc.getElementsByTagName("adres")[i].childNodes[0].nodeValue;
		}
		
		//=====data======
		//data update
		var saleLineItem = {post: xmlDoc.getElementsByTagName("zipNo").childNodes[0].nodeValue, address: xmlDoc.getElementsByTagName("adres")[0].childNodes[0].nodeValue };
		//====view=======
		//view update    	
		$("#total").text(sale.total);
		var lineItemRow = $("<div class='lineItemRow'>"
			 		 +"<div class='post'></div>"
			 		 +"<div class='address'></div></div>");
		
		$("#LineItemTable").append(lineItemRow);
		lineItemRow[0].__data__ = saleLineItem;
		updateSaleLineItemAndTotal(saleLineItem,lineItemRow);
		//clear item inputs view
		$("#no").val("");
		$("#name").val("");
		$("#price").val("");
		$("#count").val("");
		searchedItem = null;
		
	}
</script>
</head>
<body>
	<h1>우편조회</h1>

	<select name="city" id="city">
		<option value="">시도</option>
		<option value="서울" >서울특별시</option>
		<option value="부산">부산광역시</option>
		<option value="대구">대구광역시</option>
		<option value="인천">인천광역시</option>
		<option value="광주">광주광역시</option>
		<option value="대전">대전광역시</option>
		<option value="울산">울산광역시</option>
	</select>

	<select name="town" disabled>
		<option value="">시군구</option>
		<option id="1" >서초구</option>
		<option id="2">서초구</option>
		<option id="3">서초구</option>
		<option id="4">서초구</option>
	</select>
	<input type="text" id="dong" placeholder="동명, 건물명,우편번호" name="dong">

	<input type="button" value='검색' onclick='viewPost()' />

	<div id="LineItemTable">   
    	<div class="Heading">
    	    <div class="post">
        	    <p>우편번호</p>
        	</div>
        
        	<div class="address">
            	<p>주소</p>
        	</div>
    	</div>
	</div>
	
	</body>
</html>










