<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<!-- 스타일 -->

<style type="text/css">
#main {
	/* border: solid;
	border-width: thin;
	border-color: #444444;  */
	padding: 20px;
	padding-top: 0;
	width:70%;
	border: 50%;
	position: relative;
	margin-left:auto;
	margin-right:auto;
	
	/* top: 12%; */
	z-index: -1;
	border: 50%;
}

#afindPassword{
	display: table;
	width: 100%;
	border: solid;
	border-color: #ff7012;
	border-left :none;
	border-right :none;
	border-bottom :none;
	border-width: medium;
	background-color: white;
	margin-bottom: 0.2cm;
	margin-left: auto;
	margin-right: auto;
	
}

#name{
	width : 80%;
	margin-left: 20px;
}

#nameText{
	width : 100%;
	height : 30px;
}

#id{
	width : 80%;
	margin-left: 20px;
	margin-top: 10px;
}

#idText{
	width : 100px;
	height :30px;
}

#email{
	width : 80%;
	margin-left :20px;
	margin-top: 10px;
}

#emailText{
	width : 100%;
	height :30px;
}

#phone{
	width: 80%;
	margin-left : 20px;
	margin-top: 10px;
}

#phoneText{
	width : 100%;
	height : 30px;
	
}
#password{
	width : 80%;
	margin-left : 20px;
	margin-top: 10px;
}

#passwordText{
	width : 100%;
	height : 30px;
}


#confirm{
	width : 80%;
	margin-left: 20px;
	margin-top: 10px;
}

#confirmButton{
	width : 100%;
	height : 30px;
}

</style>

<!-- 스크립트 -->
<script type="text/javascript">
$(document).ready(function(){
	
	$("#confirmButton").click(function(){
		
		var memberPerson={
				idname :	$("#idText").val(),
				name :		$("#nameText").val(),
				email :		$("#emailText").val(),
				phoneno :	$("#phoneText").val()
		}
		
		$.ajax({ 
			url: "/celab/FindPassword.do",
    	    type: 'POST', 
    	    dataType: 'text', //response가 text일때 json이면 json표기
    	    data: JSON.stringify(memberPerson), 
    	    contentType: 'application/json ; charset=UTF-8',
    	    mimeType: 'application/json',
    	    
    	    success: function(data) { 
    	    	$("#passwordText").val(data);
    	    	alert(data);
    	    },
    	    error:function( ) { 
    	        alert("error");
    	    }
    	});
		
		
	});
	
	
});

</script>

</head>

<!-- HTML -->
<body>

<%-- <c:choose>
  <c:when test="${message ne null}">
  	alert(메세지);
  </c:when>
</c:choose> --%>

<jsp:include page="/mainpage/headerMoebius.jsp" flush="false"/>
<section class="sectiondiv">
	<div id="main">
		<div class="sectiontitle">
			<font size=6>비밀번호 찾기</font>
		</div>
	<div id=afindPassword>
		<div id = 'name'>
			<input type='text' id='nameText' placeholder='이름'/>
		</div>
		
		<div id = 'id'>
			<input type='text' id='idText' placeholder='아이디'/>
		</div>
		
		<div id = 'email'>
			<input type='email' id='emailText' placeholder="이메일"/>
		</div>
		
		<div id = 'phone'>
			<input type='text' id='phoneText' placeholder="폰번호"/>
		</div>
		
		<div id ='confirm'>
			<input type='button' id='confirmButton' value ='비밀번호 찾기'/>
		</div>	
		
		<div id = 'password'>
			<input type='text' id='passwordText' placeholder="비밀번호" disabled/>
		</div>
		
		
		</div>
	</div>
</section>
</body>

</html>