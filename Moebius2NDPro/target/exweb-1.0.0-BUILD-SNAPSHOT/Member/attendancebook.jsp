<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html>
<html>
<head>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>출석체크</title>

<!-- 스타일 -->
<style type="text/css">
#date{
	width : 50%;
}

#dateText{
	width :30%;
	height :30px;
}

#LineItemTable
{
    display: table;
    width: 800px;
}
.Heading
{
	display: table-row;
    font-weight: bold;
    text-align: center;
}
.lineItemRow
{
    display: table-row;
}
.no,.name
{
    display: table-cell;
    border: solid;
    border-width: thin;
    padding-left: 5px;
    padding-right: 5px;
    width: 20%;
}

.status
{
    display: table-cell;
    border: solid;
    border-width: thin;
    padding-left: 5px;
    padding-right: 5px;
    width: 15%;
}

#attend
{
	border : none;
	width: 15%;
}

#absence
{
	border : none;
	width: 15%;
}

#lateness
{
	border : none;
	width: 15%;
}

</style>

<!-- 스크립트 -->
<script>
	

	$(document).ready(function(){
		
		var selectedvolunteer = $('#volunteer').val();
		var volunteerNum = selectedvolunteer.split('#')[0];
		var startDate = selectedvolunteer.split('#')[1];
		var endDate = selectedvolunteer.split('#')[2];
		var num = $('#membersNum').val(); //참여자 수
		
	for(var i=1; i<= num; i++){ //참여자 수 만큼 출석부 창에 찍어준다.
		var selectedMember =$('#member'+i).val();
		var memberNo	= selectedMember.split('#')[0];
		var memberName	= selectedMember.split('#')[1];
		
		var lineItemRow = $("<div class='lineItemRow'>"+
				"<div class='no'>"+i+"</div>"+
				"<div class='name'>"+memberName+"</div>"+
				"<div class='status'></div>"+
				"</div>");
				$("#LineItemTable").append(lineItemRow);

				var textAttendRow = $("<input type='text' value='출석' id='attend' />"); 
				var checkAttendRow  = $("<input type='radio' value='출석' id='status"+i+"' name='status"+i+"'/> ");
				
				var textAbsenceRow = $("<input type='text' value='결석' id='absence'/> ");
				var checkAbsenceRow  = $("<input type='radio' value='결석' id='status"+i+"' name='status"+i+"'/>");
				
				var textLatenessRow = $("<input type='text' value='지각' id='lateness'/> ");
				var checkLatenessRow  = $("<input type='radio' value='지각' id='status"+i+"' name='status"+i+"'/>");
				
				lineItemRow.children(".status").append(textAttendRow);
				lineItemRow.children(".status").append(checkAttendRow);
				
				lineItemRow.children(".status").append(textAbsenceRow);
				lineItemRow.children(".status").append(checkAbsenceRow);	
				
				lineItemRow.children(".status").append(textLatenessRow);
				lineItemRow.children(".status").append(checkLatenessRow);
		}
	
	
	
	
	
	
	$("#btnRegist").click(function(data, status){
		
		var volunteer ={
				no :volunteerNum,
		};
		
		
			  		
	  	for(var i=1; i<= num; i++){
	  		var selectedMember =$('#member'+i).val();
			var memberNo	= selectedMember.split('#')[0];
	  		var checkedVal =$("#status"+i+":checked").val();
	  		
	  		var member ={
					idno :memberNo
			};
	  		
	  		var attendance ={
	  			date: $('#dateText').val(),
	  			status:checkedVal,
	  			volunteer : volunteer,
	  			member : member
	  			};
	  		
	  		$.ajax({ 
	    	    url: "RegistAttendance.do",    
	    	    type: 'POST', 
	    	    dataType: 'text', //response가 text일때 json이면 json표기
	    	    data: JSON.stringify(attendance),
	    	    contentType: 'application/json ; charset=UTF-8',
	    	    mimeType: 'application/json',
	    	    success: function(data) { 
	    	    	alert(data);
	    	    },
	    	    error:function( ) { 
	    	        alert("error");
	    	    }
	    	});
	  		
	  	}
	  		
	    	
	    });
	$("#btnSearch").click(function(){
		location.href = "/celab/searchAttendance.do?date="+$("#searchDate").val()+"&no="+volunteerNum;
	});
		
	});

</script>

</head>

<!-- HTML -->
<c:choose>
	<c:when test="${result == 'true'}">
	<div id='date'>
		<input type="text" id='dateText' value ="${date}"/>
	</div>
	
	<div id="LineItemTable">   
    	<div class="Heading">
        	<div class="no">
            	<p>번호</p>
        	</div>
        
        	<div class="name">
            	<p>이름</p>		 
        	</div>
       
        	<div class="status">
            	<p>출결상황</p>
        	</div>
    	</div>
	</div>

	<input type="text" value="지난 출석부 보기" id = 'textSearch' disabled/>
	<input type="date" id="searchDate" min="2015-01-01" max="2017-08-31" name='searchDate' step="1"/>
	<input type="button" value ="검색" id ='btnSearch' />
	<input type="button" value="출석부 제출하기" id = 'btnRegist'/>
	</c:when>
	
	<c:when test="${result == 'false'}">
	
	<script> alert("이미 완료된 출석체크 입니다.")</script>
	
	<div id='date'>
		<input type="text" id='dateText' value ="${date}"/>
	</div>
	
	<div id="LineItemTable">   
    	<div class="Heading">
        	<div class="no">
            	<p>번호</p>
        	</div>
        
        	<div class="name">
            	<p>이름</p>		 
        	</div>
       
        	<div class="status">
            	<p>출결상황</p>
        	</div>
    	</div>
	</div>

	<input type="text" value="지난 출석부 보기" id = 'textSearch' disabled/>
	<input type="date" id="searchDate" min="2015-01-01" max="2017-08-31" name='searchDate' step="1"/>
	<input type="button" value ="검색" id ='btnSearch' />
	<input type="button" value="출석부 제출하기" id = 'btnRegist'/>
	</c:when>
	
	<c:when test="${result == 'false1'}">
	
	<script> alert("오늘 이전의 출석부를 검색해 주세요.")</script>
	
	<div id='date'>
		<input type="text" id='dateText' value ="${date}"/>
	</div>
	
	<div id="LineItemTable">   
    	<div class="Heading">
        	<div class="no">
            	<p>번호</p>
        	</div>
        
        	<div class="name">
            	<p>이름</p>		 
        	</div>
       
        	<div class="status">
            	<p>출결상황</p>
        	</div>
    	</div>
	</div>

	<input type="text" value="지난 출석부 보기" id = 'textSearch' disabled/>
	<input type="date" id="searchDate" min="2015-01-01" max="2017-08-31" name='searchDate' step="1"/>
	<input type="button" value ="검색" id ='btnSearch' />
	<input type="button" value="출석부 제출하기" id = 'btnRegist'/>
	</c:when>

	<c:otherwise>
	
		<script> alert("검색결과가 없습니다.")</script>
		
		<div id='date'>
		<input type="text" id='dateText' value ="${date}"/>
		</div>
		
		<div id="LineItemTable">   
    	
    	<div class="Heading">
        	<div class="no">
            	<p>번호</p>
        	</div>
        
        	<div class="name">
            	<p>이름</p>		 
        	</div>
       
        	<div class="status">
            	<p>출결상황</p>
        	</div>
    	</div>
	</div>
		<input type="text" value="지난 출석부 보기" id = 'textSearch' disabled/>
		<input type="date" id="searchDate" min="2015-01-01" max="2017-08-31" name='searchDate' step="1"/>
		<input type="button" value ="검색" id ='btnSearch' />
	</c:otherwise>
</c:choose>


<input type="hidden" value="${volunteer.no}#${volunteer.startDate}#${volunteer.endDate}" id ="volunteer"/>
<input type="hidden" value="${membersNum}" id ="membersNum"/>
   
    <c:forEach var="member" items="${members}" varStatus="status">
		<input type="hidden" value="${member.idno}#${member.name}" id="member${status.count}"/>
		<br/>
	</c:forEach>

</body>
</html>