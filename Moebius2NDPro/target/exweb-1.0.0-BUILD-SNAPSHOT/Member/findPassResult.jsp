<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<!-- 스타일 -->
<style type="text/css">
p{
color:red;;
}

#main {
	
	padding: 20px;
	padding-top: 0;
	width:70%;
	border: 50%;
	position: relative;
	margin-left:auto;
	margin-right:auto;
	z-index: -1;
	border: 50%;
}

#findIdResultArea {
	display: table;
	width: 100%;
	border: none;
	background-color: white;
	margin-top: 150px;
}

#result {
	margin-top: 20px;
	border: 1px solid #dddddd;
	border-radius: 25px;
	background: #dddddd;
	width: 90%;
}

#password {
	margin-left: 10px;
	margin-top: 6%;
	width: 100%;
}

#passText {
	width: 60%;
	height: 30px;
}

#confirmPassText {
	margin-top: 5px;
	width: 60%;
	height: 30px;
	margin-bottom: 20px;
}

#confirmBtn{
	margin-top : 10px;
	width: 20%;
	height: 30px;
	margin-left: 450px;
	border: none;
}

</style>

<!-- 스크립트 -->

<script type="text/javascript">

	var cNumber; 
	
	$(document).ready(function() {
		
		$("#receiveBtn").click(function() {
			
			var url = CONTEXT + "/receiveCnumber.do";
			var params = {
					name : $("#nameText").val(),
					email : $("#emailText").val()
				}
			
			sendAjax(url, params, receiveCertificationCallback);
			
		});
		function receiveCertificationCallback(result){
			cNumber = result;
			
		}
		
	});
	
	function checkPassword(){
		
		var password =$("#passText").val();
		var rPassword =$("#confirmPassText").val()
		var pattern = /\s/g;
		
		if(password == ""){
			alert("비밀번호를 입력해주세요.");
			return false;
		}
		else if(password.match(pattern)){
			alert("공백을 제거해 주세요");
		}
		else if(password.length<6 || password.length >16 ){
			alert("PASSWORD는 6~16 자 사이만 가능합니다.");
		}
		else if(password != rPassword){
			alert("비밀번호가 일치하지 않습니다.");
			return false;
		}
		else{
			$("#f").submit();
			return true;		
		}
	}
	
</script>

</head>

<!-- HTML -->
<body>

<jsp:include page="/mainpage/headerMoebius.jsp" flush="false"/> 
<section class="sectiondiv">
	<div id="main">
		<div class="sectiontitle">
			<font size=6>비밀번호 재설정</font>
		</div>
		<div id="findPassResultArea">
			<font>새로 사용할 비밀번호를 입력해 주세요.</font>
			<br>
			<font>사용하시던 비밀번호는 뫼비우스도 알 수 없습니다. 비밀번호를 새로 설정해 주세요.</font>
			
			<form id="f" action="/celab/updatePass.do" method=post>
			<div id="result">
				<br> 
				<font>&nbsp;&nbsp;&nbsp;&nbsp;뫼비우스 아이디 : ${findPassMember.idname}</font>
				<div id='password'>
					<input type='password' name='password' id='passText' placeholder="새 비밀번호"/>
					<input type='password' id='confirmPassText' placeholder="새 비밀번호 확인"/>
				<br>
				<font>영문, 숫자, 특수문자를 함께 사용하면(6자 이상 16자 이하)보다 안전합니다.</font>
				<br>
				<br>
				<font>다른 사이트와 다른 뫼비우스 아이디만의 비밀번호를 만들어 주세요.</font>
				<br>
				<br>	
				</div>
			</div>
			</form>
		</div>
		
		<input type="button" id="confirmBtn" value="확인" onclick="checkPassword()"/>
	</div>
	
</section>
</body>

</html>