<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>Insert title here</title>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<c:set var="ctx" value="${pageContext.request.contextPath}"></c:set>
<link rel="stylesheet" type="text/css" href="/celab/css/allcss.css" />
<style>
 /*  body {
	overflow-x: hidden;
	background-image:url("../images/background5.png");
}  */
#leftmenu 
{
	background-color:#605E5E;
	position: fixed;
	width: 15%;
	height: 110%;
	top:-5px;
	left:-5px; 
	border-left: 12px solid #FF7012;
	z-index: 1;
	
}

#leftmenu ul {
	list-style-type: none;
	/* width:100%; */
	height: 50px;
	margin: 0;
	padding: 0;

}

#leftmenu li {
	height: 100%;
	display: block;
	font-weight: bold;
	font-size: 20px;
	color: #F6F6F6;
	/* background-color: #98bf21; */
	text-align: center;
	padding: 20px;
	text-decoration: none;
}

#leftmenu a
{
	cursor: pointer; /*커서부분  */
	text-decoration: none;
	color:#F6F6F6;
}
#leftmenu a:HOVER{
color:#FF7012;
}
#leftmenu hr {
	margin-left: 45%;
	margin-right: 45%;
	margin-bottom: 10%;
}




.logindiv {
	position: relative;
	width: 100%;
	/* border: 1px solid #FFFFFF; */
	top:40%;
	z-index: -1;
}
.logindiv input{
	margin: 0px;
}
#login1_Header {
	width : 100%;
	display : flex;
	margin-top : 5px;
}
#lInformation_Header{
	
	width : 70%;
}

#idpass_Header{
	width : 100%;
	margin-left : 5px;
}
#idText_Header{
	margin-top : 5px;
	width : 93%;
	height : 30px;
	
}


#passwordText_Header{
	width: 93%;
	height : 30px;
}

#loginB_Header{
	width : 30%;
	margin-right: 5px;
}

#loginBbutton_Header{
	margin-top : 5px;
	width : 90%;
	height : 73px;
	
}

#login2_Header{
	width : 100%;
	margin-bottom: 5px;
}
#signup_Header{
	width :25%;
	border : none;
	color:#BDBDBD;
	background-color:#605E5E;
}
#signup_Header:hover{
color:#FF7012;
cursor: pointer;
}
#findId_Header{
	width :20%;
	border : none;
	color:#BDBDBD;
	background-color:#605E5E;
}
#findId_Header:hover{
color:#FF7012;
cursor: pointer;
}
#slash{
width:2%;
border:none;
color:#f6f6f6;
background-color:#605e5e;
}
#findPassword_Header{
	width :40%;
	border : none;
	color:#BDBDBD;
	background-color:#605E5E;
}
#findPassword_Header:hover{
color:#FF7012;
cursor: pointer;
}

	/* //////////////////////////////////////////////////////////////////////////////////////////////////////// */
#profile1_Header{
	width: 100%;
}

#aprofile{
	width : 99%;
	border : solid #A6A6A6;
	border-width :thin;
	display: flex;
}
#profile2_Header{
	width: 54%;
}

#photo_Header{
	width: 100%;
	background-color : #353535;
	height: 150px;
}

#profileImage_Header{
	margin-left: 13px;
	margin-top: 10px;
}


#profile3_Header{
	width: 45%;
	background-color : #353535;
}
	
#change_Header{
	width:100%;
}
	
#changeButton_Header{
	background-color : #353535;
	margin-left : 10px;
	margin-top : 15px;
	border : none;
	width: 70%;
	height: 30px;
	font-weight: bold;
	font-size: 13px;
	color: #D5D5D5;
}
#changeButton_Header:hover{
color:#FF7012;
cursor: pointer;
}

#mypage_Header{
	width:100%;
}
	
#mypageButton_Header{
	background-color : #353535;
	margin-left : 10px;
	margin-top : 15px;
	border : none;
	width: 70%;
	height: 30px;
	font-weight: bold;
	font-size: 13px;
	color: #D5D5D5;
}

#mypageButton_Header:hover{
color:#FF7012;
cursor: pointer;
}

	
#logout_Header{
	width: 100%;
}
	
#logoutButton_Header{
	background-color : #353535;
	margin-left : 10px;
	margin-top : 15px;
	border : none;
	width: 70%;
	height: 30px;
	font-weight: bold;
	font-size: 13px;
	color: #D5D5D5;
}

#logoutButton_Header:hover{
	color:#FF7012;
	cursor: pointer;
}

#id_Header{
	width: 100%;
	background-color : #353535;
}

#idText1_Header{
	width: 37%;
	height: 30px;
	border : none;
	text-align: center;
	background-color : #353535;
	font-weight: bold;
	font-size: 16px;
	color: #D5D5D5;
}
	
#idWelcomeText_Header{
	width: 60%;
	height: 30px;
	border : none;
	background-color : #353535;
	font-weight: bold;
	font-size: 16px;
	color: #D5D5D5;
}

#ability_Header{
	width: 100%;
	height: 120px;
	overflow: auto;
	background-color : #353535;
	border-bottom: solid #D5D5D5;
	border-width: thin;
	
}

#abilityText_Header{
	width: 100%;
	height: 30px;
	border : none;
	text-align: center;
	background-color : #595959;
	font-weight: bold;
	font-size: 16px;
	color: #D5D5D5;
}
.txtAbility{
	display: inline-block;
	
}	
#abilityText1_Header{
	height: 30px;
	border : none;
	text-align: center;
	margin-bottom : 5px;
	background-color : #353535;
	font-weight: bold;
	font-size: 16px;
	color: #D5D5D5;
}

.logomenu
{
position:relative;
display: flex;
top:2%;
left:10%;
margin-bottom: 25px;
}
.leftmenuSub
{
	position: fixed;
	width: 10%;
	height: 110%;
	top:-1px;
	left:15%;
	background:#9A9494;
	
	
}
.leftmenuSub ol
{
list-style-type: none;
height: 10px;
margin:0px;
padding:0px;
width: 100%;
	

}
.leftmenuSub li
{
margin:0px;
padding:0px;
}
#menuBoardsub a, #menuPointShopsub a, #menuSponsorsub a
{
	cursor: pointer; /*커서부분  */
	font-weight: normal;
	font-size: 15px;
	color: #FFFFFF;
	width: 100%;
}
#menuBoardsub
{
position: absolute;
top:18%;
}
#menuPointShopsub
{
position: absolute;
top:28%;
}
#menuSponsorsub
{
position: absolute;
top:37%;
}
</style>

<script type="text/javascript">
var CONTEXT = '${pageContext.request.contextPath}';

function starRegist(){
	location.href="/celab/PrepareRegistMember.do";
}

function findId(){
	location.href="/celab/Member/findId.jsp";
	
}
function findPassword(){
	location.href="/celab/Member/findPass.jsp";
}

function changeMember(){
	location.href="/celab/PrepareChangeMember.do";
}
function myPage(){
	location.href="/celab/Member/mypage.jsp";
}

function login(){
	var url = CONTEXT +"/idPassCheck.do";
	var id = $("#idText_Header").val();
	var password = $("#passwordText_Header").val();
		var params = {
			id : 		id,
			password : 	password
	}
	sendAjax(url, params, loginCallback);
}

function loginCallback(data){
	if(data =="success"){
		$("#login").submit();
	}
	else{
		alert("ID나 PASSWORD를 확인해주세요.");
	}
}

function logout(){
	location.href="/celab/logout.do";
}

function startManageVolunteer() {
	var myWindow = window.open("/celab/PrepareManageMain.do", "출석체크하기"); 
} 

$(document).ready(function() {
$(".leftmenuSub").hide();	
	$("#menubar li").hover(function(){
        $(this).find("div").stop().slideToggle();
}, function(){
        $(this).find("div").stop().slideToggle();
});
	
});
	

	function sendAjax(url, params, doneCallback) {
		
		$.ajax({
			url : url,
			type : 'POST',
			dataType : 'text', //받는 타입
			data : params,
			traditional: true,
			cache: false,
			//contentType: 'application/json ; charset=UTF-8',
			//contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
			//mimeType: 'application/json',(mimeType은 동영상, 음악 파일 등을 쏠때 해당)
			//contentType: 'application/json ; charset=UTF-8',
		    //mimeType: 'application/json',

			success : function(data) {
				doneCallback(data);
			},
			
			error: function(request,status,error){
		        alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
			}
		});
	}


</script>


</head>

<body>
	
	<div id="leftmenu">
	<div class="logomenu">
	<img src="http://192.168.0.23:8080/celab/images/mlogo.png" width="50" height="50"><h2 style="color:#FFFFFF;">&nbsp;Moebius</h2>
	</div>

		<ul id="menubar">
			<li><a href="/celab/MainMoebius.do">Home</a>
		
				</li>
			<li><a href="/celab/AllVolunteer.do?listno=1&chk_menu=1" id="menuBoard">Volunteer</a>
			
				<div class="leftmenuSub">
		 <ol id="menuBoardsub">
			<li>
			<a href="/celab/AllVolunteer.do?listno=1&chk_sex=0&chk_address=0&chk_ability=0">기부활동 검색</a>
			</li>
			<li>
			<a href="/celab/loadAbility.do">기부활동 등록</a>
			</li>
			<li>
			<a href="/celab/BestDetailView.do">베스트 기부활동</a>
			</li>
			<li>
			<a onclick ="startManageVolunteer()"> 기부활동 관리</a>
			</li>
			</ol> 
			
			</div>
				
				
				</li>
			<li><a href="/celab/pointproductload.do?check_shopping=1" id="menuPointShop">Point Shop</a>
				
				<div class="leftmenuSub">
				<ol id="menuPointShopsub">
			<li>
			<a href="/celab/pointproductload.do?check_shopping=1">포인트몰</a>
			</li>
			<li>
			<a href="/celab/shopping/qna.jsp">Q&A</a>
			</li>
			</ol>
				</div>
				
				</li>
			<li><a href="/celab/sponsorload.do?listno=1" id="menuSponsor">Sponsor</a>
			
			<div class="leftmenuSub">
			<ol id="menuSponsorsub">
			<li>
			<a href="/celab/sponsorload.do?listno=1">후원사 소개</a>
			</li>
			<li>
			<a href="/celab/Sponsor/sponsorRequest.jsp">후원 신청</a>
			</li>
			</ol>
			</div>
				</li>
		<li><a href="/celab/mainpage/viewMoebius.jsp">Moebius?</a>
			
			
		</ul>	
			<div class="logindiv">
				<c:choose>
					<c:when test="${member eq null}">
						<div id="login1_Header">
							<div id="lInformation_Header"> 
								<form id="login" action="/celab/login.do" method=post>
								<div id="idpass_Header">
									<input type='text' id='idText_Header' name='id' placeholder="아이디"  onkeydown="if(event.keyCode == 13){login(); return false;}" />
									<input type='password' id='passwordText_Header' name='password' placeholder="비밀번호" onkeydown="if(event.keyCode == 13){login(); return false;}" />			
								</div>
								</form>		
							</div>
		
							<div id="loginB_Header">
								<input type="button" id='loginBbutton_Header' onclick='login()' value ='로그인'/>
							</div>
						</div>
			
						<div id="login2_Header">
							<input type='text' id='signup_Header' value='회원가입' onclick='starRegist()'/>
							<input type='text' id='findId_Header' value = '아이디' onclick='findId()'/>
							<input type='text' id='slash' value = '/' />
							<input type='text' id='findPassword_Header' value = '비밀번호 찾기' onclick='findPassword()'/>
						</div>
					</c:when>
									
				<c:otherwise>
						<div id="aprofile">
						<div id="profile2_Header">
							<div id="photo_Header">
				 				<input type="image" src="${ctx}/images/${member.profileImage}" id="profileImage_Header" width="100" height="130">
							</div>
						</div>
						<div id="profile3_Header">
							<div id="change_Header">
								<input type="button" id='changeButton_Header' onclick='changeMember()' value="정보수정"/>
							</div>
							<div id="mypage_Header">
								<input type="button" id='mypageButton_Header' onclick='myPage()' value="마이페이지"/>
							</div>
							<div id="logout_Header">
								<input type="button" id='logoutButton_Header' onclick='logout()' value="로그아웃"/>
							</div>
						</div>
					</div>
						<div id="profile1_Header">
							<div id="id_Header">
								<input type='text' id='idText1_Header' value="${member.name}님" disabled/>
								<input type='text' id='idWelcomeText_Header' value ="환영 합니다." disabled/>
							</div>
							<div id="ability_Header">
								<input type='text' id='abilityText_Header' value="나의 재능" disabled/>
								<c:forEach var="ability" items="${member.abilitys}">
									<div class= txtAbility>
										<input type="text" id='abilityText1_Header' value ='${ability.abilityName}' disabled/> 
									</div>
								</c:forEach>
							</div>
						</div>
				</c:otherwise>
			</c:choose>
			</div>
	</div>
	<input type="hidden" id="status" value="${status}"/>

</body>
</html>