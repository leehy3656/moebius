<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="EUC-KR"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="/celab/css/allcss.css" />
<style type="text/css">
.sponsorRequest {
	position: relative;
	width: 70%;
	margin-left: auto;
	margin-right: auto;
}

.txt {
	width: 640px;
	height: 20%;
	
}

.middlebar {
	width: 640px;
	height: 200px;
	border-color: #747474;
}
.bottombar{
	position:static;
	width:640px;
	height: 300px;
	border-color:#747474;
}
.leftmenu{
	float: left;
	width : 100px;
	height:40px;
	vertical-align: middle;
	text-align:center;
}
.bottombar p{
	margin-left: 20px;
}
.leftmenu2{
	
	float: left;
	width:350px;
	height:40px;
	vertical-align: middle;
	text-align: center;
	
}
.leftmenu3{
	float: left;
	width:190px;
	height:40px;
	vertical-align: middle;
	text-align: center;
}


.step{
	width: 150px;
	height: 50px;
	border: silver;

}
.steptxt{
	position:static;
	
}

.topmargin {
	margin-top: 30px;
}


</style>
</head>
<body>
	<jsp:include page="../mainpage/headerMoebius.jsp" flush="false" />
	<section class="sectiondiv">
		<div class="sponsorRequest">
			<div class="sectiontitle">
				<font size=6>후원사 신청</font>
			</div>
			<div class="txt">
			<div class="topmargin">
			</div>
				
				<h2>후원사 신청 안내</h2>
				<hr color="e77026">
				<h5>
					뫼비우스의 후원사 신청은 담당자와의 상담이 먼저 이루어져야 합니다.<br>문의하기나 유선으로 연락을 주시면
					조속히 상담해 드리겠습니다.
				</h5>
				
			</div>
			<div class="topmargin">
				<div class="middlebar">
			
				<h2>후원사신청 과정</h2>
				<hr color="e77026">
					<div class="step">
						<img src="http://192.168.0.8:8080/celab/images/step3.png">
					</div>
					
				</div>	
			</div>	
			<div class="topmargin">
				<div class="bottombar">
					<h2>관련 문의사항</h2>
					<hr color="e77026">
					<font size=4><b>
					<div class="leftmenu">담당자</div>
					<div class="leftmenu2">연락처</div>
					<div class="leftmenu3">이메일</div>
					</b></font>
					<hr>
					<div class="leftmenu">박준용</div>
					<div class="leftmenu2">010-0000-0000</div>
					<div class="leftmenu3">test1@naver.com</div>
					<hr>
					<div class="leftmenu">리형렬</div>
					<div class="leftmenu2">010-0000-0000</div>
					<div class="leftmenu3">test2@daum.net</div>
					<hr>
					<div class="leftmenu">안형욱</div>
					<div class="leftmenu2">010-0000-0000</div>
					<div class="leftmenu3">test3@google.com</div>
					<hr>
					<div class="leftmenu">김강섭</div>
					<div class="leftmenu2">010-1234-0000</div>
					<div class="leftmenu3">test4@google.com</div>
					<hr>
				</div>
			</div>
		</div>
	</section>
</body>
</html>