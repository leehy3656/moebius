<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html>
<html>
<head>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<meta charset="UTF-8" />
<link rel="stylesheet" type="text/css" href="/celab/css/allcss.css" />

<!-- 스타일 -->
<style type="text/css">
.sectiondiv p
{
font-weight: bold;
} 
.volunteerdetailsee{
	position: relative;
	width: 70%;
	margin-left: auto;
	margin-right: auto;
}
.detailtextarea{
	margin-left:auto;
	margin-right: auto;
}

.detailtextarea textarea{
	background-color: #FFFFFF;
}

.amagin
{
height: 13px;
}
.place1,.place2
{
display: inline;
}
.place1
{
float: left;
}
.place2
{
float: right;
}
.fontbold
{
padding-top:10px;
width: 510px;
}
.maginleft{
	 width:250px;
	
	
}
.maginheight{
	height:10px;
}
</style>

<!-- 스크립트 -->
<script>
	
	function updatevolunteer(){
			location.href = "/celab/updateVolunteer.do?no=${volunteer.no}";
	
	}
		
	$(document).ready(function() {
						
					});
</script>


<title>Insert title here</title>
</head>

<!-- HTML -->
<body>
<jsp:include page="../mainpage/headerMoebius.jsp" flush="false"/>
<section class="sectiondiv">
	<div class="volunteerdetailsee">
		<div class="sectiontitle">
			<font size=6>봉사게시판</font>
		</div>


	<div class = "fontbold"><p>제목</p> 
		<hr color="e77026" width=510 align="left">
			<div>${volunteer.title}</div>
			</div>
			<div class="maginheight"></div>
	

	<div class = "fontbold"><p>주소</p>
		<hr color="e77026" width=510 align="left">
<div>연&nbsp;락&nbsp;처&nbsp;:&nbsp;${volunteer.phoneno}</div>
<div class="place1">장&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;소&nbsp;:&nbsp;${volunteer.place}</div><div class="place2"><div class="maginleft">상세장소&nbsp;:&nbsp;${volunteer.place2}</div></div></div><div class="maginheight"></div>
			
			
			
	<div class = "fontbold"><p>기간</p>
		<hr color="e77026" width=510 align="left">
			<div class="place1">${volunteer.startDate}&nbsp;-&nbsp;${volunteer.endDate}</div><div class="place2"><div class="maginleft">요청요일&nbsp;:&nbsp;월,화,수</div></div><br><div class="maginheight"></div>
			</div>

	<!--  -->

	<div class = "fontbold"><p>모집인원</p>
		<hr color="e77026" width=510 align="left">
			<div class="place1">성&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;별&nbsp;&nbsp;:&nbsp;</div>
			<div class="place2"><div class="maginleft">모집인원&nbsp;:&nbsp;${volunteer.participant}&nbsp;명</div></div><br><div class="maginheight"></div>
			</div>
			
		<div class = "fontbold"><p>재능</p>
		<hr color="e77026" width=510 align="left">
	<div>국어</div>
	</div>
	<div class = "amagin">
</div>

<div class="detailtextarea">
	<textarea  rows="15" cols="70" name="memo" disabled="disabled">${volunteer.memo}</textarea>
</div>

<input type="button" value="봉사신청하기" id="testbtn" name='regist' onclick="ListBoard()">
<input type="button" value="수정" name='update' onclick='updatevolunteer()'>

</div>	

</section>

</body>

</html>