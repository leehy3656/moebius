<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="/celab/css/allcss.css" />
<script>
$(document).ready(function() {
$(".logolist img").hover(function(){
	$(this).stop().css("width","+=100");
	$(this).stop().css("height","+=100");
	},function(){
		
		$(this).stop().css("width","-=100");
		$(this).stop().css("height","-=100");
		
	});
	
	
});
</script>
<style type="text/css">
body{
background-color: black;
}
.paging
{	
	text-align: center;
	position: relative;
	clear:both;
	font-weight: bold;
	
}	
.paging a:LINK
{
color:black;
}
.logosee
{
position: relative;
width: 80%;
margin-left: auto;
 margin-right: auto; 
}
 .logolist
 {
 width: 100%;

 }
 .logolist div
{

position: relative;
float: left;

 margin-left: 8%;

/* margin-left: auto;
margin-right: auto;
margin-bottom: auto;
margin-top: auto; */
/* text-align: center; */

} 
 .logolist img
 {
 margin: 20px;
 }
.magintop{
	height: 20px;

}
.maginbottom{
	height: 20px;
}

</style>

</head>

<body>
<jsp:include page="../mainpage/headerMoebius.jsp" flush="false"/> 

<section class="sectiondiv">


<div class="logosee">
<div class="sectiontitle">
		<font size=6>후원사</font>
</div>
<div class="maginbottom">
<hr color ="e77026">

</div>

<div class="logolist">
<c:forEach var="sponsorl" items="${sponserlist}" varStatus="status"  begin="${(listno-1)*listSize }" end="${listno*listSize-1}">

<div>
<%-- "/celab/sponsordetail.do?no=${sponsorl.no}" --%>
<a href="${sponsorl.webaddress}"onclick="window.open(this.href, '_blank'); return false;"><img src="${ctx}/celab/sponsorimage/${sponsorl.logoimg}" style="width: 120px; height: 100px" 
onmouseover='this.src="${ctx}/celab/sponsordetail/${sponsorl.logoimgDetail}"'
onmouseout='this.src="${ctx}/celab/sponsorimage/${sponsorl.logoimg}"'></a>
<!-- onclick="window.open(this.href, '_blank'); return false;"> -->
<!-- onclick="window.open(this.href, '_blank', 'width=400,height=300,toolbars=no,scrollbars=no'); return false;"> -->
</div>
</c:forEach>

</div>
<br>
<br>

<div class="paging">

<div class="magintop">
<hr color ="e77026">
</div>
 <c:forEach var="no" begin="1" end="${pageSize}">
 &nbsp;&nbsp;&nbsp;
 <a href="/celab/sponsorload.do?listno=${no}" style="text-decoration: none;">${no}</a> 
</c:forEach>
</div>
</div>
</section>
</body>
</html>