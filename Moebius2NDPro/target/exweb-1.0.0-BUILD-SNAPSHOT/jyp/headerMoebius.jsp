<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>Insert title here</title>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="/celab/css/allcss.css" />
<style>




#leftmenu {
	background-color:#605E5E;
	position: fixed;
	width: 15%;
	height: 110%;
	top:-5px;
	left:-5px; 
	
	border-left: 12px solid #FF7012;
	
}

#leftmenu ul {
	list-style-type: none;
	/* width:100%; */
	height: 50px;
	margin: 0;
	padding: 0;
}

#leftmenu li {
	height: 100%;
	display: block;
	font-weight: bold;
	font-size: 20px;
	color: #F6F6F6;
	/* background-color: #98bf21; */
	text-align: center;
	padding: 20px;
	text-decoration: none;
}

#leftmenu a:hover, #leftmenu a:active {
	/* background-color: #7A991A; */
	color: #FF7012;
	cursor: pointer; /*커서부분  */
}

#leftmenu hr {
	margin-left: 45%;
	margin-right: 45%;
	margin-bottom: 10%;
}




.logindiv {
	position: relative;
	width: 100%;
	border: 1px solid #FFFFFF;
}
.logindiv input{
	margin: 0px;
}

#login1_Header {
	width : 100%;
	display : flex;
	margin-top : 5px;
}
#lInformation_Header{
	
	width : 70%;
}

#idpass_Header{
	width : 100%;
}

#idText_Header{
	margin-top : 5px;
	width : 85%;
	height : 30px;
	
}


#passwordText_Header{
	margin-top : 5px;
	width: 85%;
	height : 30px;
}

#loginB_Header{
	width : 28%;
	
}

#loginBbutton_Header{
	margin-top : 5px;
	width : 85%;
	height : 70px;
	
}

#login2_Header{
	width : 100%;
}

#signup_Header{
	width :25%;
	border : none;
}
#findId_Header{
	width :22%;
	border : none;
}
#findPassword_Header{
	width :37%;
	border : none;
}

	
#profile1_Header{
	width: 100%;
	border: none;
}

#id_Header{
	width: 100%;
}
	
#idText1_Header{
	width: 30%;
	height: 30px;
}
	
#idWelcomeText_Header{
	width: 60%;
	height: 30px;
}

#ability_Header{
	width: 100%;
}

#abilityText_Header{
	width: 100%;
	height: 30px;
}
	
#abilityText1_Header{
	width: 100%;
	height: 30px;
}

#aprofile{
	width : 100%;
	display: flex;
}
#profile2_Header{
	width: 66%;
}

#photo_Header{
	width: 100%;
	height: 100px;
}
	
#image_Header{
	width: 100%;
	height: 100px;
}
	
#profile3_Header{
	width: 33%;
}
	
#change_Header{
	width:100%;
}
	
#changeButton_Header{
	width: 100%;
	height: 50px;
}
	
#logout_Header{
	width: 100%;
}
	
#logoutButton_Header{
	width: 100%;
	height: 50px;
}
.logomenu
{
position:relative;
display: flex;
top:2%;
left:10%;
margin-bottom: 25px;
}

</style>

<script type="text/javascript">

function starRegist(){
	location.href="/celab/PrepareRegistMember.do";
}

function findId(){
	location.href="/celab/Member/findId.jsp";
	
}
function findPassword(){
	location.href="/celab/Member/findPassword.jsp";
}

function changeMember(){
	location.href="/celab/PrepareChangeMember.do";
}

function login(){
	var id =  $("#idText_Header").val(); 
	var password = $("#passwordText_Header").val();
	location.href="/celab/login.do?id="+id+"&password="+password;
}
function logout(){
	location.href="/celab/logout.do";
}

</script>


</head>

<body>
	
	<div id="leftmenu">
	<div class="logomenu">
	<img src="http://192.168.0.15:8080/celab/images/mlogo.png" width="70" height="70"><h2 style="color:#FFFFFF;">Moebius</h2>
	</div>

		<ul>
			<li><a>Home</a>
				<hr></li>
			<li><a>Volunteer</a>
				<hr></li>
			<li><a>Best Work</a>
				<hr></li>
			<li><a>Point Shop</a>
				<hr></li>
			<li><a>Sponsor</a>
				<hr></li>
			<li>
			
			<div class="logindiv">
				<c:choose>
					<c:when test="${member eq null}">
						<div id="login1_Header">
							<div id="lInformation_Header"> 
								<div id="idpass_Header">
									<input type='text' id='idText_Header' name='id'  />
									<input type='password' id='passwordText_Header' name='password'/>			
								</div>		
							</div>
		
							<div id="loginB_Header">
								<input type="button" id='loginBbutton_Header' onclick='login()'/>
							</div>
						</div>
			
						<div id="login2_Header">
							<input type='text' id='signup_Header' value='회원가입' onclick='starRegist()'/>
							<input type='text' id='findId_Header' value = '아이디/' onclick='findId()'/>
							<input type='text' id='findPassword_Header' value = '비밀번호 찾기' onclick='findPassword()'/>
						</div>
					</c:when>
		
				<c:otherwise>
						<div id="profile1_Header">
							<div id="id">
								<input type='text' id='idText1_Header' value="${member.name}"/>
								<input type='text' id='idWelcomeText_Header' value ="님 환영 합니다."/>
							</div>
			
							<div id="ability_Header">
								<input type='text' id='abilityText_Header' value="나의 재능"/>
								<input type='text' id='abilityText1_Header'/>
							</div>
						</div>
						
						<div id="aprofile">
		
						<div id="profile2_Header">
							<div id="photo_Header">
				 				<input type="image" src="http://192.168.0.23:8080/celab/images/c1.png" width="48" height="48">

							</div>
						</div>
		
						<div id="profile3_Header">
							<div id="change_Header">
								<input type="button" id='changeButton_Header' onclick='changeMember()' value="정보수정"/>
							</div>
				
							<div id="logout_Header">
								<input type="button" id='logoutButton_Header' onclick='logout()' value="로그아웃"/>
							</div>
						</div>
					</div>
				</c:otherwise>
			</c:choose>
			</div>
		<hr>
			</li>	
		</ul>
		
	</div>
	
	
	<!-- <div class="image">
		<img src="http://192.168.0.23:8080/celab/images/c1.png"
			class="floating1" width="50" height="50">
	</div>
	<div class="image">
		<img src="http://192.168.0.23:8080/celab/images/b1.PNG"
			class="floating2" width="80" height="80">
	</div>
	<div class="image">
		<img src="http://192.168.0.23:8080/celab/images/b1.PNG"
			class="floating3" width="300" height="300">
	</div> -->
</body>
</html>