<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>Insert title here</title>
<style type="text/css">

.detailpointproduct
{
position: relative;

}
.detailname, .detailimage,.detailtextarea{

	position: relative;
	top:120px;
	left:18%;

	padding: 20px;
	
	float: left;
}
.detailname
{
width:22%;

padding: 10px;
margin: 5px;

}
.detailname p
{
padding: 5px;
}
.detailname hr
{
border-color: #FFCA6C;
}

.detailtextarea
{
clear: both;
}
.detailtextarea textarea
{
background-color: #FFFFFF;
}
.detailtextarea button
{
float:right;

}

</style>
</head>
<body>
<jsp:include page="headerMoebius.jsp" flush="false"/>
<section class="sectiondiv">
<div class="detailpointproduct">
<div class="detailimage">
<img src="http://192.168.0.23:8080/celab/images/${pointproduct.imagesrc}" style="width: 300px;height: 300px;">
</div>
<div class="detailname">
<p>물품내용</p>
<hr>
<p>후원:&nbsp; ${pointproduct.sponsor}</p>
<hr>
<p>품명:&nbsp; ${pointproduct.name}</p>
<hr>
<p>포인트:&nbsp;${pointproduct.price}&nbsp;.p</p>
<hr>
<p>수량:	&nbsp;<select name="detailquantity" style="width: 50px;">
				<c:forEach var="no" begin="1" end="${pointproduct.quantity}">
				<option value="${no}">${no}</option>
				</c:forEach>
				</select>
</p>
</div>
<div class="detailtextarea">
<textarea rows="15" cols="78" disabled="disabled">
${pointproduct.content}
</textarea><br>
<button name="buycancel">취소</button><button name="buyproduct">구매</button>
</div>

</div>
</section>
</body>
</html>