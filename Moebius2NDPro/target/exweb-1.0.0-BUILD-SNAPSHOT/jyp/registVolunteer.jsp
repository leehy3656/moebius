<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="/celab/css/allcss.css" />
<meta charset="UTF-8" />
<style type="text/css">


#registVolunteer input {
	margin: 4px;
}

.titlediv, .phonenodiv, .datediv, .sexdiv, .abilitydiv, .textdiv {
	border: 2px solid #A6A6A6;
	padding: 3px;
}
.sexdiv{

}

.colordiv {
	width: 100%;
	height: 30px;
	background-color:#605E5E;
	/* #FFC81E; */
	color: #FFFFFF;
}

.spacediv
{
height: 5px;
}
#registVolunteer {
	z-index: -1;
	position: relative;
	width:70%; 
	padding: 0px;
	margin-left:auto;
	margin-right:auto;
	font-weight: bold;
	
}
.abilitydiv{
overflow-y: scroll;
}
.abilitydiv div
 {
	
	float: left;
	margin-right: 10%;
	
}
</style>
<title>Insert title here</title>
</head>
<body>
	<c:if test="${message ne null}">
		<script type="text/javascript">
			alert('${message}');
		</script>
	</c:if>

<jsp:include page="headerMoebius.jsp" flush="false"/> 

<section class="sectiondiv">
	<div id="registVolunteer">
	<div class="sectiontitle">
		<font size=6>재능기부활동 요청</font>&nbsp;&nbsp; <font size=2>별표시(*)는
			필수입력사항입니다.</font>
			</div>
		<form action="/celab/registVolunteer.do" method=post>
			<input type="hidden" value="idname" name="writer">

			<div class="colordiv">&nbsp;제목</div>
			<div class="titlediv">
				제&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;목*&nbsp;<input type="text"
					name="title" size=75>
			</div>
			<div class="spacediv"></div>

			<div class="colordiv">&nbsp;주소</div>
			<div class="phonenodiv">
				연&nbsp;락&nbsp;처*<input type="text" name="phoneno"
					style="ime-mode: disabled;" onkeydown="return onlyNumber(event)">&nbsp;<font
					size=1>숫자만 입력가능합니다.</font><br>
				장&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;소*<input type="text" name="place">
				<button>찾기</button>
				&nbsp;상세장소*&nbsp;<input type="text" id="place2" name="place2">
			</div>
			<div class="spacediv"></div>


			<div class="colordiv">&nbsp;기간</div>
			<div class="datediv">

				날&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;짜*&nbsp;<input type="date"
					max="2050-01-01" step=1 name="startDate" id="startDate">- <input
					type="date" max="2050-01-01" step=1 name="endDate" id="endtDate"><input
					type="button" value="요일선택" id="weekbtn">
				<div class="weekday">
					<input type="checkbox" id="1" value="1" name="weekday" checked>월
					<input type="checkbox" id="2" value="2" name="weekday" checked>화
					<input type="checkbox" id="3" value="3" name="weekday" checked>수
					<input type="checkbox" id="4" value="4" name="weekday" checked>목
					<input type="checkbox" id="5" value="5" name="weekday" checked>금
					<input type="checkbox" id="6" value="6" name="weekday" checked>토
					<input type="checkbox" id="7" value="7" name="weekday" checked>일
				</div>
			</div>

			<div class="spacediv"></div>
			<div class="colordiv">&nbsp;모집인원</div>
			<div class="sexdiv">
				성&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;별&nbsp;&nbsp;<input type="radio"
					value="3" checked name="sex">무관<input type="radio" id="sex"
					value="1" name="sex">남자<input type="radio" value="2"
					name="sex">여 <br> 모집인원*&nbsp;<input type='text'
					id="participant" name="participant" size=5
					style="ime-mode: disabled;" onkeydown="return onlyNumber(event)"
					value=1>명&nbsp;<font size=1>숫자만 입력가능합니다.</font> <br>
			</div>


			<div class="spacediv"></div>
			<div class="colordiv">&nbsp;필요재능</div>
			<!-- <div class="abilitydiv">
				재&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;능&nbsp; <input type="checkbox"
					value="재능1" name="ability">재능1 <input type="checkbox"
					name="ability" value="재능2">재능2 <input type="checkbox"
					name="ability" value="재능3">재능3 <input type="checkbox"
					name="ability" value="재능4">재능4 <input type="checkbox"
					name="ability" value="재능5">재능5
			</div> -->

		<div class="abilitydiv">
				<div class="container">
					<c:forEach var="ability" items="${abilitys1}">
						<input type="checkbox" value="${ability.adNo}-${ability.abilityName}" name='ability'/> ${ability.abilityName}
						 <br/>
					</c:forEach>
				</div>

				<div class="container">
					<c:forEach var="ability" items="${abilitys2}">
						<input type="checkbox" value="${ability.adNo}-${ability.abilityName}" name='ability'/> ${ability.abilityName} <br/>
					</c:forEach>
				</div>

				<div class="container">
					<c:forEach var="ability" items="${abilitys3}">
						<input type="checkbox" value="${ability.adNo}-${ability.abilityName}" name='ability'/> ${ability.abilityName} <br/>
					</c:forEach>
				</div>

				
			</div>


			<div class="spacediv"></div>
			<div class="colordiv">&nbsp;구체적사항</div>
			<div class="textdiv">
			<input type="file" name="filevolunteer">

				<pre>
<textarea rows="12" cols="90" name="memo">
만남장소,시간:
활동사항:

기타사항:

</textarea>
</pre>
			</div>


			<div align="right">
				<input type="submit" value="등록"><input type="button"
					value="취소" id="testbtn">
			</div>
		</form>
	</div>
</section>












</body>
<script>
function weekdayinsert(){
	var time = new Date();
	var year = time.getYear();
	var month = time.getMonth() + 1;
	var day = time.getDate() + 7;

	var month2;
	var day2;
	if (day > 10) {
		day2 = day;
	}
	if (day < 10) {
		day2 = "0" + day;

	} else if (day == 10) {
		day2 = day;

	} else if (day > 28) {

		if ((year % 4 == 0) && (year % 100 != 0)
				|| (year % 400 == 0) && (month == 2)) {
			if (day > 29) {
				month = month + 1;
				day2 = day - 29;
				if (day2 < 10) {
					day2 = "0" + day2;

				}

			} else {
				day2 = day;
			}

		}
	}

	else {
		if (month == 2) {
			if (day > 28) {
				month = month + 1;
				day2 = day - 28;
				if (day2 < 10) {
					day2 = "0" + day2;

				}
			} else {
				day2 = day;
			}
		}
		if (month == 1 || month == 3 || month == 5|| month == 7 || month == 8 || month == 10|| month == 12) {

			if (day > 31) {
				month = month + 1;
				day2 = day - 31;
				if (day2 < 10) {
					day2 = "0" + day2;

				}
			} else {
				day2 = day;
			}
		} else {

			if (day > 30) {
				month = month + 1;
				day2 = day - 30;
				if (day2 < 10) {
					day2 = "0" + day2;

				}
			} else {
				day2 = day;
			}
		}

	}

	if (month < 10) {
		month2 = "0" + month;
	} else {
		month2 = month;
		if (month > 12) {
			month2 = 1;
		}
	}

	year = (year < 1000) ? year + 1900 : year;

	var today = year + "-" + month2 + "-" + day2;

	$("input[id=startDate]").prop("min", today);
	$("input[id=endtDate]").prop("min", today);
	document.getElementById("startDate").value = today;
	document.getElementById("endtDate").value = today;



	$(".weekday").hide();
	


////////

	var dateString = document.getElementById("startDate").value;

	var dateArray = dateString.split("-");

	var dateObj = new Date(dateArray[0],dateArray[1],dateArray[2]);

	var dateString2 = document.getElementById("endtDate").value;

	var dateArray2 = dateString2.split("-");

	var dateObj2 = new Date(dateArray2[0],dateArray2[1],dateArray2[2]);

	var betweenDay = (dateObj2.getTime() - dateObj.getTime())/ 1000 / 60 / 60 / 24;
	//alert(betweenDay);

	var y = parseInt(dateArray[0]);
	var m = parseInt(dateArray[1]);
	var d = parseInt(dateArray[2]);

	var a;
	var b;
	if (m == 1 || m == 2) {
		y = y - 1
		a = y + y / 4 - y / 100 + y/ 400+ (m * 26 + 16)/ 10 + d;

		b = parseInt(a % 7);
		//alert(b);
	} else {
		a = y + y / 4 - y / 100 + y/ 400+ (m * 26 + 16)/ 10 + d;

		b = parseInt(a % 7);

	}

	var j = 0;

	if (betweenDay > 7) {
		for (var i = 1; i < 8; i++) {

			$("input[id=" + i + "]").prop('disabled',false);
			$("input[id=" + i + "]").prop('checked',true);

		}

	} else {
		for (var i = 1; i < 8; i++) {

			$("input[id=" + i + "]").prop('disabled',true);
			$("input[id=" + i + "]").prop('checked',false);

		}

		for (var i = b; j < betweenDay + 1; i++) {

			$("input[id=" + i + "]").prop('disabled',false);
			$("input[id=" + i + "]").prop('checked',true);

			j++;
			if (i > 7) {
				i = 0;
			}
		}
	}


}
	function onlyNumber(event) {

		var key = window.event ? event.keyCode : event.which;

		if ((event.shiftKey == false)
				&& ((key > 47 && key < 58) || (key > 95 && key < 106)

				|| key == 35 || key == 36 || key == 37 || key == 39

				|| key == 8 || key == 46)

		) {

			return true;

		} else {

			return false;

		}

	};

	$(document).ready(function() {
		var check = 0;
		weekdayinsert();
	$("#weekbtn").click(function() {

											if (check == 0) {
												for (var i = 1; i < 8; i++) {

													$("input[id=" + i + "]")
															.prop('disabled',
																	false);
													$("input[id=" + i + "]")
															.prop('checked',
																	true);

												}

												$(".weekday").show();
												document.getElementById("weekbtn").value = "요일결정";
												check = 1;

												var dateString = document.getElementById("startDate").value;

												var dateArray = dateString.split("-");

												var dateObj = new Date(dateArray[0],dateArray[1],dateArray[2]);

												var dateString2 = document.getElementById("endtDate").value;

												var dateArray2 = dateString2.split("-");

												var dateObj2 = new Date(dateArray2[0],dateArray2[1],dateArray2[2]);

												var betweenDay = (dateObj2.getTime() - dateObj.getTime())/ 1000 / 60 / 60 / 24;
												//alert(betweenDay);

												var y = parseInt(dateArray[0]);
												var m = parseInt(dateArray[1]);
												var d = parseInt(dateArray[2]);

												var a;
												var b;
												if (m == 1 || m == 2) {
													y = y - 1
													a = y + y / 4 - y / 100 + y/ 400+ (m * 26 + 16)/ 10 + d;

													b = parseInt(a % 7);
													//alert(b);
												} else {
													a = y + y / 4 - y / 100 + y/ 400+ (m * 26 + 16)/ 10 + d;

													b = parseInt(a % 7);

												}

												var j = 0;

												if (betweenDay > 7) {
													for (var i = 1; i < 8; i++) {

														$("input[id=" + i + "]").prop('disabled',false);
														$("input[id=" + i + "]").prop('checked',true);

													}

												} else {
													for (var i = 1; i < 8; i++) {

														$("input[id=" + i + "]").prop('disabled',true);
														$("input[id=" + i + "]").prop('checked',false);

													}

													for (var i = b; j < betweenDay + 1; i++) {

														$("input[id=" + i + "]").prop('disabled',false);
														$("input[id=" + i + "]").prop('checked',true);

														j++;
														if (i > 7) {
															i = 0;
														}
													}
												}

											}

											else {

												$(".weekday").hide();
												document.getElementById("weekbtn").value = "요일선택";
												check = 0;
											}
										});

					});
</script>
</html>
