<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>출석체크</title>

<!-- 스타일 -->
<style type="text/css">
#main {
	padding: 20px;
	padding-top: 0;
	width: 800px;
	border: 50%;
	position: relative;
	margin-left: auto;
	margin-right: auto;
	z-index: -1;
	border: 50%;
}

.AttendanceArea {
	display: table;
	width: 100%;
	border: solid;
	border-color: #ff7012;
	border-left: none;
	border-right: none;
	border-bottom: none;
	border-width: medium;
	background-color: white;
	margin-bottom: 0.2cm;
	margin-left: auto;
	margin-right: auto;
}

#volunteerInfo {
	width: 100%;
	border: none;
}

#volunteerTitle {
	border: none;
	width: 62%;
	height: 40px;
	background-color: white;
	margin-top: 0.05cm;
	font-size: 0.7cm;
}

#startText {
	width: 15%;
	height: 30px;
	border: none;
	text-align: center;
	background-color: white;
}

#endText {
	width: 15%;
	height: 30px;
	border: none;
	text-align: center;
	background-color: white;
}

#dateText {
	width: 25%;
	height: 100px;
	text-align: center;
	background-color: white;
	margin-bottom: 20px;
	border: none;
	font-size: 1cm;
}

#backwardBtn {
	margin-left : 250px;
	width: 4.85%;
	height: 40px;
	border: none; 
}

#forwardBtn {
	width: 5.5%;
	height: 40px;
	border: none;
}

#LineItemTable {
	margin-top: 20px;
	width: 800px;
	
	
}

.Heading {
	display: table;
	font-weight: bold;
	text-align: center;
	background-color: #FCFCFC;
	width :100%;
	border-bottom: solid #D5D5D5;
	border-width: thin;
	margin-bottom: 0.05cm;
	
}

.lineItemRow {
	display: table;
	background-color: #F6F6F6;
	text-align: center;
	border-bottom: solid #D5D5D5;
	border-width: thin;
	margin-bottom: 0.05cm;
	}

.no, .name {
	display: table-cell;
	border: solid;
	border-width: thin;
	padding-left: 5px;
	padding-right: 5px;
	width: 20%;
	border : none;
	padding-bottom: 30px;
	color: #4C4C4C;
}

.status {
	display: table-cell;
	border: solid;
	border-width: thin;
	padding-left: 5px;
	padding-right: 5px;
	width: 15%;
	border : none;
	padding-bottom: 30px;
}

#attend {
	border: none;
	width: 15%;
	background-color: #F6F6F6;
	color: #4C4C4C;
}

#absence {
	border: none;
	width: 15%;
	background-color: #F6F6F6;
	color: #4C4C4C;
}

#lateness {
	border: none;
	width: 15%;
	background-color: #F6F6F6;
	color: #4C4C4C;
}

#searchDate {
	margin-left: 254px;
	width: 15%;
}

#btnRegist {
	width: 20%;
}

#btnStatus {
	width: 20%;
}
</style>

<!-- 스크립트 -->
<script>
	
	function check(){
		
		var num = $('#membersNum').val(); //참여자 수
		var check;
		
		for(var i=1; i<=num; i++){
			var checkedVal =$("#status"+i+":checked").val();
			
			if(checkedVal == undefined){
				check = false;
				return check;
			}
			
			else{
				check = true;
			}
		}
		
		return check;
	}

	$(document).ready(function(){
		
		var selectedvolunteer = $('#volunteer').val();
		var volunteerNum = selectedvolunteer.split('#')[0];
		var startDate = selectedvolunteer.split('#')[1];
		var endDate = selectedvolunteer.split('#')[2];
		var num = $('#membersNum').val(); //참여자 수
		
	for(var i=1; i<= num; i++){ //참여자 수 만큼 출석부 창에 찍어준다.
		var selectedMember =$('#member'+i).val();
		var memberNo	= selectedMember.split('#')[0];
		var memberName	= selectedMember.split('#')[1];
		
		var lineItemRow = $("<div class='lineItemRow'>"+
				"<div class='no'>"+i+"</div>"+
				"<div class='name'>"+memberName+"</div>"+
				"<div class='status'></div>"+
				"</div>");
				$("#LineItemTable").append(lineItemRow);

				var textAttendRow = $("<input type='text' value='출석' id='attend' />"); 
				var checkAttendRow  = $("<input type='radio' value='출석' id='status"+i+"' name='status"+i+"'/> ");
				
				var textAbsenceRow = $("<input type='text' value='결석' id='absence'/> ");
				var checkAbsenceRow  = $("<input type='radio' value='결석' id='status"+i+"' name='status"+i+"'/>");
				
				var textLatenessRow = $("<input type='text' value='지각' id='lateness'/> ");
				var checkLatenessRow  = $("<input type='radio' value='지각' id='status"+i+"' name='status"+i+"'/>");
				
				lineItemRow.children(".status").append(textAttendRow);
				lineItemRow.children(".status").append(checkAttendRow);
				
				lineItemRow.children(".status").append(textAbsenceRow);
				lineItemRow.children(".status").append(checkAbsenceRow);	
				
				lineItemRow.children(".status").append(textLatenessRow);
				lineItemRow.children(".status").append(checkLatenessRow);
		}
	
	
	
	
	
	
	$("#btnRegist").click(function(data, status){
		var result = new Array();
		var volunteer ={
				no :volunteerNum,
		};
		
		if(check()){	  		
	  		for(var i=1; i<= num; i++){
	  			
	  			var selectedMember =$('#member'+i).val();
				var memberNo	= selectedMember.split('#')[0];
	  			var checkedVal =$("#status"+i+":checked").val();
	  			var member ={
					idno :memberNo
				};
	  		
	  			var attendance ={
	  				date: $('#dateText').val(),
	  				status:checkedVal,
	  				volunteer : volunteer,
	  				member : member
	  			};
	  		
	  			$.ajax({ 
	    	    	url: "RegistAttendance.do",    
	    	    	type: 'POST', 
	    	    	dataType: 'text', //response가 text일때 json이면 json표기
	    	    	data: JSON.stringify(attendance),
	    	    	contentType: 'application/json ; charset=UTF-8',
	    	    	mimeType: 'application/json',
	    	    
	    	    	success: function(data) {
	    	    		
	    	    		result.push(data);
	    	    		
	    	    		if(result.length == num){
	    	    			
	    	    			if(result[num-1] == "attendance"){
	    	    				alert("출석체크 완료");
	    	    				location.href = "/celab/forwardAttendance.do?date="+$("#dateText").val()+"&no="+volunteerNum;
	    	    			}
	    	    			else if(result[num-1] == "point"){
	    	    				alert("포인트 지급 완료");
	    	    				location.href = "/celab/prepareStatusAttendance1.do?&no="+volunteerNum;
	    	    			}
	    	    			else if(result[num-1] == "fail"){
	    	    				alert("이미 완료된 출석 입니다.");
	    	    			}
	    	    		}
	    	    	},
	    	    	
	    	    	error:function() { 
	    	    		result = "error";
	    	        	alert("error");
	    	    	}
	    		});
	  		}
		}
		
		else{
			alert("모든 출석체크를 완료해주세요.");
		}
	    	
	});
	
	
	$("#btnSearch").click(function(){
		location.href = "/celab/searchAttendance.do?date="+$("#searchDate").val()+"&no="+volunteerNum;
	});
	
	$("#forwardBtn").click(function(){
		location.href = "/celab/forwardAttendance.do?date="+$("#dateText").val()+"&no="+volunteerNum;
	});
	
	$("#backwardBtn").click(function(){
		location.href = "/celab/backwardAttendance.do?date="+$("#dateText").val()+"&no="+volunteerNum;
	});
	
	$("#btnStatus").click(function() {
		var myWindow = window.open("/celab/prepareStatusAttendance.do?&no="+volunteerNum, "출석현황보기",
		"resizable=no,width=605, height=710"); 
	}); 
		
	});
	
	function search(){
		var selectedvolunteer = $('#volunteer').val();
		var volunteerNum = selectedvolunteer.split('#')[0];
		location.href = "/celab/searchAttendance.do?date="+$("#searchDate").val()+"&no="+volunteerNum;
	}
</script>

</head>

<!-- HTML -->
<body>
	<jsp:include page="/Manage/headerVolunteerManage.jsp" flush="false" />
	<section class="sectiondiv">

		<div id="main">
			<div class="sectiontitle">
				<font size=6>출석체크</font>
			</div>
			<div class="AttendanceArea">
				<c:choose>
					<c:when test="${result == 'true'}">
						<div id='volunteerInfo'>
							<input type="text" id='volunteerTitle' value="${volunteer.title}" disabled /> 
							<a>(</a> 
							<input type="text" id='startText' value="${volunteer.startDate}" disabled /> 
							<a>~</a> 
							<input type="text" id='endText' value="${volunteer.endDate}" disabled />
							<a>)</a>
						</div>

								<input type="button" id='backwardBtn'
									style="font-weight: bold; background-image: url('http://192.168.0.23:8080/celab/images/왼쪽화살표.png')"
									value="" /> 
									
										<input type="text" id='dateText' value="${date}" disabled /> 
									
									<input type="button" id='forwardBtn' style="font-weight: bold; background-image: url('http://192.168.0.23:8080/celab/images/오른쪽화살표.png')"
									value="" />

						<div id="LineItemTable">
							<div class="Heading">
								<div class="no">
									<p>번호</p>
								</div>

								<div class="name">
									<p>이름</p>
								</div>

								<div class="status">
									<p>출결상황</p>
								</div>
							</div>
						</div>
					</c:when>

					<c:when test="${result == 'false'}">
						<script> alert("이미 완료된 출석체크 입니다.")</script>
						<div id='volunteerInfo'>
							<input type="text" id='volunteerTitle' value="${volunteer.title}" disabled /> 
							<a>(</a> 
							<input type="text" id='startText' value="${volunteer.startDate}" disabled /> 
							<a>~</a> 
							<input type="text" id='endText' value="${volunteer.endDate}" disabled />
							<a>)</a>
						</div>
					</c:when>

					<c:when test="${result == 'false1'}">
						<script> alert("날짜를 확인해주세요.")</script>
						<div id='volunteerInfo'>
							<input type="text" id='volunteerTitle' value="${volunteer.title}" disabled /> 
							<a>(</a> 
							<input type="text" id='startText' value="${volunteer.startDate}" disabled /> 
							<a>~</a> 
							<input type="text" id='endText' value="${volunteer.endDate}" disabled />
							<a>)</a>
						</div>

					</c:when>

					<c:otherwise>
						<script> alert("검색결과가 없습니다.")</script>
						<div id='volunteerInfo'>
							<input type="text" id='volunteerTitle' value="${volunteer.title}" disabled /> 
							<a>(</a> 
							<input type="text" id='startText' value="${volunteer.startDate}" disabled /> 
							<a>~</a> 
							<input type="text" id='endText' value="${volunteer.endDate}" disabled />
							<a>)</a>
						</div>

					</c:otherwise>
				</c:choose>
				<br>  
				<input type="button" value="출석부 제출하기" id='btnRegist' /> 
				<input type="button" value="출석 현황보기" id='btnStatus' />
				<input type="date" id="searchDate" min="2015-01-01"	max="2017-08-31" name='searchDate' step="1" onkeydown="if(event.keyCode == 13){search(); return false;}" />
				<input type="button" value="검색" id='btnSearch' /> 
			</div>


			<input type="hidden"
				value="${volunteer.no}#${volunteer.startDate}#${volunteer.endDate}"
				id="volunteer" /> <input type="hidden" value="${membersNum}"
				id="membersNum" />
			<c:forEach var="member" items="${members}" varStatus="status">
				<input type="hidden" value="${member.idno}#${member.name}"
					id="member${status.count}" />
				<br />
			</c:forEach>

		</div>
	</section>
</body>
</html>