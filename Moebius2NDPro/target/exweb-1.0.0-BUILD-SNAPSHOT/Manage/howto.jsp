<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html >
<html>
<style>
body {
	background-image: url("../images/mainimage/backwi.png");
	overflow-y: hidden;
}

.main {
	margin-top: 200px;
	height: 1250px;
	width: 1550px;
}

#rightmenu {
	position: fixed;
	left: 10%;
	top: 15%;
}

#rightmenu ul {
	list-style-type: none;
}

.page {
	width: 1600px;
	height: 1024px;
}

.page img {
	z-index: -2;
}

.page img {
	position: absolute;
}

#nav1img {
	top: 250px;
	left: 400px;
	width: 60%;
	height: 60%;
}
#nav2img2 {
	top: 150%;
	left: 600px;
	width: 60%;
	height: 80%;
}

#nav3img {
	top: 250%;
	left: 200px;
}

.textdiv {
	position: relative;
	left: 20%;
	top: 10%;
	z-index: -1;
}
</style>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<title>Insert title here</title>

<script>
function a(a){
	$('.page#' + a+ " .textdiv").slideUp();
	$('.page#' + a+ " .textdiv").slideDown(1000);
	$('.page#' + a+ " img").animate({width: '+=5px',height: '+=5px'});
	$('.page#' + a+ " img").animate({width: '-=5px',height: '-=5px'});
} 


$(document).ready(function(){
	
	 function scrollPage(destination) {
		if(destination =="nav1"){
			$('body,html').stop().animate({'scrollTop':$('#volunteerManage_vHeader').offset().top
			},1000,a(destination))
		} 
		 
		else{ $('body,html').stop().animate({'scrollTop':$('.page#' + destination).offset().top
		},1000,a(destination))
		}
		
	}
	 
	$('ul#nav a').click(function(e){
		e.preventDefault();
		 scrollPage($(this).attr('href').slice(1));
	});
	
	
	
});
</script>
</head>
<body>
<jsp:include page="/Manage/headerVolunteerManage.jsp" flush="false" />  
<div class="main">
<div id="rightmenu">
	<ul id="nav">
	
		<li><a href="#nav1">
		<img src="http://192.168.0.23:8080/celab/images/mainimage/btn1.png">
		</a></li>
		<li><a href="#nav2">
		<img src="http://192.168.0.23:8080/celab/images/mainimage/btn2.png">
		</a></li>
		<li><a href="#nav3">
		<img src="http://192.168.0.23:8080/celab/images/mainimage/btn3.png">
		</a></li>
	</ul>
</div>

<section class="page" id="nav1">
<img src="http://192.168.0.23:8080/celab/images/howToImage1.png" id="nav1img">
<div class="textdiv">
<br>
<font size="6px" color="blue"><b>출석체크하기 -> 봉사활동 클릭 </b></font>
</div>
</section>

<section class="page" id="nav2">
<img src="http://192.168.0.23:8080/celab/images/howToImage2.png" id="nav2img2">
<div class="textdiv">
<font size="6px" color="blue"><b>출결상태 확인</b></font>
<br>
재능기부자의 출결상태를 확인하여 <br>
출석체크를 하고 출석부를 제출<br>
</div>
</section>

<section class="page" id="nav3">
<img src="http://192.168.0.23:8080/celab/images/howToImage3.png" id="nav3img">
<div class="textdiv">
<br><br><br><br><br><br><br><br><br><br><br>
<font size="6px" color="blue"><b>출석 현황보기를 통해 </b></font><br>
<font size="6px" color="blue"><b>출석 현황을 확인</b></font>
</div>
</section>
</div>

</body>
</html>