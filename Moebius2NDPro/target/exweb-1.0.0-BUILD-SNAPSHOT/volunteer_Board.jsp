<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<meta charset="UTF-8" />
<style type="text/css">
body {
	overflow-x: hidden;
	background-color: #EAEAEA;
}

#menu {
	left: 23%;
	position: fixed;
	top: 8%;
}

#menu ul {
	list-style-type: none;
	width: 100%;
	height: 30px;
	margin: 0;
	padding: 0;
}

#menu a:link, #menu a:visited {
	/* display: block;
    font-weight: bold;
    color: #FFFFFF;
    background-color: #98bf21;
    width: 100px;
    text-align: center;
    padding: 4px; */
	text-decoration: none;
	color: #000000;

	/* text-transform: uppercase; */
}

#menu li {
	height: 100%;
	display: block;
	font-weight: bold;
	font-size: 20px;
	color: #000000;
	/* background-color: #98bf21; */
	text-align: center;
	padding: 4px;
	text-decoration: none;
	text-transform: uppercase;
}

#menu a:hover, #menu a:active {
	/* background-color: #7A991A; */
	color: #FF0000;
}

#loginmenu {
	font-size: 15px;
	width: 99%;
	height: 100px;
	color: black;
	text-align: left;
	border: 0px;
	background-color: #DBFFD5;
	position: relative;
	top: 7px;
	left: 0px;
	right: 0px;
	padding: 4px;
	margin: 0px;
}

#loginmenu input {
	margin: 5px;
}

#leftmenu {
	background-color: #A6A6A6;
	position: fixed;
	width: 15%;
	height: 100%;
	top: 8%;
	border: 1px solid #000000;
}

#leftmenu ul {
	list-style-type: none;
	/* width:100%; */
	height: 50px;
	margin: 0;
	padding: 0;
}

#leftmenu li {
	height: 100%;
	display: block;
	font-weight: bold;
	font-size: 20px;
	color: #000000;
	/* background-color: #98bf21; */
	text-align: center;
	padding: 20px;
	text-decoration: none;
}

#leftmenu hr {
	margin-left: 45%;
	margin-right: 45%;
	margin-bottom: 10%;
}

.image .floating1 {
	position: absolute;
	left: 85%;
	z-index: 1;
	top: 180px;
}

.image .floating2 {
	position: absolute;
	left: 70%;
	z-index: 2;
	top: 280px;
}

.image .floating3 {
	position: absolute;
	left: 78%;
	z-index: 3;
	top: 550px;
}

#headrdiv {
	position: fixed;
	width: 100%;
	height: 7%;
	/* display:flex; */
	/* border: 1px solid #000000; */
	padding: 0px;
	margin: 0px;
	bottom: 93%;
}

.moebiusdiv, .logindiv {
	left: 23%;
	height: 100%;
	/* border: 1px solid #000000; */
	width: 12.5%;
	background-color: #000000;
	color: #F6F6F6;
	padding: 0px;
	margin: 0px;
	float: left;
}

.moebiusdiv {
	font-size: 45px;
	font-weight: bold;
	text-align: left;
	position: relative;
	width: 30%;
	border: 1px solid #FFFFFF;
	background-color: #212121;
	padding-left: 5px;
}

.logindiv {
	font-size: 45px;
	font-weight: bold;
	text-align: left;
	position: relative;
	width: 15%;
	border: 1px solid #FFFFFF;
	background-color: #212121;
	padding-left: 5px;
}

.headerhr {
	position: relative;
	top: 2px;
	right: 5px;
	clear: both;
}

.headerhr hr {
	border-style: groove;
	border-width: 3px;
	border-color: #000000;
}

h1 {
	color: gray;
	padding: 0;
	margin: 0;
}

h3 {
	color: gray;
}

a {
	text-decoration: none;
}

p {
	color: white;
}

#main {
	border: solid;
	border-width: thin;
	border-color: #444444;
	padding: 20px;
	padding-top: 0;
	width: 670px;
	border: 50%;
	/* margin-right: auto; */
	position: absolute;
	left: 24%;
	top: 12%;
	z-index: -1;
	border: 50%;
	width: 670px;
}

.SearchArea {
	display: table;
	width: 100%;
	border: solid;
	border-width: thin;
	border-color: #444444;
	background-color: #F6F6F6;
	margin-bottom: 0.2cm;
	margin-left: auto;
	margin-right: auto;
}

.Heading {
	display: table;
	width: 100.3%;
	background-color: #444444;
	margin-left: auto;
	margin-right: auto;
	border-top: solid #444444;
	border-bottom: solid #444444;
	border-withd: thin;
	font-weight: bold;
	text-align: center;
	margin-right: auto;
}

.TableObject {
	display: table;
	width: 100.3%;
	margin-left: auto;
	margin-right: auto;
	border-bottom: solid #444444;
	border-width: thin;
	font-weight: bold;
	text-align: center;
	margin-left: auto;
}

.no, .articleImg, .title, .writer, .place, .startDate, .endDate,
	.participants, .count, .sex {
	display: table-cell;
	width: 13%;
	border-withd: thin;
	font-weight: bold;
	text-align: center;
	margin-left: auto;
	margin-right: auto;
	margin-left: auto;
}

#no, #articleImg, #title, #writer, #place, #startDate, #endDate,
	#participants, #count, #sex {
	display: table-cell;
	width: 13%;
	height: 2cm;
	vertical-align: middle;
	border-withd: thin;
	font-weight: bold;
	text-align: center;
	margin-left: auto;
	margin-right: auto;
}

.buttonBox {
	width: 100%;
	align: center;
	margin: auto;
}

#SB {
	width: 20%;
	height: 50px;
	margin-left: 39%;
}

.page {
	text-align: center;
}
</style>




<title>Insert title here</title>
</head>
<body>



	<header>

		<div id="headrdiv">

			<div class="moebiusdiv">Moebius</div>

			<div class="logindiv"></div>
			<div class="headerhr">
				<hr>
			</div>
		</div>

	</header>

	<%-- <c:choose>
  <c:when test="${message ne null}">
  	alert(메세지);
  </c:when>
</c:choose> --%>



	<div id="menu">

		<!-- <ul>
  <li><a href="">Home</a></li>
  <li><a href="">TEST</a></li>
  <li><a href="">TEST</a></li>
  <li><a href="">TEST</a></li>
  <li><a href="">TEST</a></li>
  <li id="login"><a>Login</a> 
 <div id="loginmenu">
  <br>
  <form >
 
  아&nbsp;이&nbsp;디:&nbsp;&nbsp;<input type="text" ><br>
  비밀번호: <input type="text" >
  <br>
  </form>
     </div>
 </li>
 </ul> -->
		<ul>
			<li><a id="test123">Menu</a></li>
		</ul>

	</div>
	<div id="leftmenu">

		<ul>
			<li><a>HOME</a>
				<hr></li>
			<li><a>BOARD</a>
				<hr></li>
			<li><a>REVIEW</a>
				<hr></li>
			<li><a>LOGO</a>
				<hr></li>
			<li><a>TEST</a>
				<hr></li>
			<li><a>ABCD</a>
				<hr></li>


		</ul>






	</div>

	<div class="image">
		<img src="http://192.168.0.23:8080/celab/images/c1.png"
			class="floating1" width="50" height="50">
	</div>
	<div class="image">
		<img src="http://192.168.0.23:8080/celab/images/b1.PNG"
			class="floating2" width="230" height="230">
	</div>
	<div class="image">
		<img src="http://192.168.0.23:8080/celab/images/b1.PNG"
			class="floating3" width="500" height="500">
	</div>
	<div id="main">
		<h1>봉사정보</h1>
		<div class="SearchArea">
			<form action="/celab/AllVolunteer.do" method=post>
				<input type="hidden" name="listno" value=1> <br>&nbsp;
				<select name="SelectPlace" id="SelectPlace">
					<option value="서울">서울</option>
					<option value="경기">경기</option>
					<option value="제주">제주</option>
				</select>
				<div class="Address1">
					<h3>&nbsp;서울특별시</h3>
					&nbsp; <input type="checkbox" name="chk_address" value="0" checked>서울지역1
					&nbsp; <input type="checkbox" name="chk_address" value="1">서울지역2
					&nbsp; <input type="checkbox" name="chk_address" value="2">서울지역3
					&nbsp; <input type="checkbox" name="chk_address" value="3">서울지역4
					&nbsp; <input type="checkbox" name="chk_address" value="4">서울지역5
					&nbsp;
				</div>
				<div class="Address2">
					<h3>&nbsp;경기도</h3>
					&nbsp; <input type="checkbox" name="chk_address" value="0" checked>경기지역1
					&nbsp; <input type="checkbox" name="ch지역2" value="">경기지역2
					&nbsp; <input type="checkbox" name="ch지역3" value="">경기지역3
					&nbsp; <input type="checkbox" name="ch지역4" value="">경기지역4
					&nbsp; <input type="checkbox" name="ch지역5" value="">경기지역5
					&nbsp;
				</div>
				<div class="Address3">
					<h3>&nbsp;제주도</h3>
					&nbsp; <input type="checkbox" name="chk_address" value="0" checked>제주지역1
					&nbsp; <input type="checkbox" name="ch지역2" value="">제주지역2
					&nbsp; <input type="checkbox" name="ch지역3" value="">제주지역3
					&nbsp; <input type="checkbox" name="ch지역4" value="">제주지역4
					&nbsp; <input type="checkbox" name="ch지역5" value="">제주지역5
					&nbsp;
				</div>


				<h3>&nbsp;재능</h3>
				&nbsp; <input type="checkbox" name="chk_ability" value="0" checked>재능1
				&nbsp; <input type="checkbox" name="chk_ability" value="1">재능2
				&nbsp; <input type="checkbox" name="chk_ability" value="2">재능3
				&nbsp; <input type="checkbox" name="chk_ability" value="3">재능4
				&nbsp; <input type="checkbox" name="chk_ability" value="4">재능5

				<h3>&nbsp;성별</h3>
				&nbsp; <input type="radio" name="chk_sex" value="1" checked>남성&nbsp;&nbsp;
				<input type="radio" name="chk_sex" value="2">여성&nbsp;&nbsp;
				<input type="radio" name="chk_sex" value="3">무관&nbsp;&nbsp;

				<div class="buttonBox">
					<input type="submit" name="searchButton" id="SB" value="검색">
				</div>
				<br>
			</form>
		</div>

		<div class="Heading">
			<div class="no">
				<p>번호</p>
			</div>
			<div class="articleImg">&nbsp;</div>
			<div class="title">
				<p>제목</p>
			</div>
			<div class="writer">
				<p>작성자</p>
			</div>
			<!-- <div class="startDate">
				<p>시작일</p>
			</div>
			<div class="endDate">
				<p>종료일</p>
			</div>
			<div class="place">
				<p>장소</p>
			</div> -->
			<div class="participants">
				<p>모집인원</p>
			</div>
			<div class="sex">
				<p>성별</p>
			</div>
			<div class="count">
				<p>조회수</p>
			</div>


		</div>
		
	</div>


</body>
<script>
	function allVolunteer() {

		var a = document.getElementsByName("chk_sex");
		alert(a);
		//location.href="/firstMVC/AllVolunteer.do?listno=1&chk_sex="+$("input[name="+chk_sex+"]")+"&chk_address=0&chk_ability=0";

	}
	$(document).ready(function() {

		$("#leftmenu").hide();

		$("#test123").click(function() {

			$("#leftmenu").animate({
				width : 'toggle'
			}, 300);

		});
		$(".Address2").hide();
		$(".Address3").hide();
		$("#SelectPlace").change(function() {
			if (this.value == "서울") {
				$(".Address2").hide();
				$(".Address3").hide();
				$(".Address1").show();
				alert("aaa");
			} else if (this.value == "경기") {
				$(".Address1").hide();
				$(".Address3").hide();
				$(".Address2").show();
			} else if (this.value == "제주") {
				$(".Address1").hide();
				$(".Address2").hide();
				$(".Address3").show();
			}
			//alert(this.value);
		});
	});
</script>


</html>
