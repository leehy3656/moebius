<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html>
<html>
<head>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<meta charset="UTF-8" />

<style>
a
{
	text-decoration: none;
	color: #6A84B7;
}
.main {
	border-top :solid #ff7012;
	width: 70%;
	padding-top: 0.2cm;
	margin-top: 1cm;
	margin-left: auto;
	margin-right: auto;
}
.detailImage
{
position:relative;
width:100%;

}

.epilogue {
	margin-bottom:1cm;
	display: flex;
}

#EpTextArea {
	width: 12.7cm;
	height: 3cm;
}


.EpObject {
	display: flex;
 	border-top: solid #8C8C8C;
	border-width: thin; 
	width: 100%;
	height: 4.7cm;
	margin-left: auto;
	margin-right: auto;
}
.UpdatePrepare, #RemoveA, .Update{
	height:0.8cm;
	width:1.5cm;
	cursor: pointer;
	text-decoration: none;
	color: #6A84B7;
}
#contentAndtextBox{
	
	width:72%;
}
#ImgBox{
	margin-top:0.5cm;
	width:20%;
}

#epContentBox{
	/* border:solid;
	border-width:thin; */
	height:1cm;
	padding-left: 0.2cm;
	padding-top: 0.2cm;
	display:flex;
}
.EpText{
}
.epTextArea {
	width: 97%;
	height: 3cm;
	padding: 0.2cm;
	background-color: white;
}

.EpButtonBox {
	height: auto;
	width:30%;
}

#EpRegist, #ImgRegist {
	height: 1.5cm;
	width: 90%; 
	margin-left: 0.5cm; 
}

.page {
	border: solid;
	margin-left: auto;
	margin-right: auto;
}

</style>
</head>
<body>
<div class="main">
<form action="EpRegist.do" method="post" enctype="multipart/form-data">
		<div class="epilogue">
		<div class ="EpText">
			<textarea id="EpTextArea" name ="CONTENT">후기를 등록해 주세요.</textarea>
			</div>
			<div class="EpButtonBox">  
				<input type="file" id="ImgRegist" name="IMG">
				<!-- <input type="button" id="ImgRegist" value="사진 첨부" onclick="ImgRegist()">  -->
				<input type="hidden" name="volunteer" id="volunteer" value='${volunteer.no}'>
				<input type="submit" id="EpRegist" value="후기 등록">
			</div>
		</div>
		</form>
		<br>
		
		<c:forEach var="epilogue" items="${epilogueList}" varStatus="status"
			begin="${(listno)*listSize-5}" end="${(listno)*listSize-1}">
			<div>
			<form action = "EpUpdate.do" method="post" enctype="multipart/form-data" id="ep${epilogue.no}">
			<div class="EpObject">
			
			<div id="contentAndtextBox">
				<div id="epContentBox">
				NO : ${status.count+(listno-1)*listSize}&nbsp;&nbsp;&nbsp;ID : ${epilogue.member.idname}&nbsp;&nbsp;&nbsp;${epilogue.date}
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				
				<div class="Update">
				<a onclick="updateEp(${epilogue.no},this)">완료</a>
				</div>
				<div class="UpdatePrepare">수정</div>|&nbsp;
				<div id="RemoveA">
				<a href="/celab/EpRemove.do?no=${epilogue.no}&listno=1">삭제</a>
				</div>
				</div>
					<textarea name="content" class="epTextArea" disabled="disabled">${epilogue.content}</textarea>
				</div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<div id="ImgBox">
				<input type="file" id="fileEp" class="fileEp" name="fileEp" onchange="readURL(this,${epilogue.no})"/> 
				<img src="${ctx}/celab/epimages/${epilogue.img}"
						class="floating1" width="150px" height="150px" id="EpImg${epilogue.no}">
					<input type="hidden" name="no" value="${epilogue.no}"/>	
				</div>
					
			</div>
		</form>
		</div>
			<br>
		</c:forEach>

		<br>
		<div class="page">
			<c:forEach var="no" begin="1" end="${pageSize}">
				<a href="/celab/EpSelect.do?listno=${no}&no=${volunteer.no}">${no}</a>
			</c:forEach>
		</div>
	</div>

</body>
</html>