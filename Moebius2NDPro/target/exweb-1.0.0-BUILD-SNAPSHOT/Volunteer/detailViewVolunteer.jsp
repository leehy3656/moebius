<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html>
<html>
<head>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<meta charset="UTF-8" />
<link rel="stylesheet" type="text/css" href="/celab/css/allcss.css" />

<!-- 스타일 -->
<style type="text/css">
.sectiondiv p
{
font-weight: bold;
} 
.volunteerdetailsee{
	position: relative;
	width: 70%;
	margin-left: auto;
	margin-right: auto;
}
.detailtextarea{
	margin-left:auto;
	margin-right: auto;
}

.detailtextarea textarea{
	background-color: #FFFFFF;
}

.amagin
{
height: 13px;
}
.place1,.place2
{
display: inline;
}
.place1
{
float: left;
}
.place2
{
float: right;
}
.fontbold
{
padding-top:10px;
width: 100%;
}
/*======================================= */

a
{
	text-decoration: none;
	color: #6A84B7;
}
.main {
	border-top :solid #ff7012;
	width: 70%;
	padding-top: 0.2cm;
	margin-top: 1cm;
	margin-left: auto;
	margin-right: auto;
}
.detailImage
{
position:relative;
width:100%;

}

.epilogue {
	margin-bottom:1cm;
	display: flex;
}

#EpTextArea {
	width: 12.7cm;
	height: 3cm;
}


.EpObject {
	display: flex;
 	border-top: solid #8C8C8C;
	border-width: thin; 
	width: 100%;
	height: 4.7cm;
	margin-left: auto;
	margin-right: auto;
}
.UpdatePrepare, #RemoveA, .Update{
	height:0.8cm;
	width:1.5cm;
	cursor: pointer;
	text-decoration: none;
	color: #6A84B7;
}
#contentAndtextBox{
	
	width:72%;
}
#ImgBox{
	margin-top:0.5cm;
	width:20%;
}

#epContentBox{
	/* border:solid;
	border-width:thin; */
	height:1cm;
	padding-left: 0.2cm;
	padding-top: 0.2cm;
	display:flex;
}
.EpText{
}
.epTextArea {
	width: 97%;
	height: 3cm;
	padding: 0.2cm;
	background-color: white;
}

.EpButtonBox {
	height: auto;
	width:30%;
}

#EpRegist, #ImgRegist {
	height: 1.5cm;
	width: 90%; 
	margin-left: 0.5cm; 
}

.page {
	border: solid;
	margin-left: auto;
	margin-right: auto;
}

.abilitydiv{
overflow-y: scroll;
}
.abilitydiv div
 {
	
	float: left;
	margin-right: 10%;
	
}



</style>

<!-- 스크립트 -->
<script>
	
	
	function updatevolunteer(){
		location.href = "/celab/updateVolunteer.do?no=${volunteer.no}";
	
	}
	
	function applyVolunteer(){
		var url = CONTEXT +"/applyVolunteer.do";
		var volunteerNum = ${volunteer.no};
		var params={
			volunteerNum : volunteerNum,
		}
		sendAjax(url, params, applyVolunteerCallback);
	}
	
	function applyVolunteerCallback(result){
		if(result =="fail"){
			alert("이미 신청하셨습니다.");
		}
		else if( result =="success") {
			alert("신청 완료");
		}
	}
		
	$(document).ready(function() {

						function sex1() {
							var csex = ${volunteer.sex};
							var sex = document.getElementsByName("sex");
							/* alert(csex); */
							for (var i = 0; i < sex.length; i++) {

								if (sex.item(i).value == csex) {
									sex.item(i).checked = "true";
								} else if (sex.item(i).value == csex) {
									sex.item(i).checked = "true";
								} else if (sex.item(i).value == csex) {
									sex.item(i).checked = "true";
								}
							}
						}
						sex1();
					});
	
	/*======================================================================================  */
		function readURL(input,no) {   
	 if(input.files && input.files[0]){
	   var reader = new FileReader();
	   reader.onload = function(e){
	    //$('#uploadImage').html("<img id=img src=''>");
	   
	 $('#EpImg'+no).attr('src', e.target.result);
	    
	   }
	   reader.readAsDataURL(input.files[0]);
	 }  
	  
	}
	
	function updateEp(no,e) { 
		
		$('#ep'+no).submit();

	
 /*    $.ajax({
        url: '/celab/EpUpdate.do',
        type: "post",
        dataType: "text",
        data: data,
        cache: false,
        processData: false,
        contentType: false,
        success: function(data) {
        	alert("123");
        }, error: function(data) {
        	alert("error");
        }
    }); */
	

	}
	  
	/*function EpRegist() {
		 var content = document.getElementById("EpTextArea").value;
		var img = document.getElementById("ImgRegist").value;
		var listno = 1;
		location.href = "/celab/EpRegist.do?content=" + content + "&img=" + img + "&listno=" + listno+"&no="+${volunteer.no} ; 
	}*/

	function ImgRegist() {
		location.href = "ImgRegist.do";
	} 
	$(document).ready(function() {	
		$(".requestvl").hide();
		$(".updatevl").hide();
		var showupdate = ${volunteer.memberPerson.idno};
		var member = ${member.idno};
		if(showupdate == member){
			$(".updatevl").show();
		}else{
			
			$(".requestvl").show();
		}
		
		$(".main").hide();
		var showhide=${volunteer.status};
		
		
		if(showhide==7){
			$(".main").show();		
		}
		
		$(".Update").hide();
		$(".fileEp").hide();
	$(".EpObject .UpdatePrepare").click(function () { 
		$(this).parent().parent().parent().find("textarea").prop("disabled",false);
		$(this).hide();
		$(this).parent().find(".Update").show();
		$(this).parent().parent().parent().find(".fileEp").show();  
	}); 
	
	});
</script>

<title>Insert title here</title>
</head>

<!-- HTML -->
<body>
<jsp:include page="../mainpage/headerMoebius.jsp" flush="false"/>
<section class="sectiondiv">
	<div class="volunteerdetailsee">
		<div class="sectiontitle">
			<font size=6>봉사게시판</font>
		</div>

	<div class = "fontbold"><p>제목</p> 
		<hr color="e77026" width=100% align="left">
			<div>${volunteer.title}</div>
			</div>
	
	<div class = "fontbold"><p>주소</p>
		<hr color="e77026" width=100% align="left">
<div>연락처:${volunteer.phoneno}</div>
<div class="place1">장소:${volunteer.place}</div><div class="place2">상세장소&nbsp;:&nbsp;${volunteer.place2}&nbsp;&nbsp;&nbsp;&nbsp;</div></div>
	<div class = "fontbold"><p>기간</p>
		<hr color="e77026" width=100% align="left">
			<div class="place1">${volunteer.startDate}&nbsp;-&nbsp;${volunteer.endDate}</div><div class="place2">요청요일&nbsp;:&nbsp;
			<c:forEach var="weekday" items="${weekdays}">
						${weekday.name}&nbsp;
					</c:forEach></div><br>
			</div>
	<!--  -->
	<div class = "fontbold"><p>모집인원</p>
		<hr color="e77026" width=100% align="left">
			<div class="place1">성&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;별&nbsp;&nbsp;:&nbsp;<c:choose>
									<c:when test="${volunteer.sex == '1'}">남성</c:when>
									<c:when test="${volunteer.sex == '2'}">여성</c:when>
									<c:when test="${volunteer.sex == '3'}">무관</c:when>
								</c:choose></div>
			<div class="place2">모집인원&nbsp;:&nbsp;${volunteer.participant}&nbsp;명&nbsp;&nbsp;&nbsp;</div><br>
			</div>
		<div class = "fontbold"><p>재능</p>
		<hr color="e77026" width=100% align="left">
		<div class="abilitydiv">
					<c:forEach var="ability" items="${abilitys}">
						${ability.abilityName}&nbsp;
					</c:forEach>
		</div>
				
	</div>
	<div class = "amagin">
</div>
<div class = "fontbold"><p>구체적사항</p>
<hr color="e77026" width=100% align="left">
<div class="detailImage">
<img src="${ctx}/celab/images/${volunteer.filevolunteer}" width=100% height=30%>
</div>
</div>
<div class="detailtextarea">
	<textarea  rows="15" cols="92" name="memo" disabled="disabled">${volunteer.memo}</textarea>
</div>
<input type="button" value="봉사신청하기" id="testbtn" name='regist' onclick="applyVolunteer()" class= requestvl>
<input type="button" value="수정" name='update' onclick='updatevolunteer()' class='updatevl'>
</div>
	
<div class="main">
<form action="EpRegist.do" method="post" enctype="multipart/form-data">
		<div class="epilogue">
		<div class ="EpText">
			<textarea id="EpTextArea" name ="CONTENT">후기를 등록해 주세요.</textarea>
			</div>
			<div class="EpButtonBox">  
				<input type="file" id="ImgRegist" name="IMG">
				<!-- <input type="button" id="ImgRegist" value="사진 첨부" onclick="ImgRegist()">  -->
				<input type="hidden" name="volunteer" id="volunteer" value='${volunteer.no}'>
				<input type="submit" id="EpRegist" value="후기 등록">
			</div>
		</div>
		</form>
		<br>
		
		<c:forEach var="epilogue" items="${epilogueList}" varStatus="status"
			begin="${(listno)*listSize-5}" end="${(listno)*listSize-1}">
			<div>
			<form action = "EpUpdate.do" method="post" enctype="multipart/form-data" id="ep${epilogue.no}">
			<div class="EpObject">
			
			<div id="contentAndtextBox">
				<div id="epContentBox">
				NO : ${status.count+(listno-1)*listSize}&nbsp;&nbsp;&nbsp;ID : ${epilogue.member.idname}&nbsp;&nbsp;&nbsp;${epilogue.date}
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				
				<div class="Update">
				<a onclick="updateEp(${epilogue.no},this)">완료</a>
				</div>
				<div class="UpdatePrepare">수정</div>|&nbsp;
				<div id="RemoveA">
				<a href="/celab/EpRemove.do?no=${epilogue.no}&listno=1">삭제</a>
				</div>
				</div>
					<textarea name="content" class="epTextArea" disabled="disabled">${epilogue.content}</textarea>
				</div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<div id="ImgBox">
				<input type="file" id="fileEp" class="fileEp" name="fileEp" onchange="readURL(this,${epilogue.no})"/> 
				<img src="${ctx}/celab/epimages/${epilogue.img}"
						class="floating1" width="150px" height="150px" id="EpImg${epilogue.no}">
					<input type="hidden" name="no" value="${epilogue.no}"/>	
				</div>
					
			</div>
		</form>
		</div>
			<br>
		</c:forEach>

		<br>
		<div class="page">
			<c:forEach var="no" begin="1" end="${pageSize}">
				<a href="/celab/EpSelect.do?listno=${no}&no=${volunteer.no}">${no}</a>
			</c:forEach>
		</div>
	</div>
</section>

</body>

</html>