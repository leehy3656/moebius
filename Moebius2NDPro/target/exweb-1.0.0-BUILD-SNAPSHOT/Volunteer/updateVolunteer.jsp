<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="/celab/css/allcss.css" />
<meta charset="UTF-8" />
<style type="text/css">
#updateVolunteer input {
	margin: 4px;
}

.titlediv, .phonenodiv, .datediv, .sexdiv, .abilitydiv, .textdiv {
	/* border: 2px solid #A6A6A6; */
	margin-top:0.1cm;
	padding: 5px;
	background-color:#F0F0F0;
}
.sex{}
.#sex{}

#img
{
position:relative;
width:100%;
height: 100%;
}

.colordiv {
	width: 100%;
	height: 30px;
	/* background-color:#EAEAEA; */
	/* #FFC81E; */
	color: #000000;
	
}
.colordiv hr
{
border-color: #FF7012;
}
.spacediv
{
height: 10px;
}
#updateVolunteer {
	z-index: -1;
	position: relative;
	width:70%; 
	padding: 0px;
	margin-left:auto;
	margin-right:auto;
	font-weight: bold;
	
}
.abilitydiv{
overflow-y: scroll;
}
.abilitydiv div
 {
	
	float: left;
	margin-right: 10%;
	
}
</style>
<title>Insert title here</title>
</head>


<body>
	<c:if test="${message ne null}">
		<script type="text/javascript">
			alert('${message}');
		</script>
	</c:if>

<jsp:include page="/mainpage/headerMoebius.jsp" flush="false"/> 

<section class="sectiondiv">
	<div id="updateVolunteer">
	<div class="sectiontitle">
		<font size=6>재능기부활동 글 수정</font>&nbsp;&nbsp; <font size=2>별표시(*)는
			필수입력사항입니다.</font>
			</div>
		<form action="/celab/updateVolunteer.do" method=post id ="updateForm" name="updateForm">
			<input type="hidden" value='${member}' name="memberPerson" id="memberPerson">
			<input type="hidden" value=0 name="hit" id="hit">
			<input type="hidden" value='${volunteer.status}' name="status" id="status2">
			<input type="hidden" value='${volunteer.filevolunteer}' name="filevolunteer" id="filevolunteersrc">
			
<!--participant:    $("#participant").val(),
					hit:    		$("#hit").val()  -->
			<div class="colordiv">&nbsp;제목<hr></div>
			
			<div class="titlediv">
				제&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;목*&nbsp;<input type="text"
					name="title" size=75 id="title" value='${volunteer.title}'>
			</div>
			<div class="spacediv"></div>

			<div class="colordiv">&nbsp;주소<hr></div>
			<div class="phonenodiv">
				연&nbsp;락&nbsp;처*<input type="text" name="phoneno" id="phoneno"
					style="ime-mode: disabled;" onkeydown="return onlyNumber(event)" value='${volunteer.phoneno}'>&nbsp;<font
					size=1>숫자만 입력가능합니다.</font><br>
					
				장&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;소*
				<input type="text" name="post" id="post" value='${volunteer.post}'>
				<input type="text" name="place" id="place" value='${volunteer.place}'>
				<input type='button'  value="찾기" onclick='startSearPost()' />
				<br>
				상세장소*<input type="text" id="place2" name="place2" id="place2" value='${volunteer.place2}'>
			</div>
			<div class="spacediv"></div>


			<div class="colordiv">&nbsp;기간<hr></div>
			<div class="datediv">

				날&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;짜*&nbsp;
				<input type="date"max="2050-01-01" step=1 name="startDate" id="startDate"value="${volunteer.startDate}" onchange="weekdayset()">- 
				<input type="date" max="2050-01-01" step=1 name="endDate" id="endDate"value="${volunteer.endDate}" onchange="weekdayset()">
				<!-- <input type="button" value="요일선택" id="weekbtn"/> -->
				<div class="weekday">
				
					<input type="checkbox" id="1" value="${weekday[0].no}-${weekday[0].name}" name="weekday" />일
					<input type="checkbox" id="2" value="${weekday[1].no}-${weekday[1].name}" name="weekday" />월
					<input type="checkbox" id="3" value="${weekday[2].no}-${weekday[2].name}" name="weekday" />화
					<input type="checkbox" id="4" value="${weekday[3].no}-${weekday[3].name}" name="weekday" />수
					<input type="checkbox" id="5" value="${weekday[4].no}-${weekday[4].name}" name="weekday" />목
					<input type="checkbox" id="6" value="${weekday[5].no}-${weekday[5].name}" name="weekday" />금
					<input type="checkbox" id="7" value="${weekday[6].no}-${weekday[6].name}" name="weekday" />토
					
					<c:forEach var="checkWeekdays" items ="${volunteer.weekday}">
					<%-- <input type="hidden" name="checkWeekday" id="getweekday" value ="${checkWeekdays.no}-${checkWeekdays.name}"/> --%>
					<script>
					$("input[id=${checkWeekdays.no}]").prop('checked',true);
					</script>			
					</c:forEach>
					
				</div>
			</div>

			<div class="spacediv"></div>
			<div class="colordiv">&nbsp;모집인원<hr></div>
			<div class="sexdiv">
				성&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;별&nbsp;&nbsp; 
				<input type="radio" value="3" name="sex" id="sex">무관
				<input type="radio" value="1" name="sex" id="sex">남성
				<input type="radio" value="2" name="sex" id="sex">여성
				<br> 
				모집인원*&nbsp;<input type='text' id="participant" name="participant" size=5 style="ime-mode: disabled;" onkeydown="return onlyNumber(event)"
						value='${volunteer.participant}'>명&nbsp;<font size=1>숫자만 입력가능합니다.</font> <br>
			</div>

			<div class="spacediv"></div>
			<div class="colordiv">&nbsp;필요재능<hr></div>
		<div class="abilitydiv">
				<div class="container">
				
					<c:forEach var="ability" items="${abilitys1}">
						<input type="checkbox" value="${ability.adNo}-${ability.abilityName}" name='ability'/> ${ability.abilityName}
						 <br/>
					</c:forEach>
				</div>
				<div class="container">
					<c:forEach var="ability" items="${abilitys2}">
						<input type="checkbox" value="${ability.adNo}-${ability.abilityName}" name='ability'/> ${ability.abilityName} <br/>
					</c:forEach>
				</div>
				<div class="container">
					<c:forEach var="ability" items="${abilitys3}">
						<input type="checkbox" value="${ability.adNo}-${ability.abilityName}" name='ability'/> ${ability.abilityName} <br/>
					</c:forEach>
				</div>
				<c:forEach var="volunteerAbility" items ="${volunteer.abilitys}">
					<input type="hidden" name="checkAbility" value ="${volunteerAbility.adNo}-${volunteerAbility.abilityName}"/>
				</c:forEach>
			</div>
			
			<div class="spacediv"></div>
			<div class="colordiv">&nbsp;구체적사항<hr></div>
			<div class="textdiv">
			<div id="uploadImage"></div>
			<!-- <input type="file" name="filevolunteer"> -->
<!-- <input type="text" name="filevolunteer" id="filevolunteer" value="test"> -->
<img id=img src="${ctx}/celab/images/${volunteer.filevolunteer}"/>
  <input name="filevolunteer" id="filevolunteer" type="file" onchange="readURL(this)"
<%--   value="${ctx}/celab/images/${volunteer.filevolunteer}" --%>
  />
  
	<input type="hidden" id="no" value = "${volunteer.no}"/>
<textarea rows="8" cols="90" name="memo" id="memo">${volunteer.memo}</textarea>

			</div>
			<div align="right">
					<input type="button" value="수정" id="updateVbtn">
					<input type="button"value="삭제" id="deleteVbtn" onclick="deletev()">
					<input type="button"value="취소" id="testbtn" onclick="cansel()">
			</div>
		</form>
	</div>
</section>


</body>
<!-- 스크립트 -->
<script>






function readURL(input){
	  
	 if(input.files && input.files[0]){
	   var reader = new FileReader();
	   reader.onload = function(e){
	    //$('#uploadImage').html("<img id=img src=''>");
	    $('#img').attr('src', e.target.result);
	   }
	   reader.readAsDataURL(input.files[0]);
	 }  
	  
	}


function weekdayset(){

	/* if (check == 0) {
		for (var i = 1; i < 8; i++) {

			$("input[id=" + i + "]")
					.prop('disabled',
							false);
			$("input[id=" + i + "]")
					.prop('checked',
							true);

		}

		$(".weekday").show(); 
		document.getElementById("weekbtn").value = "요일결정";
		check = 1;
		*/
		var dateString = document.getElementById("startDate").value;

		var dateArray = dateString.split("-");

		var dateObj = new Date(dateArray[0],dateArray[1],dateArray[2]);

		var dateString2 = document.getElementById("endDate").value;

		var dateArray2 = dateString2.split("-");

		var dateObj2 = new Date(dateArray2[0],dateArray2[1],dateArray2[2]);

		var betweenDay = (dateObj2.getTime() - dateObj.getTime())/ 1000 / 60 / 60 / 24;
		//alert(betweenDay);

		var y = parseInt(dateArray[0]);
		var m = parseInt(dateArray[1]);
		var d = parseInt(dateArray[2]);

		var a;
		var b;
		if (m == 1 || m == 2) {
			y = y - 1
			a = y + y / 4 - y / 100 + y/ 400+ (m * 26 + 16)/ 10 + d;

			b = parseInt(a % 7);
			//alert(b);
		} else {
			a = y + y / 4 - y / 100 + y/ 400+ (m * 26 + 16)/ 10 + d;

			b = parseInt(a % 7);

		}

		var j = 0;

		if (betweenDay > 7) {
			for (var i = 1; i < 8; i++) {

				$("input[id=" + i + "]").prop('disabled',false);
				$("input[id=" + i + "]").prop('checked',true);

			}

		} else {
			for (var i = 1; i < 8; i++) {

				$("input[id=" + i + "]").prop('disabled',true);
				$("input[id=" + i + "]").prop('checked',false);

			}

			for (var i = b; j < betweenDay+1; i++) {

				$("input[id=" + i + "]").prop('disabled',false);
				$("input[id=" + i + "]").prop('checked',true);

				j++;
				if (i >= 7) {
					i = 0;
				}
			}
		}

	}

function cansel(){
	location.href = "AllVolunteer.do?listno=1&chk_menu=0";	
}
function deletev(){
	alert("삭제?넘어오긴함?");
	location.href = "/celab/delete.do?no=${volunteer.no}";	
	
}
function startSearPost() {
	var myWindow = window.open("Member/Post/address.jsp", "우편주소검색",
	"resizable=no,width=605, height=710"); 
} 
function 주소받다(post, postAddress) {
	var txtPost = document.getElementById("post");
	var txtPostAddress = document.getElementById("place");
	txtPost.value = post;
	txtPostAddress.value = postAddress;
}

	$(document).ready(function() {
	
	
	function updateV() {
		var volunteer={'@idno':1,
				no:				$("#no").val(),
				title:			$("#title").val(),
				startDate:		$("#startDate").val(),
				endDate:		$("#endDate").val(),
				sex:			parseInt($("#sex:checked").val()),
				memo:			$("#memo").val(),
				post:			$("#post").val(),
				place:			$("#place").val(),
				place2:			$("#place2").val(),
				abilitys:		[], 
				weekday:		[], 
				phoneno:		$("#phoneno").val(),
				memberPerson:	 null,  
				filevolunteer:	$("#filevolunteersrc").val(),
				participant:    parseInt($("#participant").val()),
				hit:    		parseInt($("#hit").val()),
				status: parseInt($("#status2").val())
				};

		
$("input[name=ability]:checked").each(function() {
			
			var selectedAbility = $(this).val();
			var adNo = selectedAbility.split('-')[0];
			var parcedAdNo=parseInt(adNo);
			
			var abilityName = selectedAbility.split('-')[1]; 
				
			var ab={
					adNo: parcedAdNo,
					abilityName: abilityName
			};
			
			volunteer.abilitys.push(ab);
							
		}); 
 		 $("input[name=weekday]:checked").each(function() {
			
			var weekday = $(this).val();
			var wno = weekday.split('-')[0];
			var no=parseInt(wno);
			var wname = weekday.split('-')[1]; 
				
			var weekd={
						no: no,
					name: wname
			};
			
			volunteer.weekday.push(weekd);
							
		}); 
		 
		 
		var replacer= function(key, value) {	
	  		  if (key=="volunteer" && typeof value == "object") {
				  
	  		    return value['@idno'];
	  		    
	  		  }
	  		  return value;
	  		};
	  		
	  		
	    	$.ajax({ 
	    	    url: "/celab/updateVolunteerajax.do",    
	    	    type: 'POST', 
	    	    dataType: 'text', //response가 text일때 json이면 json표기
	    	    data: JSON.stringify(volunteer, replacer), 
	    	    contentType: 'application/json ; charset=UTF-8',
	    	    //mimeType: 'application/json',
	    	    
	    	    success: function(data) { 
	    	    	alert("수정되었습니다.");
	    	    	location.href="/celab/AllVolunteer.do?listno=1&chk_sex=0&chk_address=0&chk_ability=0";
	    	    },
	    	    
	    	    error:function( ) { 
	    	        alert("error");
	    	    } 
	    	});
	}
	$("#updateVbtn").click(function(){
		var windowsform = window.document.updateForm;
		
/* 		if(windowsform.title.value.length<1){
			alert("제목을 입력해주세요");
			return;
		
		}
		if(windowsform.phoneno.value.length<10){
			alert("연락처를 확인해주세요");
			return;
		
		}
		if(windowsform.post.value.length<1||windowsform.place.value.length<1||winform.place2.value.length<1){
			alert("주소를 확인해주세요");
			return;
		
		}
		if(windowsform.startDate.value>windowsform.endDate.value){
			alert("기간을 확인해주세요");
			return;
		} */
		
		
		var data = new FormData();
		var afile=$('#filevolunteer')[0].files[0];
		
		if(afile==null){
			updateV();
	
			
		}else{
		data.append("filevolunteer",afile);
		
        $.ajax({
            url: '/celab/updateupload.do',
            type: "post",
            dataType: "text",
            data: data,
            cache: false,
            processData: false,
            contentType: false,
            success: function(data, textStatus, jqXHR) {
            	updateV();
            }, error: function(jqXHR, textStatus, errorThrown) {
            	alert("error");
            }
        });
		}
		
	});	
	
	
	function sex1() {
		var csex = ${volunteer.sex};
		var sex = document.getElementsByName("sex");
		for (var i = 0; i < sex.length; i++) {

			if (sex.item(i).value == csex) {
				sex.item(i).checked = "true";
			} 
		}
	}
	function checkAbility(){
		var ability = document.getElementsByName("ability");
		var checkAbility = document.getElementsByName("checkAbility");
		for(var i=0; i<ability.length; i++){
			for(var j=0; j<checkAbility.length; j++){
				if(ability[i].value == checkAbility[j].value){
					ability[i].checked =true;
				}
			}
		} 
		
	}
	
	checkAbility();
	sex1();
});
</script>
</html>
