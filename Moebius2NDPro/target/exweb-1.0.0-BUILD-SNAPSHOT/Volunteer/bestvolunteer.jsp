<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/celab/css/allcss.css" />
<style type="text/css">
#bestvolunteer {
	z-index: -1;
	position: relative;
	width:70%; 
	padding: 0px;
	margin-left:auto;
	margin-right:auto;
	font-weight: bold;
	
	
}
.sectiondiv p
{
font-weight: bold;
}
.detailtextarea{
	margin-left:auto;
	margin-right: auto;
}

.detailtextarea textarea{
	background-color: #FFFFFF;
}

.amagin
{
height: 13px;
}
.place1,.place2
{
display: inline;
}
.place1
{
float: left;
}
.place2
{
float: right;
}
.fontbold
{
padding-top:10px;
width: 100%;
}

a
{
	text-decoration: none;
	color: #6A84B7;
}
.main {
	border-top :solid #ff7012;
	width: 70%;
	padding-top: 0.2cm;
	margin-top: 1cm;
	margin-left: auto;
	margin-right: auto;
}
.detailImage
{
position:relative;
width:100%;

}

.epilogue {
	margin-bottom:1cm;
	display: flex;
}

#EpTextArea {
	width: 12.7cm;
	height: 3cm;
}


.EpObject {
	display: flex;
 	border-top: solid #8C8C8C;
	border-width: thin; 
	width: 100%;
	height: 4.7cm;
	margin-left: auto;
	margin-right: auto;
}
.UpdatePrepare, #RemoveA, .Update{
	height:0.8cm;
	width:1.5cm;
	cursor: pointer;
	text-decoration: none;
	color: #6A84B7;
}
#contentAndtextBox{
	
	width:72%;
}
#ImgBox{
	margin-top:0.7cm;
	width:20%;
}
#epContentBox{
	/* border:solid;
	border-width:thin; */
	height:1cm;
	padding-left: 0.2cm;
	padding-top: 0.2cm;
	display:flex;
}
.EpText{
}
.epTextArea {
	width: 97%;
	height: 3cm;
	padding: 0.2cm;
	background-color: white;
}

.EpButtonBox {
	height: auto;
	width:30%;
}

#EpRegist, #ImgRegist {
	height: 1.5cm;
	width: 90%; 
	margin-left: 0.5cm; 
}

.page {
	border: solid;
	margin-left: auto;
	margin-right: auto;
}

.abilitydiv{
overflow-y: scroll;
}
.abilitydiv div
 {
	
	float: left;
	margin-right: 10%;
	
}
</style>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<jsp:include page="/mainpage/headerMoebius.jsp" flush="false"/> 
<section class="sectiondiv">
	<div id="bestvolunteer">
		<div class="sectiontitle">
		<font size=6>Best Volunteer</font>
			</div>
	<div class = "fontbold"><p>제목</p> 
		<hr color="e77026" width=100% align="left">
			<div>${volunteer.title}</div>
			</div>
	
	<div class = "fontbold"><p>주소</p>
		<hr color="e77026" width=100% align="left">
<div>연락처:${volunteer.phoneno}</div>
<div class="place1">장소:${volunteer.place}</div><div class="place2">상세장소&nbsp;:&nbsp;${volunteer.place2}&nbsp;&nbsp;&nbsp;&nbsp;</div></div>
	<div class = "fontbold"><p>기간</p>
		<hr color="e77026" width=100% align="left">
			<div class="place1">${volunteer.startDate}&nbsp;-&nbsp;${volunteer.endDate}</div><div class="place2">요청요일&nbsp;:&nbsp;
			<c:forEach var="weekday" items="${weekdays}">
						${weekday.name}&nbsp;
					</c:forEach></div><br>
			</div>
	<!--  -->
	<div class = "fontbold"><p>모집인원</p>
		<hr color="e77026" width=100% align="left">
			<div class="place1">성&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;별&nbsp;&nbsp;:&nbsp;<c:choose>
									<c:when test="${volunteer.sex == '1'}">남성</c:when>
									<c:when test="${volunteer.sex == '2'}">여성</c:when>
									<c:when test="${volunteer.sex == '3'}">무관</c:when>
								</c:choose></div>
			<div class="place2">모집인원&nbsp;:&nbsp;${volunteer.participant}&nbsp;명&nbsp;&nbsp;&nbsp;</div><br>
			</div>
		<div class = "fontbold"><p>재능</p>
		<hr color="e77026" width=100% align="left">
		<div class="abilitydiv">
					<c:forEach var="ability" items="${abilitys}">
						${ability.abilityName}&nbsp;
					</c:forEach>
		</div>
				
	</div>
	<div class = "amagin">
</div>
<div class = "fontbold"><p>구체적사항</p>
<hr color="e77026" width=100% align="left">
<div class="detailImage">
<img src="${ctx}/celab/images/${volunteer.filevolunteer}" width=100% height=30%>
</div>
</div>
<div class="detailtextarea">
	<textarea  rows="15" cols="92" name="memo" disabled="disabled">${volunteer.memo}</textarea>
</div>
</div>	
<div class="main">
		<c:forEach var="epilogue" items="${epilogueList}">
			<div class="EpObject">
			<div id="contentAndtextBox">
				<div id="epContentBox">
				NO : ${status.count}&nbsp;&nbsp;&nbsp;ID : ${epilogue.member.idname}&nbsp;&nbsp;&nbsp;${epilogue.date}
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</div>
					<textarea class="epTextArea" disabled="disabled">${epilogue.content}</textarea>
				</div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<div id="ImgBox">
				<img src="${ctx}/celab/epimages/${epilogue.img}"
						class="floating1" width="150px" height="150px">
				<%-- ${epilogue.img} --%>
				</div>
			</div>
			<br>
		</c:forEach>

		
		<div class="totalpage">
			<c:forEach var="no" begin="1" end="${totalsize}">
				<a href="/celab/BestDetailView.do?listno=${no}">${no}</a>
			</c:forEach>
		</div>
		
			</div>
</section>
</body>
</html>