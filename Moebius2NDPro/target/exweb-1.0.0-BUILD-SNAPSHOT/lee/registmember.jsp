<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
   <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
  
<!DOCTYPE html>
<html>
<head>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<meta charset="UTF-8" />
<style type="text/css">
#main {
	border: solid;
	padding: 20px;
	width: 500px;
	margin-left: auto;
	margin-right: auto;
	background-color: #EAEAEA;
	position: absolute;
	left:33%;
	top:9%;
	z-index: -1;
}

#logo {
	border: solid;
	border-width: thin;
	background-color: #212121;
	color: white;
	width: 100%;
}
/* 아이디, 비밀번호  */
#id {
	border: solid;
	border-width: thin;
	width: 100%;
	height: 40px;
	border-top: none;
	border-bottom: none;
	background-color: white;
}

#idText {
	width: 33%;
	height: 38px;
	border: none;
	margin-left: 2%;
}

#idCheckText {
	width: 37%;
	height: 38px;
	border: none;
	background-color: white;
}

#idCheck {
	width: 25%;
	height: 38px;
}

#password {
	border: solid;
	border-width: thin;
	width: 100%;
	height: 40px;
	background-color: white;
}

#passwordText {
	border: none;
	width: 98%;
	margin-left: 2%;
	height: 38px;
}

#rpassword {
	border: solid;
	border-width: thin;
	border-top : none;
	width: 100%;
	height: 40px;
	
	background-color: white;
}
#rPasswordText {
	width: 98%;
	margin-left: 2%;
	height: 38px;
	border: none;
}
/* 이름, 성별, 생년월일 , 이메일  */
#information {
	border: solid;
	border-width: thin;
	display: flex;
	width: 100%;
	margin-top: 2%;
	background-color: white;
}

#Pinformation {
	width: 70%;
}

#profile {
	width: 30%;
}

#profileImage {
	width: 97%;
	height: 133px;
}

#uploadImage {
	width: 98%;
	height: 25px;
	border: solid;
	border-width: thin;
	border-right: none;
	border-bottom: none;
}

#name {
	border: solid;
	border-width: thin;
	border-top: none;
	border-left: none;
	border-right: none;
	width: 100%;
	height: 40px;
}

#nameText {
	width: 98%;
	margin-left: 2%;
	height: 38px;
	border: none;
}

#sex {
	border: solid;
	border-width: thin;
	border-top: none;
	border-left: none;
	width: 100%;
	height: 40px;
}

#sexText {
	width: 48%;
	margin-left: 2%;
	height: 38px;
	border: none;
	background-color: white;
}

#sexRadio {
	width: 10%;
	height: 18px;
}

#birth {
	border: solid;
	border-width: thin;
	border-top: none;
	border-left: none;
	border-right: none;
	width: 100%;
	height: 40px;
}

#birthDate {
	height: 38px;
	width: 98%;
	margin-left: 2%;
	border: none;
}

#email {
	width: 100%;
	height: 40px;
	border-width: thin;
}

#emailEmail {
	width: 98%;
	margin-left: 2%;
	height: 38px;
	border: none;
}
/* 우편번호, 상세주소, 휴대폰 번호  */
#post {
	margin-top: 10px;
	border: solid;
	border-width: thin;
	width: 100%;
	height: 40px;
	background-color: white;
}

#postText {
	border: none;
	margin-left: 2%;
	width: 27%;
	height: 38px;
	background-color: white;
}
#AddressText{
	width: 40%;
	height: 38px;
	border: none;
	background-color: white;
	
}

#postCheck {
	width: 28%;
	height: 38px;
}

#detailAddress {
	border: solid;
	border-width: thin;
	border-top: none;
	width: 100%;
	height: 40px;
	background-color: white;
}

#detailAddressText {
	margin-left: 2%;
	width: 98%;
	height: 38px;
	border: none;
}

#phone {
	border: solid;
	border-width: thin;
	border-top: none;
	width: 100%;
	height: 40px;
	background-color: white;
}

#phoneText {
	margin-left: 2%;
	width: 98%;
	height: 38px;
	border: none;
}

#ability {
	border: solid;
	border-width: thin;
	border-top: none;
	width: 100%;
	background-color: white;
}

.container {
	width: 32%;
	float: left;
	margin-top: 5px;
	margin-left: 5px;
	height: 100px;
}

#signup {
	border: solid;
	border-width: thin;
	border: none;
	width: 100%;
}

#signupButton {
	width: 100%;
	height: 40px;
	background-color: #212121;
	color: white;
}
//////////////////////////////////////////////////w=

body 
{
overflow-x : hidden;
background-color: #EAEAEA;

}

#menu
{
left:23%;
position:fixed;
top:8%;

}


#menu ul {

    list-style-type: none;
  	width:100%;
  	height:30px;
    margin: 0;
    padding: 0;
}

 #menu a:link,#menu a:visited {
    /* display: block;
    font-weight: bold;
    color: #FFFFFF;
    background-color: #98bf21;
    width: 100px;
    text-align: center;
    padding: 4px; */
    text-decoration: none;
    color: #000000;
    
    /* text-transform: uppercase; */
} 
#menu li{
	height:100%;
	display: block;
    font-weight: bold;
    font-size : 20px;
    color: #000000;
    /* background-color: #98bf21; */
    text-align: center;
    padding: 4px;
    text-decoration: none;
    text-transform: uppercase;
}

#menu a:hover,#menu a:active {
    /* background-color: #7A991A; */
    color:#FF0000;
}
#loginmenu
{

font-size :15px;
width:99%;
height:100px;
color:black;
text-align:left;
border:0px; 
background-color:#DBFFD5; 
position: relative;
top:7px;
left:0px;
right:0px;
padding:4px;
margin: 0px;
}
#loginmenu input
{
margin: 5px;
}
#leftmenu
{
background-color:#A6A6A6;
position:fixed;
width:15%;
height: 100%;
top:8%;
border: 1px solid #000000;
}
#leftmenu ul
{
 	list-style-type: none;
  	/* width:100%; */
  	 height:50px; 
    margin: 0;
    padding: 0;
}
#leftmenu li
{
	height:100%;
	display: block;
    font-weight: bold;
    font-size : 20px;
    color: #000000;
    /* background-color: #98bf21; */
    text-align: center;
    padding: 20px;
    text-decoration: none;
}
#leftmenu hr
{
margin-left: 45%;
margin-right: 45%;
margin-bottom: 10%;
}


.image .floating1 {
 position: absolute;
  left:85%;
  z-index: 1;
  top:180px;
 
}
.image .floating2 {
 position: absolute;
  left:70%;
  z-index: 2;
  top:280px;
  
}
.image .floating3 {
 position: absolute;
  left:78%;
  z-index: 3;
   top:550px;
}

/* .weekday
{
position: absolute;
} */



#headrdiv
{

position:fixed; 
width:100%;
height:7%;
/* display:flex; */

/* border: 1px solid #000000; */
padding: 0px;
margin: 0px;
bottom: 93%;
}
.moebiusdiv,.logindiv
{
left:23%;
height:100%;
/* border: 1px solid #000000; */
width:12.5%;
background-color: #000000;
color:#F6F6F6;
padding: 0px;
margin: 0px;
float:left;
}
.moebiusdiv
{
font-size:45px;
font-weight: bold;
text-align: left;
position:relative;
width:30%;
 border: 1px solid #FFFFFF; 
background-color: #212121;
padding-left:5px; 
}
.logindiv
{
font-size:45px;
font-weight: bold;
text-align: left;
position:relative;
width:15%;
 border: 1px solid #FFFFFF; 
background-color: #212121;
padding-left:5px;
}
.headerhr
{

position:relative;
top:2px;
right:5px;
clear:both;

}
.headerhr hr
{

        border-style: groove;
        border-width: 3px;
        border-color: #000000;
        
}

</style>

<script>
	var idCheck = null;
	function idCheckResult(text, status) {
		idCheck = text;
		$("#idCheckText").val(text);
	}

	$(document).ready(function() {
		$("#idCheck").click(function() {
			$.post("idCheck.do", {
				idname : $("#idText").val()
			}, idCheckResult);
		});

		$("#profileImage").click(function() {
		});
		
		$("#signupButton").click(function() {

			var memberperson={'@id':1,
						idname:			$("#idText").val(),
						password:		$("#passwordText").val(),
						name:			$("#nameText").val(),
						sex:			$("#sexRadio").val(),
						birthDate:		$("#birthDate").val(),
						email:			$("#emailEmail").val(),
						phoneNo:		$("#phoneText").val(),
						abilitys:		[],
						detailAddress:	$("#detailAddressText").val()
						};
			
			$("input[name=ability]:checked").each(function() {
				
				var selectedAbility = $(this).val();
				var adNo = selectedAbility.split('-')[0];
				var parcedAdNo=parseInt(adNo);
				var abilityName = selectedAbility.split('-')[1]; 
 				
				var ability={
 						adNo: parcedAdNo,
						abilityName: abilityName
				};
				
				memberperson.abilitys.push(ability);
								
			});
						
			var replacer= function(key, value) {	
		  		  if (key=="memberperson" && typeof value == "object") {
					  
		  		    return value['@id'];
		  		    
		  		  }
		  		  return value;
		  		};
		  		
		    	$.ajax({ 
		    	    url: "RegistMember.do",    
		    	    type: 'POST', 
		    	    dataType: 'text', //response가 text일때 json이면 json표기
		    	    data: JSON.stringify(memberperson, replacer), 
		    	    contentType: 'application/json ; charset=UTF-8',
		    	    mimeType: 'application/json',
		    	    success: function(data) { 
		    	    	location.href="/celab/lee/notifyregistedmember.jsp";
		    	    },
		    	    error:function( ) { 
		    	        alert("error");
		    	    }
		    	});
			
			

		});

	});
	
	function startSearPost() {
		var myWindow = window.open("searchPost.jsp", "",
				"width=500, height=700");
	}
	function 아이디받다(id) {
		var txtId = document.getElementById("txtId");
		txtId.value = id;
	}
	function 주소받다(post, postAddress) {
		var txtPost = document.getElementById("txtPost");
		var txtPostAddress = document.getElementById("txtPostAddress");
		txtPost.value = post;
		txtPostAddress.value = postAddress;
	}
</script>


<title>Insert title here</title>
</head>
<body>

  

<header>

<div id="headrdiv">

<div class="moebiusdiv">
Moebius
</div>

<div class="logindiv">

</div>
<div class="headerhr">
<hr>
</div>
</div>

</header>

<%-- <c:choose>
  <c:when test="${message ne null}">
  	alert(메세지);
  </c:when>
</c:choose> --%>



<div id="menu" >

<!-- <ul>
  <li><a href="">Home</a></li>
  <li><a href="">TEST</a></li>
  <li><a href="">TEST</a></li>
  <li><a href="">TEST</a></li>
  <li><a href="">TEST</a></li>
  <li id="login"><a>Login</a> 
 <div id="loginmenu">
  <br>
  <form >
 
  아&nbsp;이&nbsp;디:&nbsp;&nbsp;<input type="text" ><br>
  비밀번호: <input type="text" >
  <br>
  </form>
     </div>
 </li>
 </ul> -->
  <ul><li>
  <a id="test123">Menu</a>
  </li></ul>

</div>
<div id="leftmenu">

<ul>
<li>
<a>HOME</a>
<hr>
</li>
<li>
<a>BOARD</a>
<hr>
</li>
<li>
<a>REVIEW</a>
<hr>
</li>
<li>
<a>LOGO</a>
<hr>
</li>
<li>
<a>TEST</a>
<hr>
</li>
<li>
<a>ABCD</a>
<hr>
</li>


</ul>






</div>

  <div class="image" >
<img src="http://192.168.0.23:8080/celab/images/c1.png" class="floating1" width="50" height="50">
</div>
<div class="image" >
<img src="http://192.168.0.23:8080/celab/images/b1.PNG" class="floating2" width="230" height="230">
</div> 
<div class="image" >
<img src="http://192.168.0.23:8080/celab/images/b1.PNG" class="floating3" width="500" height="500">
</div>    

<br>
	
		<div id="main">
			<div id="logo" style="font-size: 2.0em">
				<p align="center">회원가입</p>
			</div>

			<div id="id">
				<input type='text' id="idText" name='idname' placeholder="아이디" /> 
				<input type='text' id="idCheckText" disabled /> 
				<input type='button' id="idCheck" value='중복확인' />
			</div>


			<div id="password">
				<input type='password' id="passwordText" name='password'
					placeholder="비밀번호" /> 
			</div>
			
			<div id="rpassword">
				 <input type='password' id="rPasswordText" placeholder="비밀번호 확인" />
			</div>


			<div id="information">
				<div id="Pinformation">
					<div id="name">
						<input type='text' id="nameText" name='name' placeholder="이름" />
					</div>

					<div id="sex">
						<input type='text' id="sexText" value="성별" disabled /> <input
							type='radio' name='sex' id="sexRadio" value="male" />남자 <input
							type='radio' name='sex' id="sexRadio" value="female" />여자
					</div>

					<div id="birth">
						<input type="date" id="birthDate" min="1900-01-01"
							max="2015-08-31" name='birthDate' step="1"/>
					</div>

					<div id="email">
						<input type="email" id="emailEmail" placeholder="이메일" name='email'>
					</div>

				</div>

				<div id="profile">
					<input type='img' id="profileImage" placeholder="사진을 올려주세요." /> 
					<input type="file" id="uploadImage" name="img" />

				</div>

			</div>

			<div id="post">
				<input type='text' id="postText" placeholder="우편번호" /> 
				<input type='text' id="AddressText" placeholder="주소" />
				<input
					type='button' id="postCheck" value="찾기" onclick='startSearPost()' />
			</div>

			<div id="detailAddress">
				<input type='text' id="detailAddressText" placeholder="상세주소" />
			</div>

			<div id="phone">
				<input type='text' id="phoneText" name='phoneNo' placeholder="폰번호" />
			</div>
			
			<div id="ability">
				<div class="container">
					<c:forEach var="ability" items="${abilitys1}">
						<input type="checkbox" value="${ability.adNo}-${ability.abilityName}" name='ability'/> ${ability.abilityName}
						 <br/>
					</c:forEach>
				</div>

				<div class="container">
					<c:forEach var="ability" items="${abilitys2}">
						<input type="checkbox" value="${ability.adNo}-${ability.abilityName}" name='ability'/> ${ability.abilityName} <br/>
					</c:forEach>
				</div>

				<div class="container">
					<c:forEach var="ability" items="${abilitys3}">
						<input type="checkbox" value="${ability.adNo}-${ability.abilityName}" name='ability'/> ${ability.abilityName} <br/>
					</c:forEach>
				</div>

				<div id="signup">
					<input type='submit' id="signupButton" value="가입하기" />
				</div>
			</div>
		</div>
			
	


</body>
<script type="text/javascript">
$(document).ready(function(){
	$("#leftmenu").hide();
	$("#test123").click(function(){
		 
		 $("#leftmenu").animate({width:'toggle'},300);
		 

		 
	 });
	
	
});

</script>



</html>
